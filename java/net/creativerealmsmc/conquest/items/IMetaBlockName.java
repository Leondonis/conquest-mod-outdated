package net.creativerealmsmc.conquest.items;

import net.minecraft.item.ItemStack;

public interface IMetaBlockName 
{
    public String getSpecialName(ItemStack var1);
}

