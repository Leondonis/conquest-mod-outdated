package net.creativerealmsmc.conquest.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class DoubleSlabBlock extends SlabBlock
{
	public DoubleSlabBlock(Material materialIn, float hardness, float resistance, SoundType sound, float lightLevel, String tool, int toolLevel, boolean canSustainPlant, Block halfSlab)
	{
		super(materialIn, hardness, resistance, sound, lightLevel, tool, toolLevel, canSustainPlant);
		this.halfSlab = halfSlab;
	}

	@Override
	public boolean isDouble() {
		return true;
	}
}