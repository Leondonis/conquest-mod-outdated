package net.creativerealmsmc.conquest.blocks.MetaBlocks;

import java.util.List;
import java.util.Locale;

import net.creativerealmsmc.conquest.items.IMetaBlockName;
import net.minecraft.block.BlockBeacon;
import net.minecraft.block.BlockPane;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created by Artemisia on 11/24/2016.
 */

public class BlockPaneMeta extends BlockPane implements IMetaBlockName
{
    public static final PropertyEnum<EnumType> VARIANT = PropertyEnum.<EnumType>create("variant", EnumType.class);
    private boolean canSustainPlant;

    public BlockPaneMeta(String nameIn, String unlocalizedNameIn, Material materialIn, float hardness, float resistance, SoundType sound, float lightLevel, String tool, int toolLevel, boolean canSustainPlant, CreativeTabs tab)
    {
        super(materialIn, false);
        this.setDefaultState(this.blockState.getBaseState().withProperty(NORTH, Boolean.valueOf(false)).withProperty(EAST, Boolean.valueOf(false)).withProperty(SOUTH, Boolean.valueOf(false)).withProperty(WEST, Boolean.valueOf(false)).withProperty(VARIANT, EnumType.ALPHA));
        this.setRegistryName(nameIn);
        this.setUnlocalizedName(unlocalizedNameIn);
        this.setCreativeTab(tab);
        this.setHardness(hardness);
        this.setResistance(resistance);
        this.setSoundType(sound);
        this.setLightLevel(lightLevel);
        if(tool != null)
        {
            this.setHarvestLevel(tool, toolLevel);
        }
        this.canSustainPlant = canSustainPlant;
    }
    
    @Override
    public String getSpecialName(ItemStack stack) 
    {
        return EnumType.values()[stack.getItemDamage()].name().toLowerCase(Locale.ENGLISH);
    }
    
    public boolean canSustainPlant(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing direction, net.minecraftforge.common.IPlantable plantable)
    {
        return canSustainPlant;
    }
    
    public int damageDropped(IBlockState state)
    {
        return ((EnumType)state.getValue(VARIANT)).getMetadata();
    }
    
    @SideOnly(Side.CLIENT)
    public void getSubBlocks(Item itemIn, CreativeTabs tab, List<ItemStack> list)
    {
        for (int i = 0; i < EnumType.values().length; ++i)
        {
            list.add(new ItemStack(itemIn, 1, i));
        }
    }
    
    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.TRANSLUCENT;
    }
    
    public IBlockState getStateFromMeta(int meta)
    {
        return this.getDefaultState().withProperty(VARIANT, EnumType.byMetadata(meta));
    }
    
    public int getMetaFromState(IBlockState state)
    {
        return ((EnumType)state.getValue(VARIANT)).getMetadata();
    }
    
    public IBlockState withRotation(IBlockState state, Rotation rot)
    {
        switch (rot)
        {
            case CLOCKWISE_180:
                return state.withProperty(NORTH, state.getValue(SOUTH)).withProperty(EAST, state.getValue(WEST)).withProperty(SOUTH, state.getValue(NORTH)).withProperty(WEST, state.getValue(EAST));
            case COUNTERCLOCKWISE_90:
                return state.withProperty(NORTH, state.getValue(EAST)).withProperty(EAST, state.getValue(SOUTH)).withProperty(SOUTH, state.getValue(WEST)).withProperty(WEST, state.getValue(NORTH));
            case CLOCKWISE_90:
                return state.withProperty(NORTH, state.getValue(WEST)).withProperty(EAST, state.getValue(NORTH)).withProperty(SOUTH, state.getValue(EAST)).withProperty(WEST, state.getValue(SOUTH));
            default:
                return state;
        }
    }
    
    public IBlockState withMirror(IBlockState state, Mirror mirrorIn)
    {
        switch (mirrorIn)
        {
            case LEFT_RIGHT:
                return state.withProperty(NORTH, state.getValue(SOUTH)).withProperty(SOUTH, state.getValue(NORTH));
            case FRONT_BACK:
                return state.withProperty(EAST, state.getValue(WEST)).withProperty(WEST, state.getValue(EAST));
            default:
                return super.withMirror(state, mirrorIn);
        }
    }

    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, new IProperty[] {NORTH, EAST, WEST, SOUTH, VARIANT});
    }
    
    public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state)
    {
        if (!worldIn.isRemote)
        {
            BlockBeacon.updateColorAsync(worldIn, pos);
        }
    }
    
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
    {
        if (!worldIn.isRemote)
        {
            BlockBeacon.updateColorAsync(worldIn, pos);
        }
    }
    
    public static enum EnumType implements IStringSerializable
    {
        ALPHA(0, "alpha"),
        BRAVO(1, "bravo"),
        CHARLIE(2, "charlie"),
        DELTA(3, "delta"),
        ECHO(4, "echo"),
        FOX(5, "fox"),
        GOLF(6, "golf"),
        HOTEL(7, "hotel"),
        INDIA(8, "india"),
    	JULIET(9, "juliet"),
    	KILO(10, "kilo"),
    	LIMA(11, "lima"),
    	MIKE(12, "mike"),
    	NOVEMBER(13, "november"),
    	OSCAR(14, "oscar"),
    	PAPA(15, "papa");

        private static final BlockPaneMeta.EnumType[] META_LOOKUP = new BlockPaneMeta.EnumType[values().length];
        private final int meta;
        private final String name;

        private EnumType(int metaIn, String nameIn)
        {
            this.meta = metaIn;
            this.name = nameIn;
        }
        
        public int getMetadata()
        {
            return this.meta;
        }
        
        public String toString()
        {
            return this.name;
        }
        
        public static BlockPaneMeta.EnumType byMetadata(int meta)
        {
            if (meta < 0 || meta >= META_LOOKUP.length)
            {
                meta = 0;
            }

            return META_LOOKUP[meta];
        }

        public String getName()
        {
            return this.name;
        }

        static
        {
            for (BlockPaneMeta.EnumType blockpane$enumtype : values())
            {
                META_LOOKUP[blockpane$enumtype.getMetadata()] = blockpane$enumtype;
            }
        }
    }
}