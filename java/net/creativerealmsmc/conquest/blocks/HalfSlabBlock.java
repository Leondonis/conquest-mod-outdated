package net.creativerealmsmc.conquest.blocks;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class HalfSlabBlock extends SlabBlock
{
	public HalfSlabBlock(Material materialIn, float hardness, float resistance, SoundType sound, float lightLevel, String tool, int toolLevel, boolean canSustainPlant)
	{
		super(materialIn, hardness, resistance, sound, lightLevel, tool, toolLevel, canSustainPlant);
		this.halfSlab = this;
	}

	@Override
	public boolean isDouble() {
		return false;
	}
}