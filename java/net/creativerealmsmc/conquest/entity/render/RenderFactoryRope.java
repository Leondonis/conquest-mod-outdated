package net.creativerealmsmc.conquest.entity.render;

import net.creativerealmsmc.conquest.entity.EntityRopeKnot;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraftforge.fml.client.registry.IRenderFactory;

public class RenderFactoryRope implements IRenderFactory<EntityRopeKnot> {
	@Override
	public Render<? super EntityRopeKnot> createRenderFor(RenderManager manager) {
		return new RenderRopeKnot(manager);
	}
}
