package net.creativerealmsmc.conquest.entity;

import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.base.Predicate;
import net.minecraft.block.BlockFence;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityHanging;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.server.SPacketEntityAttach;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityRopeKnot extends Entity {

	public EntityRopeKnot(World worldIn) {
		super(worldIn);
		this.setSize(0.5F, 0.5F);

	}

	public EntityRopeKnot(World worldIn, BlockPos hangingPositionIn) {
		this(worldIn);
		this.hangingPosition = hangingPositionIn;

		this.setPosition((double) hangingPositionIn.getX() + 0.5D, (double) hangingPositionIn.getY() + 0.5D, (double) hangingPositionIn.getZ() + 0.5D);
		/* float f = 0.125F; float f1 = 0.1875F; float f2 = 0.25F; */
		this.setEntityBoundingBox(new AxisAlignedBB(this.posX - 0.1875D, this.posY - 0.25D + 0.125D, this.posZ - 0.1875D, this.posX + 0.1875D, this.posY + 0.25D + 0.125D, this.posZ + 0.1875D));
	}

	private boolean isLeashed;
	private Entity leashedToEntity;
	private NBTTagCompound leashNBTTag;

	//TODO: implemented Entity methods
	/** Get whether this Entity's AI is disabled */
	/*public boolean isAIDisabled() {
		return true;
	}*/

	@Override
	protected void entityInit() {}

	//TODO: Entity Leash

	/** Sets the x,y,z of the entity from the given parameters. Also seems to set up a bounding box. */
	public void setPosition(double x, double y, double z) {
		x = (double) MathHelper.floor_double(x) + 0.5D;
		y = (double) MathHelper.floor_double(y) + 0.5D;
		z = (double) MathHelper.floor_double(z) + 0.5D;
		//super:
		this.hangingPosition = new BlockPos(x, y, z);
		this.updateBoundingBox();
		this.isAirBorne = true;
	}

	/** Updates the entity bounding box based on current facing */
	protected void updateBoundingBox() {
		this.posX = (double) this.hangingPosition.getX() + 0.5D;
		this.posY = (double) this.hangingPosition.getY() + 0.5D;
		this.posZ = (double) this.hangingPosition.getZ() + 0.5D;
	}

	/** Updates facing and bounding box based on it */
	public void updateFacingWithBoundingBox(EnumFacing facingDirectionIn) {}

	public int getWidthPixels() {
		return 9;
	}

	public int getHeightPixels() {
		return 9;
	}

	public float getEyeHeight() {
		return -0.0625F;
	}

	/** Checks if the entity is in range to render. */
	@SideOnly(Side.CLIENT)
	public boolean isInRangeToRenderDist(double distance) {
		return distance < 1024.0D;
	}

	/** Called when this entity is broken. Entity parameter may be null. */
	public void onBroken(@Nullable Entity brokenEntity) {
		this.playSound(SoundEvents.ENTITY_LEASHKNOT_BREAK, 1.0F, 1.0F);
	}

	/** Either write this entity to the NBT tag given and return true, or return false without doing anything. If this returns false the entity is not saved on disk. Ridden entities return false here as they are saved with their rider. */
	public boolean writeToNBTOptional(NBTTagCompound compound) {
		return false;
	}

	public boolean processInitialInteractLeash(EntityPlayer player, @Nullable ItemStack stack, EnumHand hand) //custom, changed name
	{
		if (this.worldObj.isRemote) {
			return true;
		} else {
			boolean flag = false;

			if (stack != null && stack.getItem() == Items.LEAD) {
				for (EntityLiving entityliving : this.worldObj.getEntitiesWithinAABB(EntityLiving.class, new AxisAlignedBB(this.posX - 7.0D, this.posY - 7.0D, this.posZ - 7.0D, this.posX + 7.0D, this.posY + 7.0D, this.posZ + 7.0D))) {
					if (entityliving.getLeashed() && entityliving.getLeashedToEntity() == player) {
						entityliving.setLeashedToEntity(this, true);
						flag = true;
					}
				}
			}

			if (!flag) {
				this.setDead();

				if (player.capabilities.isCreativeMode) {
					for (EntityLiving entityliving1 : this.worldObj.getEntitiesWithinAABB(EntityLiving.class, new AxisAlignedBB(this.posX - 7.0D, this.posY - 7.0D, this.posZ - 7.0D, this.posX + 7.0D, this.posY + 7.0D, this.posZ + 7.0D))) {
						if (entityliving1.getLeashed() && entityliving1.getLeashedToEntity() == this) {
							entityliving1.clearLeashed(true, false);
						}
					}
				}
			}

			return true;
		}
	}

	/** checks to make sure painting can be placed there */
	public boolean onValidSurface() {
		return this.worldObj.getBlockState(this.hangingPosition).getBlock() instanceof BlockFence;
	}

	public static EntityRopeKnot createKnot(World worldIn, BlockPos fence) {
		EntityRopeKnot entityleashknot = new EntityRopeKnot(worldIn, fence);
		entityleashknot.forceSpawn = true;
		worldIn.spawnEntityInWorld(entityleashknot);
		entityleashknot.playPlaceSound();
		return entityleashknot;
	}

	public static EntityRopeKnot getKnotForPosition(World worldIn, BlockPos pos) {
		int i = pos.getX();
		int j = pos.getY();
		int k = pos.getZ();

		for (EntityRopeKnot entityleashknot : worldIn.getEntitiesWithinAABB(EntityRopeKnot.class, new AxisAlignedBB((double) i - 1.0D, (double) j - 1.0D, (double) k - 1.0D, (double) i + 1.0D, (double) j + 1.0D, (double) k + 1.0D))) {
			if (entityleashknot.getHangingPosition().equals(pos)) {
				return entityleashknot;
			}
		}

		return null;
	}

	public void playPlaceSound() {
		this.playSound(SoundEvents.ENTITY_LEASHKNOT_PLACE, 1.0F, 1.0F);
	}

	//TODO: Entity Hanging

	private static final Predicate<Entity> IS_HANGING_ENTITY = new Predicate<Entity>() {
		public boolean apply(@Nullable Entity p_apply_1_) {
			return p_apply_1_ instanceof EntityHanging;
		}
	};
	private int tickCounter1;
	protected BlockPos hangingPosition;
	/** The direction the entity is facing */
	@Nullable
	public EnumFacing facingDirection;

	private double func_190202_a(int p_190202_1_) {
		return p_190202_1_ % 32 == 0 ? 0.5D : 0.0D;
	}

	/** Returns true if other Entities should be prevented from moving through this Entity. */
	public boolean canBeCollidedWith() {
		return true;
	}

	/** Called when a player attacks an entity. If this returns true the attack will not happen. */
	public boolean hitByEntity(Entity entityIn) {
		return entityIn instanceof EntityPlayer ? this.attackEntityFrom(DamageSource.causePlayerDamage((EntityPlayer) entityIn), 0.0F) : false;
	}

	/** Gets the horizontal facing direction of this Entity. */
	public EnumFacing getHorizontalFacing() {
		return this.facingDirection;
	}

	/** Called when the entity is attacked. */
	public boolean attackEntityFrom(DamageSource source, float amount) {
		if (this.isEntityInvulnerable(source)) {
			return false;
		} else {
			if (!this.isDead && !this.worldObj.isRemote) {
				this.setDead();
				this.setBeenAttacked();
				this.onBroken(source.getEntity());
			}

			return true;
		}
	}

	/** Tries to move the entity towards the specified location. */
	public void moveEntity(double x, double y, double z) {
		if (!this.worldObj.isRemote && !this.isDead && x * x + y * y + z * z > 0.0D) {
			this.setDead();
			this.onBroken((Entity) null);
		}
	}

	/** Adds to the current velocity of the entity. */
	public void addVelocity(double x, double y, double z) {
		if (!this.worldObj.isRemote && !this.isDead && x * x + y * y + z * z > 0.0D) {
			this.setDead();
			this.onBroken((Entity) null);
		}
	}

	/** (abstract) Protected helper method to write subclass entity data to NBT. */
	/* public void writeEntityToNBT(NBTTagCompound compound) { compound.setByte("Facing", (byte)this.facingDirection.getHorizontalIndex()); BlockPos blockpos = this.getHangingPosition(); compound.setInteger("TileX", blockpos.getX()); compound.setInteger("TileY", blockpos.getY()); compound.setInteger("TileZ", blockpos.getZ()); } */

	/** (abstract) Protected helper method to read subclass entity data from NBT. */
	/* public void readEntityFromNBT(NBTTagCompound compound) { this.hangingPosition = new BlockPos(compound.getInteger("TileX"), compound.getInteger("TileY"), compound.getInteger("TileZ")); this.updateFacingWithBoundingBox(EnumFacing.getHorizontal(compound.getByte("Facing"))); } */

	/** Called when this entity is broken. Entity parameter may be null. */

	/** Drops an item at the position of the entity. */
	public EntityItem entityDropItem(ItemStack stack, float offsetY) {
		EntityItem entityitem = new EntityItem(this.worldObj, this.posX + (double) ((float) this.facingDirection.getFrontOffsetX() * 0.15F), this.posY + (double) offsetY, this.posZ + (double) ((float) this.facingDirection.getFrontOffsetZ() * 0.15F), stack);
		entityitem.setDefaultPickupDelay();
		this.worldObj.spawnEntityInWorld(entityitem);
		return entityitem;
	}

	protected boolean shouldSetPosAfterLoading() {
		return false;
	}

	public BlockPos getHangingPosition() {
		return this.hangingPosition;
	}

	/** Transforms the entity's current yaw with the given Rotation and returns it. This does not have a side-effect. */
	@SuppressWarnings("incomplete-switch")
	public float getRotatedYaw(Rotation transformRotation) {
		if (this.facingDirection != null && this.facingDirection.getAxis() != EnumFacing.Axis.Y) {
			switch (transformRotation) {
			case CLOCKWISE_180:
				this.facingDirection = this.facingDirection.getOpposite();
				break;
			case COUNTERCLOCKWISE_90:
				this.facingDirection = this.facingDirection.rotateYCCW();
				break;
			case CLOCKWISE_90:
				this.facingDirection = this.facingDirection.rotateY();
			}
		}

		float f = MathHelper.wrapDegrees(this.rotationYaw);

		switch (transformRotation) {
		case CLOCKWISE_180:
			return f + 180.0F;
		case COUNTERCLOCKWISE_90:
			return f + 90.0F;
		case CLOCKWISE_90:
			return f + 270.0F;
		default:
			return f;
		}
	}

	/** Transforms the entity's current yaw with the given Mirror and returns it. This does not have a side-effect. */
	public float getMirroredYaw(Mirror transformMirror) {
		return this.getRotatedYaw(transformMirror.toRotation(this.facingDirection));
	}

	/** Called when a lightning bolt hits the entity. */
	public void onStruckByLightning(EntityLightningBolt lightningBolt) {}

	//TODO: LEASHES

	/** Called to update the entity's position/logic. */
	public void onUpdate() {
		this.prevPosX = this.posX;
		this.prevPosY = this.posY;
		this.prevPosZ = this.posZ;

		if (this.tickCounter1++ == 100 && !this.worldObj.isRemote) {
			this.tickCounter1 = 0;

			if (!this.isDead && !this.onValidSurface()) {
				this.setDead();
				this.onBroken((Entity) null);
			}
		}
		//endsuper hanging entity
		if (!this.worldObj.isRemote) {
			this.updateLeashedState();

		}
	}

	/** (abstract) Protected helper method to write subclass entity data to NBT. */
	public void writeEntityToNBT(NBTTagCompound compound) {
		compound.setBoolean("Leashed", this.isLeashed);

		if (this.leashedToEntity != null) {
			NBTTagCompound nbttagcompound2 = new NBTTagCompound();

			if (this.leashedToEntity instanceof EntityLivingBase) {
				UUID uuid = this.leashedToEntity.getUniqueID();
				nbttagcompound2.setUniqueId("UUID", uuid);
			} else if (this.leashedToEntity instanceof EntityHanging) {
				BlockPos blockpos = ((EntityHanging) this.leashedToEntity).getHangingPosition();
				nbttagcompound2.setInteger("X", blockpos.getX());
				nbttagcompound2.setInteger("Y", blockpos.getY());
				nbttagcompound2.setInteger("Z", blockpos.getZ());
			}

			compound.setTag("Leash", nbttagcompound2);
		}

	}

	/** (abstract) Protected helper method to read subclass entity data from NBT. */
	@Override
	public void readEntityFromNBT(NBTTagCompound compound) {

		this.isLeashed = compound.getBoolean("Leashed");

		if (this.isLeashed && compound.hasKey("Leash", 10)) {
			this.leashNBTTag = compound.getCompoundTag("Leash");
		}

	}

	public final boolean processInitialInteract(EntityPlayer player, @Nullable ItemStack stack, EnumHand hand) {
		if (this.getLeashed() && this.getLeashedToEntity() == player) {
			this.clearLeashed(true, !player.capabilities.isCreativeMode);
			return true;
		} else if (stack != null && stack.getItem() == Items.LEAD && this.canBeLeashedTo(player)) {
			this.setLeashedToEntity(player, true);
			--stack.stackSize;
			return true;
		} else {
			return this.processInteract(player, hand, stack) ? true : super.processInitialInteract(player, stack, hand);
		}
	}

	protected boolean processInteract(EntityPlayer player, EnumHand hand, @Nullable ItemStack stack) {
		return processInitialInteractLeash(player, stack, hand);
	}

	/** Applies logic related to leashes, for example dragging the entity or breaking the leash. */
	protected void updateLeashedState() {
		if (this.leashNBTTag != null) {
			this.recreateLeash();
		}

		if (this.isLeashed) {
			if (!this.isEntityAlive()) {
				this.clearLeashed(true, true);
			}

			if (this.leashedToEntity == null || this.leashedToEntity.isDead) {
				this.clearLeashed(true, true);
			}
		}
	}

	/** Removes the leash from this entity */
	public void clearLeashed(boolean sendPacket, boolean dropLead) {
		if (this.isLeashed) {
			this.isLeashed = false;
			this.leashedToEntity = null;

			if (!this.worldObj.isRemote && dropLead) {
				this.dropItem(Items.LEAD, 1);
			}

			if (!this.worldObj.isRemote && sendPacket && this.worldObj instanceof WorldServer) {
				((WorldServer) this.worldObj).getEntityTracker().sendToAllTrackingEntity(this, new SPacketEntityAttach(this, (Entity) null));
			}
		}
	}

	public boolean canBeLeashedTo(EntityPlayer player) {
		return !this.getLeashed() && !(this instanceof IMob);
	}

	public boolean getLeashed() {
		return this.isLeashed;
	}

	public Entity getLeashedToEntity() {
		return this.leashedToEntity;
	}

	/** Sets the entity to be leashed to. */
	public void setLeashedToEntity(Entity entityIn, boolean sendAttachNotification) {
		this.isLeashed = true;
		this.leashedToEntity = entityIn;

		if (!this.worldObj.isRemote && sendAttachNotification && this.worldObj instanceof WorldServer) {
			((WorldServer) this.worldObj).getEntityTracker().sendToAllTrackingEntity(this, new SPacketEntityAttach(this, this.leashedToEntity));
		}

		if (this.isRiding()) {
			this.dismountRidingEntity();
		}
	}

	private void recreateLeash() {
		if (this.isLeashed && this.leashNBTTag != null) {
			if (this.leashNBTTag.hasUniqueId("UUID")) {
				UUID uuid = this.leashNBTTag.getUniqueId("UUID");

				for (EntityLivingBase entitylivingbase : this.worldObj.getEntitiesWithinAABB(EntityLivingBase.class, this.getEntityBoundingBox().expandXyz(10.0D))) {
					if (entitylivingbase.getUniqueID().equals(uuid)) {
						this.leashedToEntity = entitylivingbase;
						break;
					}
				}
			} else if (this.leashNBTTag.hasKey("X", 99) && this.leashNBTTag.hasKey("Y", 99) && this.leashNBTTag.hasKey("Z", 99)) {
				BlockPos blockpos = new BlockPos(this.leashNBTTag.getInteger("X"), this.leashNBTTag.getInteger("Y"), this.leashNBTTag.getInteger("Z"));
				EntityRopeKnot entityleashknot = EntityRopeKnot.getKnotForPosition(this.worldObj, blockpos);

				if (entityleashknot == null) {
					entityleashknot = EntityRopeKnot.createKnot(this.worldObj, blockpos);
				}

				this.leashedToEntity = entityleashknot;
			} else {
				this.clearLeashed(false, true);
			}
		}

		this.leashNBTTag = null;
	}

}
