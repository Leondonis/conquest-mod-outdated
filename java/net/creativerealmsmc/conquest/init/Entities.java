package net.creativerealmsmc.conquest.init;

import net.creativerealmsmc.conquest.Main;
import net.creativerealmsmc.conquest.entity.EntityRopeKnot;
import net.creativerealmsmc.conquest.entity.render.RenderFactoryRope;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.registry.EntityRegistry;

public class Entities {
	//	https://github.com/micdoodle8/Galacticraft/tree/master/src/main/java/micdoodle8/mods/galacticraft/core/entities
	//https://github.com/micdoodle8/Galacticraft/blob/edab080014e1ad2603d0555a6449567c41f7560a/src/main/java/micdoodle8/mods/galacticraft/core/util/GCCoreUtil.java
	public static void init() {
		registerEntity(EntityRopeKnot.class, "Rope", 0);

	}

	public static void registerRenders() {
		RenderingRegistry.registerEntityRenderingHandler(EntityRopeKnot.class, new RenderFactoryRope());
	}

	public static void registerEntity(Class entityClass, String name, int id) {
		EntityRegistry.registerModEntity(entityClass, name, id, Main.instance, 64, 3, false);
	}
}
