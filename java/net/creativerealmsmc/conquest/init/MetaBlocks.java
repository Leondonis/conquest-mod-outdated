package net.creativerealmsmc.conquest.init;

import net.creativerealmsmc.conquest.blocks.*;
import net.creativerealmsmc.conquest.blocks.MetaBlocks.*;
import net.creativerealmsmc.conquest.items.*;
import net.minecraft.block.Block;
import net.minecraft.block.BlockDaylightDetector;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class MetaBlocks 
{
	/**
	 * TODO: Declare blocks here:
	 * For blocks with metadata variants: public static Block material_blockmodel_number
	 * For blocks without metadata variants: blockname_material/color/type_blockmodel
	 *
	 * keep this organized! use comments to break up sections!
	 */
	//Full Stone Blocks
	public static Block stone_full_1;
	public static Block stone_full_2;
	public static Block stone_full_3;
	public static Block stone_full_4;
	public static Block stone_full_5;
	public static Block stone_full_6;
	public static Block stone_full_7;
	public static Block stone_full_8;
	public static Block stone_full_9;
	public static Block stone_full_10;
	public static Block stone_full_11;
	public static Block stone_full_12;
	public static Block stone_full_13;
	public static Block stone_full_14;
	public static Block stone_full_15;
	public static Block stone_full_16;
	public static Block stone_full_17;

	//Stone Damage Light 0.6 Block
	public static Block stone_fulldamagelight6_1;
	public static Block stone_layerdamagelight6_1;

	//Stone Damage Light 0.8 Block
	public static Block stone_layerdamagelight8_1;

	//Partial Full Stone Blocks
	public static Block stone_fullpartial_1;
	public static Block stone_fullpartial_2;
	public static Block stone_fullpartial_3;
	public static Block stone_fullpartial_4;
	public static Block stone_fullpartial_5;
	public static Block stone_fullpartial_6;

	//Directional Full Partial Stone Blocks
	public static Block stone_directionalfullpartial_1;

	//Corner Stone Blocks
	public static Block stone_corner_1;
	public static Block stone_corner_2;
	public static Block stone_corner_3;
	public static Block stone_corner_4;
	public static Block stone_corner_5;
	public static Block stone_corner_6;
	public static Block stone_corner_7;
	public static Block stone_corner_8;
	public static Block stone_corner_9;
	public static Block stone_corner_10;
	public static Block stone_corner_11;
	public static Block stone_corner_12;
	public static Block stone_corner_13;
	public static Block stone_corner_14;
	public static Block stone_corner_15;
	public static Block stone_corner_16;
	public static Block stone_corner_17;
	public static Block stone_corner_18;
	public static Block stone_corner_19;
	public static Block stone_corner_20;
	public static Block stone_corner_21;
	public static Block stone_corner_22;
	public static Block stone_corner_23;
	public static Block stone_corner_24;
	public static Block stone_corner_25;
	public static Block stone_corner_26;
	public static Block stone_corner_27;
	public static Block stone_corner_28;
	public static Block stone_corner_29;
	public static Block stone_corner_30;
	public static Block stone_corner_31;
	public static Block stone_corner_32;
	public static Block stone_corner_33;
	public static Block stone_corner_34;
	public static Block stone_corner_35;
	public static Block stone_corner_36;
	public static Block stone_corner_37;
	public static Block stone_corner_38;
	public static Block stone_corner_39;
	public static Block stone_corner_40;
	public static Block stone_corner_41;
	public static Block stone_corner_42;
	public static Block stone_corner_43;
	public static Block stone_corner_44;
	public static Block stone_corner_45;
	public static Block stone_corner_46;
	public static Block stone_corner_47;
	public static Block stone_corner_48;
	public static Block stone_corner_49;
	public static Block stone_corner_50;
	public static Block stone_corner_51;
	public static Block stone_corner_52;
	public static Block stone_corner_53;
	public static Block stone_corner_54;
	public static Block stone_corner_55;
	public static Block stone_corner_56;
	public static Block stone_corner_57;
	public static Block stone_corner_58;
	public static Block stone_corner_59;
	public static Block stone_corner_60;
	public static Block stone_corner_61;
	public static Block stone_corner_62;
	public static Block stone_corner_63;
	public static Block stone_corner_64;

	//Vertical Slab Stone Blocks
	public static Block stone_verticalslab_1;
	public static Block stone_verticalslab_2;
	public static Block stone_verticalslab_3;
	public static Block stone_verticalslab_4;
	public static Block stone_verticalslab_5;
	public static Block stone_verticalslab_6;
	public static Block stone_verticalslab_7;
	public static Block stone_verticalslab_8;
	public static Block stone_verticalslab_9;
	public static Block stone_verticalslab_10;
	public static Block stone_verticalslab_11;
	public static Block stone_verticalslab_12;
	public static Block stone_verticalslab_13;
	public static Block stone_verticalslab_14;
	public static Block stone_verticalslab_15;
	public static Block stone_verticalslab_16;
	public static Block stone_verticalslab_17;
	public static Block stone_verticalslab_18;
	public static Block stone_verticalslab_19;
	public static Block stone_verticalslab_20;
	public static Block stone_verticalslab_21;
	public static Block stone_verticalslab_22;
	public static Block stone_verticalslab_23;
	public static Block stone_verticalslab_24;
	public static Block stone_verticalslab_25;
	public static Block stone_verticalslab_26;
	public static Block stone_verticalslab_27;
	public static Block stone_verticalslab_28;
	public static Block stone_verticalslab_29;
	public static Block stone_verticalslab_30;
	public static Block stone_verticalslab_31;
	public static Block stone_verticalslab_32;
	public static Block stone_verticalslab_33;
	public static Block stone_verticalslab_34;
	public static Block stone_verticalslab_35;
	public static Block stone_verticalslab_36;
	public static Block stone_verticalslab_37;
	public static Block stone_verticalslab_38;
	public static Block stone_verticalslab_39;
	public static Block stone_verticalslab_40;
	public static Block stone_verticalslab_41;
	public static Block stone_verticalslab_42;
	public static Block stone_verticalslab_43;
	public static Block stone_verticalslab_44;
	public static Block stone_verticalslab_45;
	public static Block stone_verticalslab_46;
	public static Block stone_verticalslab_47;
	public static Block stone_verticalslab_48;
	public static Block stone_verticalslab_49;
	public static Block stone_verticalslab_50;
	public static Block stone_verticalslab_51;
	public static Block stone_verticalslab_52;
	public static Block stone_verticalslab_53;
	public static Block stone_verticalslab_54;
	public static Block stone_verticalslab_55;
	public static Block stone_verticalslab_56;
	public static Block stone_verticalslab_57;
	public static Block stone_verticalslab_58;
	public static Block stone_verticalslab_59;
	public static Block stone_verticalslab_60;
	public static Block stone_verticalslab_61;
	public static Block stone_verticalslab_62;
	public static Block stone_verticalslab_63;
	public static Block stone_verticalslab_64;
	public static Block stone_verticalslab_65;
	public static Block stone_verticalslab_66;
	public static Block stone_verticalslab_67;
	public static Block stone_verticalslab_68;
	public static Block stone_verticalslab_69;
	public static Block stone_verticalslab_70;
	public static Block stone_verticalslab_71;

	//Arrow Slit Stone Blocks
	public static Block stone_arrowslit_1;
	public static Block stone_arrowslit_2;
	public static Block stone_arrowslit_3;
	public static Block stone_arrowslit_4;
	public static Block stone_arrowslit_5;
	public static Block stone_arrowslit_6;
	public static Block stone_arrowslit_7;
	public static Block stone_arrowslit_8;
	public static Block stone_arrowslit_9;
	public static Block stone_arrowslit_10;
	public static Block stone_arrowslit_11;
	public static Block stone_arrowslit_12;
	public static Block stone_arrowslit_13;
	public static Block stone_arrowslit_14;
	public static Block stone_arrowslit_15;
	public static Block stone_arrowslit_16;
	public static Block stone_arrowslit_17;
	public static Block stone_arrowslit_18;
	public static Block stone_arrowslit_19;
	public static Block stone_arrowslit_20;
	public static Block stone_arrowslit_21;
	public static Block stone_arrowslit_22;
	public static Block stone_arrowslit_23;
	public static Block stone_arrowslit_24;

	//Log Stone Blocks
	public static Block stone_log_1;
	public static Block stone_log_2;

	//Wall Stone Blocks
	public static Block stone_wall_1;
	public static Block stone_wall_2;
	public static Block stone_wall_3;
	public static Block stone_wall_4;
	public static Block stone_wall_5;
	public static Block stone_wall_6;
	public static Block stone_wall_7;
	public static Block stone_wall_8;
	public static Block stone_wall_9;

	//Pillar Stone Blocks
	public static Block stone_pillar_1;
	public static Block stone_pillar_2;
	public static Block stone_pillar_3;
	public static Block stone_pillar_4;
	public static Block stone_pillar_5;
	public static Block stone_pillar_6;
	public static Block stone_pillar_7;
	public static Block stone_pillar_8;
	public static Block stone_pillar_9;

	//Fence Stone Blocks
	public static Block stone_fence_1;
	public static Block stone_fence_2;

	//Fencegate Stone Blocks
	public static Block stone_fencegate_1;

	//Small Pillar Stone Blocks
	public static Block stone_smallpillar_1;
	public static Block stone_smallpillar_2;
	public static Block stone_smallpillar_3;
	public static Block stone_smallpillar_4;
	public static Block stone_smallpillar_5;
	public static Block stone_smallpillar_6;
	public static Block stone_smallpillar_7;
	public static Block stone_smallpillar_8;
	public static Block stone_smallpillar_9;
	public static Block stone_smallpillar_10;

	//Full Slit Stone Blocks
	public static Block stone_fullslit_1;
	public static Block stone_fullslit_2;
	public static Block stone_fullslit_3;
	public static Block stone_fullslit_4;
	public static Block stone_fullslit_5;
	public static Block stone_fullslit_6;
	public static Block stone_fullslit_7;
	public static Block stone_fullslit_8;
	public static Block stone_fullslit_9;
	public static Block stone_fullslit_10;
	public static Block stone_fullslit_11;
	public static Block stone_fullslit_12;
	public static Block stone_fullslit_13;
	public static Block stone_fullslit_14;
	public static Block stone_fullslit_15;
	public static Block stone_fullslit_16;
	public static Block stone_fullslit_17;
	public static Block stone_fullslit_18;
	public static Block stone_fullslit_19;
	public static Block stone_fullslit_20;
	public static Block stone_fullslit_21;
	public static Block stone_fullslit_22;
	public static Block stone_fullslit_23;
	public static Block stone_fullslit_24;

	//Anvil Stone Blocks
	public static Block stone_anvil_1;
	public static Block stone_anvil_2;
	public static Block stone_anvil_3;
	public static Block stone_anvil_4;
	public static Block stone_anvil_5;
	public static Block stone_anvil_6;
	public static Block stone_anvil_7;
	public static Block stone_anvil_8;
	public static Block stone_anvil_9;
	public static Block stone_anvil_10;
	public static Block stone_anvil_11;
	public static Block stone_anvil_12;
	public static Block stone_anvil_13;
	public static Block stone_anvil_14;
	public static Block stone_anvil_15;
	public static Block stone_anvil_16;
	public static Block stone_anvil_17;
	public static Block stone_anvil_18;
	public static Block stone_anvil_19;
	public static Block stone_anvil_20;
	public static Block stone_anvil_21;
	public static Block stone_anvil_22;
	public static Block stone_anvil_23;
	public static Block stone_anvil_24;
	public static Block stone_anvil_25;
	public static Block stone_anvil_26;
	public static Block stone_anvil_27;
	public static Block stone_anvil_28;

	//Stone Hopper Full Blocks
	public static Block stone_hopperfull_1;
	public static Block stone_hopperfull_2;
	public static Block stone_hopperfull_3;
	public static Block stone_hopperfull_4;
	public static Block stone_hopperfull_5;
	public static Block stone_hopperfull_6;
	public static Block stone_hopperfull_7;

	//Stone Hopper Directional Blocks
	public static Block stone_hopperdirectional_1;
	public static Block stone_hopperdirectional_2;
	public static Block stone_hopperdirectional_3;
	public static Block stone_hopperdirectional_4;
	public static Block stone_hopperdirectional_5;
	public static Block stone_hopperdirectional_6;
	public static Block stone_hopperdirectional_7;
	public static Block stone_hopperdirectional_8;
	public static Block stone_hopperdirectional_9;
	public static Block stone_hopperdirectional_10;
	public static Block stone_hopperdirectional_11;
	public static Block stone_hopperdirectional_12;
	public static Block stone_hopperdirectional_13;
	public static Block stone_hopperdirectional_14;
	public static Block stone_hopperdirectional_15;
	public static Block stone_hopperdirectional_16;
	public static Block stone_hopperdirectional_17;
	public static Block stone_hopperdirectional_18;
	public static Block stone_hopperdirectional_19;
	public static Block stone_hopperdirectional_20;
	public static Block stone_hopperdirectional_21;
	public static Block stone_hopperdirectional_22;
	public static Block stone_hopperdirectional_23;
	public static Block stone_hopperdirectional_24;
	public static Block stone_hopperdirectional_25;
	//Stairs Stone Blocks
	public static Block stone_stairs_1;
	public static Block stone_stairs_2;
	public static Block stone_stairs_3;
	public static Block stone_stairs_4;
	public static Block stone_stairs_5;
	public static Block stone_stairs_6;
	public static Block stone_stairs_7;
	public static Block stone_stairs_8;
	public static Block stone_stairs_9;
	public static Block stone_stairs_10;
	public static Block stone_stairs_11;
	public static Block stone_stairs_12;
	public static Block stone_stairs_13;
	public static Block stone_stairs_14;
	public static Block stone_stairs_15;
	public static Block stone_stairs_16;
	public static Block stone_stairs_17;
	public static Block stone_stairs_18;
	public static Block stone_stairs_19;
	public static Block stone_stairs_20;
	public static Block stone_stairs_21;
	public static Block stone_stairs_22;
	public static Block stone_stairs_23;
	public static Block stone_stairs_24;
	public static Block stone_stairs_25;
	public static Block stone_stairs_26;
	public static Block stone_stairs_27;
	public static Block stone_stairs_28;
	public static Block stone_stairs_29;
	public static Block stone_stairs_30;
	public static Block stone_stairs_31;
	public static Block stone_stairs_32;
	public static Block stone_stairs_33;
	public static Block stone_stairs_34;
	public static Block stone_stairs_35;
	public static Block stone_stairs_36;
	public static Block stone_stairs_37;
	public static Block stone_stairs_38;
	public static Block stone_stairs_39;
	public static Block stone_stairs_40;
	public static Block stone_stairs_41;
	public static Block stone_stairs_42;
	public static Block stone_stairs_43;
	public static Block stone_stairs_44;
	public static Block stone_stairs_45;
	public static Block stone_stairs_46;
	public static Block stone_stairs_47;
	public static Block stone_stairs_48;
	public static Block stone_stairs_49;
	public static Block stone_stairs_50;
	public static Block stone_stairs_51;
	public static Block stone_stairs_52;
	public static Block stone_stairs_53;
	public static Block stone_stairs_54;
	public static Block stone_stairs_55;
	public static Block stone_stairs_56;
	public static Block stone_stairs_57;
	public static Block stone_stairs_58;
	public static Block stone_stairs_59;
	public static Block stone_stairs_60;
	public static Block stone_stairs_61;
	public static Block stone_stairs_62;
	public static Block stone_stairs_63;
	public static Block stone_stairs_64;
	public static Block stone_stairs_65;
	public static Block stone_stairs_66;
	public static Block stone_stairs_67;
	public static Block stone_stairs_68;
	public static Block stone_stairs_69;
	public static Block stone_stairs_70;
	public static Block stone_stairs_71;
	public static Block stone_stairs_72;
	public static Block stone_stairs_73;
	public static Block stone_stairs_74;
	public static Block stone_stairs_75;
	public static Block stone_stairs_76;
	public static Block stone_stairs_77;
	public static Block stone_stairs_78;
	public static Block stone_stairs_79;
	public static Block stone_stairs_80;
	public static Block stone_stairs_81;
	public static Block stone_stairs_82;
	public static Block stone_stairs_83;
	public static Block stone_stairs_84;
	public static Block stone_stairs_85;
	public static Block stone_stairs_86;
	public static Block stone_stairs_87;
	public static Block stone_stairs_88;
	public static Block stone_stairs_89;
	public static Block stone_stairs_90;
	public static Block stone_stairs_91;
	public static Block stone_stairs_92;
	public static Block stone_stairs_93;
	public static Block stone_stairs_94;
	public static Block stone_stairs_95;
	public static Block stone_stairs_96;
	public static Block stone_stairs_97;
	public static Block stone_stairs_98;
	public static Block stone_stairs_99;
	public static Block stone_stairs_100;
	public static Block stone_stairs_101;
	public static Block stone_stairs_102;
	public static Block stone_stairs_103;
	public static Block stone_stairs_104;
	public static Block stone_stairs_105;
	public static Block stone_stairs_106;
	public static Block stone_stairs_107;
	public static Block stone_stairs_108;
	public static Block stone_stairs_109;

	//Slab Stone Blocks
	public static Block stone_slab_1;
	public static Block stone_slab_2;
	public static Block stone_slab_3;
	public static Block stone_slab_4;
	public static Block stone_slab_5;
	public static Block stone_slab_6;
	public static Block stone_slab_7;
	public static Block stone_slab_8;
	public static Block stone_slab_9;
	public static Block stone_slab_10;
	public static Block stone_slab_11;
	public static Block stone_slab_12;
	public static Block stone_slab_13;
	public static Block stone_slab_14;
	public static Block stone_slab_15;
	public static Block stone_slab_16;
	public static Block stone_slab_17;
	public static Block stone_slab_18;
	public static Block stone_slab_19;
	public static Block stone_slab_20;

	//Stone Trapdoor Blocks
	public static Block stone_trapdoormodel_1;
	public static Block stone_trapdoormodel_2;
	public static Block stone_trapdoormodel_3;
	public static Block stone_trapdoormodel_4;
	public static Block stone_trapdoormodel_5;
	public static Block stone_trapdoormodel_6;
	public static Block stone_trapdoormodel_7;
	public static Block stone_trapdoormodel_8;
	public static Block stone_trapdoormodel_9;
	public static Block stone_trapdoormodel_10;
	public static Block stone_trapdoormodel_11;
	public static Block stone_trapdoormodel_12;
	public static Block stone_trapdoormodel_13;
	public static Block stone_trapdoormodel_14;
	public static Block stone_trapdoormodel_15;
	public static Block stone_trapdoormodel_16;
	public static Block stone_trapdoormodel_17;
	public static Block stone_trapdoormodel_18;
	public static Block stone_trapdoormodel_19;
	public static Block stone_trapdoormodel_20;
	public static Block stone_trapdoormodel_21;
	public static Block stone_trapdoormodel_22;
	public static Block stone_trapdoormodel_23;
	public static Block stone_trapdoormodel_24;
	public static Block stone_trapdoormodel_25;
	public static Block stone_trapdoormodel_26;
	public static Block stone_trapdoormodel_27;
	public static Block stone_trapdoormodel_28;
	public static Block stone_trapdoormodel_29;
	public static Block stone_trapdoormodel_30;
	public static Block stone_trapdoormodel_31;
	public static Block stone_trapdoormodel_32;
	public static Block stone_trapdoormodel_33;
	public static Block stone_trapdoormodel_34;
	public static Block stone_trapdoormodel_35;
	public static Block stone_trapdoormodel_36;
	public static Block stone_trapdoormodel_37;
	public static Block stone_trapdoormodel_38;
	public static Block stone_trapdoormodel_39;
	public static Block stone_trapdoormodel_40;
	public static Block stone_trapdoormodel_41;
	public static Block stone_trapdoormodel_42;
	public static Block stone_trapdoormodel_43;
	public static Block stone_trapdoormodel_44;
	public static Block stone_trapdoormodel_45;
	public static Block stone_trapdoormodel_46;
	public static Block stone_trapdoormodel_47;
	public static Block stone_trapdoormodel_48;
	public static Block stone_trapdoormodel_49;
	public static Block stone_trapdoormodel_50;
	public static Block stone_trapdoormodel_51;
	public static Block stone_trapdoormodel_52;
	public static Block stone_trapdoormodel_53;
	public static Block stone_trapdoormodel_54;
	public static Block stone_trapdoormodel_55;
	public static Block stone_trapdoormodel_56;
	public static Block stone_trapdoormodel_57;
	public static Block stone_trapdoormodel_58;
	public static Block stone_trapdoormodel_59;
	public static Block stone_trapdoormodel_60;
	public static Block stone_trapdoormodel_61;
	public static Block stone_trapdoormodel_62;
	public static Block stone_trapdoormodel_63;
	public static Block stone_trapdoormodel_64;
	public static Block stone_trapdoormodel_65;
	public static Block stone_trapdoormodel_66;
	public static Block stone_trapdoormodel_67;
	public static Block stone_trapdoormodel_68;
	public static Block stone_trapdoormodel_69;
	public static Block stone_trapdoormodel_70;
	public static Block stone_trapdoormodel_71;
	public static Block stone_trapdoormodel_72;
	public static Block stone_trapdoormodel_73;
	public static Block stone_trapdoormodel_74;
	public static Block stone_trapdoormodel_75;
	public static Block stone_trapdoormodel_76;
	public static Block stone_trapdoormodel_77;
	public static Block stone_trapdoormodel_78;
	public static Block stone_trapdoormodel_79;
	public static Block stone_trapdoormodel_80;
	public static Block stone_trapdoormodel_81;
	public static Block stone_trapdoormodel_82;
	public static Block stone_trapdoormodel_83;
	public static Block stone_trapdoormodel_84;
	public static Block stone_trapdoormodel_85;
	public static Block stone_trapdoormodel_86;
	public static Block stone_trapdoormodel_87;
	public static Block stone_trapdoormodel_88;
	public static Block stone_trapdoormodel_89;
	public static Block stone_trapdoormodel_90;
	public static Block stone_trapdoormodel_91;
	public static Block stone_trapdoormodel_92;
	public static Block stone_trapdoormodel_93;
	public static Block stone_trapdoormodel_94;
	public static Block stone_trapdoormodel_95;
	public static Block stone_trapdoormodel_96;
	public static Block stone_trapdoormodel_97;
	public static Block stone_trapdoormodel_98;
	public static Block stone_trapdoormodel_99;
	public static Block stone_trapdoormodel_100;
	public static Block stone_trapdoormodel_101;
	public static Block stone_trapdoormodel_102;
	public static Block stone_trapdoormodel_103;
	public static Block stone_trapdoormodel_104;
	public static Block stone_trapdoormodel_105;
	public static Block stone_trapdoormodel_106;
	public static Block stone_trapdoormodel_107;
	public static Block stone_trapdoormodel_108;
	public static Block stone_trapdoormodel_109;
	public static Block stone_trapdoormodel_110;
	public static Block stone_trapdoormodel_111;
	public static Block stone_trapdoormodel_112;
	public static Block stone_trapdoormodel_113;
	public static Block stone_trapdoormodel_114;
	public static Block stone_trapdoormodel_115;
	public static Block stone_trapdoormodel_116;
	public static Block stone_trapdoormodel_117;
	public static Block stone_trapdoormodel_118;
	public static Block stone_trapdoormodel_119;
	public static Block stone_trapdoormodel_120;
	public static Block stone_trapdoormodel_121;
	public static Block stone_trapdoormodel_122;
	public static Block stone_trapdoormodel_123;
	public static Block stone_trapdoormodel_124;
	public static Block stone_trapdoormodel_125;
	public static Block stone_trapdoormodel_126;
	public static Block stone_trapdoormodel_127;
	public static Block stone_trapdoormodel_128;
	public static Block stone_trapdoormodel_129;
	public static Block stone_trapdoormodel_130;
	public static Block stone_trapdoormodel_131;
	public static Block stone_trapdoormodel_132;
	public static Block stone_trapdoormodel_133;
	public static Block stone_trapdoormodel_134;
	public static Block stone_trapdoormodel_135;
	public static Block stone_trapdoormodel_136;
	public static Block stone_trapdoormodel_137;
	public static Block stone_trapdoormodel_138;
	public static Block stone_trapdoormodel_139;
	public static Block stone_trapdoormodel_140;
	public static Block stone_trapdoormodel_141;
	public static Block stone_trapdoormodel_142;
	public static Block stone_trapdoormodel_143;
	public static Block stone_trapdoormodel_144;

	//Carpet Stone Blocks
	public static Block stone_carpet_1;
	public static Block stone_carpet_2;
	public static Block stone_carpet_3;
	public static Block stone_carpet_4;

	//Stone Layer Blocks
	public static Block stone_layer_1;
	public static Block stone_layer_2;
	public static Block stone_layer_3;
	public static Block stone_layer_4;
	public static Block stone_layer_5;
	public static Block stone_layer_6;
	public static Block stone_layer_7;
	public static Block stone_layer_8;

	//End Frame Stone Blocks
	public static Block base_marblesandstone_endframe;
	public static Block urn_terracotta_endframe;

	//Stone Ladder Blocks
	public static Block climbingrocks;

	//Directional No Collision Stone Blocks
	public static Block stone_directionalnocollision_1;

	//No Collision Stone Blocks
	public static Block stone_nocollision_1;
	public static Block stone_nocollision_2;

	//Full Wood Blocks
	public static Block wood_full_1;
	public static Block wood_full_2;
	public static Block wood_full_3;
	public static Block wood_full_4;
	public static Block wood_full_5;
	public static Block wood_full_6;
	public static Block wood_full_7;

	//Full Slit Wood Blocks
	public static Block wood_fullslit_1;
	public static Block wood_fullslit_2;
	public static Block wood_fullslit_3;
	public static Block wood_fullslit_4;
	public static Block wood_fullslit_5;
	public static Block wood_fullslit_6;
	public static Block wood_fullslit_7;

	//Log Wood Blocks
	public static Block wood_log_1;
	public static Block wood_log_2;
	public static Block wood_log_3;
	public static Block wood_log_4;
	public static Block wood_log_5;
	public static Block wood_log_6;

	//ConnectedXZ Wood Blocks
	public static Block wood_connectedxz_1;

	//Wood Anvil Blocks
	public static Block wood_anvil_1;
	public static Block wood_anvil_2;
	public static Block wood_anvil_3;
	public static Block wood_anvil_4;

	//Half Wood Blocks
	public static Block wood_half_1;

	//Daylight Detector Wood Blocks
	public static Block wood_daylightdetector_1;

	//Wood Hopper Full Blocks
	public static Block wood_hopperfull_1;

	//Wood Hopper Directional Blocks
	public static Block wood_hopperdirectional_1;
	public static Block wood_hopperdirectional_2;
	public static Block wood_hopperdirectional_3;
	public static Block wood_hopperdirectional_4;

	//Wood Walls Blocks
	public static Block wood_wall_1;
	public static Block wood_wall_2;
	public static Block wood_wall_3;
	public static Block wood_wall_4;

	//Wood Pillar Blocks
	public static Block wood_pillar_1;
	public static Block wood_pillar_2;
	public static Block wood_pillar_3;
	public static Block wood_pillar_4;

	//Fence Wood Blocks
	public static Block wood_fence_1;
	public static Block wood_fence_2;
	public static Block wood_fence_3;

	//Fencegate Wood Blocks
	public static Block wood_fencegate_1;
	public static Block wood_fencegate_2;
	public static Block wood_fencegate_3;
	public static Block wood_fencegate_4;
	public static Block wood_fencegate_5;
	public static Block wood_fencegate_6;
	public static Block wood_fencegate_7;
	public static Block wood_fencegate_8;
	public static Block wood_fencegate_9;
	public static Block wood_fencegate_10;
	public static Block wood_fencegate_11;
	public static Block wood_fencegate_12;

	//Door Wood Blocks
	public static Block planks_whiteweathered_door;
	public static Block planks_redweathered_door;
	public static Block planks_lightredweathered_door;
	public static Block planks_orangepainted_door;
	public static Block planks_yellowweathered_door;
	public static Block planks_greenweathered_door;
	public static Block planks_limeweathered_door;
	public static Block planks_cyanweathered_door;
	public static Block planks_darkblueweathered_door;
	public static Block planks_blueweathered_door;
	public static Block planks_lightblueweathered_door;
	public static Block planks_purplepainted_door;
	public static Block planks_brownweathered_door;

	//Small Pillar Wood Blocks
	public static Block wood_smallpillar_1;
	public static Block wood_smallpillar_2;
	public static Block wood_smallpillar_3;

	//Full Partial Wood Blocks
	public static Block wood_fullpartial_1;
	public static Block wood_fullpartial_2;

	//Stairs Wood Blocks
	public static Block wood_stairs_1;
	public static Block wood_stairs_2;
	public static Block wood_stairs_3;
	public static Block wood_stairs_4;
	public static Block wood_stairs_5;
	public static Block wood_stairs_6;
	public static Block wood_stairs_7;
	public static Block wood_stairs_8;
	public static Block wood_stairs_9;
	public static Block wood_stairs_10;
	public static Block wood_stairs_11;
	public static Block wood_stairs_12;
	public static Block wood_stairs_13;
	public static Block wood_stairs_14;
	public static Block wood_stairs_15;
	public static Block wood_stairs_16;
	public static Block wood_stairs_17;
	public static Block wood_stairs_18;
	public static Block wood_stairs_19;
	public static Block wood_stairs_20;
	public static Block wood_stairs_21;
	public static Block wood_stairs_22;
	public static Block wood_stairs_23;
	public static Block wood_stairs_24;
	public static Block wood_stairs_25;
	public static Block wood_stairs_26;
	public static Block wood_stairs_27;

	//Slab Wood Blocks
	public static Block wood_slab_1;
	public static Block wood_slab_2;
	public static Block wood_slab_3;
	public static Block wood_slab_4;
	public static Block wood_slab_5;
	public static Block wood_slab_6;
	public static Block wood_slab_7;

	//Wood Trapdoor Blocks
	public static Block wood_trapdoormodel_1;
	public static Block wood_trapdoormodel_2;
	public static Block wood_trapdoormodel_3;
	public static Block wood_trapdoormodel_4;
	public static Block wood_trapdoormodel_5;
	public static Block wood_trapdoormodel_6;
	public static Block wood_trapdoormodel_7;
	public static Block wood_trapdoormodel_8;
	public static Block wood_trapdoormodel_9;
	public static Block wood_trapdoormodel_10;
	public static Block wood_trapdoormodel_11;
	public static Block wood_trapdoormodel_12;
	public static Block wood_trapdoormodel_13;
	public static Block wood_trapdoormodel_14;
	public static Block wood_trapdoormodel_15;
	public static Block wood_trapdoormodel_16;
	public static Block wood_trapdoormodel_17;
	public static Block wood_trapdoormodel_18;
	public static Block wood_trapdoormodel_19;
	public static Block wood_trapdoormodel_20;
	public static Block wood_trapdoormodel_21;
	public static Block wood_trapdoormodel_22;
	public static Block wood_trapdoormodel_23;
	public static Block wood_trapdoormodel_24;
	public static Block wood_trapdoormodel_25;
	public static Block wood_trapdoormodel_26;
	public static Block wood_trapdoormodel_27;
	public static Block wood_trapdoormodel_28;
	public static Block wood_trapdoormodel_29;
	public static Block wood_trapdoormodel_30;
	public static Block wood_trapdoormodel_31;
	public static Block wood_trapdoormodel_32;
	public static Block wood_trapdoormodel_33;
	public static Block wood_trapdoormodel_34;
	public static Block wood_trapdoormodel_35;
	public static Block wood_trapdoormodel_36;
	public static Block wood_trapdoormodel_37;
	public static Block wood_trapdoormodel_38;
	public static Block wood_trapdoormodel_39;

	//Wood Vertical Slab Blocks
	public static Block wood_verticalslab_1;
	public static Block wood_verticalslab_2;
	public static Block wood_verticalslab_3;
	public static Block wood_verticalslab_4;
	public static Block wood_verticalslab_5;
	public static Block wood_verticalslab_6;
	public static Block wood_verticalslab_7;
	public static Block wood_verticalslab_8;
	public static Block wood_verticalslab_9;
	public static Block wood_verticalslab_10;
	public static Block wood_verticalslab_11;
	public static Block wood_verticalslab_12;
	public static Block wood_verticalslab_13;
	public static Block wood_verticalslab_14;
	public static Block wood_verticalslab_15;
	public static Block wood_verticalslab_16;
	public static Block wood_verticalslab_17;
	public static Block wood_verticalslab_18;
	public static Block wood_verticalslab_19;

	//Wood Corner Blocks
	public static Block wood_corner_1;
	public static Block wood_corner_2;
	public static Block wood_corner_3;
	public static Block wood_corner_4;
	public static Block wood_corner_5;
	public static Block wood_corner_6;
	public static Block wood_corner_7;
	public static Block wood_corner_8;
	public static Block wood_corner_9;
	public static Block wood_corner_10;
	public static Block wood_corner_11;
	public static Block wood_corner_12;
	public static Block wood_corner_13;
	public static Block wood_corner_14;
	public static Block wood_corner_15;
	public static Block wood_corner_16;
	public static Block wood_corner_17;
	public static Block wood_corner_18;
	public static Block wood_corner_19;

	//Wood Functional Trapdoor Blocks
	public static Block woodenboard;
	public static Block wheel;

	//Wood Shutters Blocks
	public static Block shutters_light;
	public static Block shutters_dark;

	//Wood Trapdoor Rail Blocks
	public static Block wood_railing;
	public static Block wood_railing_straight;
	public static Block spruce_railing;
	public static Block spruce_railing_straight;
	public static Block birch_railing;
	public static Block birch_railing_straight;
	public static Block jungle_railing;
	public static Block jungle_railing_straight;
	public static Block acacia_railing;
	public static Block acacia_railing_straight;

	//Wood Ironbar Blocks
	public static Block wood_ironbar_1;

	//Wood Carpet Blocks
	public static Block wood_carpet_1;

	//Wood Ladder Blocks
	public static Block ladder;

	//No Collision Wood Blocks
	public static Block wood_nocollision_1;

	//No Collision Damage Wood Blocks
	public static Block wood_nocollisiondamage_1;

	//Directional No Collision Wood Blocks
	public static Block wood_directionalnocollision_1;

	//Directional Collision Trapdoor Wood Blocks
	public static Block wood_directionalcollisiontrapdoor_1;
	public static Block wood_directionalcollisiontrapdoor_2;
	public static Block wood_directionalcollisiontrapdoor_3;

	//Bed Wood Blocks
	public static Block bed_wolf;
	public static Block bed_rustic;
	public static Block bed_bear;
	public static Block bed_fancygreen;

	//Cauldron Wood Blocks
	public static Block barrel_grille_cauldron;
	public static Block barrel_cauldron;

	//Enchanted Wood Blocks
	public static Block wood_enchantedbook_1;

	//Wood Rail Blocks
	public static Block jungle_rail;
	public static Block spruce_floor_rail;

	//Full Iron Blocks
	public static Block iron_full_1;

	//Full Partial Iron Light 1.0 Blocks
	public static Block iron_fullpartiallight10_1;

	//Iron Anvil Blocks
	public static Block iron_anvil_1;

	//Iron Ironbar Blocks
	public static Block iron_ironbar_1;

	//Stairs Iron Blocks
	public static Block iron_stairs_1;
	public static Block iron_stairs_2;

	//Iron Trapdoor Blocks
	public static Block iron_trapdoormodel_1;
	public static Block iron_trapdoormodel_2;

	//Iron Trapdoor Railing Blocks
	public static Block iron_railing;
	public static Block iron_railing_straight;

	//Cauldron Iron Blocks
	public static Block cauldron_irongrille;
	public static Block cauldron;

	//Directional No Collision Iron Blocks
	public static Block iron_directionalnocollision_1;
	public static Block iron_directionalnocollision_2;

	//Brewingstand Iron Blocks
	public static Block iron_brewingstand_1;
	public static Block iron_brewingstandlight10_1;

	//Iron Rail Blocks
	public static Block ironchains_rail;

	//Plants Log Blocks
	public static Block plants_log_1;

	//Stairs Plants Blocks
	public static Block plants_stairs_1;

	//Slab Plants Blocks
	public static Block plants_slab_1;

	//No Collision Plants Blocks
	public static Block plants_nocollision_1;
	public static Block plants_nocollision_2;
	public static Block plants_nocollision_3;
	public static Block plants_nocollision_4;
	public static Block plants_nocollision_5;

	//No Collision Biome Plants Blocks
	public static Block plants_nocollisionbiome_1;

	//Lilypad Plants Blocks
	public static Block plants_lilypad_1;
	public static Block plants_lilypad_2;

	//Full Leaves Blocks
	public static Block leaves_full_1;

	//Vine Vine Blocks
	public static Block vine_jungle;
	public static Block vine_ivy;
	public static Block vine_moss;

	//Snowlayer Biome Grass Blocks
	public static Block grass_snowlayer_1;

	//Slab Cloth Blocks
	public static Block cloth_slab_1;
	public static Block cloth_slab_2;

	//Pane Cloth Blocks
	public static Block cloth_pane_1;

	//Climbable Iron Bar Cloth Blocks
	public static Block cloth_climbironbar_1;

	//Climbable Iron Bar Iron/Cloth Blocks
	public static Block cloth_ironclimbironbar_1;

	//Full Cloth Light 1.0 Blocks
	public static Block cloth_fulllight10_1;

	//Cloth Ladder Blocks
	public static Block rope_ladder;
	public static Block rope_climbing_ladder;

	//No Collision Cloth Block
	public static Block cloth_nocollision_1;

	//No Collision Cloth Light 1.0 Blocks
	public static Block cloth_nocollisionlight10_1;

	//Directional No Collision Cloth Blocks
	public static Block cloth_directionalnocollision_1;

	//Cloth Rail Blocks
	public static Block rope_rail;

	//Vine Cloth Blocks
	public static Block curtain_black_vine;
	public static Block curtain_blue_vine;
	public static Block curtain_brown_vine;
	public static Block curtain_green_vine;
	public static Block curtain_purple_vine;
	public static Block curtain_red_vine;
	public static Block curtain_white_vine;

	//Full Sand Block
	public static Block sand_full_1;

	//Sand Layer Blocks
	public static Block sand_layer_1;
	public static Block sand_layer_2;
	public static Block sand_layer_3;
	public static Block sand_layer_4;
	public static Block sand_layer_5;
	public static Block sand_layer_6;
	//Full Ground Blocks
	public static Block ground_full_1;
	public static Block ground_full_2;
	//Soulsand Ground Blocks
	public static Block ground_soulsand_1;
	//Directional Full Partial Ground Blocks
	public static Block ground_directionalfullpartial_1;
	//Ground Layer Blocks
	public static Block ground_layer_1;
	public static Block ground_layer_2;
	public static Block ground_layer_3;
	public static Block ground_layer_4;
	public static Block ground_layer_5;
	public static Block ground_layer_6;
	public static Block ground_layer_7;
	public static Block ground_layer_8;
	public static Block ground_layer_9;
	public static Block ground_layer_10;
	public static Block ground_layer_11;
	public static Block ground_layer_12;
	public static Block ground_layer_13;

	//Translucent Ice Blocks
	public static Block ice_translucent_1;

	//Translucent Ice Blocks
	public static Block ice_slab_1;

	//Translucent No Collision Ice Blocks
	public static Block ice_translucentnocollision_1;

	//No Collision Ice Blocks
	public static Block ice_nocollision_1;

	//No Collision Damage Ice Blocks
	public static Block ice_nocollisiondamage_1;

	//Lilypad Ice Blocks
	public static Block ice_lilypad_1;

	//Stairs Snow Blocks
	public static Block snow_stairs_1;

	//No Collision No Material Blocks
	public static Block nomaterial_nocollision_1;

	//No Collision No Material Light 0.5 Blocks
	public static Block nomaterial_nocollisionlight5_1;

	//No Collision No Material Light 0.6 Blocks
	public static Block invisible_light6;

	//No Collision No Material Light 0.8 Blocks
	public static Block invisible_light8;

	// No Collision No Material Light 1.0 Blocks
	public static Block invisible_light10;

	//Full Glass Blocks
	public static Block glass_full_1;

	//Glass Glass Blocks
	public static Block glass_glass_1;
	public static Block glass_glass_2;

	//Glass Vertical Slab Blocks
	public static Block glass_verticalslab_1;

	//Glass Slab Blocks
	public static Block glass_trapdoormodel_1;
	public static Block glass_trapdoormodel_2;
	public static Block glass_trapdoormodel_3;
	public static Block glass_trapdoormodel_4;

	//Glass Pane Blocks
	public static Block glass_pane_1;
	public static Block glass_pane_2;

	//Cake Cake Block
	public static Block cake_cheesewheel;
	public static Block cake_applepie;
	public static Block cake_icingfruit;
	public static Block cake_icingchocolate;
	public static Block cake_bread;

	//Furnace Blocks
	public static Block furnace_iron;
	public static Block furnace_sandstone;
	//Lit Furnace Blocks
	public static Block furnace_ironlit;
	public static Block furnace_cobblelit;
	public static Block furnace_sandstonelit;
	//Dispenser Blocks
	public static Block dispenser_aztec;
	public static Block dispenser_iron;
	public static Block dispenser_egyptian;
	//Dropper Blocks
	public static Block dropper_sandstone;
	public static Block dropper_iron;
	//Flower Pot Blocks
	public static Block flowerpot;
	public static Block flowerpot_black;
	//Button Blocks
	public static Block wood_button;
	public static Block stone_button;
	//Lever Blocks
	public static Block lever;
	//Hook Blocks
	public static Block hook;

	public static void init() {
		/**
		 blockname = new BlockClassName("registryName", "unlocalizedName", Material, Hardness, Resistance, SoundType, LightLevel, "ToolName", ToolLevel, canSustainPlant, CreativeTab)
		 -common registryName and unlocalizedName for each meta values of block.
		 -ItemBlockMeta and IMetaBlockName assigns "unlocalizedName + value name" for each meta value.
		 Example: unlocalizedName is stone, and value name is alpha --> registered unlocalizedName = "stone.alpha"
		 in lang file we can register names like this = tiles.stone.alpha.name=Stone Name
		 */
	//Full Stone Blocks
		stone_full_1 = new BlockSimpleMeta("stone_full_1", "stone_full_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_full_2 = new BlockSimpleMeta("stone_full_2", "stone_full_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_full_3 = new BlockSimpleMeta("stone_full_3", "stone_full_3", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_full_4 = new BlockSimpleMeta("stone_full_4", "stone_full_4", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_full_5 = new BlockSimpleMeta("stone_full_5", "stone_full_5", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_full_6 = new BlockSimpleMeta("stone_full_6", "stone_full_6", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_full_7 = new BlockSimpleMeta("stone_full_7", "stone_full_7", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_full_8 = new BlockSimpleMeta("stone_full_8", "stone_full_8", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_full_9 = new BlockSimpleMeta("stone_full_9", "stone_full_9", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_full_10 = new BlockSimpleMeta("stone_full_10", "stone_full_10", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_full_11 = new BlockSimpleMeta("stone_full_11", "stone_full_11", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_full_12 = new BlockSimpleMeta("stone_full_12", "stone_full_12", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_full_13 = new BlockSimpleMeta("stone_full_13", "stone_full_13", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_full_14 = new BlockSimpleMeta("stone_full_14", "stone_full_14", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_full_15 = new BlockSimpleMeta("stone_full_15", "stone_full_15", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_full_16 = new BlockSimpleMeta("stone_full_16", "stone_full_16", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_full_17 = new BlockSimpleMeta("stone_full_17", "stone_full_17", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Stone Damage Light 0.6 Block
		stone_fulldamagelight6_1 = new DamageBlock("stone_fulldamagelight6_1", "stone_fulldamagelight6_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0.6F, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_layerdamagelight6_1 = new DamageLayerBlock("stone_layerdamagelight6_1", "stone_layerdamagelight6_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0.6F, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Stone Damage Light 0.8 Block
		stone_layerdamagelight8_1 = new DamageLayerBlock("stone_layerdamagelight8_1", "stone_layerdamagelight8_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0.8F, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Partial Full Stone Blocks
		stone_fullpartial_1 = new BlockPartialBasicMeta("stone_fullpartial_1", "stone_fullpartial_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullpartial_2 = new BlockPartialBasicMeta("stone_fullpartial_2", "stone_fullpartial_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullpartial_3 = new BlockPartialBasicMeta("stone_fullpartial_3", "stone_fullpartial_3", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullpartial_4 = new BlockPartialBasicMeta("stone_fullpartial_4", "stone_fullpartial_4", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullpartial_5 = new BlockPartialBasicMeta("stone_fullpartial_5", "stone_fullpartial_5", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullpartial_6 = new BlockPartialBasicMeta("stone_fullpartial_6", "stone_fullpartial_6", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Directional Full Partial Stone Blocks
		stone_directionalfullpartial_1 = new BlockDirectionalFullPartialMeta("stone_directionalfullpartial_1", "stone_directionalfullpartial_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Corner Stone Blocks
		stone_corner_1 = new BlockCornerMeta("stone_corner_1", "stone_corner_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_2 = new BlockCornerMeta("stone_corner_2", "stone_corner_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_3 = new BlockCornerMeta("stone_corner_3", "stone_corner_3", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_4 = new BlockCornerMeta("stone_corner_4", "stone_corner_4", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_5 = new BlockCornerMeta("stone_corner_5", "stone_corner_5", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_6 = new BlockCornerMeta("stone_corner_6", "stone_corner_6", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_7 = new BlockCornerMeta("stone_corner_7", "stone_corner_7", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_8 = new BlockCornerMeta("stone_corner_8", "stone_corner_8", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_9 = new BlockCornerMeta("stone_corner_9", "stone_corner_9", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_10 = new BlockCornerMeta("stone_corner_10", "stone_corner_10", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_11 = new BlockCornerMeta("stone_corner_11", "stone_corner_11", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_12 = new BlockCornerMeta("stone_corner_12", "stone_corner_12", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_13 = new BlockCornerMeta("stone_corner_13", "stone_corner_13", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_14 = new BlockCornerMeta("stone_corner_14", "stone_corner_14", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_15 = new BlockCornerMeta("stone_corner_15", "stone_corner_15", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_16 = new BlockCornerMeta("stone_corner_16", "stone_corner_16", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_17 = new BlockCornerMeta("stone_corner_17", "stone_corner_17", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_18 = new BlockCornerMeta("stone_corner_18", "stone_corner_18", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_19 = new BlockCornerMeta("stone_corner_19", "stone_corner_19", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_20 = new BlockCornerMeta("stone_corner_20", "stone_corner_20", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_21 = new BlockCornerMeta("stone_corner_21", "stone_corner_21", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_22 = new BlockCornerMeta("stone_corner_22", "stone_corner_22", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_23 = new BlockCornerMeta("stone_corner_23", "stone_corner_23", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_24 = new BlockCornerMeta("stone_corner_24", "stone_corner_24", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_25 = new BlockCornerMeta("stone_corner_25", "stone_corner_25", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_26 = new BlockCornerMeta("stone_corner_26", "stone_corner_26", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_27 = new BlockCornerMeta("stone_corner_27", "stone_corner_27", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_28 = new BlockCornerMeta("stone_corner_28", "stone_corner_28", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_29 = new BlockCornerMeta("stone_corner_29", "stone_corner_29", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_30 = new BlockCornerMeta("stone_corner_30", "stone_corner_30", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_31 = new BlockCornerMeta("stone_corner_31", "stone_corner_31", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_32 = new BlockCornerMeta("stone_corner_32", "stone_corner_32", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_33 = new BlockCornerMeta("stone_corner_33", "stone_corner_33", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_34 = new BlockCornerMeta("stone_corner_34", "stone_corner_34", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_35 = new BlockCornerMeta("stone_corner_35", "stone_corner_35", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_36 = new BlockCornerMeta("stone_corner_36", "stone_corner_36", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_37 = new BlockCornerMeta("stone_corner_37", "stone_corner_37", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_38 = new BlockCornerMeta("stone_corner_38", "stone_corner_38", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_39 = new BlockCornerMeta("stone_corner_39", "stone_corner_39", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_40 = new BlockCornerMeta("stone_corner_40", "stone_corner_40", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_41 = new BlockCornerMeta("stone_corner_41", "stone_corner_41", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_42 = new BlockCornerMeta("stone_corner_42", "stone_corner_42", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_43 = new BlockCornerMeta("stone_corner_43", "stone_corner_43", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_44 = new BlockCornerMeta("stone_corner_44", "stone_corner_44", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_45 = new BlockCornerMeta("stone_corner_45", "stone_corner_45", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_46 = new BlockCornerMeta("stone_corner_46", "stone_corner_46", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_47 = new BlockCornerMeta("stone_corner_47", "stone_corner_47", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_48 = new BlockCornerMeta("stone_corner_48", "stone_corner_48", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_49 = new BlockCornerMeta("stone_corner_49", "stone_corner_49", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_50 = new BlockCornerMeta("stone_corner_50", "stone_corner_50", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_51 = new BlockCornerMeta("stone_corner_51", "stone_corner_51", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_52 = new BlockCornerMeta("stone_corner_52", "stone_corner_52", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_53 = new BlockCornerMeta("stone_corner_53", "stone_corner_53", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_54 = new BlockCornerMeta("stone_corner_54", "stone_corner_54", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_55 = new BlockCornerMeta("stone_corner_55", "stone_corner_55", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_56 = new BlockCornerMeta("stone_corner_56", "stone_corner_56", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_57 = new BlockCornerMeta("stone_corner_57", "stone_corner_57", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_58 = new BlockCornerMeta("stone_corner_58", "stone_corner_58", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_59 = new BlockCornerMeta("stone_corner_59", "stone_corner_59", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_60 = new BlockCornerMeta("stone_corner_60", "stone_corner_60", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_61 = new BlockCornerMeta("stone_corner_61", "stone_corner_61", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_62 = new BlockCornerMeta("stone_corner_62", "stone_corner_62", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_63 = new BlockCornerMeta("stone_corner_63", "stone_corner_63", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_corner_64 = new BlockCornerMeta("stone_corner_64", "stone_corner_64", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Vertical Slab Stone Blocks
		stone_verticalslab_1 = new BlockVerticalSlabMeta("stone_verticalslab_1", "stone_verticalslab_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_2 = new BlockVerticalSlabMeta("stone_verticalslab_2", "stone_verticalslab_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_3 = new BlockVerticalSlabMeta("stone_verticalslab_3", "stone_verticalslab_3", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_4 = new BlockVerticalSlabMeta("stone_verticalslab_4", "stone_verticalslab_4", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_5 = new BlockVerticalSlabMeta("stone_verticalslab_5", "stone_verticalslab_5", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_6 = new BlockVerticalSlabMeta("stone_verticalslab_6", "stone_verticalslab_6", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_7 = new BlockVerticalSlabMeta("stone_verticalslab_7", "stone_verticalslab_7", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_8 = new BlockVerticalSlabMeta("stone_verticalslab_8", "stone_verticalslab_8", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_9 = new BlockVerticalSlabMeta("stone_verticalslab_9", "stone_verticalslab_9", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_10 = new BlockVerticalSlabMeta("stone_verticalslab_10", "stone_verticalslab_10", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_11 = new BlockVerticalSlabMeta("stone_verticalslab_11", "stone_verticalslab_11", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_12 = new BlockVerticalSlabMeta("stone_verticalslab_12", "stone_verticalslab_12", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_13 = new BlockVerticalSlabMeta("stone_verticalslab_13", "stone_verticalslab_13", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_14 = new BlockVerticalSlabMeta("stone_verticalslab_14", "stone_verticalslab_14", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_15 = new BlockVerticalSlabMeta("stone_verticalslab_15", "stone_verticalslab_15", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_16 = new BlockVerticalSlabMeta("stone_verticalslab_16", "stone_verticalslab_16", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_17 = new BlockVerticalSlabMeta("stone_verticalslab_17", "stone_verticalslab_17", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_18 = new BlockVerticalSlabMeta("stone_verticalslab_18", "stone_verticalslab_18", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_19 = new BlockVerticalSlabMeta("stone_verticalslab_19", "stone_verticalslab_19", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_20 = new BlockVerticalSlabMeta("stone_verticalslab_20", "stone_verticalslab_20", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_21 = new BlockVerticalSlabMeta("stone_verticalslab_21", "stone_verticalslab_21", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_22 = new BlockVerticalSlabMeta("stone_verticalslab_22", "stone_verticalslab_22", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_23 = new BlockVerticalSlabMeta("stone_verticalslab_23", "stone_verticalslab_23", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_24 = new BlockVerticalSlabMeta("stone_verticalslab_24", "stone_verticalslab_24", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_25 = new BlockVerticalSlabMeta("stone_verticalslab_25", "stone_verticalslab_25", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_26 = new BlockVerticalSlabMeta("stone_verticalslab_26", "stone_verticalslab_26", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_27 = new BlockVerticalSlabMeta("stone_verticalslab_27", "stone_verticalslab_27", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_28 = new BlockVerticalSlabMeta("stone_verticalslab_28", "stone_verticalslab_28", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_29 = new BlockVerticalSlabMeta("stone_verticalslab_29", "stone_verticalslab_29", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_30 = new BlockVerticalSlabMeta("stone_verticalslab_30", "stone_verticalslab_30", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_31 = new BlockVerticalSlabMeta("stone_verticalslab_31", "stone_verticalslab_31", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_32 = new BlockVerticalSlabMeta("stone_verticalslab_32", "stone_verticalslab_32", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_33 = new BlockVerticalSlabMeta("stone_verticalslab_33", "stone_verticalslab_33", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_34 = new BlockVerticalSlabMeta("stone_verticalslab_34", "stone_verticalslab_34", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_35 = new BlockVerticalSlabMeta("stone_verticalslab_35", "stone_verticalslab_35", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_36 = new BlockVerticalSlabMeta("stone_verticalslab_36", "stone_verticalslab_36", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_37 = new BlockVerticalSlabMeta("stone_verticalslab_37", "stone_verticalslab_37", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_38 = new BlockVerticalSlabMeta("stone_verticalslab_38", "stone_verticalslab_38", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_39 = new BlockVerticalSlabMeta("stone_verticalslab_39", "stone_verticalslab_39", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_40 = new BlockVerticalSlabMeta("stone_verticalslab_40", "stone_verticalslab_40", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_41 = new BlockVerticalSlabMeta("stone_verticalslab_41", "stone_verticalslab_41", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_42 = new BlockVerticalSlabMeta("stone_verticalslab_42", "stone_verticalslab_42", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_43 = new BlockVerticalSlabMeta("stone_verticalslab_43", "stone_verticalslab_43", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_44 = new BlockVerticalSlabMeta("stone_verticalslab_44", "stone_verticalslab_44", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_45 = new BlockVerticalSlabMeta("stone_verticalslab_45", "stone_verticalslab_45", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_46 = new BlockVerticalSlabMeta("stone_verticalslab_46", "stone_verticalslab_46", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_47 = new BlockVerticalSlabMeta("stone_verticalslab_47", "stone_verticalslab_47", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_48 = new BlockVerticalSlabMeta("stone_verticalslab_48", "stone_verticalslab_48", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_49 = new BlockVerticalSlabMeta("stone_verticalslab_49", "stone_verticalslab_49", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_50 = new BlockVerticalSlabMeta("stone_verticalslab_50", "stone_verticalslab_50", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_51 = new BlockVerticalSlabMeta("stone_verticalslab_51", "stone_verticalslab_51", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_52 = new BlockVerticalSlabMeta("stone_verticalslab_52", "stone_verticalslab_52", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_53 = new BlockVerticalSlabMeta("stone_verticalslab_53", "stone_verticalslab_53", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_54 = new BlockVerticalSlabMeta("stone_verticalslab_54", "stone_verticalslab_54", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_55 = new BlockVerticalSlabMeta("stone_verticalslab_55", "stone_verticalslab_55", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_56 = new BlockVerticalSlabMeta("stone_verticalslab_56", "stone_verticalslab_56", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_57 = new BlockVerticalSlabMeta("stone_verticalslab_57", "stone_verticalslab_57", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_58 = new BlockVerticalSlabMeta("stone_verticalslab_58", "stone_verticalslab_48", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_59 = new BlockVerticalSlabMeta("stone_verticalslab_59", "stone_verticalslab_49", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_60 = new BlockVerticalSlabMeta("stone_verticalslab_60", "stone_verticalslab_60", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_61 = new BlockVerticalSlabMeta("stone_verticalslab_61", "stone_verticalslab_61", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_62 = new BlockVerticalSlabMeta("stone_verticalslab_62", "stone_verticalslab_62", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_63 = new BlockVerticalSlabMeta("stone_verticalslab_63", "stone_verticalslab_63", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_64 = new BlockVerticalSlabMeta("stone_verticalslab_64", "stone_verticalslab_64", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_65 = new BlockVerticalSlabMeta("stone_verticalslab_65", "stone_verticalslab_65", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_66 = new BlockVerticalSlabMeta("stone_verticalslab_66", "stone_verticalslab_66", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_67 = new BlockVerticalSlabMeta("stone_verticalslab_67", "stone_verticalslab_67", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_68 = new BlockVerticalSlabMeta("stone_verticalslab_68", "stone_verticalslab_68", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_69 = new BlockVerticalSlabMeta("stone_verticalslab_69", "stone_verticalslab_69", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_70 = new BlockVerticalSlabMeta("stone_verticalslab_70", "stone_verticalslab_70", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_verticalslab_71 = new BlockVerticalSlabMeta("stone_verticalslab_71", "stone_verticalslab_71", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Arrow Slit Stone Blocks
		stone_arrowslit_1 = new BlockArrowSlitMeta("stone_arrowslit_1", "stone_arrowslit_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_2 = new BlockArrowSlitMeta("stone_arrowslit_2", "stone_arrowslit_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_3 = new BlockArrowSlitMeta("stone_arrowslit_3", "stone_arrowslit_3", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_4 = new BlockArrowSlitMeta("stone_arrowslit_4", "stone_arrowslit_4", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_5 = new BlockArrowSlitMeta("stone_arrowslit_5", "stone_arrowslit_5", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_6 = new BlockArrowSlitMeta("stone_arrowslit_6", "stone_arrowslit_6", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_7 = new BlockArrowSlitMeta("stone_arrowslit_7", "stone_arrowslit_7", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_8 = new BlockArrowSlitMeta("stone_arrowslit_8", "stone_arrowslit_8", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_9 = new BlockArrowSlitMeta("stone_arrowslit_9", "stone_arrowslit_9", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_10 = new BlockArrowSlitMeta("stone_arrowslit_10", "stone_arrowslit_10", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_11 = new BlockArrowSlitMeta("stone_arrowslit_11", "stone_arrowslit_11", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_12 = new BlockArrowSlitMeta("stone_arrowslit_12", "stone_arrowslit_12", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_13 = new BlockArrowSlitMeta("stone_arrowslit_13", "stone_arrowslit_13", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_14 = new BlockArrowSlitMeta("stone_arrowslit_14", "stone_arrowslit_14", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_15 = new BlockArrowSlitMeta("stone_arrowslit_15", "stone_arrowslit_15", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_16 = new BlockArrowSlitMeta("stone_arrowslit_16", "stone_arrowslit_16", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_17 = new BlockArrowSlitMeta("stone_arrowslit_17", "stone_arrowslit_17", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_18 = new BlockArrowSlitMeta("stone_arrowslit_18", "stone_arrowslit_18", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_19 = new BlockArrowSlitMeta("stone_arrowslit_19", "stone_arrowslit_19", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_20 = new BlockArrowSlitMeta("stone_arrowslit_20", "stone_arrowslit_20", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_21 = new BlockArrowSlitMeta("stone_arrowslit_21", "stone_arrowslit_21", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_22 = new BlockArrowSlitMeta("stone_arrowslit_22", "stone_arrowslit_22", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_23 = new BlockArrowSlitMeta("stone_arrowslit_23", "stone_arrowslit_23", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_arrowslit_24 = new BlockArrowSlitMeta("stone_arrowslit_24", "stone_arrowslit_24", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Full Slit Stone Blocks
		stone_fullslit_1 = new BlockRotatedModelMeta("stone_fullslit_1", "stone_fullslit_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_2 = new BlockRotatedModelMeta("stone_fullslit_2", "stone_fullslit_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_3 = new BlockRotatedModelMeta("stone_fullslit_3", "stone_fullslit_3", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_4 = new BlockRotatedModelMeta("stone_fullslit_4", "stone_fullslit_4", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_5 = new BlockRotatedModelMeta("stone_fullslit_5", "stone_fullslit_5", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_6 = new BlockRotatedModelMeta("stone_fullslit_6", "stone_fullslit_6", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_7 = new BlockRotatedModelMeta("stone_fullslit_7", "stone_fullslit_7", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_8 = new BlockRotatedModelMeta("stone_fullslit_8", "stone_fullslit_8", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_9 = new BlockRotatedModelMeta("stone_fullslit_9", "stone_fullslit_9", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_10 = new BlockRotatedModelMeta("stone_fullslit_10", "stone_fullslit_10", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_11 = new BlockRotatedModelMeta("stone_fullslit_11", "stone_fullslit_11", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_12 = new BlockRotatedModelMeta("stone_fullslit_12", "stone_fullslit_12", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_13 = new BlockRotatedModelMeta("stone_fullslit_13", "stone_fullslit_13", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_14 = new BlockRotatedModelMeta("stone_fullslit_14", "stone_fullslit_14", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_15 = new BlockRotatedModelMeta("stone_fullslit_15", "stone_fullslit_15", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_16 = new BlockRotatedModelMeta("stone_fullslit_16", "stone_fullslit_16", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_17 = new BlockRotatedModelMeta("stone_fullslit_17", "stone_fullslit_17", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_18 = new BlockRotatedModelMeta("stone_fullslit_18", "stone_fullslit_18", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_19 = new BlockRotatedModelMeta("stone_fullslit_19", "stone_fullslit_19", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_20 = new BlockRotatedModelMeta("stone_fullslit_20", "stone_fullslit_20", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_21 = new BlockRotatedModelMeta("stone_fullslit_21", "stone_fullslit_21", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_22 = new BlockRotatedModelMeta("stone_fullslit_22", "stone_fullslit_22", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_23 = new BlockRotatedModelMeta("stone_fullslit_23", "stone_fullslit_23", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fullslit_24 = new BlockRotatedModelMeta("stone_fullslit_24", "stone_fullslit_24", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Log Stone Blocks
		stone_log_1 = new BlockRotatedModelMeta("stone_log_1", "stone_log_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_log_2 = new BlockRotatedModelMeta("stone_log_2", "stone_log_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Wall Stone Blocks
		stone_wall_1 = new BlockWallMeta("stone_wall_1", "stone_wall_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_wall_2 = new BlockWallMeta("stone_wall_2", "stone_wall_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_wall_3 = new BlockWallMeta("stone_wall_3", "stone_wall_3", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_wall_4 = new BlockWallMeta("stone_wall_4", "stone_wall_4", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_wall_5 = new BlockWallMeta("stone_wall_5", "stone_wall_5", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_wall_6 = new BlockWallMeta("stone_wall_6", "stone_wall_6", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_wall_7 = new BlockWallMeta("stone_wall_7", "stone_wall_7", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_wall_8 = new BlockWallMeta("stone_wall_8", "stone_wall_8", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_wall_9 = new BlockWallMeta("stone_wall_9", "stone_wall_9", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Pillar Stone Blocks
		stone_pillar_1 = new BlockPillarMeta("stone_pillar_1", "stone_pillar_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_pillar_2 = new BlockPillarMeta("stone_pillar_2", "stone_pillar_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_pillar_3 = new BlockPillarMeta("stone_pillar_3", "stone_pillar_3", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_pillar_4 = new BlockPillarMeta("stone_pillar_4", "stone_pillar_4", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_pillar_5 = new BlockPillarMeta("stone_pillar_5", "stone_pillar_5", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_pillar_6 = new BlockPillarMeta("stone_pillar_6", "stone_pillar_6", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_pillar_7 = new BlockPillarMeta("stone_pillar_7", "stone_pillar_7", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_pillar_8 = new BlockPillarMeta("stone_pillar_8", "stone_pillar_8", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_pillar_9 = new BlockPillarMeta("stone_pillar_9", "stone_pillar_9", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Anvil Stone Blocks
		stone_anvil_1 = new BlockRotatedModelMeta("stone_anvil_1", "stone_anvil_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_2 = new BlockRotatedModelMeta("stone_anvil_2", "stone_anvil_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_3 = new BlockRotatedModelMeta("stone_anvil_3", "stone_anvil_3", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_4 = new BlockRotatedModelMeta("stone_anvil_4", "stone_anvil_4", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_5 = new BlockRotatedModelMeta("stone_anvil_5", "stone_anvil_5", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_6 = new BlockRotatedModelMeta("stone_anvil_6", "stone_anvil_6", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_7 = new BlockRotatedModelMeta("stone_anvil_7", "stone_anvil_7", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_8 = new BlockRotatedModelMeta("stone_anvil_8", "stone_anvil_8", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_9 = new BlockRotatedModelMeta("stone_anvil_9", "stone_anvil_9", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_10 = new BlockRotatedModelMeta("stone_anvil_10", "stone_anvil_10", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_11 = new BlockRotatedModelMeta("stone_anvil_11", "stone_anvil_11", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_12 = new BlockRotatedModelMeta("stone_anvil_12", "stone_anvil_12", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_13 = new BlockRotatedModelMeta("stone_anvil_13", "stone_anvil_13", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_14 = new BlockRotatedModelMeta("stone_anvil_14", "stone_anvil_14", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_15 = new BlockRotatedModelMeta("stone_anvil_15", "stone_anvil_15", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_16 = new BlockRotatedModelMeta("stone_anvil_16", "stone_anvil_16", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_17 = new BlockRotatedModelMeta("stone_anvil_17", "stone_anvil_17", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_18 = new BlockRotatedModelMeta("stone_anvil_18", "stone_anvil_18", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_19 = new BlockRotatedModelMeta("stone_anvil_19", "stone_anvil_19", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_20 = new BlockRotatedModelMeta("stone_anvil_20", "stone_anvil_20", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_21 = new BlockRotatedModelMeta("stone_anvil_21", "stone_anvil_21", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_22 = new BlockRotatedModelMeta("stone_anvil_22", "stone_anvil_22", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_23 = new BlockRotatedModelMeta("stone_anvil_23", "stone_anvil_23", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_24 = new BlockRotatedModelMeta("stone_anvil_24", "stone_anvil_24", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_25 = new BlockRotatedModelMeta("stone_anvil_25", "stone_anvil_25", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_26 = new BlockRotatedModelMeta("stone_anvil_26", "stone_anvil_26", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_27 = new BlockRotatedModelMeta("stone_anvil_27", "stone_anvil_27", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_anvil_28 = new BlockRotatedModelMeta("stone_anvil_28", "stone_anvil_28", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Hopper Full Stone Blocks
		stone_hopperfull_1 = new BlockPartialBasicMeta("stone_hopperfull_1", "stone_hopperfull_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperfull_2 = new BlockPartialBasicMeta("stone_hopperfull_2", "stone_hopperfull_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperfull_3 = new BlockPartialBasicMeta("stone_hopperfull_3", "stone_hopperfull_3", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperfull_4 = new BlockPartialBasicMeta("stone_hopperfull_4", "stone_hopperfull_4", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperfull_5 = new BlockPartialBasicMeta("stone_hopperfull_5", "stone_hopperfull_5", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperfull_6 = new BlockPartialBasicMeta("stone_hopperfull_6", "stone_hopperfull_6", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperfull_7 = new BlockPartialBasicMeta("stone_hopperfull_7", "stone_hopperfull_7", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Hopper Directional Stone Blocks
		stone_hopperdirectional_1 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_1", "stone_hopperdirectional_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_2 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_2", "stone_hopperdirectional_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_3 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_3", "stone_hopperdirectional_3", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_4 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_4", "stone_hopperdirectional_4", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_5 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_5", "stone_hopperdirectional_5", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_6 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_6", "stone_hopperdirectional_6", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_7 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_7", "stone_hopperdirectional_7", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_8 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_8", "stone_hopperdirectional_8", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_9 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_9", "stone_hopperdirectional_9", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_10 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_10", "stone_hopperdirectional_10", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_11 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_11", "stone_hopperdirectional_11", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_12 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_12", "stone_hopperdirectional_12", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_13 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_13", "stone_hopperdirectional_13", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_14 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_14", "stone_hopperdirectional_14", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_15 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_15", "stone_hopperdirectional_15", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_16 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_16", "stone_hopperdirectional_16", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_17 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_17", "stone_hopperdirectional_17", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_18 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_18", "stone_hopperdirectional_18", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_19 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_19", "stone_hopperdirectional_19", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_20 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_20", "stone_hopperdirectional_20", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_21 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_21", "stone_hopperdirectional_21", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_22 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_22", "stone_hopperdirectional_22", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_23 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_23", "stone_hopperdirectional_23", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_24 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_24", "stone_hopperdirectional_24", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_hopperdirectional_25 = new BlockDirectionalFullPartialMeta("stone_hopperdirectional_25", "stone_hopperdirectional_25", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Fence Stone Blocks
		stone_fence_1 = new BlockFenceMeta("stone_fence_1", "stone_fence_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_fence_2 = new BlockFenceMeta("stone_fence_2", "stone_fence_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Fencegate Stone Blocks
		stone_fencegate_1 = new BlockFenceGateMeta("stone_fencegate_1", "stone_fencegate_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Small Pillar Stone Blocks
		stone_smallpillar_1 = new BlockSmallPillarMeta("stone_smallpillar_1", "stone_smallpillar_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_smallpillar_2 = new BlockSmallPillarMeta("stone_smallpillar_2", "stone_smallpillar_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_smallpillar_3 = new BlockSmallPillarMeta("stone_smallpillar_3", "stone_smallpillar_3", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_smallpillar_4 = new BlockSmallPillarMeta("stone_smallpillar_4", "stone_smallpillar_4", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_smallpillar_5 = new BlockSmallPillarMeta("stone_smallpillar_5", "stone_smallpillar_5", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_smallpillar_6 = new BlockSmallPillarMeta("stone_smallpillar_6", "stone_smallpillar_6", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_smallpillar_7 = new BlockSmallPillarMeta("stone_smallpillar_7", "stone_smallpillar_7", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_smallpillar_8 = new BlockSmallPillarMeta("stone_smallpillar_8", "stone_smallpillar_8", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_smallpillar_9 = new BlockSmallPillarMeta("stone_smallpillar_9", "stone_smallpillar_9", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_smallpillar_10 = new BlockSmallPillarMeta("stone_smallpillar_10", "stone_smallpillar_10", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Stairs Stone Blocks
		stone_stairs_1 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_1", "stone_stairs_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_2 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_2", "stone_stairs_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_3 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_3", "stone_stairs_3", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_4 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_4", "stone_stairs_4", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_5 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_5", "stone_stairs_5", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_6 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_6", "stone_stairs_6", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_7 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_7", "stone_stairs_7", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_8 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_8", "stone_stairs_8", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_9 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_9", "stone_stairs_9", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_10 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_10", "stone_stairs_10", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_11 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_11", "stone_stairs_11", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_12 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_12", "stone_stairs_12", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_13 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_13", "stone_stairs_13", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_14 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_14", "stone_stairs_14", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_15 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_15", "stone_stairs_15", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_16 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_16", "stone_stairs_16", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_17 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_17", "stone_stairs_17", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_18 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_18", "stone_stairs_18", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_19 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_19", "stone_stairs_19", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_20 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_20", "stone_stairs_20", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_21 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_21", "stone_stairs_21", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_22 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_22", "stone_stairs_22", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_23 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_23", "stone_stairs_23", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_24 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_24", "stone_stairs_24", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_25 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_25", "stone_stairs_25", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_26 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_26", "stone_stairs_26", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_27 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_27", "stone_stairs_27", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_28 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_28", "stone_stairs_28", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_29 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_29", "stone_stairs_29", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_30 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_30", "stone_stairs_30", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_31 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_31", "stone_stairs_31", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_32 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_32", "stone_stairs_32", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_33 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_33", "stone_stairs_33", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_34 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_34", "stone_stairs_34", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_35 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_35", "stone_stairs_35", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_36 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_36", "stone_stairs_36", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_37 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_37", "stone_stairs_37", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_38 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_38", "stone_stairs_38", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_39 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_39", "stone_stairs_39", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_40 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_40", "stone_stairs_40", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_41 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_41", "stone_stairs_41", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_42 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_42", "stone_stairs_42", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_43 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_43", "stone_stairs_43", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_44 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_44", "stone_stairs_44", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_45 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_45", "stone_stairs_45", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_46 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_46", "stone_stairs_46", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_47 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_47", "stone_stairs_47", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_48 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_48", "stone_stairs_48", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_49 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_49", "stone_stairs_49", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_50 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_50", "stone_stairs_50", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_51 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_51", "stone_stairs_51", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_52 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_52", "stone_stairs_52", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_53 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_53", "stone_stairs_53", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_54 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_54", "stone_stairs_54", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_55 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_55", "stone_stairs_55", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_56 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_56", "stone_stairs_56", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_57 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_57", "stone_stairs_57", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_58 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_58", "stone_stairs_58", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_59 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_59", "stone_stairs_59", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_60 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_60", "stone_stairs_60", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_61 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_61", "stone_stairs_61", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_62 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_62", "stone_stairs_62", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_63 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_63", "stone_stairs_63", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_64 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_64", "stone_stairs_64", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_65 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_65", "stone_stairs_65", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_66 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_66", "stone_stairs_66", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_67 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_67", "stone_stairs_67", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_68 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_68", "stone_stairs_68", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_69 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_69", "stone_stairs_69", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_70 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_70", "stone_stairs_70", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_71 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_71", "stone_stairs_71", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_72 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_72", "stone_stairs_72", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_73 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_73", "stone_stairs_73", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_74 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_74", "stone_stairs_74", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_75 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_75", "stone_stairs_75", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_76 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_76", "stone_stairs_76", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_77 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_77", "stone_stairs_77", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_78 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_78", "stone_stairs_78", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_79 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_79", "stone_stairs_79", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_80 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_80", "stone_stairs_80", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_81 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_81", "stone_stairs_81", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_82 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_82", "stone_stairs_82", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_83 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_83", "stone_stairs_83", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_84 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_84", "stone_stairs_84", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_85 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_85", "stone_stairs_85", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_86 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_86", "stone_stairs_86", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_87 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_87", "stone_stairs_87", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_88 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_88", "stone_stairs_88", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_89 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_89", "stone_stairs_89", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_90 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_90", "stone_stairs_90", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_91 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_91", "stone_stairs_91", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_92 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_92", "stone_stairs_92", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_93 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_93", "stone_stairs_93", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_94 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_94", "stone_stairs_94", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_95 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_95", "stone_stairs_95", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_96 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_96", "stone_stairs_96", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_97 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_97", "stone_stairs_97", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_98 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_98", "stone_stairs_98", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_99 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_99", "stone_stairs_99", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_100 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_100", "stone_stairs_100", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_101 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_101", "stone_stairs_101", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_102 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_102", "stone_stairs_102", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_103 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_103", "stone_stairs_103", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_104 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_104", "stone_stairs_104", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_105 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_105", "stone_stairs_105", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_106 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_106", "stone_stairs_106", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_107 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_107", "stone_stairs_107", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_108 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_108", "stone_stairs_108", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_stairs_109 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "stone_stairs_109", "stone_stairs_109", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Slab Stone Blocks
		stone_slab_1 = new BlockSlabMeta("stone_slab_1", "stone_slab_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_2 = new BlockSlabMeta("stone_slab_2", "stone_slab_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_3 = new BlockSlabMeta("stone_slab_3", "stone_slab_3", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_4 = new BlockSlabMeta("stone_slab_4", "stone_slab_4", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_5 = new BlockSlabMeta("stone_slab_5", "stone_slab_5", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_6 = new BlockSlabMeta("stone_slab_6", "stone_slab_6", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_7 = new BlockSlabMeta("stone_slab_7", "stone_slab_7", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_8 = new BlockSlabMeta("stone_slab_8", "stone_slab_8", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_9 = new BlockSlabMeta("stone_slab_9", "stone_slab_9", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_10 = new BlockSlabMeta("stone_slab_10", "stone_slab_10", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_11 = new BlockSlabMeta("stone_slab_11", "stone_slab_11", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_12 = new BlockSlabMeta("stone_slab_12", "stone_slab_12", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_13 = new BlockSlabMeta("stone_slab_13", "stone_slab_13", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_14 = new BlockSlabMeta("stone_slab_14", "stone_slab_14", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_15 = new BlockSlabMeta("stone_slab_15", "stone_slab_15", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_16 = new BlockSlabMeta("stone_slab_16", "stone_slab_16", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_17 = new BlockSlabMeta("stone_slab_17", "stone_slab_17", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_18 = new BlockSlabMeta("stone_slab_18", "stone_slab_18", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_19 = new BlockSlabMeta("stone_slab_19", "stone_slab_19", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_slab_20 = new BlockSlabMeta("stone_slab_20", "stone_slab_20", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Stone Trapdoor Blocks
		stone_trapdoormodel_1 = new BlockTrapDoorMeta("stone_trapdoormodel_1", "stone_trapdoormodel_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_2 = new BlockTrapDoorMeta("stone_trapdoormodel_2", "stone_trapdoormodel_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_3 = new BlockTrapDoorMeta("stone_trapdoormodel_3", "stone_trapdoormodel_3", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_4 = new BlockTrapDoorMeta("stone_trapdoormodel_4", "stone_trapdoormodel_4", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_5 = new BlockTrapDoorMeta("stone_trapdoormodel_5", "stone_trapdoormodel_5", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_6 = new BlockTrapDoorMeta("stone_trapdoormodel_6", "stone_trapdoormodel_6", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_7 = new BlockTrapDoorMeta("stone_trapdoormodel_7", "stone_trapdoormodel_7", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_8 = new BlockTrapDoorMeta("stone_trapdoormodel_8", "stone_trapdoormodel_8", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_9 = new BlockTrapDoorMeta("stone_trapdoormodel_9", "stone_trapdoormodel_9", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_10 = new BlockTrapDoorMeta("stone_trapdoormodel_10", "stone_trapdoormodel_10", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_11 = new BlockTrapDoorMeta("stone_trapdoormodel_11", "stone_trapdoormodel_11", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_12 = new BlockTrapDoorMeta("stone_trapdoormodel_12", "stone_trapdoormodel_12", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_13 = new BlockTrapDoorMeta("stone_trapdoormodel_13", "stone_trapdoormodel_13", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_14 = new BlockTrapDoorMeta("stone_trapdoormodel_14", "stone_trapdoormodel_14", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_15 = new BlockTrapDoorMeta("stone_trapdoormodel_15", "stone_trapdoormodel_15", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_16 = new BlockTrapDoorMeta("stone_trapdoormodel_16", "stone_trapdoormodel_16", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_17 = new BlockTrapDoorMeta("stone_trapdoormodel_17", "stone_trapdoormodel_17", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_18 = new BlockTrapDoorMeta("stone_trapdoormodel_18", "stone_trapdoormodel_18", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_19 = new BlockTrapDoorMeta("stone_trapdoormodel_19", "stone_trapdoormodel_19", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_20 = new BlockTrapDoorMeta("stone_trapdoormodel_20", "stone_trapdoormodel_20", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_21 = new BlockTrapDoorMeta("stone_trapdoormodel_21", "stone_trapdoormodel_21", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_22 = new BlockTrapDoorMeta("stone_trapdoormodel_22", "stone_trapdoormodel_22", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_23 = new BlockTrapDoorMeta("stone_trapdoormodel_23", "stone_trapdoormodel_23", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_24 = new BlockTrapDoorMeta("stone_trapdoormodel_24", "stone_trapdoormodel_24", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_25 = new BlockTrapDoorMeta("stone_trapdoormodel_25", "stone_trapdoormodel_25", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_26 = new BlockTrapDoorMeta("stone_trapdoormodel_26", "stone_trapdoormodel_26", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_27 = new BlockTrapDoorMeta("stone_trapdoormodel_27", "stone_trapdoormodel_27", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_28 = new BlockTrapDoorMeta("stone_trapdoormodel_28", "stone_trapdoormodel_28", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_29 = new BlockTrapDoorMeta("stone_trapdoormodel_29", "stone_trapdoormodel_29", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_30 = new BlockTrapDoorMeta("stone_trapdoormodel_30", "stone_trapdoormodel_30", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_31 = new BlockTrapDoorMeta("stone_trapdoormodel_31", "stone_trapdoormodel_31", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_32 = new BlockTrapDoorMeta("stone_trapdoormodel_32", "stone_trapdoormodel_32", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_33 = new BlockTrapDoorMeta("stone_trapdoormodel_33", "stone_trapdoormodel_33", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_34 = new BlockTrapDoorMeta("stone_trapdoormodel_34", "stone_trapdoormodel_34", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_35 = new BlockTrapDoorMeta("stone_trapdoormodel_35", "stone_trapdoormodel_35", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_36 = new BlockTrapDoorMeta("stone_trapdoormodel_36", "stone_trapdoormodel_36", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_37 = new BlockTrapDoorMeta("stone_trapdoormodel_37", "stone_trapdoormodel_37", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_38 = new BlockTrapDoorMeta("stone_trapdoormodel_38", "stone_trapdoormodel_38", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_39 = new BlockTrapDoorMeta("stone_trapdoormodel_39", "stone_trapdoormodel_39", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_40 = new BlockTrapDoorMeta("stone_trapdoormodel_40", "stone_trapdoormodel_40", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_41 = new BlockTrapDoorMeta("stone_trapdoormodel_41", "stone_trapdoormodel_41", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_42 = new BlockTrapDoorMeta("stone_trapdoormodel_42", "stone_trapdoormodel_42", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_43 = new BlockTrapDoorMeta("stone_trapdoormodel_43", "stone_trapdoormodel_43", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_44 = new BlockTrapDoorMeta("stone_trapdoormodel_44", "stone_trapdoormodel_44", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_45 = new BlockTrapDoorMeta("stone_trapdoormodel_45", "stone_trapdoormodel_45", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_46 = new BlockTrapDoorMeta("stone_trapdoormodel_46", "stone_trapdoormodel_46", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_47 = new BlockTrapDoorMeta("stone_trapdoormodel_47", "stone_trapdoormodel_47", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_48 = new BlockTrapDoorMeta("stone_trapdoormodel_48", "stone_trapdoormodel_48", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_49 = new BlockTrapDoorMeta("stone_trapdoormodel_49", "stone_trapdoormodel_49", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_50 = new BlockTrapDoorMeta("stone_trapdoormodel_50", "stone_trapdoormodel_50", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_51 = new BlockTrapDoorMeta("stone_trapdoormodel_51", "stone_trapdoormodel_51", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_52 = new BlockTrapDoorMeta("stone_trapdoormodel_52", "stone_trapdoormodel_52", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_53 = new BlockTrapDoorMeta("stone_trapdoormodel_53", "stone_trapdoormodel_53", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_54 = new BlockTrapDoorMeta("stone_trapdoormodel_54", "stone_trapdoormodel_54", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_55 = new BlockTrapDoorMeta("stone_trapdoormodel_55", "stone_trapdoormodel_55", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_56 = new BlockTrapDoorMeta("stone_trapdoormodel_56", "stone_trapdoormodel_56", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_57 = new BlockTrapDoorMeta("stone_trapdoormodel_57", "stone_trapdoormodel_57", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_58 = new BlockTrapDoorMeta("stone_trapdoormodel_58", "stone_trapdoormodel_58", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_59 = new BlockTrapDoorMeta("stone_trapdoormodel_59", "stone_trapdoormodel_59", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_60 = new BlockTrapDoorMeta("stone_trapdoormodel_60", "stone_trapdoormodel_60", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_61 = new BlockTrapDoorMeta("stone_trapdoormodel_61", "stone_trapdoormodel_61", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_62 = new BlockTrapDoorMeta("stone_trapdoormodel_62", "stone_trapdoormodel_62", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_63 = new BlockTrapDoorMeta("stone_trapdoormodel_63", "stone_trapdoormodel_63", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_64 = new BlockTrapDoorMeta("stone_trapdoormodel_64", "stone_trapdoormodel_64", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_65 = new BlockTrapDoorMeta("stone_trapdoormodel_65", "stone_trapdoormodel_65", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_66 = new BlockTrapDoorMeta("stone_trapdoormodel_66", "stone_trapdoormodel_66", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_67 = new BlockTrapDoorMeta("stone_trapdoormodel_67", "stone_trapdoormodel_67", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_68 = new BlockTrapDoorMeta("stone_trapdoormodel_68", "stone_trapdoormodel_68", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_69 = new BlockTrapDoorMeta("stone_trapdoormodel_69", "stone_trapdoormodel_69", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_70 = new BlockTrapDoorMeta("stone_trapdoormodel_70", "stone_trapdoormodel_70", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_71 = new BlockTrapDoorMeta("stone_trapdoormodel_71", "stone_trapdoormodel_71", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_72 = new BlockTrapDoorMeta("stone_trapdoormodel_72", "stone_trapdoormodel_72", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_73 = new BlockTrapDoorMeta("stone_trapdoormodel_73", "stone_trapdoormodel_73", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_74 = new BlockTrapDoorMeta("stone_trapdoormodel_74", "stone_trapdoormodel_74", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_75 = new BlockTrapDoorMeta("stone_trapdoormodel_75", "stone_trapdoormodel_75", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_76 = new BlockTrapDoorMeta("stone_trapdoormodel_76", "stone_trapdoormodel_76", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_77 = new BlockTrapDoorMeta("stone_trapdoormodel_77", "stone_trapdoormodel_77", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_78 = new BlockTrapDoorMeta("stone_trapdoormodel_78", "stone_trapdoormodel_78", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_79 = new BlockTrapDoorMeta("stone_trapdoormodel_79", "stone_trapdoormodel_79", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_80 = new BlockTrapDoorMeta("stone_trapdoormodel_80", "stone_trapdoormodel_80", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_81 = new BlockTrapDoorMeta("stone_trapdoormodel_81", "stone_trapdoormodel_81", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_82 = new BlockTrapDoorMeta("stone_trapdoormodel_82", "stone_trapdoormodel_82", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_83 = new BlockTrapDoorMeta("stone_trapdoormodel_83", "stone_trapdoormodel_83", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_84 = new BlockTrapDoorMeta("stone_trapdoormodel_84", "stone_trapdoormodel_84", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_85 = new BlockTrapDoorMeta("stone_trapdoormodel_85", "stone_trapdoormodel_85", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_86 = new BlockTrapDoorMeta("stone_trapdoormodel_86", "stone_trapdoormodel_86", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_87 = new BlockTrapDoorMeta("stone_trapdoormodel_87", "stone_trapdoormodel_87", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_88 = new BlockTrapDoorMeta("stone_trapdoormodel_88", "stone_trapdoormodel_88", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_89 = new BlockTrapDoorMeta("stone_trapdoormodel_89", "stone_trapdoormodel_89", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_90 = new BlockTrapDoorMeta("stone_trapdoormodel_90", "stone_trapdoormodel_90", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_91 = new BlockTrapDoorMeta("stone_trapdoormodel_91", "stone_trapdoormodel_91", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_92 = new BlockTrapDoorMeta("stone_trapdoormodel_92", "stone_trapdoormodel_92", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_93 = new BlockTrapDoorMeta("stone_trapdoormodel_93", "stone_trapdoormodel_93", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_94 = new BlockTrapDoorMeta("stone_trapdoormodel_94", "stone_trapdoormodel_94", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_95 = new BlockTrapDoorMeta("stone_trapdoormodel_95", "stone_trapdoormodel_95", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_96 = new BlockTrapDoorMeta("stone_trapdoormodel_96", "stone_trapdoormodel_96", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_97 = new BlockTrapDoorMeta("stone_trapdoormodel_97", "stone_trapdoormodel_97", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_98 = new BlockTrapDoorMeta("stone_trapdoormodel_98", "stone_trapdoormodel_98", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_99 = new BlockTrapDoorMeta("stone_trapdoormodel_99", "stone_trapdoormodel_99", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_100 = new BlockTrapDoorMeta("stone_trapdoormodel_100", "stone_trapdoormodel_100", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_101 = new BlockTrapDoorMeta("stone_trapdoormodel_101", "stone_trapdoormodel_101", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_102 = new BlockTrapDoorMeta("stone_trapdoormodel_102", "stone_trapdoormodel_102", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_103 = new BlockTrapDoorMeta("stone_trapdoormodel_103", "stone_trapdoormodel_103", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_104 = new BlockTrapDoorMeta("stone_trapdoormodel_104", "stone_trapdoormodel_104", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_105 = new BlockTrapDoorMeta("stone_trapdoormodel_105", "stone_trapdoormodel_105", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_106 = new BlockTrapDoorMeta("stone_trapdoormodel_106", "stone_trapdoormodel_106", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_107 = new BlockTrapDoorMeta("stone_trapdoormodel_107", "stone_trapdoormodel_107", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_108 = new BlockTrapDoorMeta("stone_trapdoormodel_108", "stone_trapdoormodel_108", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_109 = new BlockTrapDoorMeta("stone_trapdoormodel_109", "stone_trapdoormodel_109", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_110 = new BlockTrapDoorMeta("stone_trapdoormodel_110", "stone_trapdoormodel_110", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_111 = new BlockTrapDoorMeta("stone_trapdoormodel_111", "stone_trapdoormodel_111", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_112 = new BlockTrapDoorMeta("stone_trapdoormodel_112", "stone_trapdoormodel_112", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_113 = new BlockTrapDoorMeta("stone_trapdoormodel_113", "stone_trapdoormodel_113", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_114 = new BlockTrapDoorMeta("stone_trapdoormodel_114", "stone_trapdoormodel_114", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_115 = new BlockTrapDoorMeta("stone_trapdoormodel_115", "stone_trapdoormodel_115", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_116 = new BlockTrapDoorMeta("stone_trapdoormodel_116", "stone_trapdoormodel_116", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_117 = new BlockTrapDoorMeta("stone_trapdoormodel_117", "stone_trapdoormodel_117", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_118 = new BlockTrapDoorMeta("stone_trapdoormodel_118", "stone_trapdoormodel_118", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_119 = new BlockTrapDoorMeta("stone_trapdoormodel_119", "stone_trapdoormodel_119", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_120 = new BlockTrapDoorMeta("stone_trapdoormodel_120", "stone_trapdoormodel_120", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_121 = new BlockTrapDoorMeta("stone_trapdoormodel_121", "stone_trapdoormodel_121", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_122 = new BlockTrapDoorMeta("stone_trapdoormodel_122", "stone_trapdoormodel_122", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_123 = new BlockTrapDoorMeta("stone_trapdoormodel_123", "stone_trapdoormodel_123", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_124 = new BlockTrapDoorMeta("stone_trapdoormodel_124", "stone_trapdoormodel_124", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_125 = new BlockTrapDoorMeta("stone_trapdoormodel_125", "stone_trapdoormodel_125", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_126 = new BlockTrapDoorMeta("stone_trapdoormodel_126", "stone_trapdoormodel_126", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_127 = new BlockTrapDoorMeta("stone_trapdoormodel_127", "stone_trapdoormodel_127", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_128 = new BlockTrapDoorMeta("stone_trapdoormodel_128", "stone_trapdoormodel_128", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_129 = new BlockTrapDoorMeta("stone_trapdoormodel_129", "stone_trapdoormodel_129", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_130 = new BlockTrapDoorMeta("stone_trapdoormodel_130", "stone_trapdoormodel_130", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_131 = new BlockTrapDoorMeta("stone_trapdoormodel_131", "stone_trapdoormodel_131", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_132 = new BlockTrapDoorMeta("stone_trapdoormodel_132", "stone_trapdoormodel_132", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_133 = new BlockTrapDoorMeta("stone_trapdoormodel_133", "stone_trapdoormodel_133", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_134 = new BlockTrapDoorMeta("stone_trapdoormodel_134", "stone_trapdoormodel_134", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_135 = new BlockTrapDoorMeta("stone_trapdoormodel_135", "stone_trapdoormodel_135", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_136 = new BlockTrapDoorMeta("stone_trapdoormodel_136", "stone_trapdoormodel_136", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_137 = new BlockTrapDoorMeta("stone_trapdoormodel_137", "stone_trapdoormodel_137", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_138 = new BlockTrapDoorMeta("stone_trapdoormodel_138", "stone_trapdoormodel_138", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_139 = new BlockTrapDoorMeta("stone_trapdoormodel_139", "stone_trapdoormodel_139", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_140 = new BlockTrapDoorMeta("stone_trapdoormodel_140", "stone_trapdoormodel_140", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_141 = new BlockTrapDoorMeta("stone_trapdoormodel_141", "stone_trapdoormodel_141", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_142 = new BlockTrapDoorMeta("stone_trapdoormodel_142", "stone_trapdoormodel_142", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_143 = new BlockTrapDoorMeta("stone_trapdoormodel_143", "stone_trapdoormodel_143", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_trapdoormodel_144 = new BlockTrapDoorMeta("stone_trapdoormodel_144", "stone_trapdoormodel_144", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Stone Carpet Blocks
		stone_carpet_1 = new BlockCarpetMeta("stone_carpet_1", "stone_carpet_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_carpet_2 = new BlockCarpetMeta("stone_carpet_2", "stone_carpet_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_carpet_3 = new BlockCarpetMeta("stone_carpet_3", "stone_carpet_3", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_carpet_4 = new BlockCarpetMeta("stone_carpet_4", "stone_carpet_4", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Stone Ladder Blocks
		climbingrocks = new LadderBlock("climbingrocks", "climbingrocks", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Stone Layer Blocks
		stone_layer_1 = new BlockLayerMeta("stone_layer_1", "stone_layer_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, CreativeTabs.SEARCH);
		stone_layer_2 = new BlockLayerMeta("stone_layer_2", "stone_layer_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, CreativeTabs.SEARCH);
		stone_layer_3 = new BlockLayerMeta("stone_layer_3", "stone_layer_3", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, CreativeTabs.SEARCH);
		stone_layer_4 = new BlockLayerMeta("stone_layer_4", "stone_layer_4", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, CreativeTabs.SEARCH);
		stone_layer_5 = new BlockLayerMeta("stone_layer_5", "stone_layer_5", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, CreativeTabs.SEARCH);
		stone_layer_6 = new BlockLayerMeta("stone_layer_6", "stone_layer_6", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, CreativeTabs.SEARCH);
		stone_layer_7 = new BlockLayerMeta("stone_layer_7", "stone_layer_7", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, CreativeTabs.SEARCH);
		stone_layer_8 = new BlockLayerMeta("stone_layer_8", "stone_layer_8", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, CreativeTabs.SEARCH);
	//End Frame Stone Blocks
		base_marblesandstone_endframe = new EndportalBlock("base_marblesandstone_endframe", "base_marblesandstone_endframe", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		urn_terracotta_endframe = new EndportalBlock("urn_terracotta_endframe", "urn_terracotta_endframe", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//No Collision Stone Blocks
		stone_directionalnocollision_1 = new BlockDirectionalNoCollisionMeta("stone_directionalnocollision_1", "stone_directionalnocollision_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//No Collision Stone Blocks
		stone_nocollision_1 = new BlockNoCollisionMeta("stone_nocollision_1", "stone_nocollision_1", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_nocollision_2 = new BlockNoCollisionMeta("stone_nocollision_2", "stone_nocollision_2", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Full Wood Blocks
		wood_full_1 = new BlockSimpleMeta("wood_full_1", "wood_full_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_full_2 = new BlockSimpleMeta("wood_full_2", "wood_full_2", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_full_3 = new BlockSimpleMeta("wood_full_3", "wood_full_3", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_full_4 = new BlockSimpleMeta("wood_full_4", "wood_full_4", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_full_5 = new BlockSimpleMeta("wood_full_5", "wood_full_5", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_full_6 = new BlockSimpleMeta("wood_full_6", "wood_full_6", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_full_7 = new BlockSimpleMeta("wood_full_7", "wood_full_7", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Full Slit Wood Blocks
		wood_fullslit_1 = new BlockRotatedModelMeta("wood_fullslit_1", "wood_fullslit_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fullslit_2 = new BlockRotatedModelMeta("wood_fullslit_2", "wood_fullslit_2", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fullslit_3 = new BlockRotatedModelMeta("wood_fullslit_3", "wood_fullslit_3", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fullslit_4 = new BlockRotatedModelMeta("wood_fullslit_4", "wood_fullslit_4", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fullslit_5 = new BlockRotatedModelMeta("wood_fullslit_5", "wood_fullslit_5", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fullslit_6 = new BlockRotatedModelMeta("wood_fullslit_6", "wood_fullslit_6", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fullslit_7 = new BlockRotatedModelMeta("wood_fullslit_7", "wood_fullslit_7", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);

	//Wood Log Blocks
		wood_log_1 = new BlockRotatedModelMeta("wood_log_1", "wood_log_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_log_2 = new BlockRotatedModelMeta("wood_log_2", "wood_log_2", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_log_3 = new BlockRotatedModelMeta("wood_log_3", "wood_log_3", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_log_4 = new BlockRotatedModelMeta("wood_log_4", "wood_log_4", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_log_5 = new BlockRotatedModelMeta("wood_log_5", "wood_log_5", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_log_6 = new BlockRotatedModelMeta("wood_log_6", "wood_log_6", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);

	//Wood Anvil Blocks
		wood_anvil_1 = new BlockRotatedModelMeta("wood_anvil_1", "wood_anvil_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_anvil_2 = new BlockRotatedModelMeta("wood_anvil_2", "wood_anvil_2", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_anvil_3 = new BlockRotatedModelMeta("wood_anvil_3", "wood_anvil_3", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_anvil_4 = new BlockRotatedModelMeta("wood_anvil_4", "wood_anvil_4", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);

	//Wood Wall Blocks
		wood_wall_1 = new BlockWallMeta("wood_wall_1", "wood_wall_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_wall_2 = new BlockWallMeta("wood_wall_2", "wood_wall_2", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_wall_3 = new BlockWallMeta("wood_wall_3", "wood_wall_3", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_wall_4 = new BlockWallMeta("wood_wall_4", "wood_wall_4", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);

	//Wood Pillar Blocks
		wood_pillar_1 = new BlockPillarMeta("wood_pillar_1", "wood_pillar_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_pillar_2 = new BlockPillarMeta("wood_pillar_2", "wood_pillar_2", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_pillar_3 = new BlockPillarMeta("wood_pillar_3", "wood_pillar_3", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_pillar_4 = new BlockPillarMeta("wood_pillar_4", "wood_pillar_4", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);

	//Fence Wood Blocks
		wood_fence_1 = new BlockFenceMeta("wood_fence_1", "wood_fence_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fence_2 = new BlockFenceMeta("wood_fence_2", "wood_fence_2", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fence_3 = new BlockFenceMeta("wood_fence_3", "wood_fence_3", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);

	//Fencegate Wood Blocks
		wood_fencegate_1 = new BlockFenceGateMeta("wood_fencegate_1", "wood_fencegate_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fencegate_2 = new BlockFenceGateMeta("wood_fencegate_2", "wood_fencegate_2", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fencegate_3 = new BlockFenceGateMeta("wood_fencegate_3", "wood_fencegate_3", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fencegate_4 = new BlockFenceGateMeta("wood_fencegate_4", "wood_fencegate_4", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fencegate_5 = new BlockFenceGateMeta("wood_fencegate_5", "wood_fencegate_5", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fencegate_6 = new BlockFenceGateMeta("wood_fencegate_6", "wood_fencegate_6", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fencegate_7 = new BlockFenceGateMeta("wood_fencegate_7", "wood_fencegate_7", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fencegate_8 = new BlockFenceGateMeta("wood_fencegate_8", "wood_fencegate_8", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fencegate_9 = new BlockFenceGateMeta("wood_fencegate_9", "wood_fencegate_9", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fencegate_10 = new BlockFenceGateMeta("wood_fencegate_10", "wood_fencegate_10", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fencegate_11 = new BlockFenceGateMeta("wood_fencegate_11", "wood_fencegate_11", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fencegate_12 = new BlockFenceGateMeta("wood_fencegate_12", "wood_fencegate_12", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);

	//Fencegate Wood Blocks
		planks_whiteweathered_door = new DoorBlock("planks_whiteweathered_door", "planks_whiteweathered_door", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		planks_redweathered_door = new DoorBlock("planks_redweathered_door", "planks_redweathered_door", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		planks_lightredweathered_door = new DoorBlock("planks_lightredweathered_door", "planks_lightredweathered_door", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		planks_orangepainted_door = new DoorBlock("planks_orangepainted_door", "planks_orangepainted_door", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		planks_yellowweathered_door = new DoorBlock("planks_yellowweathered_door", "planks_yellowweathered_door", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		planks_greenweathered_door = new DoorBlock("planks_greenweathered_door", "planks_greenweathered_door", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		planks_limeweathered_door = new DoorBlock("planks_limeweathered_door", "planks_limeweathered_door", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		planks_cyanweathered_door = new DoorBlock("planks_cyanweathered_door", "planks_cyanweathered_door", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		planks_darkblueweathered_door = new DoorBlock("planks_darkblueweathered_door", "planks_darkblueweathered_door", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		planks_blueweathered_door = new DoorBlock("planks_blueweathered_door", "planks_blueweathered_door", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		planks_lightblueweathered_door = new DoorBlock("planks_lightblueweathered_door", "planks_lightblueweathered_door", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		planks_purplepainted_door = new DoorBlock("planks_purplepainted_door", "planks_purplepainted_door", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		planks_brownweathered_door = new DoorBlock("planks_brownweathered_door", "planks_brownweathered_door", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);


	//Small Pillar Wood Blocks
		wood_smallpillar_1 = new BlockSmallPillarMeta("wood_smallpillar_1", "wood_smallpillar_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_smallpillar_2 = new BlockSmallPillarMeta("wood_smallpillar_2", "wood_smallpillar_2", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_smallpillar_3 = new BlockSmallPillarMeta("wood_smallpillar_3", "wood_smallpillar_3", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);

	//Full Partial Wood Blocks
		wood_fullpartial_1 = new BlockPartialBasicMeta("wood_fullpartial_1", "wood_fullpartial_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_fullpartial_2 = new BlockPartialBasicMeta("wood_fullpartial_2", "wood_fullpartial_2", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);

	//ConnectedXZ Wood Blocks
		wood_connectedxz_1 = new BlockConnectedXZMeta("wood_connectedxz_1", "wood_connectedxz_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Half Wood Blocks
		wood_half_1 = new BlockHalfMeta("wood_half_1", "wood_half_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Daylight Detector Wood Blocks
		wood_daylightdetector_1 = new BlockDaylightDetectorMeta("wood_daylightdetector_1", "wood_daylightdetector_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);

	//Wood Hopper Full Blocks
		wood_hopperfull_1 = new BlockPartialBasicMeta("wood_hopperfull_1", "wood_hopperfull_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Wood Hopper Directional Blocks
		wood_hopperdirectional_1 = new BlockDirectionalFullPartialMeta("wood_hopperdirectional_1", "wood_hopperdirectional_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_hopperdirectional_2 = new BlockDirectionalFullPartialMeta("wood_hopperdirectional_2", "wood_hopperdirectional_2", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_hopperdirectional_3 = new BlockDirectionalFullPartialMeta("wood_hopperdirectional_3", "wood_hopperdirectional_3", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_hopperdirectional_4 = new BlockDirectionalFullPartialMeta("wood_hopperdirectional_4", "wood_hopperdirectional_4", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);

	//Stairs Wood Blocks
		wood_stairs_1 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_1", "wood_stairs_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_2 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_2", "wood_stairs_2", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_3 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_3", "wood_stairs_3", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_4 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_4", "wood_stairs_4", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_5 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_5", "wood_stairs_5", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_6 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_6", "wood_stairs_6", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_7 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_7", "wood_stairs_7", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_8 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_8", "wood_stairs_8", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_9 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_9", "wood_stairs_9", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_10 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_10", "wood_stairs_10", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_11 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_11", "wood_stairs_11", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_12 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_12", "wood_stairs_12", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_13 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_13", "wood_stairs_13", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_14 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_14", "wood_stairs_14", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_15 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_15", "wood_stairs_15", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_16 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_16", "wood_stairs_16", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_17 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_17", "wood_stairs_17", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_18 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_18", "wood_stairs_18", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_19 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_19", "wood_stairs_19", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_20 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_20", "wood_stairs_20", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_21 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_21", "wood_stairs_21", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_22 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_22", "wood_stairs_22", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_23 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_23", "wood_stairs_23", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_24 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_24", "wood_stairs_24", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_25 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_25", "wood_stairs_25", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_26 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_26", "wood_stairs_26", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_stairs_27 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "wood_stairs_27", "wood_stairs_27", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);

	//Slab Wood Blocks
		wood_slab_1 = new BlockSlabMeta("wood_slab_1", "wood_slab_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_slab_2 = new BlockSlabMeta("wood_slab_2", "wood_slab_2", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_slab_3 = new BlockSlabMeta("wood_slab_3", "wood_slab_3", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_slab_4 = new BlockSlabMeta("wood_slab_4", "wood_slab_4", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_slab_5 = new BlockSlabMeta("wood_slab_5", "wood_slab_5", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_slab_6 = new BlockSlabMeta("wood_slab_6", "wood_slab_6", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_slab_7 = new BlockSlabMeta("wood_slab_7", "wood_slab_7", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);

	//Wood Trapdoor Blocks
		wood_trapdoormodel_1 = new BlockTrapDoorMeta("wood_trapdoormodel_1", "wood_trapdoormodel_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_2 = new BlockTrapDoorMeta("wood_trapdoormodel_2", "wood_trapdoormodel_2", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_3 = new BlockTrapDoorMeta("wood_trapdoormodel_3", "wood_trapdoormodel_3", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_4 = new BlockTrapDoorMeta("wood_trapdoormodel_4", "wood_trapdoormodel_4", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_5 = new BlockTrapDoorMeta("wood_trapdoormodel_5", "wood_trapdoormodel_5", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_6 = new BlockTrapDoorMeta("wood_trapdoormodel_6", "wood_trapdoormodel_6", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_7 = new BlockTrapDoorMeta("wood_trapdoormodel_7", "wood_trapdoormodel_7", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_8 = new BlockTrapDoorMeta("wood_trapdoormodel_8", "wood_trapdoormodel_8", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_9 = new BlockTrapDoorMeta("wood_trapdoormodel_9", "wood_trapdoormodel_9", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_10 = new BlockTrapDoorMeta("wood_trapdoormodel_10", "wood_trapdoormodel_10", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_11 = new BlockTrapDoorMeta("wood_trapdoormodel_11", "wood_trapdoormodel_11", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_12 = new BlockTrapDoorMeta("wood_trapdoormodel_12", "wood_trapdoormodel_12", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_13 = new BlockTrapDoorMeta("wood_trapdoormodel_13", "wood_trapdoormodel_13", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_14 = new BlockTrapDoorMeta("wood_trapdoormodel_14", "wood_trapdoormodel_14", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_15 = new BlockTrapDoorMeta("wood_trapdoormodel_15", "wood_trapdoormodel_15", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_16 = new BlockTrapDoorMeta("wood_trapdoormodel_16", "wood_trapdoormodel_16", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_17 = new BlockTrapDoorMeta("wood_trapdoormodel_17", "wood_trapdoormodel_17", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_18 = new BlockTrapDoorMeta("wood_trapdoormodel_18", "wood_trapdoormodel_18", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_19 = new BlockTrapDoorMeta("wood_trapdoormodel_19", "wood_trapdoormodel_19", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_20 = new BlockTrapDoorMeta("wood_trapdoormodel_20", "wood_trapdoormodel_20", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_21 = new BlockTrapDoorMeta("wood_trapdoormodel_21", "wood_trapdoormodel_21", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_22 = new BlockTrapDoorMeta("wood_trapdoormodel_22", "wood_trapdoormodel_22", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_23 = new BlockTrapDoorMeta("wood_trapdoormodel_23", "wood_trapdoormodel_23", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_24 = new BlockTrapDoorMeta("wood_trapdoormodel_24", "wood_trapdoormodel_24", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_25 = new BlockTrapDoorMeta("wood_trapdoormodel_25", "wood_trapdoormodel_25", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_26 = new BlockTrapDoorMeta("wood_trapdoormodel_26", "wood_trapdoormodel_26", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_27 = new BlockTrapDoorMeta("wood_trapdoormodel_27", "wood_trapdoormodel_27", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_28 = new BlockTrapDoorMeta("wood_trapdoormodel_28", "wood_trapdoormodel_28", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_29 = new BlockTrapDoorMeta("wood_trapdoormodel_29", "wood_trapdoormodel_29", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_30 = new BlockTrapDoorMeta("wood_trapdoormodel_30", "wood_trapdoormodel_30", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_31 = new BlockTrapDoorMeta("wood_trapdoormodel_31", "wood_trapdoormodel_31", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_32 = new BlockTrapDoorMeta("wood_trapdoormodel_32", "wood_trapdoormodel_32", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_33 = new BlockTrapDoorMeta("wood_trapdoormodel_33", "wood_trapdoormodel_33", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_34 = new BlockTrapDoorMeta("wood_trapdoormodel_34", "wood_trapdoormodel_34", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_35 = new BlockTrapDoorMeta("wood_trapdoormodel_35", "wood_trapdoormodel_35", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_36 = new BlockTrapDoorMeta("wood_trapdoormodel_36", "wood_trapdoormodel_36", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_37 = new BlockTrapDoorMeta("wood_trapdoormodel_37", "wood_trapdoormodel_37", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_38 = new BlockTrapDoorMeta("wood_trapdoormodel_38", "wood_trapdoormodel_38", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_trapdoormodel_39 = new BlockTrapDoorMeta("wood_trapdoormodel_39", "wood_trapdoormodel_39", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);

	//Wood Vertical Slab Blocks
		wood_verticalslab_1 = new BlockVerticalSlabMeta("wood_verticalslab_1", "wood_verticalslab_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_2 = new BlockVerticalSlabMeta("wood_verticalslab_2", "wood_verticalslab_2", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_3 = new BlockVerticalSlabMeta("wood_verticalslab_3", "wood_verticalslab_3", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_4 = new BlockVerticalSlabMeta("wood_verticalslab_4", "wood_verticalslab_4", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_5 = new BlockVerticalSlabMeta("wood_verticalslab_5", "wood_verticalslab_5", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_6 = new BlockVerticalSlabMeta("wood_verticalslab_6", "wood_verticalslab_6", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_7 = new BlockVerticalSlabMeta("wood_verticalslab_7", "wood_verticalslab_7", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_8 = new BlockVerticalSlabMeta("wood_verticalslab_8", "wood_verticalslab_8", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_9 = new BlockVerticalSlabMeta("wood_verticalslab_9", "wood_verticalslab_9", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_10 = new BlockVerticalSlabMeta("wood_verticalslab_10", "wood_verticalslab_10", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_11 = new BlockVerticalSlabMeta("wood_verticalslab_11", "wood_verticalslab_11", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_12 = new BlockVerticalSlabMeta("wood_verticalslab_12", "wood_verticalslab_12", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_13 = new BlockVerticalSlabMeta("wood_verticalslab_13", "wood_verticalslab_13", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_14 = new BlockVerticalSlabMeta("wood_verticalslab_14", "wood_verticalslab_14", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_15 = new BlockVerticalSlabMeta("wood_verticalslab_15", "wood_verticalslab_15", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_16 = new BlockVerticalSlabMeta("wood_verticalslab_16", "wood_verticalslab_16", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_17 = new BlockVerticalSlabMeta("wood_verticalslab_17", "wood_verticalslab_17", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_18 = new BlockVerticalSlabMeta("wood_verticalslab_18", "wood_verticalslab_18", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_verticalslab_19 = new BlockVerticalSlabMeta("wood_verticalslab_19", "wood_verticalslab_19", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);

	//Wood Corner Blocks
		wood_corner_1 = new BlockCornerMeta("wood_corner_1", "wood_corner_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_2 = new BlockCornerMeta("wood_corner_2", "wood_corner_2", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_3 = new BlockCornerMeta("wood_corner_3", "wood_corner_3", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_4 = new BlockCornerMeta("wood_corner_4", "wood_corner_4", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_5 = new BlockCornerMeta("wood_corner_5", "wood_corner_5", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_6 = new BlockCornerMeta("wood_corner_6", "wood_corner_6", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_7 = new BlockCornerMeta("wood_corner_7", "wood_corner_7", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_8 = new BlockCornerMeta("wood_corner_8", "wood_corner_8", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_9 = new BlockCornerMeta("wood_corner_9", "wood_corner_9", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_10 = new BlockCornerMeta("wood_corner_10", "wood_corner_10", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_11 = new BlockCornerMeta("wood_corner_11", "wood_corner_11", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_12 = new BlockCornerMeta("wood_corner_12", "wood_corner_12", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_13 = new BlockCornerMeta("wood_corner_13", "wood_corner_13", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_14 = new BlockCornerMeta("wood_corner_14", "wood_corner_14", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_15 = new BlockCornerMeta("wood_corner_15", "wood_corner_15", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_16 = new BlockCornerMeta("wood_corner_16", "wood_corner_16", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_17 = new BlockCornerMeta("wood_corner_17", "wood_corner_17", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_18 = new BlockCornerMeta("wood_corner_18", "wood_corner_18", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_corner_19 = new BlockCornerMeta("wood_corner_19", "wood_corner_19", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);

	//Wood Functional Trapdoor Blocks
		woodenboard = new TrapdoorBlock("woodenboard", "woodenboard", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wheel = new TrapdoorBlock("wheel", "wheel", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Wood Shutters Blocks
		shutters_light = new ShuttersBlock("shutters_light", "shutters_light", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		shutters_dark = new ShuttersBlock("shutters_dark", "shutters_dark", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Wood Trapdoor Rail Blocks
		wood_railing = new TrapdoorRailBlock("wood_railing", "wood_railing", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_railing_straight = new TrapdoorRailBlock("wood_railing_straight", "wood_railing_straight", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		spruce_railing = new TrapdoorRailBlock("spruce_railing", "spruce_railing", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		spruce_railing_straight = new TrapdoorRailBlock("spruce_railing_straight", "spruce_railing_straight", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		birch_railing = new TrapdoorRailBlock("birch_railing", "birch_railing", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		birch_railing_straight = new TrapdoorRailBlock("birch_railing_straight", "birch_railing_straight", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		jungle_railing = new TrapdoorRailBlock("jungle_railing", "jungle_railing", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		jungle_railing_straight = new TrapdoorRailBlock("jungle_railing_straight", "jungle_railing_straight", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		acacia_railing = new TrapdoorRailBlock("acacia_railing", "acacia_railing", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		acacia_railing_straight = new TrapdoorRailBlock("acacia_railing_straight", "acacia_railing_straight", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Wood Ironbar Blocks
		wood_ironbar_1 = new BlockBarsMeta("wood_ironbar_1", "wood_ironbar_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Wood Carpet Blocks
		wood_carpet_1 = new BlockCarpetMeta("wood_carpet_1", "wood_carpet_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Wood Ladder Blocks
		ladder = new LadderBlock("ladder", "ladder", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//No Collision Wood Blocks
		wood_nocollision_1 = new BlockNoCollisionMeta("wood_nocollision_1", "wood_nocollision_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//No Collision Damage Wood Blocks
		wood_nocollisiondamage_1 = new DamageNoCollisionBlock("wood_nocollisiondamage_1", "wood_nocollisiondamage_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Directional No Collision Wood Blocks
		wood_directionalnocollision_1 = new BlockDirectionalNoCollisionMeta("wood_directionalnocollision_1", "wood_directionalnocollision_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Directional Collision Trapdoor Wood Blocks
		wood_directionalcollisiontrapdoor_1 = new BlockDirectionalCollisionTrapdoorMeta("wood_directionalcollisiontrapdoor_1", "wood_directionalcollisiontrapdoor_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_directionalcollisiontrapdoor_2 = new BlockDirectionalCollisionTrapdoorMeta("wood_directionalcollisiontrapdoor_2", "wood_directionalcollisiontrapdoor_2", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		wood_directionalcollisiontrapdoor_3 = new BlockDirectionalCollisionTrapdoorMeta("wood_directionalcollisiontrapdoor_3", "wood_directionalcollisiontrapdoor_3", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Bed Wood Blocks
		bed_wolf = new BedBlock("bed_wolf", "bed_wolf", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		bed_rustic = new BedBlock("bed_rustic", "bed_rustic", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		bed_bear = new BedBlock("bed_bear", "bed_bear", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		bed_fancygreen = new BedBlock("bed_fancygreen", "bed_fancygreen", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Cauldron Wood Blocks
		barrel_grille_cauldron = new CauldronBlock("barrel_grille_cauldron", "barrel_grille_cauldron", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		barrel_cauldron = new CauldronBlock("barrel_cauldron", "barrel_cauldron", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Enchanted Wood Blocks
		wood_enchantedbook_1 = new BlockEnchantedMeta("wood_enchantedbook_1", "wood_enchantedbook_1", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Wood Rail Blocks
		jungle_rail = new RailBlock("jungle_rail", "jungle_rail", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
		spruce_floor_rail = new RailBlock("spruce_floor_rail", "spruce_floor_rail", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Full Iron Blocks
		iron_full_1 = new BlockSimpleMeta("iron_full_1", "iron_full_1", Material.IRON, 1.5F, 10.F, SoundType.METAL, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Full Partial Iron Light 1.0 Blocks
		iron_fullpartiallight10_1 = new BlockPartialBasicMeta("iron_fullpartiallight10_1", "iron_fullpartiallight10_1", Material.IRON, 1.5F, 10.F, SoundType.METAL, 1.0F, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Iron Anvil Blocks
		iron_anvil_1 = new BlockRotatedModelMeta("iron_anvil_1", "iron_anvil_1", Material.IRON, 1.5F, 10.F, SoundType.METAL, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Iron Ironbar Blocks
		iron_ironbar_1 = new BlockBarsMeta("iron_ironbar_1", "iron_ironbar_1", Material.IRON, 1.5F, 10.F, SoundType.METAL, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Stairs Iron Blocks
		iron_stairs_1 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "iron_stairs_1", "iron_stairs_1", Material.IRON, 1.5F, 10.F, SoundType.METAL, 0, "axe", 0, true, CreativeTabs.SEARCH);
		iron_stairs_2 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "iron_stairs_2", "iron_stairs_2", Material.IRON, 1.5F, 10.F, SoundType.METAL, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Iron Trapdoor Blocks
		iron_trapdoormodel_1 = new BlockTrapDoorMeta("iron_trapdoormodel_1", "iron_trapdoormodel_1", Material.IRON, 1.5F, 10.F, SoundType.METAL, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		iron_trapdoormodel_2 = new BlockTrapDoorMeta("iron_trapdoormodel_2", "iron_trapdoormodel_2", Material.IRON, 1.5F, 10.F, SoundType.METAL, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Iron Trapdoor Railing Blocks
		iron_railing = new TrapdoorRailBlock("iron_railing", "iron_railing", Material.IRON, 1.5F, 10.F, SoundType.METAL, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		iron_railing_straight = new TrapdoorRailBlock("iron_railing_straight", "iron_railing_straight", Material.IRON, 1.5F, 10.F, SoundType.METAL, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Slabs Iron Blocks
		cauldron_irongrille = new CauldronBlock("cauldron_irongrille", "cauldron_irongrille", Material.IRON, 1.5F, 10.F, SoundType.METAL, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		cauldron = new CauldronBlock("cauldron", "cauldron", Material.IRON, 1.5F, 10.F, SoundType.METAL, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Brewingstands Iron Blocks
		iron_brewingstand_1 = new BrewingstandBlock("iron_brewingstand_1", "iron_brewingstand_1", Material.IRON, 1.5F, 10.F, SoundType.METAL, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		iron_brewingstandlight10_1 = new BrewingstandBlock("iron_brewingstandlight10_1", "iron_brewingstandlight10_1", Material.IRON, 1.5F, 10.F, SoundType.METAL, 1.0F, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Directional No Collision Iron Blocks
		iron_directionalnocollision_1 = new BlockDirectionalNoCollisionMeta("iron_directionalnocollision_1", "iron_directionalnocollision_1", Material.IRON, 1.5F, 10.F, SoundType.METAL, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		iron_directionalnocollision_2 = new BlockDirectionalNoCollisionMeta("iron_directionalnocollision_2", "iron_directionalnocollision_2", Material.IRON, 1.5F, 10.F, SoundType.METAL, 1.0F, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Iron Rail Blocks
		ironchains_rail = new RailBlock("ironchains_rail", "ironchains_rail", Material.IRON, 1.5F, 10.F, SoundType.METAL, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Plants Log Blocks
		plants_log_1 = new BlockRotatedModelMeta("plants_log_1", "plants_log_1", Material.PLANTS, 1.5F, 10.F, SoundType.PLANT, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Stairs Plants Blocks
		plants_stairs_1 = new BlockStairsMeta(Blocks.OAK_STAIRS.getStateFromMeta(0), "plants_stairs_1", "plants_stairs_1", Material.PLANTS, 1.5F, 10.F, SoundType.PLANT, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Slabs Plants Block
		plants_slab_1 = new BlockSlabMeta("plants_slab_1", "plants_slab_1", Material.PLANTS, 1.5F, 10.F, SoundType.PLANT, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//No Collision Plants Block
		plants_nocollision_1 = new BlockNoCollisionMeta("plants_nocollision_1", "plants_nocollision_1", Material.PLANTS, 1.5F, 10.F, SoundType.PLANT, 0, "axe", 0, true, CreativeTabs.SEARCH);
		plants_nocollision_2 = new BlockNoCollisionMeta("plants_nocollision_2", "plants_nocollision_2", Material.PLANTS, 1.5F, 10.F, SoundType.PLANT, 0, "axe", 0, true, CreativeTabs.SEARCH);
		plants_nocollision_3 = new BlockNoCollisionMeta("plants_nocollision_3", "plants_nocollision_3", Material.PLANTS, 1.5F, 10.F, SoundType.PLANT, 0, "axe", 0, true, CreativeTabs.SEARCH);
		plants_nocollision_4 = new BlockNoCollisionMeta("plants_nocollision_4", "plants_nocollision_4", Material.PLANTS, 1.5F, 10.F, SoundType.PLANT, 0, "axe", 0, true, CreativeTabs.SEARCH);
		plants_nocollision_5 = new BlockNoCollisionMeta("plants_nocollision_5", "plants_nocollision_5", Material.PLANTS, 1.5F, 10.F, SoundType.PLANT, 0, "axe", 0, true, CreativeTabs.SEARCH);

	//No Collision Biome Plants Block
		plants_nocollisionbiome_1 = new BlockBiomeNoCollisionMeta("plants_nocollisionbiome_1", "plants_nocollisionbiome_1", Material.PLANTS, 1.5F, 10.F, SoundType.PLANT, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//No Collision Biome Plants Block
		plants_lilypad_1 = new LilypadBlock("plants_lilypad_1", "plants_lilypad_1", Material.PLANTS, 1.5F, 10.F, SoundType.PLANT, 0, "axe", 0, true, CreativeTabs.SEARCH);
		plants_lilypad_2 = new LilypadBlock("plants_lilypad_2", "plants_lilypad_2", Material.PLANTS, 1.5F, 10.F, SoundType.PLANT, 0, "axe", 0, true, CreativeTabs.SEARCH);
	//Full Leaves Block
		leaves_full_1 = new BlockLeavesMeta("leaves_full_1", "leaves_full_1", Material.PLANTS, 1.5F, 10.F, SoundType.PLANT, 0, "shears", 0, true, CreativeTabs.SEARCH);
	//Vine Vine Block
		vine_jungle = new VineBlock("vine_jungle", "vine_jungle", Material.VINE, 1.5F, 10.F, SoundType.PLANT, 0, "shears", 0, true, CreativeTabs.SEARCH);
		vine_ivy = new VineBlock("vine_ivy", "vine_ivy", Material.VINE, 1.5F, 10.F, SoundType.PLANT, 0, "shears", 0, true, CreativeTabs.SEARCH);
		vine_moss = new VineBlock("vine_moss", "vine_moss", Material.VINE, 1.5F, 10.F, SoundType.PLANT, 0, "shears", 0, true, CreativeTabs.SEARCH);
	//Snowlayer Biome Grass Block
		grass_snowlayer_1 = new SnowBlock("grass_snowlayer_1", "grass_snowlayer_1", Material.GRASS, 1.5F, 10.F, SoundType.PLANT, 0, "shovel", 0, true, CreativeTabs.SEARCH);
	//Slab Cloth Blocks
		cloth_slab_1 = new BlockSlabMeta("cloth_slab_1", "cloth_slab_1", Material.CLOTH, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		cloth_slab_2 = new BlockSlabMeta("cloth_slab_2", "cloth_slab_2", Material.CLOTH, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Pane Cloth Blocks
		cloth_pane_1 = new BlockPaneMeta("cloth_pane_1", "cloth_pane_1", Material.CLOTH, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Climbable Iron Bar Cloth Blocks
		cloth_climbironbar_1 = new BlockClimbableIronbarMeta("cloth_climbironbar_1", "cloth_climbironbar_1", Material.CLOTH, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Climbable Iron Bar Iron/Cloth Blocks
		cloth_ironclimbironbar_1 = new BlockClimbableIronbarMeta("cloth_ironclimbironbar_1", "cloth_ironclimbironbar_1", Material.IRON, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Vine Cloth Blocks
		curtain_black_vine = new VineBlock("curtain_black_vine", "curtain_black_vine", Material.IRON, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		curtain_blue_vine = new VineBlock("curtain_blue_vine", "curtain_blue_vine", Material.IRON, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		curtain_brown_vine = new VineBlock("curtain_brown_vine", "curtain_brown_vine", Material.IRON, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		curtain_green_vine = new VineBlock("curtain_green_vine", "curtain_green_vine", Material.IRON, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		curtain_purple_vine = new VineBlock("curtain_purple_vine", "curtain_purple_vine", Material.IRON, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		curtain_red_vine = new VineBlock("curtain_red_vine", "curtain_red_vine", Material.IRON, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		curtain_white_vine = new VineBlock("curtain_white_vine", "curtain_white_vine", Material.IRON, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Full Cloth Light 1.0 Blocks
		cloth_fulllight10_1 = new BlockSimpleMeta("cloth_fulllight10_1", "cloth_fulllight10_1", Material.CLOTH, 1.5F, 10.F, SoundType.CLOTH, 1.0F, "shovel", 0, true, CreativeTabs.SEARCH);
	//Cloth Ladder Blocks
		rope_ladder = new LadderBlock("rope_ladder", "rope_ladder", Material.CLOTH, 1.5F, 10.F, SoundType.CLOTH, 1.0F, "shovel", 0, true, CreativeTabs.SEARCH);
		rope_climbing_ladder = new LadderBlock("rope_climbing_ladder", "rope_climbing_ladder", Material.CLOTH, 1.5F, 10.F, SoundType.CLOTH, 1.0F, "shovel", 0, true, CreativeTabs.SEARCH);
	//No Collision Cloth Blocks
		cloth_nocollision_1 = new BlockNoCollisionMeta("cloth_nocollision_1", "cloth_nocollision_1", Material.CLOTH, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//No Collision Cloth Light 1.0 Blocks
		cloth_nocollisionlight10_1 = new NoCollisionBlock("cloth_nocollisionlight10_1", "cloth_nocollisionlight10_1", Material.CLOTH, 1.5F, 10.F, SoundType.CLOTH, 1.0F, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Directional No Collision Cloth Blocks
		cloth_directionalnocollision_1 = new BlockDirectionalNoCollisionMeta("cloth_directionalnocollision_1", "cloth_directionalnocollision_1", Material.CLOTH, 1.5F, 10.F, SoundType.CLOTH, 1.0F, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Cloth Ironbar Blocks
		rope_rail = new RailBlock("rope_rail", "rope_rail", Material.CLOTH, 1.5F, 10.F, SoundType.CLOTH, 1.0F, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Full Sand Block
		sand_full_1 = new BlockSimpleMeta("sand_full_1", "sand_full_1", Material.SAND, 1.5F, 10.F, SoundType.SAND, 0, "shovel", 0, true, CreativeTabs.SEARCH);
	//Sand Layer Blocks
		sand_layer_1 = new BlockLayerMeta("sand_layer_1", "sand_layer_1", Material.SAND, 1.5F, 10.F, SoundType.SAND, 0, "shovel", 0, CreativeTabs.SEARCH);
		sand_layer_2 = new BlockLayerMeta("sand_layer_2", "sand_layer_2", Material.SAND, 1.5F, 10.F, SoundType.SAND, 0, "shovel", 0, CreativeTabs.SEARCH);
		sand_layer_3 = new BlockLayerMeta("sand_layer_3", "sand_layer_3", Material.SAND, 1.5F, 10.F, SoundType.SAND, 0, "shovel", 0, CreativeTabs.SEARCH);
		sand_layer_4 = new BlockLayerMeta("sand_layer_4", "sand_layer_4", Material.SAND, 1.5F, 10.F, SoundType.SAND, 0, "shovel", 0, CreativeTabs.SEARCH);
		sand_layer_5 = new BlockLayerMeta("sand_layer_5", "sand_layer_5", Material.SAND, 1.5F, 10.F, SoundType.SAND, 0, "shovel", 0, CreativeTabs.SEARCH);
		sand_layer_6 = new BlockLayerMeta("sand_layer_6", "sand_layer_6", Material.SAND, 1.5F, 10.F, SoundType.SAND, 0, "shovel", 0, CreativeTabs.SEARCH);

	//Full Ground Blocks
		ground_full_1 = new BlockSimpleMeta("ground_full_1", "ground_full_1", Material.GROUND, 1.5F, 10.F, SoundType.GROUND, 0, "shovel", 0, true, CreativeTabs.SEARCH);
		ground_full_2 = new BlockSimpleMeta("ground_full_2", "ground_full_2", Material.GROUND, 1.5F, 10.F, SoundType.GROUND, 0, "shovel", 0, true, CreativeTabs.SEARCH);
	//Soulsand Ground Blocks
		ground_soulsand_1 = new BlockSoulsandMeta("ground_soulsand_1", "ground_soulsand_1", Material.GROUND, 1.5F, 10.F, SoundType.GROUND, 0, "shovel", 0, true, CreativeTabs.SEARCH);
	//Directional Full Partial Ground Blocks
		ground_directionalfullpartial_1 = new BlockDirectionalFullPartialMeta("ground_directionalfullpartial_1", "ground_directionalfullpartial_1", Material.GROUND, 1.5F, 10.F, SoundType.GROUND, 0, "shovel", 0, true, CreativeTabs.SEARCH);
	//Ground Layer Blocks
		ground_layer_1 = new BlockLayerMeta("ground_layer_1", "ground_layer_1", Material.GROUND, 1.5F, 10.F, SoundType.GROUND, 0, "shovel", 0, CreativeTabs.SEARCH);
		ground_layer_2 = new BlockLayerMeta("ground_layer_2", "ground_layer_2", Material.GROUND, 1.5F, 10.F, SoundType.GROUND, 0, "shovel", 0, CreativeTabs.SEARCH);
		ground_layer_3 = new BlockLayerMeta("ground_layer_3", "ground_layer_3", Material.GROUND, 1.5F, 10.F, SoundType.GROUND, 0, "shovel", 0, CreativeTabs.SEARCH);
		ground_layer_4 = new BlockLayerMeta("ground_layer_4", "ground_layer_4", Material.GROUND, 1.5F, 10.F, SoundType.GROUND, 0, "shovel", 0, CreativeTabs.SEARCH);
		ground_layer_5 = new BlockLayerMeta("ground_layer_5", "ground_layer_5", Material.GROUND, 1.5F, 10.F, SoundType.GROUND, 0, "shovel", 0, CreativeTabs.SEARCH);
		ground_layer_6 = new BlockLayerMeta("ground_layer_6", "ground_layer_6", Material.GROUND, 1.5F, 10.F, SoundType.GROUND, 0, "shovel", 0, CreativeTabs.SEARCH);
		ground_layer_7 = new BlockLayerMeta("ground_layer_7", "ground_layer_7", Material.GROUND, 1.5F, 10.F, SoundType.GROUND, 0, "shovel", 0, CreativeTabs.SEARCH);
		ground_layer_8 = new BlockLayerMeta("ground_layer_8", "ground_layer_8", Material.GROUND, 1.5F, 10.F, SoundType.GROUND, 0, "shovel", 0, CreativeTabs.SEARCH);
		ground_layer_9 = new BlockLayerMeta("ground_layer_9", "ground_layer_9", Material.GROUND, 1.5F, 10.F, SoundType.GROUND, 0, "shovel", 0, CreativeTabs.SEARCH);
		ground_layer_10 = new BlockLayerMeta("ground_layer_10", "ground_layer_10", Material.GROUND, 1.5F, 10.F, SoundType.GROUND, 0, "shovel", 0, CreativeTabs.SEARCH);
		ground_layer_11 = new BlockLayerMeta("ground_layer_11", "ground_layer_11", Material.GROUND, 1.5F, 10.F, SoundType.GROUND, 0, "shovel", 0, CreativeTabs.SEARCH);
		ground_layer_12 = new BlockLayerMeta("ground_layer_12", "ground_layer_12", Material.GROUND, 1.5F, 10.F, SoundType.GROUND, 0, "shovel", 0, CreativeTabs.SEARCH);
		ground_layer_13 = new BlockLayerMeta("ground_layer_13", "ground_layer_13", Material.GROUND, 1.5F, 10.F, SoundType.GROUND, 0, "shovel", 0, CreativeTabs.SEARCH);

	//Translucent Ice Blocks
		ice_translucent_1 = new BlockTranslucentMeta("ice_translucent_1", "ice_translucent_1", Material.ICE, 1.5F, 10.F, SoundType.GLASS, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Slab Ice Blocks
		ice_slab_1 = new BlockSlabMeta("ice_slab_1", "ice_slab_1", Material.ICE, 1.5F, 10.F, SoundType.GLASS, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Translucent No Collision Ice Blocks
		ice_translucentnocollision_1 = new TranslucentNoCollisionBlock("ice_translucentnocollision_1", "ice_translucentnocollision_1", Material.ICE, 1.5F, 10.F, SoundType.GLASS, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//No Collision Ice Blocks
		ice_nocollision_1 = new BlockNoCollisionMeta("ice_nocollision_1", "ice_nocollision_1", Material.ICE, 1.5F, 10.F, SoundType.GLASS, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//No Collision Damage Ice Blocks
		ice_nocollisiondamage_1 = new DamageNoCollisionBlock("ice_nocollisiondamage_1", "ice_nocollisiondamage_1", Material.ICE, 1.5F, 10.F, SoundType.GLASS, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Lilypad Ice Blocks
		ice_lilypad_1 = new LilypadBlock("ice_lilypad_1", "ice_lilypad_1", Material.ICE, 1.5F, 10.F, SoundType.GLASS, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Stairs Snow Blocks
		snow_stairs_1 = new BlockStairsMeta(Blocks.STONE_STAIRS.getStateFromMeta(0), "snow_stairs_1", "snow_stairs_1", Material.SNOW, 1.5F, 10.F, SoundType.SNOW, 0, "shovel", 0, true, CreativeTabs.SEARCH);
	//No Collision No Material Blocks
		nomaterial_nocollision_1 = new BlockNoCollisionMeta("nomaterial_nocollision_1", "nomaterial_nocollision_1", Material.BARRIER, 1.5F, 10.F, SoundType.SNOW, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//No Collision No Material Light 0.5 Blocks
		nomaterial_nocollisionlight5_1 = new NoCollisionBlock("nomaterial_nocollisionlight5_1", "nomaterial_nocollisionlight5_1", Material.BARRIER, 1.5F, 10.F, SoundType.SNOW, 0.5F, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//No Collision No Material Light 0.6 Blocks
		invisible_light6 = new NoCollisionBlock("invisible_light6", "invisible_light6", Material.BARRIER, 1.5F, 10.F, SoundType.SNOW, 0.6F, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//No Collision No Material Light 0.8 Blocks
		invisible_light8 = new NoCollisionBlock("invisible_light8", "invisible_light8", Material.BARRIER, 1.5F, 10.F, SoundType.SNOW, 0.8F, "pickaxe", 0, true, CreativeTabs.SEARCH);
	// No Collision No Material Light 1.0 Blocks
		invisible_light10 = new NoCollisionBlock("invisible_light10", "invisible_light10", Material.BARRIER, 1.5F, 10.F, SoundType.SNOW, 1.0F, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Glass Glass Blocks
		glass_full_1 = new BlockSimpleMeta("glass_full_1", "glass_full_1", Material.GLASS, 1.5F, 10.F, SoundType.GLASS, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Glass Glass Blocks
		glass_glass_1 = new BlockGlassMeta("glass_glass_1", "glass_glass_1", Material.GLASS, 1.5F, 10.F, SoundType.GLASS, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		glass_glass_2 = new BlockGlassMeta("glass_glass_2", "glass_glass_2", Material.GLASS, 1.5F, 10.F, SoundType.GLASS, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Glass Vertical Slab Blocks
		glass_verticalslab_1 = new BlockVerticalSlabMeta("glass_verticalslab_1", "glass_verticalslab_1", Material.GLASS, 1.5F, 10.F, SoundType.GLASS, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Glass Trapdoor Blocks
		glass_trapdoormodel_1 = new BlockTrapDoorMeta("glass_trapdoormodel_1", "glass_trapdoormodel_1", Material.GLASS, 1.5F, 10.F, SoundType.GLASS, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		glass_trapdoormodel_2 = new BlockTrapDoorMeta("glass_trapdoormodel_2", "glass_trapdoormodel_2", Material.GLASS, 1.5F, 10.F, SoundType.GLASS, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		glass_trapdoormodel_3 = new BlockTrapDoorMeta("glass_trapdoormodel_3", "glass_trapdoormodel_3", Material.GLASS, 1.5F, 10.F, SoundType.GLASS, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		glass_trapdoormodel_4 = new BlockTrapDoorMeta("glass_trapdoormodel_4", "glass_trapdoormodel_4", Material.GLASS, 1.5F, 10.F, SoundType.GLASS, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);

	//Glass Pane Blocks
		glass_pane_1 = new BlockPaneMeta("glass_pane_1", "glass_pane_1", Material.GLASS, 1.5F, 10.F, SoundType.GLASS, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		glass_pane_2 = new BlockPaneMeta("glass_pane_2", "glass_pane_2", Material.GLASS, 1.5F, 10.F, SoundType.GLASS, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Cake Cake Blocks
		cake_cheesewheel = new CakeBlock("cake_cheesewheel", "cake_cheesewheel", Material.CAKE, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		cake_applepie = new CakeBlock("cake_applepie", "cake_applepie", Material.CAKE, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		cake_icingfruit = new CakeBlock("cake_icingfruit", "cake_icingfruit", Material.CAKE, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		cake_icingchocolate = new CakeBlock("cake_icingchocolate", "cake_icingchocolate", Material.CAKE, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		cake_bread = new CakeBlock("cake_bread", "cake_bread", Material.CAKE, 1.5F, 10.F, SoundType.CLOTH, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Furnace Blocks
		furnace_iron = new FurnaceBlock("furnace_iron", "furnace_iron", Material.IRON, 1.5F, 10.F, SoundType.METAL, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		furnace_sandstone = new FurnaceBlock("furnace_sandstone", "furnace_sandstone", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Lit Furnace Blocks
		furnace_ironlit = new LitFurnaceBlock("furnace_ironlit", "furnace_ironlit", Material.IRON, 1.5F, 10.F, SoundType.METAL, 1.0F, "pickaxe", 0, true, CreativeTabs.SEARCH);
		furnace_cobblelit = new LitFurnaceBlock("furnace_cobblelit", "furnace_cobblelit", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 1.0F, "pickaxe", 0, true, CreativeTabs.SEARCH);
		furnace_sandstonelit = new LitFurnaceBlock("furnace_sandstonelit", "furnace_sandstonelit", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 1.0F, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Dispenser Blocks
		dispenser_aztec = new DispenserBlock("dispenser_aztec", "dispenser_aztec", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		dispenser_iron = new DispenserBlock("dispenser_iron", "dispenser_iron", Material.IRON, 1.5F, 10.F, SoundType.METAL, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		dispenser_egyptian = new DispenserBlock("dispenser_egyptian", "dispenser_egyptian", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Dropper Blocks
		dropper_sandstone = new DispenserBlock("dropper_sandstone", "dropper_sandstone", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		dropper_iron = new DispenserBlock("dropper_iron", "dropper_iron", Material.IRON, 1.5F, 10.F, SoundType.METAL, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Flower Pot Blocks
		flowerpot = new FlowerpotBlock("flowerpot", "flowerpot", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		flowerpot_black = new FlowerpotBlock("flowerpot_black", "flowerpot_black", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Button Blocks
		wood_button = new ButtonBlock("wood_button", "wood_button", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
		stone_button = new ButtonBlock("stone_button", "stone_button", Material.ROCK, 1.5F, 10.F, SoundType.STONE, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Lever Blocks
		lever = new LeverBlock("lever", "lever", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	//Hook Blocks
		hook = new HookBlock("hook", "hook", Material.WOOD, 1.5F, 10.F, SoundType.WOOD, 0, "pickaxe", 0, true, CreativeTabs.SEARCH);
	}


	public static void register()
	{
	/**
	 	registerBlock(blockname) -- This is a bridge between blockRegistry(init) and renderRegistry. Tells registerRender which block is registered.
	 */
	//Full Stone Blocks
		registerBlockStone1(stone_full_1);
		registerBlockStone2(stone_full_2);
		registerBlockStone3(stone_full_3);
		registerBlockStone4(stone_full_4);
		registerBlockStone5(stone_full_5);
		registerBlockStone6(stone_full_6);
		registerBlockStone7(stone_full_7);
		registerBlockStone8(stone_full_8);
		registerBlockStone9(stone_full_9);
		registerBlockStone10(stone_full_10);
		registerBlockStone11(stone_full_11);
		registerBlockStone12(stone_full_12);
		registerBlockStone13(stone_full_13);
		registerBlockStone14(stone_full_14);
		registerBlockStone15(stone_full_15);
		registerBlockStone16(stone_full_16);
		registerBlockStone17(stone_full_17);
	//Full Stone Damage Light 0.6 Block
		registerBlock(stone_fulldamagelight6_1);
		registerSnowlayerBlock(stone_layerdamagelight6_1);
	//Full Stone Damage Light 0.8 Block
		registerSnowlayerBlock(stone_layerdamagelight8_1);
	//Partial Full Stone Blocks
		registerBlockFullPartialStone1(stone_fullpartial_1);
		registerBlockFullPartialStone2(stone_fullpartial_2);
		registerBlockFullPartialStone3(stone_fullpartial_3);
		registerBlockFullPartialStone4(stone_fullpartial_4);
		registerBlockFullPartialStone5(stone_fullpartial_5);
		registerBlockFullPartialStone6(stone_fullpartial_6);
	//Directional Full Partial Stone Blocks
		registerBlockDirectionalFullPartialStone1(stone_directionalfullpartial_1);
	//Corner Stone Blocks
		registerBlockCornerStone1(stone_corner_1);
		registerBlockCornerStone2(stone_corner_2);
		registerBlockCornerStone3(stone_corner_3);
		registerBlockCornerStone4(stone_corner_4);
		registerBlockCornerStone5(stone_corner_5);
		registerBlockCornerStone6(stone_corner_6);
		registerBlockCornerStone7(stone_corner_7);
		registerBlockCornerStone8(stone_corner_8);
		registerBlockCornerStone9(stone_corner_9);
		registerBlockCornerStone10(stone_corner_10);
		registerBlockCornerStone11(stone_corner_11);
		registerBlockCornerStone12(stone_corner_12);
		registerBlockCornerStone13(stone_corner_13);
		registerBlockCornerStone14(stone_corner_14);
		registerBlockCornerStone15(stone_corner_15);
		registerBlockCornerStone16(stone_corner_16);
		registerBlockCornerStone17(stone_corner_17);
		registerBlockCornerStone18(stone_corner_18);
		registerBlockCornerStone19(stone_corner_19);
		registerBlockCornerStone20(stone_corner_20);
		registerBlockCornerStone21(stone_corner_21);
		registerBlockCornerStone22(stone_corner_22);
		registerBlockCornerStone23(stone_corner_23);
		registerBlockCornerStone24(stone_corner_24);
		registerBlockCornerStone25(stone_corner_25);
		registerBlockCornerStone26(stone_corner_26);
		registerBlockCornerStone27(stone_corner_27);
		registerBlockCornerStone28(stone_corner_28);
		registerBlockCornerStone29(stone_corner_29);
		registerBlockCornerStone30(stone_corner_30);
		registerBlockCornerStone31(stone_corner_31);
		registerBlockCornerStone32(stone_corner_32);
		registerBlockCornerStone33(stone_corner_33);
		registerBlockCornerStone34(stone_corner_34);
		registerBlockCornerStone35(stone_corner_35);
		registerBlockCornerStone36(stone_corner_36);
		registerBlockCornerStone37(stone_corner_37);
		registerBlockCornerStone38(stone_corner_38);
		registerBlockCornerStone39(stone_corner_39);
		registerBlockCornerStone40(stone_corner_40);
		registerBlockCornerStone41(stone_corner_41);
		registerBlockCornerStone42(stone_corner_42);
		registerBlockCornerStone43(stone_corner_43);
		registerBlockCornerStone44(stone_corner_44);
		registerBlockCornerStone45(stone_corner_45);
		registerBlockCornerStone46(stone_corner_46);
		registerBlockCornerStone47(stone_corner_47);
		registerBlockCornerStone48(stone_corner_48);
		registerBlockCornerStone49(stone_corner_49);
		registerBlockCornerStone50(stone_corner_50);
		registerBlockCornerStone51(stone_corner_51);
		registerBlockCornerStone52(stone_corner_52);
		registerBlockCornerStone53(stone_corner_53);
		registerBlockCornerStone54(stone_corner_54);
		registerBlockCornerStone55(stone_corner_55);
		registerBlockCornerStone56(stone_corner_56);
		registerBlockCornerStone57(stone_corner_57);
		registerBlockCornerStone58(stone_corner_58);
		registerBlockCornerStone59(stone_corner_59);
		registerBlockCornerStone60(stone_corner_60);
		registerBlockCornerStone61(stone_corner_61);
		registerBlockCornerStone62(stone_corner_62);
		registerBlockCornerStone63(stone_corner_63);
		registerBlockCornerStone64(stone_corner_64);
	//Vertical Slab Stone Blocks
		registerBlockVerticalSlabStone1(stone_verticalslab_1);
		registerBlockVerticalSlabStone2(stone_verticalslab_2);
		registerBlockVerticalSlabStone3(stone_verticalslab_3);
		registerBlockVerticalSlabStone4(stone_verticalslab_4);
		registerBlockVerticalSlabStone5(stone_verticalslab_5);
		registerBlockVerticalSlabStone6(stone_verticalslab_6);
		registerBlockVerticalSlabStone7(stone_verticalslab_7);
		registerBlockVerticalSlabStone8(stone_verticalslab_8);
		registerBlockVerticalSlabStone9(stone_verticalslab_9);
		registerBlockVerticalSlabStone10(stone_verticalslab_10);
		registerBlockVerticalSlabStone11(stone_verticalslab_11);
		registerBlockVerticalSlabStone12(stone_verticalslab_12);
		registerBlockVerticalSlabStone13(stone_verticalslab_13);
		registerBlockVerticalSlabStone14(stone_verticalslab_14);
		registerBlockVerticalSlabStone15(stone_verticalslab_15);
		registerBlockVerticalSlabStone16(stone_verticalslab_16);
		registerBlockVerticalSlabStone17(stone_verticalslab_17);
		registerBlockVerticalSlabStone18(stone_verticalslab_18);
		registerBlockVerticalSlabStone19(stone_verticalslab_19);
		registerBlockVerticalSlabStone20(stone_verticalslab_20);
		registerBlockVerticalSlabStone21(stone_verticalslab_21);
		registerBlockVerticalSlabStone22(stone_verticalslab_22);
		registerBlockVerticalSlabStone23(stone_verticalslab_23);
		registerBlockVerticalSlabStone24(stone_verticalslab_24);
		registerBlockVerticalSlabStone25(stone_verticalslab_25);
		registerBlockVerticalSlabStone26(stone_verticalslab_26);
		registerBlockVerticalSlabStone27(stone_verticalslab_27);
		registerBlockVerticalSlabStone28(stone_verticalslab_28);
		registerBlockVerticalSlabStone29(stone_verticalslab_29);
		registerBlockVerticalSlabStone30(stone_verticalslab_30);
		registerBlockVerticalSlabStone31(stone_verticalslab_31);
		registerBlockVerticalSlabStone32(stone_verticalslab_32);
		registerBlockVerticalSlabStone33(stone_verticalslab_33);
		registerBlockVerticalSlabStone34(stone_verticalslab_34);
		registerBlockVerticalSlabStone35(stone_verticalslab_35);
		registerBlockVerticalSlabStone36(stone_verticalslab_36);
		registerBlockVerticalSlabStone37(stone_verticalslab_37);
		registerBlockVerticalSlabStone38(stone_verticalslab_38);
		registerBlockVerticalSlabStone39(stone_verticalslab_39);
		registerBlockVerticalSlabStone40(stone_verticalslab_40);
		registerBlockVerticalSlabStone41(stone_verticalslab_41);
		registerBlockVerticalSlabStone42(stone_verticalslab_42);
		registerBlockVerticalSlabStone43(stone_verticalslab_43);
		registerBlockVerticalSlabStone44(stone_verticalslab_44);
		registerBlockVerticalSlabStone45(stone_verticalslab_45);
		registerBlockVerticalSlabStone46(stone_verticalslab_46);
		registerBlockVerticalSlabStone47(stone_verticalslab_47);
		registerBlockVerticalSlabStone48(stone_verticalslab_48);
		registerBlockVerticalSlabStone49(stone_verticalslab_49);
		registerBlockVerticalSlabStone50(stone_verticalslab_50);
		registerBlockVerticalSlabStone51(stone_verticalslab_51);
		registerBlockVerticalSlabStone52(stone_verticalslab_52);
		registerBlockVerticalSlabStone53(stone_verticalslab_53);
		registerBlockVerticalSlabStone54(stone_verticalslab_54);
		registerBlockVerticalSlabStone55(stone_verticalslab_55);
		registerBlockVerticalSlabStone56(stone_verticalslab_56);
		registerBlockVerticalSlabStone57(stone_verticalslab_57);
		registerBlockVerticalSlabStone58(stone_verticalslab_58);
		registerBlockVerticalSlabStone59(stone_verticalslab_59);
		registerBlockVerticalSlabStone60(stone_verticalslab_60);
		registerBlockVerticalSlabStone61(stone_verticalslab_61);
		registerBlockVerticalSlabStone62(stone_verticalslab_62);
		registerBlockVerticalSlabStone63(stone_verticalslab_63);
		registerBlockVerticalSlabStone64(stone_verticalslab_64);
		registerBlockVerticalSlabStone65(stone_verticalslab_65);
		registerBlockVerticalSlabStone66(stone_verticalslab_66);
		registerBlockVerticalSlabStone67(stone_verticalslab_67);
		registerBlockVerticalSlabStone68(stone_verticalslab_68);
		registerBlockVerticalSlabStone69(stone_verticalslab_69);
		registerBlockVerticalSlabStone70(stone_verticalslab_70);
		registerBlockVerticalSlabStone71(stone_verticalslab_71);
	//Arrow Slit Stone Blocks
		registerBlockArrowSlitStone1(stone_arrowslit_1);
		registerBlockArrowSlitStone2(stone_arrowslit_2);
		registerBlockArrowSlitStone3(stone_arrowslit_3);
		registerBlockArrowSlitStone4(stone_arrowslit_4);
		registerBlockArrowSlitStone5(stone_arrowslit_5);
		registerBlockArrowSlitStone6(stone_arrowslit_6);
		registerBlockArrowSlitStone7(stone_arrowslit_7);
		registerBlockArrowSlitStone8(stone_arrowslit_8);
		registerBlockArrowSlitStone9(stone_arrowslit_9);
		registerBlockArrowSlitStone10(stone_arrowslit_10);
		registerBlockArrowSlitStone11(stone_arrowslit_11);
		registerBlockArrowSlitStone12(stone_arrowslit_12);
		registerBlockArrowSlitStone13(stone_arrowslit_13);
		registerBlockArrowSlitStone14(stone_arrowslit_14);
		registerBlockArrowSlitStone15(stone_arrowslit_15);
		registerBlockArrowSlitStone16(stone_arrowslit_16);
		registerBlockArrowSlitStone17(stone_arrowslit_17);
		registerBlockArrowSlitStone18(stone_arrowslit_18);
		registerBlockArrowSlitStone19(stone_arrowslit_19);
		registerBlockArrowSlitStone20(stone_arrowslit_20);
		registerBlockArrowSlitStone21(stone_arrowslit_21);
		registerBlockArrowSlitStone22(stone_arrowslit_22);
		registerBlockArrowSlitStone23(stone_arrowslit_23);
		registerBlockArrowSlitStone24(stone_arrowslit_24);
	//Full Slit Stone Blocks
		registerBlockFullSlitStone1(stone_fullslit_1);
		registerBlockFullSlitStone2(stone_fullslit_2);
		registerBlockFullSlitStone3(stone_fullslit_3);
		registerBlockFullSlitStone4(stone_fullslit_4);
		registerBlockFullSlitStone5(stone_fullslit_5);
		registerBlockFullSlitStone6(stone_fullslit_6);
		registerBlockFullSlitStone7(stone_fullslit_7);
		registerBlockFullSlitStone8(stone_fullslit_8);
		registerBlockFullSlitStone9(stone_fullslit_9);
		registerBlockFullSlitStone10(stone_fullslit_10);
		registerBlockFullSlitStone11(stone_fullslit_11);
		registerBlockFullSlitStone12(stone_fullslit_12);
		registerBlockFullSlitStone13(stone_fullslit_13);
		registerBlockFullSlitStone14(stone_fullslit_14);
		registerBlockFullSlitStone15(stone_fullslit_15);
		registerBlockFullSlitStone16(stone_fullslit_16);
		registerBlockFullSlitStone17(stone_fullslit_17);
		registerBlockFullSlitStone18(stone_fullslit_18);
		registerBlockFullSlitStone19(stone_fullslit_19);
		registerBlockFullSlitStone20(stone_fullslit_20);
		registerBlockFullSlitStone21(stone_fullslit_21);
		registerBlockFullSlitStone22(stone_fullslit_22);
		registerBlockFullSlitStone23(stone_fullslit_23);
		registerBlockFullSlitStone24(stone_fullslit_24);

	//Anvil Stone Blocks
		registerBlockAnvilStone1(stone_anvil_1);
		registerBlockAnvilStone2(stone_anvil_2);
		registerBlockAnvilStone3(stone_anvil_3);
		registerBlockAnvilStone4(stone_anvil_4);
		registerBlockAnvilStone5(stone_anvil_5);
		registerBlockAnvilStone6(stone_anvil_6);
		registerBlockAnvilStone7(stone_anvil_7);
		registerBlockAnvilStone8(stone_anvil_8);
		registerBlockAnvilStone9(stone_anvil_9);
		registerBlockAnvilStone10(stone_anvil_10);
		registerBlockAnvilStone11(stone_anvil_11);
		registerBlockAnvilStone12(stone_anvil_12);
		registerBlockAnvilStone13(stone_anvil_13);
		registerBlockAnvilStone14(stone_anvil_14);
		registerBlockAnvilStone15(stone_anvil_15);
		registerBlockAnvilStone16(stone_anvil_16);
		registerBlockAnvilStone17(stone_anvil_17);
		registerBlockAnvilStone18(stone_anvil_18);
		registerBlockAnvilStone19(stone_anvil_19);
		registerBlockAnvilStone20(stone_anvil_20);
		registerBlockAnvilStone21(stone_anvil_21);
		registerBlockAnvilStone22(stone_anvil_22);
		registerBlockAnvilStone23(stone_anvil_23);
		registerBlockAnvilStone24(stone_anvil_24);
		registerBlockAnvilStone25(stone_anvil_25);
		registerBlockAnvilStone26(stone_anvil_26);
		registerBlockAnvilStone27(stone_anvil_27);
		registerBlockAnvilStone28(stone_anvil_28);
	//Hopper Full Stone Blocks
		registerBlockHopperFullStone1(stone_hopperfull_1);
		registerBlockHopperFullStone2(stone_hopperfull_2);
		registerBlockHopperFullStone3(stone_hopperfull_3);
		registerBlockHopperFullStone4(stone_hopperfull_4);
		registerBlockHopperFullStone5(stone_hopperfull_5);
		registerBlockHopperFullStone6(stone_hopperfull_6);
		registerBlockHopperFullStone7(stone_hopperfull_7);
	//Hopper Directional Stone Blocks
		registerBlockHopperDirectionalStone1(stone_hopperdirectional_1);
		registerBlockHopperDirectionalStone2(stone_hopperdirectional_2);
		registerBlockHopperDirectionalStone3(stone_hopperdirectional_3);
		registerBlockHopperDirectionalStone4(stone_hopperdirectional_4);
		registerBlockHopperDirectionalStone5(stone_hopperdirectional_5);
		registerBlockHopperDirectionalStone6(stone_hopperdirectional_6);
		registerBlockHopperDirectionalStone7(stone_hopperdirectional_7);
		registerBlockHopperDirectionalStone8(stone_hopperdirectional_8);
		registerBlockHopperDirectionalStone9(stone_hopperdirectional_9);
		registerBlockHopperDirectionalStone10(stone_hopperdirectional_10);
		registerBlockHopperDirectionalStone11(stone_hopperdirectional_11);
		registerBlockHopperDirectionalStone12(stone_hopperdirectional_12);
		registerBlockHopperDirectionalStone13(stone_hopperdirectional_13);
		registerBlockHopperDirectionalStone14(stone_hopperdirectional_14);
		registerBlockHopperDirectionalStone15(stone_hopperdirectional_15);
		registerBlockHopperDirectionalStone16(stone_hopperdirectional_16);
		registerBlockHopperDirectionalStone17(stone_hopperdirectional_17);
		registerBlockHopperDirectionalStone18(stone_hopperdirectional_18);
		registerBlockHopperDirectionalStone19(stone_hopperdirectional_19);
		registerBlockHopperDirectionalStone20(stone_hopperdirectional_20);
		registerBlockHopperDirectionalStone21(stone_hopperdirectional_21);
		registerBlockHopperDirectionalStone22(stone_hopperdirectional_22);
		registerBlockHopperDirectionalStone23(stone_hopperdirectional_23);
		registerBlockHopperDirectionalStone24(stone_hopperdirectional_24);
		registerBlockHopperDirectionalStone25(stone_hopperdirectional_25);
	//Log Stone Blocks
		registerBlockLogStone1(stone_log_1);
		registerBlockLogStone2(stone_log_2);
	//Wall Stone Blocks
		registerBlockWallStone1(stone_wall_1);
		registerBlockWallStone2(stone_wall_2);
		registerBlockWallStone3(stone_wall_3);
		registerBlockWallStone4(stone_wall_4);
		registerBlockWallStone5(stone_wall_5);
		registerBlockWallStone6(stone_wall_6);
		registerBlockWallStone7(stone_wall_7);
		registerBlockWallStone8(stone_wall_8);
		registerBlockWallStone9(stone_wall_9);
	//Pillar Stone Blocks
		registerBlockPillarStone1(stone_pillar_1);
		registerBlockPillarStone2(stone_pillar_2);
		registerBlockPillarStone3(stone_pillar_3);
		registerBlockPillarStone4(stone_pillar_4);
		registerBlockPillarStone5(stone_pillar_5);
		registerBlockPillarStone6(stone_pillar_6);
		registerBlockPillarStone7(stone_pillar_7);
		registerBlockPillarStone8(stone_pillar_8);
		registerBlockPillarStone9(stone_pillar_9);
	//Fence Stone Blocks
		registerBlockFenceStone1(stone_fence_1);
		registerBlockFenceStone2(stone_fence_2);
	//Fencegate Stone Blocks
		registerBlockFencegateStone1(stone_fencegate_1);
	//Small Pillar Stone Blocks
		registerBlockSmallPillarStone1(stone_smallpillar_1);
		registerBlockSmallPillarStone2(stone_smallpillar_2);
		registerBlockSmallPillarStone3(stone_smallpillar_3);
		registerBlockSmallPillarStone4(stone_smallpillar_4);
		registerBlockSmallPillarStone5(stone_smallpillar_5);
		registerBlockSmallPillarStone6(stone_smallpillar_6);
		registerBlockSmallPillarStone7(stone_smallpillar_7);
		registerBlockSmallPillarStone8(stone_smallpillar_8);
		registerBlockSmallPillarStone9(stone_smallpillar_9);
		registerBlockSmallPillarStone10(stone_smallpillar_10);

		//Stairs Stone Blocks
		registerBlockStairsStone1(stone_stairs_1);
		registerBlockStairsStone2(stone_stairs_2);
		registerBlockStairsStone3(stone_stairs_3);
		registerBlockStairsStone4(stone_stairs_4);
		registerBlockStairsStone5(stone_stairs_5);
		registerBlockStairsStone6(stone_stairs_6);
		registerBlockStairsStone7(stone_stairs_7);
		registerBlockStairsStone8(stone_stairs_8);
		registerBlockStairsStone9(stone_stairs_9);
		registerBlockStairsStone10(stone_stairs_10);
		registerBlockStairsStone11(stone_stairs_11);
		registerBlockStairsStone12(stone_stairs_12);
		registerBlockStairsStone13(stone_stairs_13);
		registerBlockStairsStone14(stone_stairs_14);
		registerBlockStairsStone15(stone_stairs_15);
		registerBlockStairsStone16(stone_stairs_16);
		registerBlockStairsStone17(stone_stairs_17);
		registerBlockStairsStone18(stone_stairs_18);
		registerBlockStairsStone19(stone_stairs_19);
		registerBlockStairsStone20(stone_stairs_20);
		registerBlockStairsStone21(stone_stairs_21);
		registerBlockStairsStone22(stone_stairs_22);
		registerBlockStairsStone23(stone_stairs_23);
		registerBlockStairsStone24(stone_stairs_24);
		registerBlockStairsStone25(stone_stairs_25);
		registerBlockStairsStone26(stone_stairs_26);
		registerBlockStairsStone27(stone_stairs_27);
		registerBlockStairsStone28(stone_stairs_28);
		registerBlockStairsStone29(stone_stairs_29);
		registerBlockStairsStone30(stone_stairs_30);
		registerBlockStairsStone31(stone_stairs_31);
		registerBlockStairsStone32(stone_stairs_32);
		registerBlockStairsStone33(stone_stairs_33);
		registerBlockStairsStone34(stone_stairs_34);
		registerBlockStairsStone35(stone_stairs_35);
		registerBlockStairsStone36(stone_stairs_36);
		registerBlockStairsStone37(stone_stairs_37);
		registerBlockStairsStone38(stone_stairs_38);
		registerBlockStairsStone39(stone_stairs_39);
		registerBlockStairsStone40(stone_stairs_40);
		registerBlockStairsStone41(stone_stairs_41);
		registerBlockStairsStone42(stone_stairs_42);
		registerBlockStairsStone43(stone_stairs_43);
		registerBlockStairsStone44(stone_stairs_44);
		registerBlockStairsStone45(stone_stairs_45);
		registerBlockStairsStone46(stone_stairs_46);
		registerBlockStairsStone47(stone_stairs_47);
		registerBlockStairsStone48(stone_stairs_48);
		registerBlockStairsStone49(stone_stairs_49);
		registerBlockStairsStone50(stone_stairs_50);
		registerBlockStairsStone51(stone_stairs_51);
		registerBlockStairsStone52(stone_stairs_52);
		registerBlockStairsStone53(stone_stairs_53);
		registerBlockStairsStone54(stone_stairs_54);
		registerBlockStairsStone55(stone_stairs_55);
		registerBlockStairsStone56(stone_stairs_56);
		registerBlockStairsStone57(stone_stairs_57);
		registerBlockStairsStone58(stone_stairs_58);
		registerBlockStairsStone59(stone_stairs_59);
		registerBlockStairsStone60(stone_stairs_60);
		registerBlockStairsStone61(stone_stairs_61);
		registerBlockStairsStone62(stone_stairs_62);
		registerBlockStairsStone63(stone_stairs_63);
		registerBlockStairsStone64(stone_stairs_64);
		registerBlockStairsStone65(stone_stairs_65);
		registerBlockStairsStone66(stone_stairs_66);
		registerBlockStairsStone67(stone_stairs_67);
		registerBlockStairsStone68(stone_stairs_68);
		registerBlockStairsStone69(stone_stairs_69);
		registerBlockStairsStone70(stone_stairs_70);
		registerBlockStairsStone71(stone_stairs_71);
		registerBlockStairsStone72(stone_stairs_72);
		registerBlockStairsStone73(stone_stairs_73);
		registerBlockStairsStone74(stone_stairs_74);
		registerBlockStairsStone75(stone_stairs_75);
		registerBlockStairsStone76(stone_stairs_76);
		registerBlockStairsStone77(stone_stairs_77);
		registerBlockStairsStone78(stone_stairs_78);
		registerBlockStairsStone79(stone_stairs_79);
		registerBlockStairsStone80(stone_stairs_80);
		registerBlockStairsStone81(stone_stairs_81);
		registerBlockStairsStone82(stone_stairs_82);
		registerBlockStairsStone83(stone_stairs_83);
		registerBlockStairsStone84(stone_stairs_84);
		registerBlockStairsStone85(stone_stairs_85);
		registerBlockStairsStone86(stone_stairs_86);
		registerBlockStairsStone87(stone_stairs_87);
		registerBlockStairsStone88(stone_stairs_88);
		registerBlockStairsStone89(stone_stairs_89);
		registerBlockStairsStone90(stone_stairs_90);
		registerBlockStairsStone91(stone_stairs_91);
		registerBlockStairsStone92(stone_stairs_92);
		registerBlockStairsStone93(stone_stairs_93);
		registerBlockStairsStone94(stone_stairs_94);
		registerBlockStairsStone95(stone_stairs_95);
		registerBlockStairsStone96(stone_stairs_96);
		registerBlockStairsStone97(stone_stairs_97);
		registerBlockStairsStone98(stone_stairs_98);
		registerBlockStairsStone99(stone_stairs_99);
		registerBlockStairsStone100(stone_stairs_100);
		registerBlockStairsStone101(stone_stairs_101);
		registerBlockStairsStone102(stone_stairs_102);
		registerBlockStairsStone103(stone_stairs_103);
		registerBlockStairsStone104(stone_stairs_104);
		registerBlockStairsStone105(stone_stairs_105);
		registerBlockStairsStone106(stone_stairs_106);
		registerBlockStairsStone107(stone_stairs_107);
		registerBlockStairsStone108(stone_stairs_108);
		registerBlockStairsStone109(stone_stairs_109);
	//Slab Stone Blocks
		registerBlockSlabStone1(stone_slab_1);
		registerBlockSlabStone2(stone_slab_2);
		registerBlockSlabStone3(stone_slab_3);
		registerBlockSlabStone4(stone_slab_4);
		registerBlockSlabStone5(stone_slab_5);
		registerBlockSlabStone6(stone_slab_6);
		registerBlockSlabStone7(stone_slab_7);
		registerBlockSlabStone8(stone_slab_8);
		registerBlockSlabStone9(stone_slab_9);
		registerBlockSlabStone10(stone_slab_10);
		registerBlockSlabStone11(stone_slab_11);
		registerBlockSlabStone12(stone_slab_12);
		registerBlockSlabStone13(stone_slab_13);
		registerBlockSlabStone14(stone_slab_14);
		registerBlockSlabStone15(stone_slab_15);
		registerBlockSlabStone16(stone_slab_16);
		registerBlockSlabStone17(stone_slab_17);
		registerBlockSlabStone18(stone_slab_18);
		registerBlockSlabStone19(stone_slab_19);
		registerBlockSlabStone20(stone_slab_20);
	//Stone Trapdoor Blocks
		registerBlockTrapdoorStone1(stone_trapdoormodel_1);
		registerBlockTrapdoorStone2(stone_trapdoormodel_2);
		registerBlockTrapdoorStone3(stone_trapdoormodel_3);
		registerBlockTrapdoorStone4(stone_trapdoormodel_4);
		registerBlockTrapdoorStone5(stone_trapdoormodel_5);
		registerBlockTrapdoorStone6(stone_trapdoormodel_6);
		registerBlockTrapdoorStone7(stone_trapdoormodel_7);
		registerBlockTrapdoorStone8(stone_trapdoormodel_8);
		registerBlockTrapdoorStone9(stone_trapdoormodel_9);
		registerBlockTrapdoorStone10(stone_trapdoormodel_10);
		registerBlockTrapdoorStone11(stone_trapdoormodel_11);
		registerBlockTrapdoorStone12(stone_trapdoormodel_12);
		registerBlockTrapdoorStone13(stone_trapdoormodel_13);
		registerBlockTrapdoorStone14(stone_trapdoormodel_14);
		registerBlockTrapdoorStone15(stone_trapdoormodel_15);
		registerBlockTrapdoorStone16(stone_trapdoormodel_16);
		registerBlockTrapdoorStone17(stone_trapdoormodel_17);
		registerBlockTrapdoorStone18(stone_trapdoormodel_18);
		registerBlockTrapdoorStone19(stone_trapdoormodel_19);
		registerBlockTrapdoorStone20(stone_trapdoormodel_20);
		registerBlockTrapdoorStone21(stone_trapdoormodel_21);
		registerBlockTrapdoorStone22(stone_trapdoormodel_22);
		registerBlockTrapdoorStone23(stone_trapdoormodel_23);
		registerBlockTrapdoorStone24(stone_trapdoormodel_24);
		registerBlockTrapdoorStone25(stone_trapdoormodel_25);
		registerBlockTrapdoorStone26(stone_trapdoormodel_26);
		registerBlockTrapdoorStone27(stone_trapdoormodel_27);
		registerBlockTrapdoorStone28(stone_trapdoormodel_28);
		registerBlockTrapdoorStone29(stone_trapdoormodel_29);
		registerBlockTrapdoorStone30(stone_trapdoormodel_30);
		registerBlockTrapdoorStone31(stone_trapdoormodel_31);
		registerBlockTrapdoorStone32(stone_trapdoormodel_32);
		registerBlockTrapdoorStone33(stone_trapdoormodel_33);
		registerBlockTrapdoorStone34(stone_trapdoormodel_34);
		registerBlockTrapdoorStone35(stone_trapdoormodel_35);
		registerBlockTrapdoorStone36(stone_trapdoormodel_36);
		registerBlockTrapdoorStone37(stone_trapdoormodel_37);
		registerBlockTrapdoorStone38(stone_trapdoormodel_38);
		registerBlockTrapdoorStone39(stone_trapdoormodel_39);
		registerBlockTrapdoorStone40(stone_trapdoormodel_40);
		registerBlockTrapdoorStone41(stone_trapdoormodel_41);
		registerBlockTrapdoorStone42(stone_trapdoormodel_42);
		registerBlockTrapdoorStone43(stone_trapdoormodel_43);
		registerBlockTrapdoorStone44(stone_trapdoormodel_44);
		registerBlockTrapdoorStone45(stone_trapdoormodel_45);
		registerBlockTrapdoorStone46(stone_trapdoormodel_46);
		registerBlockTrapdoorStone47(stone_trapdoormodel_47);
		registerBlockTrapdoorStone48(stone_trapdoormodel_48);
		registerBlockTrapdoorStone49(stone_trapdoormodel_49);
		registerBlockTrapdoorStone50(stone_trapdoormodel_50);
		registerBlockTrapdoorStone51(stone_trapdoormodel_51);
		registerBlockTrapdoorStone52(stone_trapdoormodel_52);
		registerBlockTrapdoorStone53(stone_trapdoormodel_53);
		registerBlockTrapdoorStone54(stone_trapdoormodel_54);
		registerBlockTrapdoorStone55(stone_trapdoormodel_55);
		registerBlockTrapdoorStone56(stone_trapdoormodel_56);
		registerBlockTrapdoorStone57(stone_trapdoormodel_57);
		registerBlockTrapdoorStone58(stone_trapdoormodel_58);
		registerBlockTrapdoorStone59(stone_trapdoormodel_59);
		registerBlockTrapdoorStone60(stone_trapdoormodel_60);
		registerBlockTrapdoorStone61(stone_trapdoormodel_61);
		registerBlockTrapdoorStone62(stone_trapdoormodel_62);
		registerBlockTrapdoorStone63(stone_trapdoormodel_63);
		registerBlockTrapdoorStone64(stone_trapdoormodel_64);
		registerBlockTrapdoorStone65(stone_trapdoormodel_65);
		registerBlockTrapdoorStone66(stone_trapdoormodel_66);
		registerBlockTrapdoorStone67(stone_trapdoormodel_67);
		registerBlockTrapdoorStone68(stone_trapdoormodel_68);
		registerBlockTrapdoorStone69(stone_trapdoormodel_69);
		registerBlockTrapdoorStone70(stone_trapdoormodel_70);
		registerBlockTrapdoorStone71(stone_trapdoormodel_71);
		registerBlockTrapdoorStone72(stone_trapdoormodel_72);
		registerBlockTrapdoorStone73(stone_trapdoormodel_73);
		registerBlockTrapdoorStone74(stone_trapdoormodel_74);
		registerBlockTrapdoorStone75(stone_trapdoormodel_75);
		registerBlockTrapdoorStone76(stone_trapdoormodel_76);
		registerBlockTrapdoorStone77(stone_trapdoormodel_77);
		registerBlockTrapdoorStone78(stone_trapdoormodel_78);
		registerBlockTrapdoorStone79(stone_trapdoormodel_79);
		registerBlockTrapdoorStone80(stone_trapdoormodel_80);
		registerBlockTrapdoorStone81(stone_trapdoormodel_81);
		registerBlockTrapdoorStone82(stone_trapdoormodel_82);
		registerBlockTrapdoorStone83(stone_trapdoormodel_83);
		registerBlockTrapdoorStone84(stone_trapdoormodel_84);
		registerBlockTrapdoorStone85(stone_trapdoormodel_85);
		registerBlockTrapdoorStone86(stone_trapdoormodel_86);
		registerBlockTrapdoorStone87(stone_trapdoormodel_87);
		registerBlockTrapdoorStone88(stone_trapdoormodel_88);
		registerBlockTrapdoorStone89(stone_trapdoormodel_89);
		registerBlockTrapdoorStone90(stone_trapdoormodel_90);
		registerBlockTrapdoorStone91(stone_trapdoormodel_91);
		registerBlockTrapdoorStone92(stone_trapdoormodel_92);
		registerBlockTrapdoorStone93(stone_trapdoormodel_93);
		registerBlockTrapdoorStone94(stone_trapdoormodel_94);
		registerBlockTrapdoorStone95(stone_trapdoormodel_95);
		registerBlockTrapdoorStone96(stone_trapdoormodel_96);
		registerBlockTrapdoorStone97(stone_trapdoormodel_97);
		registerBlockTrapdoorStone98(stone_trapdoormodel_98);
		registerBlockTrapdoorStone99(stone_trapdoormodel_99);
		registerBlockTrapdoorStone100(stone_trapdoormodel_100);
		registerBlockTrapdoorStone101(stone_trapdoormodel_101);
		registerBlockTrapdoorStone102(stone_trapdoormodel_102);
		registerBlockTrapdoorStone103(stone_trapdoormodel_103);
		registerBlockTrapdoorStone104(stone_trapdoormodel_104);
		registerBlockTrapdoorStone105(stone_trapdoormodel_105);
		registerBlockTrapdoorStone106(stone_trapdoormodel_106);
		registerBlockTrapdoorStone107(stone_trapdoormodel_107);
		registerBlockTrapdoorStone108(stone_trapdoormodel_108);
		registerBlockTrapdoorStone109(stone_trapdoormodel_109);
		registerBlockTrapdoorStone110(stone_trapdoormodel_110);
		registerBlockTrapdoorStone111(stone_trapdoormodel_111);
		registerBlockTrapdoorStone112(stone_trapdoormodel_112);
		registerBlockTrapdoorStone113(stone_trapdoormodel_113);
		registerBlockTrapdoorStone114(stone_trapdoormodel_114);
		registerBlockTrapdoorStone115(stone_trapdoormodel_115);
		registerBlockTrapdoorStone116(stone_trapdoormodel_116);
		registerBlockTrapdoorStone117(stone_trapdoormodel_117);
		registerBlockTrapdoorStone118(stone_trapdoormodel_118);
		registerBlockTrapdoorStone119(stone_trapdoormodel_119);
		registerBlockTrapdoorStone120(stone_trapdoormodel_120);
		registerBlockTrapdoorStone121(stone_trapdoormodel_121);
		registerBlockTrapdoorStone122(stone_trapdoormodel_122);
		registerBlockTrapdoorStone123(stone_trapdoormodel_123);
		registerBlockTrapdoorStone124(stone_trapdoormodel_124);
		registerBlockTrapdoorStone125(stone_trapdoormodel_125);
		registerBlockTrapdoorStone126(stone_trapdoormodel_126);
		registerBlockTrapdoorStone127(stone_trapdoormodel_127);
		registerBlockTrapdoorStone128(stone_trapdoormodel_128);
		registerBlockTrapdoorStone129(stone_trapdoormodel_129);
		registerBlockTrapdoorStone130(stone_trapdoormodel_130);
		registerBlockTrapdoorStone131(stone_trapdoormodel_131);
		registerBlockTrapdoorStone132(stone_trapdoormodel_132);
		registerBlockTrapdoorStone133(stone_trapdoormodel_133);
		registerBlockTrapdoorStone134(stone_trapdoormodel_134);
		registerBlockTrapdoorStone135(stone_trapdoormodel_135);
		registerBlockTrapdoorStone136(stone_trapdoormodel_136);
		registerBlockTrapdoorStone137(stone_trapdoormodel_137);
		registerBlockTrapdoorStone138(stone_trapdoormodel_138);
		registerBlockTrapdoorStone139(stone_trapdoormodel_139);
		registerBlockTrapdoorStone140(stone_trapdoormodel_140);
		registerBlockTrapdoorStone141(stone_trapdoormodel_141);
		registerBlockTrapdoorStone142(stone_trapdoormodel_142);
		registerBlockTrapdoorStone143(stone_trapdoormodel_143);
		registerBlockTrapdoorStone144(stone_trapdoormodel_144);
	//Carpet Stone Blocks
		registerBlockCarpetStone1(stone_carpet_1);
		registerBlockCarpetStone2(stone_carpet_2);
		registerBlockCarpetStone3(stone_carpet_3);
		registerBlockCarpetStone4(stone_carpet_4);
	//Stone Layer Blocks
		registerBlockLayerStone1(stone_layer_1);
		registerBlockLayerStone2(stone_layer_2);
		registerBlockLayerStone3(stone_layer_3);
		registerBlockLayerStone4(stone_layer_4);
		registerBlockLayerStone5(stone_layer_5);
		registerBlockLayerStone6(stone_layer_6);
		registerBlockLayerStone7(stone_layer_7);
		registerBlockLayerStone8(stone_layer_8);
	//End Frame Stone Blocks
		registerBlock(base_marblesandstone_endframe);
		registerBlock(urn_terracotta_endframe);
	//Stone Ladder Blocks
		registerBlock(climbingrocks);
	//Directional No Collision Stone Blocks
		registerBlockDirectionalNoCollisionStone1(stone_directionalnocollision_1);
	//No Collision Stone Blocks
		registerBlockNoCollisionStone1(stone_nocollision_1);
		registerBlockNoCollisionStone2(stone_nocollision_2);
	//Full Wood Blocks
		registerBlockWood1(wood_full_1);
		registerBlockWood2(wood_full_2);
		registerBlockWood3(wood_full_3);
		registerBlockWood4(wood_full_4);
		registerBlockWood5(wood_full_5);
		registerBlockWood6(wood_full_6);
		registerBlockWood7(wood_full_7);
	//Full Wood Blocks
		registerBlockFullSlitWood1(wood_fullslit_1);
		registerBlockFullSlitWood2(wood_fullslit_2);
		registerBlockFullSlitWood3(wood_fullslit_3);
		registerBlockFullSlitWood4(wood_fullslit_4);
		registerBlockFullSlitWood5(wood_fullslit_5);
		registerBlockFullSlitWood6(wood_fullslit_6);
		registerBlockFullSlitWood7(wood_fullslit_7);
	//Wood Hopper Full Blocks
		registerBlockHopperFullWood1(wood_hopperfull_1);
	//Wood Hopper Directional Blocks
		registerBlockHopperDirectionalWood1(wood_hopperdirectional_1);
		registerBlockHopperDirectionalWood2(wood_hopperdirectional_2);
		registerBlockHopperDirectionalWood3(wood_hopperdirectional_3);
		registerBlockHopperDirectionalWood4(wood_hopperdirectional_4);
	//Wood Log Blocks
		registerBlockLogWood1(wood_log_1);
		registerBlockLogWood2(wood_log_2);
		registerBlockLogWood3(wood_log_3);
		registerBlockLogWood4(wood_log_4);
		registerBlockLogWood5(wood_log_5);
		registerBlockLogWood6(wood_log_6);
	//Wood Anvil Blocks
		registerBlockAnvilWood1(wood_anvil_1);
		registerBlockAnvilWood2(wood_anvil_2);
		registerBlockAnvilWood3(wood_anvil_3);
		registerBlockAnvilWood4(wood_anvil_4);
	//Wood Wall Blocks
		registerBlockWallWood1(wood_wall_1);
		registerBlockWallWood2(wood_wall_2);
		registerBlockWallWood3(wood_wall_3);
		registerBlockWallWood4(wood_wall_4);
	//Wood Pillar Blocks
		registerBlockPillarWood1(wood_pillar_1);
		registerBlockPillarWood2(wood_pillar_2);
		registerBlockPillarWood3(wood_pillar_3);
		registerBlockPillarWood4(wood_pillar_4);
	//Fence Wood Blocks
		registerBlockFenceWood1(wood_fence_1);
		registerBlockFenceWood2(wood_fence_2);
		registerBlockFenceWood3(wood_fence_3);
	//Fencegate Wood Blocks
		registerBlockFencegateWood1(wood_fencegate_1);
		registerBlockFencegateWood2(wood_fencegate_2);
		registerBlockFencegateWood3(wood_fencegate_3);
		registerBlockFencegateWood4(wood_fencegate_4);
		registerBlockFencegateWood5(wood_fencegate_5);
		registerBlockFencegateWood6(wood_fencegate_6);
		registerBlockFencegateWood7(wood_fencegate_7);
		registerBlockFencegateWood8(wood_fencegate_8);
		registerBlockFencegateWood9(wood_fencegate_9);
		registerBlockFencegateWood10(wood_fencegate_10);
		registerBlockFencegateWood11(wood_fencegate_11);
		registerBlockFencegateWood12(wood_fencegate_12);
	//Fencegate Wood Blocks
		registerDoorBlock(planks_whiteweathered_door);
		registerDoorBlock(planks_redweathered_door);
		registerDoorBlock(planks_lightredweathered_door);
		registerDoorBlock(planks_orangepainted_door);
		registerDoorBlock(planks_yellowweathered_door);
		registerDoorBlock(planks_greenweathered_door);
		registerDoorBlock(planks_limeweathered_door);
		registerDoorBlock(planks_cyanweathered_door);
		registerDoorBlock(planks_darkblueweathered_door);
		registerDoorBlock(planks_blueweathered_door);
		registerDoorBlock(planks_lightblueweathered_door);
		registerDoorBlock(planks_purplepainted_door);
		registerDoorBlock(planks_brownweathered_door);

	//Small Pillar Wood Blocks
		registerBlockSmallPillarWood1(wood_smallpillar_1);
		registerBlockSmallPillarWood2(wood_smallpillar_2);
		registerBlockSmallPillarWood3(wood_smallpillar_3);
	//Full Partial Wood Blocks
		registerBlockFullPartialWood1(wood_fullpartial_1);
		registerBlockFullPartialWood2(wood_fullpartial_2);
	//ConnectedXZ Wood Blocks
		registerBlockConnectedXZWood1(wood_connectedxz_1);
	//Half Wood Blocks
		registerBlockHalfWood1(wood_half_1);
	//Half Wood Blocks
		registerBlockDaylightDetectorWood1(wood_daylightdetector_1);

	//Stairs Wood Blocks
		registerBlockStairsWood1(wood_stairs_1);
		registerBlockStairsWood2(wood_stairs_2);
		registerBlockStairsWood3(wood_stairs_3);
		registerBlockStairsWood4(wood_stairs_4);
		registerBlockStairsWood5(wood_stairs_5);
		registerBlockStairsWood6(wood_stairs_6);
		registerBlockStairsWood7(wood_stairs_7);
		registerBlockStairsWood8(wood_stairs_8);
		registerBlockStairsWood9(wood_stairs_9);
		registerBlockStairsWood10(wood_stairs_10);
		registerBlockStairsWood11(wood_stairs_11);
		registerBlockStairsWood12(wood_stairs_12);
		registerBlockStairsWood13(wood_stairs_13);
		registerBlockStairsWood14(wood_stairs_14);
		registerBlockStairsWood15(wood_stairs_15);
		registerBlockStairsWood16(wood_stairs_16);
		registerBlockStairsWood17(wood_stairs_17);
		registerBlockStairsWood18(wood_stairs_18);
		registerBlockStairsWood19(wood_stairs_19);
		registerBlockStairsWood20(wood_stairs_20);
		registerBlockStairsWood21(wood_stairs_21);
		registerBlockStairsWood22(wood_stairs_22);
		registerBlockStairsWood23(wood_stairs_23);
		registerBlockStairsWood24(wood_stairs_24);
		registerBlockStairsWood25(wood_stairs_25);
		registerBlockStairsWood26(wood_stairs_26);
		registerBlockStairsWood27(wood_stairs_27);
	//Slab Wood Blocks
		registerBlockSlabWood1(wood_slab_1);
		registerBlockSlabWood2(wood_slab_2);
		registerBlockSlabWood3(wood_slab_3);
		registerBlockSlabWood4(wood_slab_4);
		registerBlockSlabWood5(wood_slab_5);
		registerBlockSlabWood6(wood_slab_6);
		registerBlockSlabWood7(wood_slab_7);
	//Wood Trapdoor Blocks
		registerBlockTrapdoorWood1(wood_trapdoormodel_1);
		registerBlockTrapdoorWood2(wood_trapdoormodel_2);
		registerBlockTrapdoorWood3(wood_trapdoormodel_3);
		registerBlockTrapdoorWood4(wood_trapdoormodel_4);
		registerBlockTrapdoorWood5(wood_trapdoormodel_5);
		registerBlockTrapdoorWood6(wood_trapdoormodel_6);
		registerBlockTrapdoorWood7(wood_trapdoormodel_7);
		registerBlockTrapdoorWood8(wood_trapdoormodel_8);
		registerBlockTrapdoorWood9(wood_trapdoormodel_9);
		registerBlockTrapdoorWood10(wood_trapdoormodel_10);
		registerBlockTrapdoorWood11(wood_trapdoormodel_11);
		registerBlockTrapdoorWood12(wood_trapdoormodel_12);
		registerBlockTrapdoorWood13(wood_trapdoormodel_13);
		registerBlockTrapdoorWood14(wood_trapdoormodel_14);
		registerBlockTrapdoorWood15(wood_trapdoormodel_15);
		registerBlockTrapdoorWood16(wood_trapdoormodel_16);
		registerBlockTrapdoorWood17(wood_trapdoormodel_17);
		registerBlockTrapdoorWood18(wood_trapdoormodel_18);
		registerBlockTrapdoorWood19(wood_trapdoormodel_19);
		registerBlockTrapdoorWood20(wood_trapdoormodel_20);
		registerBlockTrapdoorWood21(wood_trapdoormodel_21);
		registerBlockTrapdoorWood22(wood_trapdoormodel_22);
		registerBlockTrapdoorWood23(wood_trapdoormodel_23);
		registerBlockTrapdoorWood24(wood_trapdoormodel_24);
		registerBlockTrapdoorWood25(wood_trapdoormodel_25);
		registerBlockTrapdoorWood26(wood_trapdoormodel_26);
		registerBlockTrapdoorWood27(wood_trapdoormodel_27);
		registerBlockTrapdoorWood28(wood_trapdoormodel_28);
		registerBlockTrapdoorWood29(wood_trapdoormodel_29);
		registerBlockTrapdoorWood30(wood_trapdoormodel_30);
		registerBlockTrapdoorWood31(wood_trapdoormodel_31);
		registerBlockTrapdoorWood32(wood_trapdoormodel_32);
		registerBlockTrapdoorWood33(wood_trapdoormodel_33);
		registerBlockTrapdoorWood34(wood_trapdoormodel_34);
		registerBlockTrapdoorWood35(wood_trapdoormodel_35);
		registerBlockTrapdoorWood36(wood_trapdoormodel_36);
		registerBlockTrapdoorWood37(wood_trapdoormodel_37);
		registerBlockTrapdoorWood38(wood_trapdoormodel_38);
		registerBlockTrapdoorWood39(wood_trapdoormodel_39);
	//Wood Vertical Slab Blocks
		registerBlockVerticalSlabWood1(wood_verticalslab_1);
		registerBlockVerticalSlabWood2(wood_verticalslab_2);
		registerBlockVerticalSlabWood3(wood_verticalslab_3);
		registerBlockVerticalSlabWood4(wood_verticalslab_4);
		registerBlockVerticalSlabWood5(wood_verticalslab_5);
		registerBlockVerticalSlabWood6(wood_verticalslab_6);
		registerBlockVerticalSlabWood7(wood_verticalslab_7);
		registerBlockVerticalSlabWood8(wood_verticalslab_8);
		registerBlockVerticalSlabWood9(wood_verticalslab_9);
		registerBlockVerticalSlabWood10(wood_verticalslab_10);
		registerBlockVerticalSlabWood11(wood_verticalslab_11);
		registerBlockVerticalSlabWood12(wood_verticalslab_12);
		registerBlockVerticalSlabWood13(wood_verticalslab_13);
		registerBlockVerticalSlabWood14(wood_verticalslab_14);
		registerBlockVerticalSlabWood15(wood_verticalslab_15);
		registerBlockVerticalSlabWood16(wood_verticalslab_16);
		registerBlockVerticalSlabWood17(wood_verticalslab_17);
		registerBlockVerticalSlabWood18(wood_verticalslab_18);
		registerBlockVerticalSlabWood19(wood_verticalslab_19);
	//Wood Corner Blocks
		registerBlockCornerWood1(wood_corner_1);
		registerBlockCornerWood2(wood_corner_2);
		registerBlockCornerWood3(wood_corner_3);
		registerBlockCornerWood4(wood_corner_4);
		registerBlockCornerWood5(wood_corner_5);
		registerBlockCornerWood6(wood_corner_6);
		registerBlockCornerWood7(wood_corner_7);
		registerBlockCornerWood8(wood_corner_8);
		registerBlockCornerWood9(wood_corner_9);
		registerBlockCornerWood10(wood_corner_10);
		registerBlockCornerWood11(wood_corner_11);
		registerBlockCornerWood12(wood_corner_12);
		registerBlockCornerWood13(wood_corner_13);
		registerBlockCornerWood14(wood_corner_14);
		registerBlockCornerWood15(wood_corner_15);
		registerBlockCornerWood16(wood_corner_16);
		registerBlockCornerWood17(wood_corner_17);
		registerBlockCornerWood18(wood_corner_18);
		registerBlockCornerWood19(wood_corner_19);

	//Wood Functional Trapdoor Blocks
		registerBlock(woodenboard);
		registerBlock(wheel);
	//Wood Shutters Blocks
		registerBlock(shutters_light);
		registerBlock(shutters_dark);
	//Wood Trapdoor Rail Blocks
		registerBlock(wood_railing);
		registerBlock(wood_railing_straight);
		registerBlock(spruce_railing);
		registerBlock(spruce_railing_straight);
		registerBlock(birch_railing);
		registerBlock(birch_railing_straight);
		registerBlock(jungle_railing);
		registerBlock(jungle_railing_straight);
		registerBlock(acacia_railing);
		registerBlock(acacia_railing_straight);
	//Wood Carpet Blocks
		registerBlockCarpetWood1(wood_carpet_1);
	//Wood Ironbar Blocks
		registerBlockIronbarWood1(wood_ironbar_1);
	//Wood Ladder Blocks
		registerBlock(ladder);
	//No Collision Wood Blocks
		registerBlockNoCollisionWood1(wood_nocollision_1);
	//No Collision Damage Wood Blocks
		registerBlock(wood_nocollisiondamage_1);
	//Directional No Collision Wood Blocks
		registerBlockDirectionalNoCollisionWood1(wood_directionalnocollision_1);
	//Directional Collision Trapdoor Wood Blocks
		registerBlockDirectionalCollisionTrapdoorWood1(wood_directionalcollisiontrapdoor_1);
		registerBlockDirectionalCollisionTrapdoorWood2(wood_directionalcollisiontrapdoor_2);
		registerBlockDirectionalCollisionTrapdoorWood3(wood_directionalcollisiontrapdoor_3);
	//Bed Wood Blocks
		registerBedBlock(bed_wolf);
		registerBedBlock(bed_rustic);
		registerBedBlock(bed_bear);
		registerBedBlock(bed_fancygreen);
	//Cauldron Wood Blocks
		registerBlock(barrel_grille_cauldron);
		registerBlock(barrel_cauldron);
	//Enchanted Wood Blocks
		registerBlockEnchantedWood1(wood_enchantedbook_1);
	//Wood Rail Blocks
		registerBlock(jungle_rail);
		registerBlock(spruce_floor_rail);
	//Full Iron Blocks
		registerBlockIron1(iron_full_1);
	//Full Partial Light10 Iron Blocks
		registerBlockFullPartialLight1Iron1(iron_fullpartiallight10_1);
	//Iron Anvil Blocks
		registerBlockAnvilIron1(iron_anvil_1);
	//Iron Ironbar Blocks
		registerBlockIronbarIron1(iron_ironbar_1);
	//Stairs Iron Blocks
		registerBlockStairsIron1(iron_stairs_1);
		registerBlockStairsIron2(iron_stairs_2);
	//Iron Trapdoor Blocks
		registerBlockTrapdoorIron1(iron_trapdoormodel_1);
		registerBlockTrapdoorIron2(iron_trapdoormodel_2);
	//Iron Trapdoor Railing Blocks
		registerBlock(iron_railing);
		registerBlock(iron_railing_straight);
	//Cauldron Iron Blocks
		registerBlock(cauldron_irongrille);
		registerBlock(cauldron);
	//Brewingstand Iron Blocks
		registerBlock(iron_brewingstand_1);
		registerBlock(iron_brewingstandlight10_1);
	//Directional No Collision Iron Blocks
		registerBlockDirectionalNoCollisionIron1(iron_directionalnocollision_1);
		registerBlockDirectionalNoCollisionIron2(iron_directionalnocollision_2);
	//Iron Rail Blocks
		registerBlock(ironchains_rail);
	//Plants Log Blocks
		registerBlockLogPlants1(plants_log_1);
	//Stairs Plants Blocks
		registerBlockStairsPlants1(plants_stairs_1);
	//Slabs Plants Blocks
		registerBlockSlabPlants1(plants_slab_1);
	//No Collision Plants Blocks
		registerBlockNoCollisionPlants1(plants_nocollision_1);
		registerBlockNoCollisionPlants2(plants_nocollision_2);
		registerBlockNoCollisionPlants3(plants_nocollision_3);
		registerBlockNoCollisionPlants4(plants_nocollision_4);
		registerBlockNoCollisionPlants5(plants_nocollision_5);
	//No Collision Biome Plants Blocks
		registerBlockNoCollisionBiomePlants1(plants_nocollisionbiome_1);
	//Lilypad Plants Blocks
		registerLilypadBlock(plants_lilypad_1);
		registerLilypadBlock(plants_lilypad_2);
	//Full Leaves Blocks
		registerBlockLeaves1(leaves_full_1);
	//Vine Vine Blocks
		registerBlock(vine_jungle);
		registerBlock(vine_ivy);
		registerBlock(vine_moss);
	//Snowlayer Biome Grass Blocks
		registerSnowlayerBlock(grass_snowlayer_1);
	//Slab Cloth Blocks
		registerBlockSlabCloth1(cloth_slab_1);
		registerBlockSlabCloth2(cloth_slab_2);
	//Pane Cloth Blocks
		registerBlockPaneCloth1(cloth_pane_1);
	//Climbable Iron Bar Cloth Blocks
		registerBlockClimbIronbarCloth1(cloth_climbironbar_1);
	//Climbable Iron Bar Iron/Cloth Blocks
		registerBlockIronClimbIronbarCloth1(cloth_ironclimbironbar_1);
	//Vine Cloth Blocks
		registerBlock(curtain_black_vine);
		registerBlock(curtain_blue_vine);
		registerBlock(curtain_brown_vine);
		registerBlock(curtain_green_vine);
		registerBlock(curtain_purple_vine);
		registerBlock(curtain_red_vine);
		registerBlock(curtain_white_vine);
	//Full Cloth Light 1.0 Blocks
		registerBlockLight1Cloth1(cloth_fulllight10_1);
	//Cloth Ladder Blocks
		registerBlock(rope_ladder);
		registerBlock(rope_climbing_ladder);
	//No Collision Cloth Blocks
		registerBlockNoCollisionCloth1(cloth_nocollision_1);
	//No Collision Cloth Light 1.0 Blocks
		registerBlock(cloth_nocollisionlight10_1);
	//Directional No Collision Cloth Blocks
		registerBlockDirectionalNoCollisionCloth1(cloth_directionalnocollision_1);
	//Cloth Rail Blocks
		registerBlock(rope_rail);
	//Full Sand Blocks
		registerBlockSand1(sand_full_1);
	//Sand Layer Blocks
		registerBlockLayerSand1(sand_layer_1);
		registerBlockLayerSand2(sand_layer_2);
		registerBlockLayerSand3(sand_layer_3);
		registerBlockLayerSand4(sand_layer_4);
		registerBlockLayerSand5(sand_layer_5);
		registerBlockLayerSand6(sand_layer_6);
	//Full Ground Blocks
		registerBlockGround1(ground_full_1);
		registerBlockGround2(ground_full_2);
	//Soulsand Ground Blocks
		registerBlockSoulsandGround1(ground_soulsand_1);
	//Directional Full Partial Ground Blocks
		registerBlockDirectionalFullPartialGround1(ground_directionalfullpartial_1);
	//Ground Layer Blocks
		registerBlockLayerGround1(ground_layer_1);
		registerBlockLayerGround2(ground_layer_2);
		registerBlockLayerGround3(ground_layer_3);
		registerBlockLayerGround4(ground_layer_4);
		registerBlockLayerGround5(ground_layer_5);
		registerBlockLayerGround6(ground_layer_6);
		registerBlockLayerGround7(ground_layer_7);
		registerBlockLayerGround8(ground_layer_8);
		registerBlockLayerGround9(ground_layer_9);
		registerBlockLayerGround10(ground_layer_10);
		registerBlockLayerGround11(ground_layer_11);
		registerBlockLayerGround12(ground_layer_12);
		registerBlockLayerGround13(ground_layer_13);
	//Translucent Ice Blocks
		registerBlockTranslucentIce1(ice_translucent_1);
	//Slab Ice Blocks
		registerBlockSlabIce1(ice_slab_1);
	//Translucent No Collision Ice Blocks
		registerBlock(ice_translucentnocollision_1);
	//No Collision Ice Blocks
		registerBlockNoCollisionIce1(ice_nocollision_1);
	//No Collision Ice Blocks
		registerBlock(ice_nocollisiondamage_1);
	//Lilypad Ice Blocks
		registerLilypadBlock(ice_lilypad_1);
	//Stairs Snow Blocks
		registerBlockStairsSnow1(snow_stairs_1);
	//No Collision No Material Blocks
		registerBlockNoCollisionNoMaterial1(nomaterial_nocollision_1);
	//No Collision No Material Light 0.5 Blocks
		registerBlock(nomaterial_nocollisionlight5_1);
	//No Collision No Material Light 0.6 Blocks
		registerBlock(invisible_light6);
	//No Collision No Material Light 0.8 Blocks
		registerBlock(invisible_light8);
	//No Collision No Material Light 1.0 Blocks
		registerBlock(invisible_light10);
	//Glass Glass Blocks
		registerBlockFullGlass1(glass_full_1);
	//Glass Glass Blocks
		registerBlockGlass1(glass_glass_1);
		registerBlockGlass2(glass_glass_2);
	//Glass Vertical Slab Blocks
		registerBlockVerticalSlabGlass1(glass_verticalslab_1);
	//Glass Trapdoor Blocks
		registerBlockTrapdoorGlass1(glass_trapdoormodel_1);
		registerBlockTrapdoorGlass2(glass_trapdoormodel_2);
		registerBlockTrapdoorGlass3(glass_trapdoormodel_3);
		registerBlockTrapdoorGlass4(glass_trapdoormodel_4);
	//Glass Pane Blocks
		registerBlockPaneGlass1(glass_pane_1);
		registerBlockPaneGlass2(glass_pane_2);
	//Cake Cake Blocks
		registerBlock(cake_cheesewheel);
		registerBlock(cake_applepie);
		registerBlock(cake_icingfruit);
		registerBlock(cake_icingchocolate);
		registerBlock(cake_bread);
	//Furnace Blocks
		registerBlock(furnace_iron);
		registerBlock(furnace_sandstone);
	//Lit Furnace Blocks
		registerBlock(furnace_ironlit);
		registerBlock(furnace_cobblelit);
		registerBlock(furnace_sandstonelit);
	//Dispenser Blocks
		registerBlock(dispenser_aztec);
		registerBlock(dispenser_iron);
		registerBlock(dispenser_egyptian);
	//Dropper Blocks
		registerBlock(dropper_sandstone);
		registerBlock(dropper_iron);
	//Flower Pot Blocks
		registerBlock(flowerpot);
		registerBlock(flowerpot_black);
	//Button Blocks
		registerBlock(wood_button);
		registerBlock(stone_button);
	//Lever Blocks
		registerBlock(lever);
	//Hook Blocks
		registerBlock(hook);
	}

	public static void registerRenders()
	{
	/**
		registerRender(itemblock static name, meta value, .json name in models/item) -- This registers item render for each meta value  of block.
	 	*Naming convention for ".json name in models/items" = blockname_material/color/type_blockmodel
		You should create a new registerBlock void before this entries.
	*/
	//Full Stone Blocks
		registerRenderMetas(itemblock_stone_full_1, 0, "conquest:stone_plastered_full");
		registerRenderMetas(itemblock_stone_full_1, 1, "conquest:stone_plasteredslate_full");
		registerRenderMetas(itemblock_stone_full_1, 2, "conquest:stone_hewn_full");
		registerRenderMetas(itemblock_stone_full_1, 3, "conquest:stone_hewnstonebrickcorner_full");
		registerRenderMetas(itemblock_stone_full_1, 4, "conquest:stone_slate_full");
		registerRenderMetas(itemblock_stone_full_1, 5, "conquest:stone_colorfulslate_full");
		registerRenderMetas(itemblock_stone_full_1, 6, "conquest:stone_germanicglyphs_full");
		registerRenderMetas(itemblock_stone_full_1, 7, "conquest:stone_germanicglyphs1_full");
		registerRenderMetas(itemblock_stone_full_1, 8, "conquest:cobblestone_old_full");
		registerRenderMetas(itemblock_stone_full_1, 9, "conquest:cobblestone_damaged_full");
		registerRenderMetas(itemblock_stone_full_1, 10, "conquest:cobblestone_fishscale_full");
		registerRenderMetas(itemblock_stone_full_1, 11, "conquest:cobblestone_fishscaledirty_full");
		registerRenderMetas(itemblock_stone_full_1, 12, "conquest:cobblestone_brickcorner_full");
		registerRenderMetas(itemblock_stone_full_1, 13, "conquest:lapislazuli_stonebrickcorner_full");
		registerRenderMetas(itemblock_stone_full_1, 14, "conquest:cobblestone_frozen_full");
		registerRenderMetas(itemblock_stone_full_1, 15, "conquest:cobblestone_mossyallsides_full");
		registerRenderMetas(itemblock_stone_full_2, 0, "conquest:cobblestone_overgrown_full");
		registerRenderMetas(itemblock_stone_full_2, 1, "conquest:cobblestone_vines_full");
		registerRenderMetas(itemblock_stone_full_2, 2, "conquest:stonebrick_overgrown_full");
		registerRenderMetas(itemblock_stone_full_2, 3, "conquest:stonebrick_chiseled_full");
		registerRenderMetas(itemblock_stone_full_2, 4, "conquest:cobblestone_stone_full");
		registerRenderMetas(itemblock_stone_full_2, 5, "conquest:cobblestone_reinforced_full");
		registerRenderMetas(itemblock_stone_full_2, 6, "conquest:cobblestone_reinforced_full");
		registerRenderMetas(itemblock_stone_full_2, 7, "conquest:none"); //none
		registerRenderMetas(itemblock_stone_full_2, 8, "conquest:groove_portculis_full");
		registerRenderMetas(itemblock_stone_full_2, 9, "conquest:stone_chiseled_full");
		registerRenderMetas(itemblock_stone_full_2, 10, "conquest:pillar_stone_full");
		registerRenderMetas(itemblock_stone_full_2, 11, "conquest:netherbrick_normal_full");
		registerRenderMetas(itemblock_stone_full_2, 12, "conquest:netherbrick_big_full");
		registerRenderMetas(itemblock_stone_full_2, 13, "conquest:netherbrick_pillar_full");
		registerRenderMetas(itemblock_stone_full_2, 14, "conquest:netherbrick_carved_full");
		registerRenderMetas(itemblock_stone_full_2, 15, "conquest:none"); //open slot
		registerRenderMetas(itemblock_stone_full_3, 0, "conquest:tiles_red_full");
		registerRenderMetas(itemblock_stone_full_3, 1, "conquest:tudorframe_slash_full");
		registerRenderMetas(itemblock_stone_full_3, 2, "conquest:tudorframe_backslash_full");
		registerRenderMetas(itemblock_stone_full_3, 3, "conquest:tudorframe_x_full");
		registerRenderMetas(itemblock_stone_full_3, 4, "conquest:tudorframe_up_full");
		registerRenderMetas(itemblock_stone_full_3, 5, "conquest:tudorframe_down_full");
		registerRenderMetas(itemblock_stone_full_3, 6, "conquest:tudorframe_box_full");
		registerRenderMetas(itemblock_stone_full_3, 7, "conquest:tudorframe_slash_full");
		registerRenderMetas(itemblock_stone_full_3, 8, "conquest:tudorframe_backslash_full");
		registerRenderMetas(itemblock_stone_full_3, 9, "conquest:tudorframe_box_full");
		registerRenderMetas(itemblock_stone_full_3, 10, "conquest:tudorframe_darkslash_full");
		registerRenderMetas(itemblock_stone_full_3, 11, "conquest:tudorframe_darkbackslash_full");
		registerRenderMetas(itemblock_stone_full_3, 12, "conquest:tudorframe_darkx_full");
		registerRenderMetas(itemblock_stone_full_3, 13, "conquest:tudorframe_darkup_full");
		registerRenderMetas(itemblock_stone_full_3, 14, "conquest:tudorframe_darkdown_full");
		registerRenderMetas(itemblock_stone_full_3, 15, "conquest:tudorframe_darkbox_full");
		registerRenderMetas(itemblock_stone_full_4, 0, "conquest:tudorframe_darkslash_full");
		registerRenderMetas(itemblock_stone_full_4, 1, "conquest:tudorframe_darkbackslash_full");
		registerRenderMetas(itemblock_stone_full_4, 2, "conquest:tudorframe_darkbox_full");
		registerRenderMetas(itemblock_stone_full_4, 3, "conquest:tudorframe_brickslash_full");
		registerRenderMetas(itemblock_stone_full_4, 4, "conquest:tudorframe_brickbackslash_full");
		registerRenderMetas(itemblock_stone_full_4, 5, "conquest:tudorframe_brickx_full");
		registerRenderMetas(itemblock_stone_full_4, 6, "conquest:tudorframe_brickup_full");
		registerRenderMetas(itemblock_stone_full_4, 7, "conquest:tudorframe_brickdown_full");
		registerRenderMetas(itemblock_stone_full_4, 8, "conquest:tudorframe_brickbox_full");
		registerRenderMetas(itemblock_stone_full_4, 9, "conquest:tudorframe_brickslash_full");
		registerRenderMetas(itemblock_stone_full_4, 10, "conquest:tudorframe_brickbackslash_full");
		registerRenderMetas(itemblock_stone_full_4, 11, "conquest:tudorframe_brickbox_full");
		registerRenderMetas(itemblock_stone_full_4, 12, "conquest:tudorframe_darkbrickslash_full");
		registerRenderMetas(itemblock_stone_full_4, 13, "conquest:tudorframe_darkbrickbackslash_full");
		registerRenderMetas(itemblock_stone_full_4, 14, "conquest:tudorframe_darkbrickx_full");
		registerRenderMetas(itemblock_stone_full_4, 15, "conquest:tudorframe_darkbrickup_full");
		registerRenderMetas(itemblock_stone_full_5, 0, "conquest:tudorframe_darkbrickdown_full");
		registerRenderMetas(itemblock_stone_full_5, 1, "conquest:tudorframe_darkbrickbox_full");
		registerRenderMetas(itemblock_stone_full_5, 2, "conquest:tudorframe_darkbrickslash_full");
		registerRenderMetas(itemblock_stone_full_5, 3, "conquest:tudorframe_darkbrickbackslash_full");
		registerRenderMetas(itemblock_stone_full_5, 4, "conquest:tudorframe_darkbrickbox_full");
		registerRenderMetas(itemblock_stone_full_5, 5, "conquest:tudorframe_redbrickslash_full");
		registerRenderMetas(itemblock_stone_full_5, 6, "conquest:tudorframe_redbrickbackslash_full");
		registerRenderMetas(itemblock_stone_full_5, 7, "conquest:tudorframe_redbrickx_full");
		registerRenderMetas(itemblock_stone_full_5, 8, "conquest:tudorframe_redbrickup_full");
		registerRenderMetas(itemblock_stone_full_5, 9, "conquest:tudorframe_redbrickdown_full");
		registerRenderMetas(itemblock_stone_full_5, 10, "conquest:tudorframe_redbrickbox_full");
		registerRenderMetas(itemblock_stone_full_5, 11, "conquest:tudorframe_redbrickslash_full");
		registerRenderMetas(itemblock_stone_full_5, 12, "conquest:tudorframe_redbrickbackslash_full");
		registerRenderMetas(itemblock_stone_full_5, 13, "conquest:tudorframe_redbrickbox_full");
		registerRenderMetas(itemblock_stone_full_5, 14, "conquest:brick_2_full");
		registerRenderMetas(itemblock_stone_full_5, 15, "conquest:brick_mossy_full");
		registerRenderMetas(itemblock_stone_full_6, 0, "conquest:brick_darkred_full");
		registerRenderMetas(itemblock_stone_full_6, 1, "conquest:brick_darkredmossy_full");
		registerRenderMetas(itemblock_stone_full_6, 2, "conquest:brick_red_full");
		registerRenderMetas(itemblock_stone_full_6, 3, "conquest:brick_redmossy_full");
		registerRenderMetas(itemblock_stone_full_6, 4, "conquest:brick_roman_full");
		registerRenderMetas(itemblock_stone_full_6, 5, "conquest:brick_diorite_full");
		registerRenderMetas(itemblock_stone_full_6, 6, "conquest:brick_travertine_full");
		registerRenderMetas(itemblock_stone_full_6, 7, "conquest:brick_sandstonebig_full");
		registerRenderMetas(itemblock_stone_full_6, 8, "conquest:sandstone_bluemarble_full");
		registerRenderMetas(itemblock_stone_full_6, 9, "conquest:sandstone_bluemarble1_full");
		registerRenderMetas(itemblock_stone_full_6, 10, "conquest:sandstone_chiseled_full");
		registerRenderMetas(itemblock_stone_full_6, 11, "conquest:sandstone_conglomerate_full");
		registerRenderMetas(itemblock_stone_full_6, 12, "conquest:sandstone_mossy_full");
		registerRenderMetas(itemblock_stone_full_6, 13, "conquest:frieze_sandstone_full");
		registerRenderMetas(itemblock_stone_full_6, 14, "conquest:sandstone_inscribed_full");
		registerRenderMetas(itemblock_stone_full_6, 15, "conquest:brick_sandstone_full");
		registerRenderMetas(itemblock_stone_full_7, 0, "conquest:capital_sandstone_full");
		registerRenderMetas(itemblock_stone_full_7, 1, "conquest:base_sandstone_full");
		registerRenderMetas(itemblock_stone_full_7, 2, "conquest:capital_redmarble_full");
		registerRenderMetas(itemblock_stone_full_7, 3, "conquest:base_redmarble_full");
		registerRenderMetas(itemblock_stone_full_7, 4, "conquest:capital_redsandstone_full");
		registerRenderMetas(itemblock_stone_full_7, 5, "conquest:base_redsandstone_full");
		registerRenderMetas(itemblock_stone_full_7, 6, "conquest:capital_bluemarble_full");
		registerRenderMetas(itemblock_stone_full_7, 7, "conquest:base_bluemarble_full");
		registerRenderMetas(itemblock_stone_full_7, 8, "conquest:capital_bluesandstone_full");
		registerRenderMetas(itemblock_stone_full_7, 9, "conquest:base_bluesandstone_full");
		registerRenderMetas(itemblock_stone_full_7, 10, "conquest:capital_goldmarble_full");
		registerRenderMetas(itemblock_stone_full_7, 11, "conquest:base_goldmarble_full");
		registerRenderMetas(itemblock_stone_full_7, 12, "conquest:capital_goldsandstone_full");
		registerRenderMetas(itemblock_stone_full_7, 13, "conquest:base_goldsandstone_full");
		registerRenderMetas(itemblock_stone_full_7, 14, "conquest:capital_marble_full");
		registerRenderMetas(itemblock_stone_full_7, 15, "conquest:base_marble_full");
		registerRenderMetas(itemblock_stone_full_8, 0, "conquest:capitalcorinthian_full");
		registerRenderMetas(itemblock_stone_full_8, 1, "conquest:capitalcorinthian_sandstone_full");
		registerRenderMetas(itemblock_stone_full_8, 2, "conquest:cornice_sandstone_full");
		registerRenderMetas(itemblock_stone_full_8, 3, "conquest:plinth_sandstone_full");
		registerRenderMetas(itemblock_stone_full_8, 4, "conquest:base_marblesandstone_full");
		registerRenderMetas(itemblock_stone_full_8, 5, "conquest:cornice_marble_full");
		registerRenderMetas(itemblock_stone_full_8, 6, "conquest:baseplain_marble_full");
		registerRenderMetas(itemblock_stone_full_8, 7, "conquest:marble_full");
		registerRenderMetas(itemblock_stone_full_8, 8, "conquest:marble_smoothwhite_full");
		registerRenderMetas(itemblock_stone_full_8, 9, "conquest:base_parianmarble_full");
		registerRenderMetas(itemblock_stone_full_8, 10, "conquest:capital_parianmarble_full");
		registerRenderMetas(itemblock_stone_full_8, 11, "conquest:engraved_a_full");
		registerRenderMetas(itemblock_stone_full_8, 12, "conquest:engraved_b_full");
		registerRenderMetas(itemblock_stone_full_8, 13, "conquest:engraved_c_full");
		registerRenderMetas(itemblock_stone_full_8, 14, "conquest:engraved_d_full");
		registerRenderMetas(itemblock_stone_full_8, 15, "conquest:engraved_e_full");
		registerRenderMetas(itemblock_stone_full_9, 0, "conquest:engraved_f_full");
		registerRenderMetas(itemblock_stone_full_9, 1, "conquest:engraved_g_full");
		registerRenderMetas(itemblock_stone_full_9, 2, "conquest:engraved_h_full");
		registerRenderMetas(itemblock_stone_full_9, 3, "conquest:engraved_i_full");
		registerRenderMetas(itemblock_stone_full_9, 4, "conquest:engraved_interpunct_full");
		registerRenderMetas(itemblock_stone_full_9, 5, "conquest:engraved_k_full");
		registerRenderMetas(itemblock_stone_full_9, 6, "conquest:engraved_l_full");
		registerRenderMetas(itemblock_stone_full_9, 7, "conquest:engraved_m_full");
		registerRenderMetas(itemblock_stone_full_9, 8, "conquest:engraved_n_full");
		registerRenderMetas(itemblock_stone_full_9, 9, "conquest:engraved_o_full");
		registerRenderMetas(itemblock_stone_full_9, 10, "conquest:engraved_p_full");
		registerRenderMetas(itemblock_stone_full_9, 11, "conquest:engraved_q_full");
		registerRenderMetas(itemblock_stone_full_9, 12, "conquest:engraved_r_full");
		registerRenderMetas(itemblock_stone_full_9, 13, "conquest:engraved_s_full");
		registerRenderMetas(itemblock_stone_full_9, 14, "conquest:engraved_t_full");
		registerRenderMetas(itemblock_stone_full_9, 15, "conquest:engraved_v_full");
		registerRenderMetas(itemblock_stone_full_10, 0, "conquest:bigslab_marble_full");
		registerRenderMetas(itemblock_stone_full_10, 1, "conquest:bigslab_marble1_full");
		registerRenderMetas(itemblock_stone_full_10, 2, "conquest:bigslab_sandstone_full");
		registerRenderMetas(itemblock_stone_full_10, 3, "conquest:bigslab_sandstone1_full");
		registerRenderMetas(itemblock_stone_full_10, 4, "conquest:bigslab_sandstoneinscribed_full");
		registerRenderMetas(itemblock_stone_full_10, 5, "conquest:bigslab_sandstoneinscribed1_full");
		registerRenderMetas(itemblock_stone_full_10, 6, "conquest:bigslab_black_full");
		registerRenderMetas(itemblock_stone_full_10, 7, "conquest:bigslab_black1_full");
		registerRenderMetas(itemblock_stone_full_10, 8, "conquest:bigslab_blackinscribed_full");
		registerRenderMetas(itemblock_stone_full_10, 9, "conquest:bigslab_blackinscribed1_full");
		registerRenderMetas(itemblock_stone_full_10, 10, "conquest:mosaic_roman_full");
		registerRenderMetas(itemblock_stone_full_10, 11, "conquest:mosaic_indian_full");
		registerRenderMetas(itemblock_stone_full_10, 12, "conquest:mosaic_andalusian_full");
		registerRenderMetas(itemblock_stone_full_10, 13, "conquest:stucco_full");
		registerRenderMetas(itemblock_stone_full_10, 14, "conquest:stucco_white_full");
		registerRenderMetas(itemblock_stone_full_10, 15, "conquest:stucco_magentaclean_full");
		registerRenderMetas(itemblock_stone_full_11, 0, "conquest:stucco_lightgrayclean_full");
		registerRenderMetas(itemblock_stone_full_11, 1, "conquest:stucco_yellowclean_full");
		registerRenderMetas(itemblock_stone_full_11, 2, "conquest:stucco_whiteclean_full");
		registerRenderMetas(itemblock_stone_full_11, 3, "conquest:stucco_purpleclean_full");
		registerRenderMetas(itemblock_stone_full_11, 4, "conquest:stone_orange_full");
		registerRenderMetas(itemblock_stone_full_11, 5, "conquest:mosaic_decorative_full");
		registerRenderMetas(itemblock_stone_full_11, 6, "conquest:walldesign_white1_full");
		registerRenderMetas(itemblock_stone_full_11, 7, "conquest:walldesign_white2_full");
		registerRenderMetas(itemblock_stone_full_11, 8, "conquest:walldesign_white3_full");
		registerRenderMetas(itemblock_stone_full_11, 9, "conquest:walldesign_white4_full");
		registerRenderMetas(itemblock_stone_full_11, 10, "conquest:walldesign_white5_full");
		registerRenderMetas(itemblock_stone_full_11, 11, "conquest:walldesign_white6_full");
		registerRenderMetas(itemblock_stone_full_11, 12, "conquest:stucco_white_wall_full");
		registerRenderMetas(itemblock_stone_full_11, 13, "conquest:stucco_tan_wall_full");
		registerRenderMetas(itemblock_stone_full_11, 14, "conquest:walldesign_brown_full");
		registerRenderMetas(itemblock_stone_full_11, 15, "conquest:stucco_brown_wall_full");
		registerRenderMetas(itemblock_stone_full_12, 0, "conquest:stucco_purple_wall_full");
		registerRenderMetas(itemblock_stone_full_12, 1, "conquest:walldesign_magenta_full");
		registerRenderMetas(itemblock_stone_full_12, 2, "conquest:stucco_magenta_full");
		registerRenderMetas(itemblock_stone_full_12, 3, "conquest:walldesign_red_full");
		registerRenderMetas(itemblock_stone_full_12, 4, "conquest:walldesign_red2_full");
		registerRenderMetas(itemblock_stone_full_12, 5, "conquest:walldesign_red3_full");
		registerRenderMetas(itemblock_stone_full_12, 6, "conquest:walldesign_red4_full");
		registerRenderMetas(itemblock_stone_full_12, 7, "conquest:walldesign_red5_full");
		registerRenderMetas(itemblock_stone_full_12, 8, "conquest:walldesign_red6_full");
		registerRenderMetas(itemblock_stone_full_12, 9, "conquest:walldesign_red7_full");
		registerRenderMetas(itemblock_stone_full_12, 10, "conquest:walldesign_red8_full");
		registerRenderMetas(itemblock_stone_full_12, 11, "conquest:stucco_lightred_full");
		registerRenderMetas(itemblock_stone_full_12, 12, "conquest:walldesign_orange_full");
		registerRenderMetas(itemblock_stone_full_12, 13, "conquest:walldesign_orange1_full");
		registerRenderMetas(itemblock_stone_full_12, 14, "conquest:stucco_lime_full");
		registerRenderMetas(itemblock_stone_full_12, 15, "conquest:walldesign_green_full");
		registerRenderMetas(itemblock_stone_full_13, 0, "conquest:stucco_green_full");
		registerRenderMetas(itemblock_stone_full_13, 1, "conquest:stucco_cyan_full");
		registerRenderMetas(itemblock_stone_full_13, 2, "conquest:stucco_lightblue_full");
		registerRenderMetas(itemblock_stone_full_13, 3, "conquest:stucco_blue_full");
		registerRenderMetas(itemblock_stone_full_13, 4, "conquest:stucco_black_full");
		registerRenderMetas(itemblock_stone_full_13, 5, "conquest:door_ornateroman_full");
		registerRenderMetas(itemblock_stone_full_13, 6, "conquest:door_ornateroman_full");
		registerRenderMetas(itemblock_stone_full_13, 7, "conquest:bronzeblock_gilded_full");
		registerRenderMetas(itemblock_stone_full_13, 8, "conquest:bronzeblock_fancy_full");
		registerRenderMetas(itemblock_stone_full_13, 9, "conquest:goldblock_bars_full");
		registerRenderMetas(itemblock_stone_full_13, 10, "conquest:iceblock_normal_full");
		registerRenderMetas(itemblock_stone_full_13, 11, "conquest:bones_full");
		registerRenderMetas(itemblock_stone_full_13, 12, "conquest:skeletons_full");
		registerRenderMetas(itemblock_stone_full_13, 13, "conquest:coral_red_full");
		registerRenderMetas(itemblock_stone_full_13, 14, "conquest:coral_yellow_full");
		registerRenderMetas(itemblock_stone_full_13, 15, "conquest:coral_blue_full");
		registerRenderMetas(itemblock_stone_full_14, 0, "conquest:rooftile_red_full");
		registerRenderMetas(itemblock_stone_full_14, 1, "conquest:rooftile_gray_full");
		registerRenderMetas(itemblock_stone_full_14, 2, "conquest:rooftile_brown_full");
		registerRenderMetas(itemblock_stone_full_14, 3, "conquest:oxidized_copper_full");
		registerRenderMetas(itemblock_stone_full_14, 4, "conquest:stone_icy_full");
		registerRenderMetas(itemblock_stone_full_14, 5, "conquest:bone_full");
		registerRenderMetas(itemblock_stone_full_14, 6, "conquest:stone_mossy_full");
		registerRenderMetas(itemblock_stone_full_14, 7, "conquest:stone_mossytop_full");
		registerRenderMetas(itemblock_stone_full_14, 8, "conquest:slate_wet_full");
		registerRenderMetas(itemblock_stone_full_14, 9, "conquest:slate_full");
		registerRenderMetas(itemblock_stone_full_14, 10, "conquest:marble_uncut_full");
		registerRenderMetas(itemblock_stone_full_14, 11, "conquest:granite_graypink_full");
		registerRenderMetas(itemblock_stone_full_14, 12, "conquest:sandstone_natural_full");
		registerRenderMetas(itemblock_stone_full_14, 13, "conquest:stone_mesalightbrown_full");
		registerRenderMetas(itemblock_stone_full_14, 14, "conquest:pahoehoe_full");
		registerRenderMetas(itemblock_stone_full_14, 15, "conquest:clay_drylightbrown_full");
		registerRenderMetas(itemblock_stone_full_15, 0, "conquest:clay_drybrown_full");
		registerRenderMetas(itemblock_stone_full_15, 1, "conquest:brick_romanvertical_full");
		registerRenderMetas(itemblock_stone_full_15, 2, "conquest:none");
		registerRenderMetas(itemblock_stone_full_15, 3, "conquest:architrave_polychrome_full");
		registerRenderMetas(itemblock_stone_full_15, 4, "conquest:architrave_polychromelegionaries_full");
		registerRenderMetas(itemblock_stone_full_15, 5, "conquest:architrave_polychrome2_full");
		registerRenderMetas(itemblock_stone_full_15, 6, "conquest:capitalcorinthian_polychrome_full");
		registerRenderMetas(itemblock_stone_full_15, 7, "conquest:marble_blackwhite_full");
		registerRenderMetas(itemblock_stone_full_15, 8, "conquest:marble_smalldiagonalchecker_full");
		registerRenderMetas(itemblock_stone_full_15, 9, "conquest:marble_smallchecker_full");
		registerRenderMetas(itemblock_stone_full_15, 10, "conquest:marble_redwhite_full");
		registerRenderMetas(itemblock_stone_full_15, 11, "conquest:marble_redwhite2_full");
		registerRenderMetas(itemblock_stone_full_15, 12, "conquest:marble_blueyellow_full");
		registerRenderMetas(itemblock_stone_full_15, 13, "conquest:marble_diagonalchecker_full");
		registerRenderMetas(itemblock_stone_full_15, 14, "conquest:marble_checker_full");
		registerRenderMetas(itemblock_stone_full_15, 15, "conquest:marble_black_full");
		registerRenderMetas(itemblock_stone_full_16, 0, "conquest:marble_blue_full");
		registerRenderMetas(itemblock_stone_full_16, 1, "conquest:marble_gray_full");
		registerRenderMetas(itemblock_stone_full_16, 2, "conquest:marble_green_full");
		registerRenderMetas(itemblock_stone_full_16, 3, "conquest:marble_pink_full");
		registerRenderMetas(itemblock_stone_full_16, 4, "conquest:marble_red_full");
		registerRenderMetas(itemblock_stone_full_16, 5, "conquest:brick_darkroman_full");
		registerRenderMetas(itemblock_stone_full_16, 6, "conquest:brick_darkromansandstone_full");
		registerRenderMetas(itemblock_stone_full_16, 7, "conquest:brick_romansandstone_full");
		registerRenderMetas(itemblock_stone_full_16, 8, "conquest:tudorframe_narrowslash_full");
		registerRenderMetas(itemblock_stone_full_16, 9, "conquest:tudorframe_narrowbackslash_full");
		registerRenderMetas(itemblock_stone_full_16, 10, "conquest:tudorframe_darknarrowslash_full");
		registerRenderMetas(itemblock_stone_full_16, 11, "conquest:tudorframe_darknarrowbackslash_full");
		registerRenderMetas(itemblock_stone_full_16, 12, "conquest:tudorframe_bricknarrowslash_full");
		registerRenderMetas(itemblock_stone_full_16, 13, "conquest:tudorframe_bricknarrowbackslash_full");
		registerRenderMetas(itemblock_stone_full_16, 14, "conquest:tudorframe_darkbricknarrowslash_full");
		registerRenderMetas(itemblock_stone_full_16, 15, "conquest:tudorframe_darkbricknarrowbackslash_full");
		registerRenderMetas(itemblock_stone_full_17, 0, "conquest:tudorframe_redbricknarrowslash_full");
		registerRenderMetas(itemblock_stone_full_17, 1, "conquest:tudorframe_redbricknarrowbackslash_full");
		registerRenderMetas(itemblock_stone_full_17, 2, "conquest:whiterock_mossy_full");
		registerRenderMetas(itemblock_stone_full_17, 3, "conquest:cobblestone_dark_full");
		registerRenderMetas(itemblock_stone_full_17, 4, "conquest:sandstone_roughnatural_full");
		registerRenderMetas(itemblock_stone_full_17, 5, "conquest:stone_cliff_full");
		registerRenderMetas(itemblock_stone_full_17, 6, "conquest:stone_coastal_full");
		registerRenderMetas(itemblock_stone_full_17, 7, "conquest:wallpaper_blueyellow_full");
		registerRenderMetas(itemblock_stone_full_17, 8, "conquest:wallpaper_blueyellowclean_full");
		registerRenderMetas(itemblock_stone_full_17, 9, "conquest:wallpaper_green_full");
		registerRenderMetas(itemblock_stone_full_17, 10, "conquest:wallpaper_greenclean_full");
		registerRenderMetas(itemblock_stone_full_17, 11, "conquest:wallpaper_redyellow_full");
		registerRenderMetas(itemblock_stone_full_17, 12, "conquest:wallpaper_redyellowclean_full");
		registerRenderMetas(itemblock_stone_full_17, 13, "conquest:sandstone_darkcobble_full");
		registerRenderMetas(itemblock_stone_full_17, 14, "conquest:slate_lightmortar_full");
		registerRenderMetas(itemblock_stone_full_17, 15, "conquest:none");
	//Partial Full Stone Blocks
		registerRenderMetas(itemblock_stone_fullpartial_1, 0, "conquest:stone_plastered_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_1, 1, "conquest:none"); //open block space
		registerRenderMetas(itemblock_stone_fullpartial_1, 2, "conquest:stone_normal_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_1, 3, "conquest:sandstone_normal_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_1, 4, "conquest:marble_parian_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_1, 5, "conquest:stone_hewn_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_1, 6, "conquest:stone_colorfulslate_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_1, 7, "conquest:cobblestone_old_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_1, 8, "conquest:cobblestone_damaged_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_1, 9, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_fullpartial_1, 10, "conquest:cobblestone_mossy_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_1, 11, "conquest:cobblestone_overgrown_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_1, 12, "conquest:cobblestone_vines_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_1, 13, "conquest:none"); //open slot
		registerRenderMetas(itemblock_stone_fullpartial_1, 14, "conquest:netherbrick_big_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_1, 15, "conquest:netherbrick_carved_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_2, 0, "conquest:tiles_red_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_2, 1, "conquest:marble_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_2, 2, "conquest:marble_smoothwhite_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_2, 3, "conquest:brick_diorite_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_2, 4, "conquest:brick_travertine_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_2, 5, "conquest:brick_sandstonebig_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_2, 6, "conquest:sandstone_conglomerate_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_2, 7, "conquest:sandstone_mossy_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_2, 8, "conquest:brick_sandstone_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_2, 9, "conquest:tile_clay_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_2, 10, "conquest:tile_mixedclay_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_2, 11, "conquest:tile_lightclay_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_2, 12, "conquest:ironblock_rusty_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_2, 13, "conquest:brick_mossy_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_2, 14, "conquest:brick_darkred_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_2, 15, "conquest:brick_darkredmossy_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_3, 0, "conquest:brick_red_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_3, 1, "conquest:brick_redmossy_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_3, 2, "conquest:brick_roman_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_3, 3, "conquest:stucco_white_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_3, 4, "conquest:stucco_tan_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_3, 5, "conquest:stucco_brown_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_3, 6, "conquest:stucco_purple_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_3, 7, "conquest:walldesign_red8_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_3, 8, "conquest:stucco_lightred_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_3, 9, "conquest:stucco_lime_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_3, 10, "conquest:stucco_green_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_3, 11, "conquest:stucco_cyan_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_3, 12, "conquest:stucco_lightblue_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_3, 13, "conquest:stucco_blue_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_3, 14, "conquest:stucco_black_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_3, 15, "conquest:bronzeblock_gilded_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_4, 0, "conquest:granite_polished_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_4, 1, "conquest:diorite_polished_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_4, 2, "conquest:andesite_polished_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_4, 3, "conquest:cobblestone_normal_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_4, 4, "conquest:sandstone_normal_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_4, 5, "conquest:iron_normal_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_4, 6, "conquest:bricks_normal_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_4, 7, "conquest:cobblestone_mossynormal_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_4, 8, "conquest:stonebrick_normal_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_4, 9, "conquest:stonebrick_mossy_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_4, 10, "conquest:stonebrick_cracked_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_4, 11, "conquest:netherbrick_normal_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_4, 12, "conquest:plaster_magenta_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_4, 13, "conquest:plaster_lightgray_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_4, 14, "conquest:plaster_purple_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_4, 15, "conquest:prismarine_normal_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_5, 0, "conquest:prismarine_bricks_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_5, 1, "conquest:prismarine_dark_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_5, 2, "conquest:sandstone_red_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_5, 3, "conquest:purpur_normal_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_5, 4, "conquest:endstone_bricks_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_5, 5, "conquest:netherbrick_wart_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_5, 6, "conquest:brick_romanvertical_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_5, 7, "conquest:marble_black_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_5, 8, "conquest:marble_blue_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_5, 9, "conquest:marble_gray_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_5, 10, "conquest:marble_green_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_5, 11, "conquest:marble_pink_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_5, 12, "conquest:marble_red_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_5, 13, "conquest:brick_darkroman_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_5, 14, "conquest:cobblestone_dark_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_5, 15, "conquest:sandstone_darkcobble_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_6, 0, "conquest:slate_lightmortar_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_6, 1, "conquest:bell_gilded");
		registerRenderMetas(itemblock_stone_fullpartial_6, 2, "conquest:plasterwork_white_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_6, 3, "conquest:plasterwork_lightgrayclean_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_6, 4, "conquest:plasterwork_yellowclean_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_6, 5, "conquest:plasterwork_whiteclean_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_6, 6, "conquest:plasterwork_yellowclean_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_6, 7, "conquest:plasterwork_purpleclean_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_6, 8, "conquest:plasterwork_orange_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_6, 9, "conquest:cobblestone_hewntiles_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_6, 10, "conquest:cobblestone_plasteredtiles_dragonegg");
		registerRenderMetas(itemblock_stone_fullpartial_6, 11, "conquest:none");
		registerRenderMetas(itemblock_stone_fullpartial_6, 12, "conquest:none");
		registerRenderMetas(itemblock_stone_fullpartial_6, 13, "conquest:none");
		registerRenderMetas(itemblock_stone_fullpartial_6, 14, "conquest:none");
		registerRenderMetas(itemblock_stone_fullpartial_6, 15, "conquest:none");
	//Directional Partial Full Stone Blocks
		registerRenderMetas(itemblock_stone_directionalfullpartial_1, 0, "conquest:gargoyle_stone");
		registerRenderMetas(itemblock_stone_directionalfullpartial_1, 1, "conquest:gargoyle_sandstone");
		registerRenderMetas(itemblock_stone_directionalfullpartial_1, 2, "conquest:none");
		registerRenderMetas(itemblock_stone_directionalfullpartial_1, 3, "conquest:none");
	//Corner Stone Blocks
		registerRenderMetas(itemblock_stone_corner_1, 0, "conquest:stone_plastered_corner");
		registerRenderMetas(itemblock_stone_corner_1, 1, "conquest:cobblestone_normal_corner");
		registerRenderMetas(itemblock_stone_corner_1, 2, "conquest:none");
		registerRenderMetas(itemblock_stone_corner_1, 3, "conquest:none");
		registerRenderMetas(itemblock_stone_corner_2, 0, "conquest:stone_hewn_corner");
		registerRenderMetas(itemblock_stone_corner_2, 1, "conquest:stone_colorfulslate_corner");
		registerRenderMetas(itemblock_stone_corner_2, 2, "conquest:cobblestone_old_corner");
		registerRenderMetas(itemblock_stone_corner_2, 3, "conquest:cobblestone_damaged_corner");
		registerRenderMetas(itemblock_stone_corner_3, 0, "conquest:cobblestone_fishscaledirty_corner");
		registerRenderMetas(itemblock_stone_corner_3, 1, "conquest:cobblestone_mossy_corner");
		registerRenderMetas(itemblock_stone_corner_3, 2, "conquest:cobblestone_overgrown_corner");
		registerRenderMetas(itemblock_stone_corner_3, 3, "conquest:cobblestone_vines_corner");
		registerRenderMetas(itemblock_stone_corner_4, 0, "conquest:cobblestone_reinforced_corner");
		registerRenderMetas(itemblock_stone_corner_4, 1, "conquest:none");
		registerRenderMetas(itemblock_stone_corner_4, 2, "conquest:groove_portculis_corner");
		registerRenderMetas(itemblock_stone_corner_4, 3, "conquest:stone_chiseled_corner");
		registerRenderMetas(itemblock_stone_corner_5, 0, "conquest:pillar_stone_corner");
		registerRenderMetas(itemblock_stone_corner_5, 1, "conquest:netherbrick_wall_corner");
		registerRenderMetas(itemblock_stone_corner_5, 2, "conquest:netherbrick_big_corner");
		registerRenderMetas(itemblock_stone_corner_5, 3, "conquest:netherbrick_pillar_corner");
		registerRenderMetas(itemblock_stone_corner_6, 0, "conquest:netherbrick_carved_corner");
		registerRenderMetas(itemblock_stone_corner_6, 1, "conquest:tiles_red_corner");
		registerRenderMetas(itemblock_stone_corner_6, 2, "conquest:ironblock_dark_corner");
		registerRenderMetas(itemblock_stone_corner_6, 3, "conquest:iron_normal_corner");
		registerRenderMetas(itemblock_stone_corner_7, 0, "conquest:column_sandstone_corner");
		registerRenderMetas(itemblock_stone_corner_7, 1, "conquest:capital_sandstone_corner");
		registerRenderMetas(itemblock_stone_corner_7, 2, "conquest:base_sandstone_corner");
		registerRenderMetas(itemblock_stone_corner_7, 3, "conquest:column_red_corner");
		registerRenderMetas(itemblock_stone_corner_8, 0, "conquest:capital_redmarble_corner");
		registerRenderMetas(itemblock_stone_corner_8, 1, "conquest:base_redmarble_corner");
		registerRenderMetas(itemblock_stone_corner_8, 2, "conquest:capital_redsandstone_corner");
		registerRenderMetas(itemblock_stone_corner_8, 3, "conquest:base_redsandstone_corner");
		registerRenderMetas(itemblock_stone_corner_9, 0, "conquest:column_blue_corner");
		registerRenderMetas(itemblock_stone_corner_9, 1, "conquest:capital_bluemarble_corner");
		registerRenderMetas(itemblock_stone_corner_9, 2, "conquest:base_bluemarble_corner");
		registerRenderMetas(itemblock_stone_corner_9, 3, "conquest:capital_bluesandstone_corner");
		registerRenderMetas(itemblock_stone_corner_10, 0, "conquest:base_bluesandstone_corner");
		registerRenderMetas(itemblock_stone_corner_10, 1, "conquest:column_gold_corner");
		registerRenderMetas(itemblock_stone_corner_10, 2, "conquest:capital_goldmarble_corner");
		registerRenderMetas(itemblock_stone_corner_10, 3, "conquest:base_goldmarble_corner");
		registerRenderMetas(itemblock_stone_corner_11, 0, "conquest:capital_goldsandstone_corner");
		registerRenderMetas(itemblock_stone_corner_11, 1, "conquest:base_goldsandstone_corner");
		registerRenderMetas(itemblock_stone_corner_11, 2, "conquest:column_marble_corner");
		registerRenderMetas(itemblock_stone_corner_11, 3, "conquest:capital_marble_corner");
		registerRenderMetas(itemblock_stone_corner_12, 0, "conquest:base_marble_corner");
		registerRenderMetas(itemblock_stone_corner_12, 1, "conquest:capitalcorinthian_corner");
		registerRenderMetas(itemblock_stone_corner_12, 2, "conquest:capitalcorinthian_sandstone_corner");
		registerRenderMetas(itemblock_stone_corner_12, 3, "conquest:cornice_sandstone_corner");
		registerRenderMetas(itemblock_stone_corner_13, 0, "conquest:plinth_sandstone_corner");
		registerRenderMetas(itemblock_stone_corner_13, 1, "conquest:base_marblesandstone_corner");
		registerRenderMetas(itemblock_stone_corner_13, 2, "conquest:cornice_marble_corner");
		registerRenderMetas(itemblock_stone_corner_13, 3, "conquest:baseplain_marble_corner");
		registerRenderMetas(itemblock_stone_corner_14, 0, "conquest:marble_corner");
		registerRenderMetas(itemblock_stone_corner_14, 1, "conquest:marble_smoothwhite_corner");
		registerRenderMetas(itemblock_stone_corner_14, 2, "conquest:base_parianmarble_corner");
		registerRenderMetas(itemblock_stone_corner_14, 3, "conquest:capital_parianmarble_corner");
		registerRenderMetas(itemblock_stone_corner_15, 0, "conquest:bigslab_marble_corner");
		registerRenderMetas(itemblock_stone_corner_15, 1, "conquest:bigslab_marble1_corner");
		registerRenderMetas(itemblock_stone_corner_15, 2, "conquest:bigslab_sandstone_corner");
		registerRenderMetas(itemblock_stone_corner_15, 3, "conquest:bigslab_sandstone1_corner");
		registerRenderMetas(itemblock_stone_corner_16, 0, "conquest:bigslab_sandstoneinscribed_corner");
		registerRenderMetas(itemblock_stone_corner_16, 1, "conquest:bigslab_sandstoneinscribed1_corner");
		registerRenderMetas(itemblock_stone_corner_16, 2, "conquest:bigslab_black_corner");
		registerRenderMetas(itemblock_stone_corner_16, 3, "conquest:bigslab_black1_corner");
		registerRenderMetas(itemblock_stone_corner_17, 0, "conquest:bigslab_blackinscribed_corner");
		registerRenderMetas(itemblock_stone_corner_17, 1, "conquest:bigslab_blackinscribed1_corner");
		registerRenderMetas(itemblock_stone_corner_17, 2, "conquest:mosaic_roman_corner");
		registerRenderMetas(itemblock_stone_corner_17, 3, "conquest:mosaic_indian_corner");
		registerRenderMetas(itemblock_stone_corner_18, 0, "conquest:mosaic_andalusian_corner");
		registerRenderMetas(itemblock_stone_corner_18, 1, "conquest:stucco_corner");
		registerRenderMetas(itemblock_stone_corner_18, 2, "conquest:mosaic_decorative_corner");
		registerRenderMetas(itemblock_stone_corner_18, 3, "conquest:walldesign_white1_corner");
		registerRenderMetas(itemblock_stone_corner_19, 0, "conquest:walldesign_white2_corner");
		registerRenderMetas(itemblock_stone_corner_19, 1, "conquest:walldesign_white3_corner");
		registerRenderMetas(itemblock_stone_corner_19, 2, "conquest:walldesign_white4_corner");
		registerRenderMetas(itemblock_stone_corner_19, 3, "conquest:walldesign_white5_corner");
		registerRenderMetas(itemblock_stone_corner_20, 0, "conquest:walldesign_white6_corner");
		registerRenderMetas(itemblock_stone_corner_20, 1, "conquest:brick_diorite_corner");
		registerRenderMetas(itemblock_stone_corner_20, 2, "conquest:brick_travertine_corner");
		registerRenderMetas(itemblock_stone_corner_20, 3, "conquest:brick_sandstonebig_corner");
		registerRenderMetas(itemblock_stone_corner_21, 0, "conquest:sandstone_conglomerate_corner");
		registerRenderMetas(itemblock_stone_corner_21, 1, "conquest:sandstone_mossy_corner");
		registerRenderMetas(itemblock_stone_corner_21, 2, "conquest:frieze_sandstone_corner");
		registerRenderMetas(itemblock_stone_corner_21, 3, "conquest:sandstone_inscribed_corner");
		registerRenderMetas(itemblock_stone_corner_22, 0, "conquest:brick_sandstone_corner");
		registerRenderMetas(itemblock_stone_corner_22, 1, "conquest:tile_clay_corner");
		registerRenderMetas(itemblock_stone_corner_22, 2, "conquest:tile_mixedclay_corner");
		registerRenderMetas(itemblock_stone_corner_22, 3, "conquest:tile_lightclay_corner");
		registerRenderMetas(itemblock_stone_corner_23, 0, "conquest:ironblock_rusty_corner");
		registerRenderMetas(itemblock_stone_corner_23, 1, "conquest:tudorframe_slash_corner");
		registerRenderMetas(itemblock_stone_corner_23, 2, "conquest:tudorframe_backslash_corner");
		registerRenderMetas(itemblock_stone_corner_23, 3, "conquest:tudorframe_x_corner");
		registerRenderMetas(itemblock_stone_corner_24, 0, "conquest:tudorframe_up_corner");
		registerRenderMetas(itemblock_stone_corner_24, 1, "conquest:tudorframe_down_corner");
		registerRenderMetas(itemblock_stone_corner_24, 2, "conquest:tudorframe_box_corner");
		registerRenderMetas(itemblock_stone_corner_24, 3, "conquest:tudorframe_slash_corner");
		registerRenderMetas(itemblock_stone_corner_25, 0, "conquest:tudorframe_backslash_corner");
		registerRenderMetas(itemblock_stone_corner_25, 1, "conquest:tudorframe_box_corner");
		registerRenderMetas(itemblock_stone_corner_25, 2, "conquest:tudorframe_darkslash_corner");
		registerRenderMetas(itemblock_stone_corner_25, 3, "conquest:tudorframe_darkbackslash_corner");
		registerRenderMetas(itemblock_stone_corner_26, 0, "conquest:tudorframe_darkx_corner");
		registerRenderMetas(itemblock_stone_corner_26, 1, "conquest:tudorframe_darkup_corner");
		registerRenderMetas(itemblock_stone_corner_26, 2, "conquest:tudorframe_darkdown_corner");
		registerRenderMetas(itemblock_stone_corner_26, 3, "conquest:tudorframe_darkbox_corner");
		registerRenderMetas(itemblock_stone_corner_27, 0, "conquest:tudorframe_darkslash_corner");
		registerRenderMetas(itemblock_stone_corner_27, 1, "conquest:tudorframe_darkbackslash_corner");
		registerRenderMetas(itemblock_stone_corner_27, 2, "conquest:tudorframe_darkbox_corner");
		registerRenderMetas(itemblock_stone_corner_27, 3, "conquest:tudorframe_brickslash_corner");
		registerRenderMetas(itemblock_stone_corner_28, 0, "conquest:tudorframe_brickbackslash_corner");
		registerRenderMetas(itemblock_stone_corner_28, 1, "conquest:tudorframe_brickx_corner");
		registerRenderMetas(itemblock_stone_corner_28, 2, "conquest:tudorframe_brickup_corner");
		registerRenderMetas(itemblock_stone_corner_28, 3, "conquest:tudorframe_brickdown_corner");
		registerRenderMetas(itemblock_stone_corner_29, 0, "conquest:tudorframe_brickbox_corner");
		registerRenderMetas(itemblock_stone_corner_29, 1, "conquest:tudorframe_brickslash_corner");
		registerRenderMetas(itemblock_stone_corner_29, 2, "conquest:tudorframe_brickbackslash_corner");
		registerRenderMetas(itemblock_stone_corner_29, 3, "conquest:tudorframe_brickbox_corner");
		registerRenderMetas(itemblock_stone_corner_30, 0, "conquest:tudorframe_darkbrickslash_corner");
		registerRenderMetas(itemblock_stone_corner_30, 1, "conquest:tudorframe_darkbrickbackslash_corner");
		registerRenderMetas(itemblock_stone_corner_30, 2, "conquest:tudorframe_darkbrickx_corner");
		registerRenderMetas(itemblock_stone_corner_30, 3, "conquest:tudorframe_darkbrickup_corner");
		registerRenderMetas(itemblock_stone_corner_31, 0, "conquest:tudorframe_darkbrickdown_corner");
		registerRenderMetas(itemblock_stone_corner_31, 1, "conquest:tudorframe_darkbrickbox_corner");
		registerRenderMetas(itemblock_stone_corner_31, 2, "conquest:tudorframe_darkbrickslash_corner");
		registerRenderMetas(itemblock_stone_corner_31, 3, "conquest:tudorframe_darkbrickbackslash_corner");
		registerRenderMetas(itemblock_stone_corner_32, 0, "conquest:tudorframe_darkbrickbox_corner");
		registerRenderMetas(itemblock_stone_corner_32, 1, "conquest:tudorframe_redbrickslash_corner");
		registerRenderMetas(itemblock_stone_corner_32, 2, "conquest:tudorframe_redbrickbackslash_corner");
		registerRenderMetas(itemblock_stone_corner_32, 3, "conquest:tudorframe_redbrickx_corner");
		registerRenderMetas(itemblock_stone_corner_33, 0, "conquest:tudorframe_redbrickup_corner");
		registerRenderMetas(itemblock_stone_corner_33, 1, "conquest:tudorframe_redbrickdown_corner");
		registerRenderMetas(itemblock_stone_corner_33, 2, "conquest:tudorframe_redbrickbox_corner");
		registerRenderMetas(itemblock_stone_corner_33, 3, "conquest:tudorframe_redbrickslash_corner");
		registerRenderMetas(itemblock_stone_corner_34, 0, "conquest:tudorframe_redbrickbackslash_corner");
		registerRenderMetas(itemblock_stone_corner_34, 1, "conquest:tudorframe_redbrickbox_corner");
		registerRenderMetas(itemblock_stone_corner_34, 2, "conquest:brick_mossy_corner");
		registerRenderMetas(itemblock_stone_corner_34, 3, "conquest:brick_darkred_corner");
		registerRenderMetas(itemblock_stone_corner_35, 0, "conquest:brick_darkredmossy_corner");
		registerRenderMetas(itemblock_stone_corner_35, 1, "conquest:brick_red_corner");
		registerRenderMetas(itemblock_stone_corner_35, 2, "conquest:brick_redmossy_corner");
		registerRenderMetas(itemblock_stone_corner_35, 3, "conquest:brick_roman_corner");
		registerRenderMetas(itemblock_stone_corner_36, 0, "conquest:stucco_white_corner");
		registerRenderMetas(itemblock_stone_corner_36, 1, "conquest:stucco_tan_corner");
		registerRenderMetas(itemblock_stone_corner_36, 2, "conquest:walldesign_brown_corner");
		registerRenderMetas(itemblock_stone_corner_36, 3, "conquest:stucco_brown_corner");
		registerRenderMetas(itemblock_stone_corner_37, 0, "conquest:stucco_purple_corner");
		registerRenderMetas(itemblock_stone_corner_37, 1, "conquest:walldesign_magenta_corner");
		registerRenderMetas(itemblock_stone_corner_37, 2, "conquest:stucco_magenta_corner");
		registerRenderMetas(itemblock_stone_corner_37, 3, "conquest:walldesign_red_corner");
		registerRenderMetas(itemblock_stone_corner_38, 0, "conquest:walldesign_red2_corner");
		registerRenderMetas(itemblock_stone_corner_38, 1, "conquest:walldesign_red3_corner");
		registerRenderMetas(itemblock_stone_corner_38, 2, "conquest:walldesign_red4_corner");
		registerRenderMetas(itemblock_stone_corner_38, 3, "conquest:walldesign_red5_corner");
		registerRenderMetas(itemblock_stone_corner_39, 0, "conquest:walldesign_red6_corner");
		registerRenderMetas(itemblock_stone_corner_39, 1, "conquest:walldesign_red7_corner");
		registerRenderMetas(itemblock_stone_corner_39, 2, "conquest:walldesign_red8_corner");
		registerRenderMetas(itemblock_stone_corner_39, 3, "conquest:stucco_lightred_corner");
		registerRenderMetas(itemblock_stone_corner_40, 0, "conquest:walldesign_orange_corner");
		registerRenderMetas(itemblock_stone_corner_40, 1, "conquest:walldesign_orange1_corner");
		registerRenderMetas(itemblock_stone_corner_40, 2, "conquest:stucco_lime_corner");
		registerRenderMetas(itemblock_stone_corner_40, 3, "conquest:walldesign_green_corner");
		registerRenderMetas(itemblock_stone_corner_41, 0, "conquest:stucco_green_corner");
		registerRenderMetas(itemblock_stone_corner_41, 1, "conquest:stucco_cyan_corner");
		registerRenderMetas(itemblock_stone_corner_41, 2, "conquest:stucco_lightblue_corner");
		registerRenderMetas(itemblock_stone_corner_41, 3, "conquest:stucco_blue_corner");
		registerRenderMetas(itemblock_stone_corner_42, 0, "conquest:stucco_black_corner");
		registerRenderMetas(itemblock_stone_corner_42, 1, "conquest:door_ornateroman_corner");
		registerRenderMetas(itemblock_stone_corner_42, 2, "conquest:door_ornateroman_corner");
		registerRenderMetas(itemblock_stone_corner_42, 3, "conquest:bronzeblock_gilded_corner");
		registerRenderMetas(itemblock_stone_corner_43, 0, "conquest:bronzeblock_fancy_corner");
		registerRenderMetas(itemblock_stone_corner_43, 1, "conquest:goldblock_bars_corner");
		registerRenderMetas(itemblock_stone_corner_43, 2, "conquest:granite_polished_corner");
		registerRenderMetas(itemblock_stone_corner_43, 3, "conquest:diorite_polished_corner");
		registerRenderMetas(itemblock_stone_corner_44, 0, "conquest:andesite_polished_corner");
		registerRenderMetas(itemblock_stone_corner_44, 1, "conquest:sandstone_normal_corner");
		registerRenderMetas(itemblock_stone_corner_44, 2, "conquest:sandstone_chiseled_corner");
		registerRenderMetas(itemblock_stone_corner_44, 3, "conquest:sandstone_smooth_corner");
		registerRenderMetas(itemblock_stone_corner_45, 0, "conquest:iron_normal_corner");
		registerRenderMetas(itemblock_stone_corner_45, 1, "conquest:bricks_normal_corner");
		registerRenderMetas(itemblock_stone_corner_45, 2, "conquest:cobblestone_mossynormal_corner");
		registerRenderMetas(itemblock_stone_corner_45, 3, "conquest:stonebrick_normal_corner");
		registerRenderMetas(itemblock_stone_corner_46, 0, "conquest:stonebrick_mossy_corner");
		registerRenderMetas(itemblock_stone_corner_46, 1, "conquest:stonebrick_cracked_corner");
		registerRenderMetas(itemblock_stone_corner_46, 2, "conquest:stonebrick_chiseled_corner");
		registerRenderMetas(itemblock_stone_corner_46, 3, "conquest:netherbrick_normal_corner");
		registerRenderMetas(itemblock_stone_corner_47, 0, "conquest:quartz_normal_corner");
		registerRenderMetas(itemblock_stone_corner_47, 1, "conquest:quartz_chiseled_corner");
		registerRenderMetas(itemblock_stone_corner_47, 2, "conquest:quartz_pillar_corner");
		registerRenderMetas(itemblock_stone_corner_47, 3, "conquest:plaster_magenta_corner");
		registerRenderMetas(itemblock_stone_corner_48, 0, "conquest:plaster_lightgray_corner");
		registerRenderMetas(itemblock_stone_corner_48, 1, "conquest:plaster_purple_corner");
		registerRenderMetas(itemblock_stone_corner_48, 2, "conquest:prismarine_normal_corner");
		registerRenderMetas(itemblock_stone_corner_48, 3, "conquest:prismarine_bricks_corner");
		registerRenderMetas(itemblock_stone_corner_49, 0, "conquest:prismarine_dark_corner");
		registerRenderMetas(itemblock_stone_corner_49, 1, "conquest:sandstone_red_corner");
		registerRenderMetas(itemblock_stone_corner_49, 2, "conquest:sandstone_redchiseled_corner");
		registerRenderMetas(itemblock_stone_corner_49, 3, "conquest:sandstone_redsmooth_corner");
		registerRenderMetas(itemblock_stone_corner_50, 0, "conquest:purpur_normal_corner");
		registerRenderMetas(itemblock_stone_corner_50, 1, "conquest:purpur_pillar_corner");
		registerRenderMetas(itemblock_stone_corner_50, 2, "conquest:endstone_bricks_corner");
		registerRenderMetas(itemblock_stone_corner_50, 3, "conquest:netherbrick_wart_corner");
		registerRenderMetas(itemblock_stone_corner_51, 0, "conquest:netherbrick_red_corner");
		registerRenderMetas(itemblock_stone_corner_51, 1, "conquest:pile_bones_corner");
		registerRenderMetas(itemblock_stone_corner_51, 2, "conquest:pile_skeletons_corner");
		registerRenderMetas(itemblock_stone_corner_51, 3, "conquest:brick_romanvertical_corner");
		registerRenderMetas(itemblock_stone_corner_52, 0, "conquest:architrave_polychrome_corner");
		registerRenderMetas(itemblock_stone_corner_52, 1, "conquest:architrave_polychromelegionaries_corner");
		registerRenderMetas(itemblock_stone_corner_52, 2, "conquest:architrave_polychrome2_corner");
		registerRenderMetas(itemblock_stone_corner_52, 3, "conquest:capitalcorinthian_polychrome_corner");
		registerRenderMetas(itemblock_stone_corner_53, 0, "conquest:marble_blackwhite_corner");
		registerRenderMetas(itemblock_stone_corner_53, 1, "conquest:marble_smalldiagonalchecker_corner");
		registerRenderMetas(itemblock_stone_corner_53, 2, "conquest:marble_smallchecker_corner");
		registerRenderMetas(itemblock_stone_corner_53, 3, "conquest:marble_redwhite_corner");
		registerRenderMetas(itemblock_stone_corner_54, 0, "conquest:marble_redwhite2_corner");
		registerRenderMetas(itemblock_stone_corner_54, 1, "conquest:marble_blueyellow_corner");
		registerRenderMetas(itemblock_stone_corner_54, 2, "conquest:marble_diagonalchecker_corner");
		registerRenderMetas(itemblock_stone_corner_54, 3, "conquest:marble_checker_corner");
		registerRenderMetas(itemblock_stone_corner_55, 0, "conquest:marble_black_corner");
		registerRenderMetas(itemblock_stone_corner_55, 1, "conquest:marble_blue_corner");
		registerRenderMetas(itemblock_stone_corner_55, 2, "conquest:marble_gray_corner");
		registerRenderMetas(itemblock_stone_corner_55, 3, "conquest:marble_green_corner");
		registerRenderMetas(itemblock_stone_corner_56, 0, "conquest:marble_pink_corner");
		registerRenderMetas(itemblock_stone_corner_56, 1, "conquest:marble_red_corner");
		registerRenderMetas(itemblock_stone_corner_56, 2, "conquest:brick_darkroman_corner");
		registerRenderMetas(itemblock_stone_corner_56, 3, "conquest:brick_darkromansandstone_corner");
		registerRenderMetas(itemblock_stone_corner_57, 0, "conquest:brick_romansandstone_corner");
		registerRenderMetas(itemblock_stone_corner_57, 1, "conquest:tudorframe_narrowslash_corner");
		registerRenderMetas(itemblock_stone_corner_57, 2, "conquest:tudorframe_narrowbackslash_corner");
		registerRenderMetas(itemblock_stone_corner_57, 3, "conquest:tudorframe_darknarrowslash_corner");
		registerRenderMetas(itemblock_stone_corner_58, 0, "conquest:tudorframe_darknarrowbackslash_corner");
		registerRenderMetas(itemblock_stone_corner_58, 1, "conquest:tudorframe_bricknarrowslash_corner");
		registerRenderMetas(itemblock_stone_corner_58, 2, "conquest:tudorframe_bricknarrowbackslash_corner");
		registerRenderMetas(itemblock_stone_corner_58, 3, "conquest:tudorframe_darkbricknarrowslash_corner");
		registerRenderMetas(itemblock_stone_corner_59, 0, "conquest:tudorframe_darkbricknarrowbackslash_corner");
		registerRenderMetas(itemblock_stone_corner_59, 1, "conquest:tudorframe_redbricknarrowslash_corner");
		registerRenderMetas(itemblock_stone_corner_59, 2, "conquest:tudorframe_redbricknarrowbackslash_corner");
		registerRenderMetas(itemblock_stone_corner_59, 3, "conquest:cobblestone_dark_corner");
		registerRenderMetas(itemblock_stone_corner_60, 0, "conquest:wallpaper_blueyellow_corner");
		registerRenderMetas(itemblock_stone_corner_60, 1, "conquest:wallpaper_blueyellowclean_corner");
		registerRenderMetas(itemblock_stone_corner_60, 2, "conquest:wallpaper_green_corner");
		registerRenderMetas(itemblock_stone_corner_60, 3, "conquest:wallpaper_greenclean_corner");
		registerRenderMetas(itemblock_stone_corner_61, 0, "conquest:wallpaper_redyellow_corner");
		registerRenderMetas(itemblock_stone_corner_61, 1, "conquest:wallpaper_redyellowclean_corner");
		registerRenderMetas(itemblock_stone_corner_61, 2, "conquest:sandstone_darkcobble_corner");
		registerRenderMetas(itemblock_stone_corner_61, 3, "conquest:slate_lightmortar_corner");
		registerRenderMetas(itemblock_stone_corner_62, 0, "conquest:plasterwork_white_corner");
		registerRenderMetas(itemblock_stone_corner_62, 1, "conquest:plasterwork_magentaclean_corner");
		registerRenderMetas(itemblock_stone_corner_62, 2, "conquest:plasterwork_lightgrayclean_corner");
		registerRenderMetas(itemblock_stone_corner_62, 3, "conquest:plasterwork_yellowclean_corner");
		registerRenderMetas(itemblock_stone_corner_63, 0, "conquest:plasterwork_whiteclean_corner");
		registerRenderMetas(itemblock_stone_corner_63, 1, "conquest:plasterwork_purpleclean_corner");
		registerRenderMetas(itemblock_stone_corner_63, 2, "conquest:plasterwork_orange_corner");
		registerRenderMetas(itemblock_stone_corner_63, 3, "conquest:cobblestone_hewntiles_corner");
		registerRenderMetas(itemblock_stone_corner_64, 0, "conquest:cobblestone_plasteredtiles_corner");
		registerRenderMetas(itemblock_stone_corner_64, 1, "conquest:none");
		registerRenderMetas(itemblock_stone_corner_64, 2, "conquest:none");
		registerRenderMetas(itemblock_stone_corner_64, 3, "conquest:none");
	//Vertical Slab Stone Blocks
		registerRenderMetas(itemblock_stone_verticalslab_1, 0, "conquest:stone_plastered_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_1, 1, "conquest:cobblestone_normal_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_1, 2, "conquest:bay_base");
		registerRenderMetas(itemblock_stone_verticalslab_1, 3, "conquest:bay_window");
		registerRenderMetas(itemblock_stone_verticalslab_2, 0, "conquest:stone_hewn_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_2, 1, "conquest:stone_colorfulslate_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_2, 2, "conquest:cobblestone_old_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_2, 3, "conquest:cobblestone_damaged_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_3, 0, "conquest:cobblestone_fishscaledirty_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_3, 1, "conquest:cobblestone_mossy_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_3, 2, "conquest:cobblestone_overgrown_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_3, 3, "conquest:cobblestone_vines_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_4, 0, "conquest:cobblestone_reinforced_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_4, 1, "conquest:cobblestone_reinforced_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_4, 2, "conquest:groove_portculis_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_4, 3, "conquest:stone_chiseled_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_5, 0, "conquest:pillar_stone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_5, 1, "conquest:netherbrick_wall_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_5, 2, "conquest:netherbrick_big_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_5, 3, "conquest:netherbrick_pillar_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_6, 0, "conquest:netherbrick_carved_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_6, 1, "conquest:tiles_red_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_6, 2, "conquest:ironblock_dark_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_6, 3, "conquest:iron_normal_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_7, 0, "conquest:column_sandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_7, 1, "conquest:capital_sandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_7, 2, "conquest:base_sandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_7, 3, "conquest:column_red_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_8, 0, "conquest:capital_redmarble_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_8, 1, "conquest:base_redmarble_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_8, 2, "conquest:capital_redsandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_8, 3, "conquest:base_redsandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_9, 0, "conquest:column_blue_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_9, 1, "conquest:capital_bluemarble_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_9, 2, "conquest:base_bluemarble_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_9, 3, "conquest:capital_bluesandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_10, 0, "conquest:base_bluesandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_10, 1, "conquest:column_gold_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_10, 2, "conquest:capital_goldmarble_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_10, 3, "conquest:base_goldmarble_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_11, 0, "conquest:capital_goldsandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_11, 1, "conquest:base_goldsandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_11, 2, "conquest:column_marble_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_11, 3, "conquest:capital_marble_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_12, 0, "conquest:base_marble_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_12, 1, "conquest:capitalcorinthian_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_12, 2, "conquest:capitalcorinthian_sandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_12, 3, "conquest:cornice_sandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_13, 0, "conquest:plinth_sandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_13, 1, "conquest:base_marblesandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_13, 2, "conquest:cornice_marble_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_13, 3, "conquest:baseplain_marble_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_14, 0, "conquest:marble_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_14, 1, "conquest:marble_smoothwhite_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_14, 2, "conquest:base_parianmarble_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_14, 3, "conquest:capital_parianmarble_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_15, 0, "conquest:bigslab_marble_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_15, 1, "conquest:bigslab_marble1_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_15, 2, "conquest:bigslab_sandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_15, 3, "conquest:bigslab_sandstone1_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_16, 0, "conquest:bigslab_sandstoneinscribed_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_16, 1, "conquest:bigslab_sandstoneinscribed1_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_16, 2, "conquest:bigslab_black_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_16, 3, "conquest:bigslab_black1_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_17, 0, "conquest:bigslab_blackinscribed_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_17, 1, "conquest:bigslab_blackinscribed1_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_17, 2, "conquest:mosaic_roman_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_17, 3, "conquest:mosaic_indian_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_18, 0, "conquest:mosaic_andalusian_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_18, 1, "conquest:stucco_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_18, 2, "conquest:mosaic_decorative_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_18, 3, "conquest:walldesign_white1_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_19, 0, "conquest:walldesign_white2_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_19, 1, "conquest:walldesign_white3_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_19, 2, "conquest:walldesign_white4_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_19, 3, "conquest:walldesign_white5_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_20, 0, "conquest:walldesign_white6_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_20, 1, "conquest:brick_diorite_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_20, 2, "conquest:brick_travertine_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_20, 3, "conquest:brick_sandstonebig_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_21, 0, "conquest:sandstone_conglomerate_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_21, 1, "conquest:sandstone_mossy_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_21, 2, "conquest:frieze_sandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_21, 3, "conquest:sandstone_inscribed_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_22, 0, "conquest:brick_sandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_22, 1, "conquest:tile_clay_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_22, 2, "conquest:tile_mixedclay_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_22, 3, "conquest:tile_lightclay_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_23, 0, "conquest:ironblock_rusty_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_23, 1, "conquest:tudorframe_slash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_23, 2, "conquest:tudorframe_backslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_23, 3, "conquest:tudorframe_x_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_24, 0, "conquest:tudorframe_up_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_24, 1, "conquest:tudorframe_down_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_24, 2, "conquest:tudorframe_box_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_24, 3, "conquest:tudorframe_slash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_25, 0, "conquest:tudorframe_backslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_25, 1, "conquest:tudorframe_box_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_25, 2, "conquest:tudorframe_darkslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_25, 3, "conquest:tudorframe_darkbackslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_26, 0, "conquest:tudorframe_darkx_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_26, 1, "conquest:tudorframe_darkup_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_26, 2, "conquest:tudorframe_darkdown_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_26, 3, "conquest:tudorframe_darkbox_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_27, 0, "conquest:tudorframe_darkslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_27, 1, "conquest:tudorframe_darkbackslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_27, 2, "conquest:tudorframe_darkbox_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_27, 3, "conquest:tudorframe_brickslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_28, 0, "conquest:tudorframe_brickbackslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_28, 1, "conquest:tudorframe_brickx_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_28, 2, "conquest:tudorframe_brickup_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_28, 3, "conquest:tudorframe_brickdown_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_29, 0, "conquest:tudorframe_brickbox_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_29, 1, "conquest:tudorframe_brickslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_29, 2, "conquest:tudorframe_brickbackslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_29, 3, "conquest:tudorframe_brickbox_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_30, 0, "conquest:tudorframe_darkbrickslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_30, 1, "conquest:tudorframe_darkbrickbackslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_30, 2, "conquest:tudorframe_darkbrickx_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_30, 3, "conquest:tudorframe_darkbrickup_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_31, 0, "conquest:tudorframe_darkbrickdown_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_31, 1, "conquest:tudorframe_darkbrickbox_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_31, 2, "conquest:tudorframe_darkbrickslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_31, 3, "conquest:tudorframe_darkbrickbackslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_32, 0, "conquest:tudorframe_darkbrickbox_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_32, 1, "conquest:tudorframe_redbrickslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_32, 2, "conquest:tudorframe_redbrickbackslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_32, 3, "conquest:tudorframe_redbrickx_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_33, 0, "conquest:tudorframe_redbrickup_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_33, 1, "conquest:tudorframe_redbrickdown_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_33, 2, "conquest:tudorframe_redbrickbox_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_33, 3, "conquest:tudorframe_redbrickslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_34, 0, "conquest:tudorframe_redbrickbackslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_34, 1, "conquest:tudorframe_redbrickbox_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_34, 2, "conquest:brick_mossy_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_34, 3, "conquest:brick_darkred_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_35, 0, "conquest:brick_darkredmossy_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_35, 1, "conquest:brick_red_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_35, 2, "conquest:brick_redmossy_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_35, 3, "conquest:brick_roman_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_36, 0, "conquest:stucco_white_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_36, 1, "conquest:stucco_tan_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_36, 2, "conquest:walldesign_brown_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_36, 3, "conquest:stucco_brown_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_37, 0, "conquest:stucco_purple_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_37, 1, "conquest:walldesign_magenta_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_37, 2, "conquest:stucco_magenta_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_37, 3, "conquest:walldesign_red_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_38, 0, "conquest:walldesign_red2_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_38, 1, "conquest:walldesign_red3_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_38, 2, "conquest:walldesign_red4_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_38, 3, "conquest:walldesign_red5_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_39, 0, "conquest:walldesign_red6_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_39, 1, "conquest:walldesign_red7_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_39, 2, "conquest:walldesign_red8_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_39, 3, "conquest:stucco_lightred_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_40, 0, "conquest:walldesign_orange_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_40, 1, "conquest:walldesign_orange1_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_40, 2, "conquest:stucco_lime_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_40, 3, "conquest:walldesign_green_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_41, 0, "conquest:stucco_green_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_41, 1, "conquest:stucco_cyan_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_41, 2, "conquest:stucco_lightblue_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_41, 3, "conquest:stucco_blue_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_42, 0, "conquest:stucco_black_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_42, 1, "conquest:door_ornateroman_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_42, 2, "conquest:door_ornateroman_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_42, 3, "conquest:bronzeblock_gilded_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_43, 0, "conquest:bronzeblock_fancy_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_43, 1, "conquest:goldblock_bars_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_43, 2, "conquest:granite_polished_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_43, 3, "conquest:diorite_polished_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_44, 0, "conquest:andesite_polished_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_44, 1, "conquest:sandstone_normal_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_44, 2, "conquest:sandstone_chiseled_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_44, 3, "conquest:sandstone_smooth_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_45, 0, "conquest:iron_normal_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_45, 1, "conquest:bricks_normal_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_45, 2, "conquest:cobblestone_mossynormal_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_45, 3, "conquest:stonebrick_normal_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_46, 0, "conquest:stonebrick_mossy_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_46, 1, "conquest:stonebrick_cracked_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_46, 2, "conquest:stonebrick_chiseled_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_46, 3, "conquest:netherbrick_normal_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_47, 0, "conquest:quartz_normal_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_47, 1, "conquest:quartz_chiseled_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_47, 2, "conquest:quartz_pillar_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_47, 3, "conquest:plaster_magenta_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_48, 0, "conquest:plaster_lightgray_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_48, 1, "conquest:plaster_purple_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_48, 2, "conquest:prismarine_normal_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_48, 3, "conquest:prismarine_bricks_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_49, 0, "conquest:prismarine_dark_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_49, 1, "conquest:sandstone_red_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_49, 2, "conquest:sandstone_redchiseled_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_49, 3, "conquest:sandstone_redsmooth_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_50, 0, "conquest:purpur_normal_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_50, 1, "conquest:purpur_pillar_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_50, 2, "conquest:endstone_bricks_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_50, 3, "conquest:netherbrick_wart_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_51, 0, "conquest:netherbrick_red_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_51, 1, "conquest:stone_icy_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_51, 2, "conquest:stone_icytop_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_51, 3, "conquest:stone_mossy_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_52, 0, "conquest:stone_mossytop_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_52, 1, "conquest:slate_wet_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_52, 2, "conquest:slate_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_52, 3, "conquest:marble_uncut_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_53, 0, "conquest:granite_graypink_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_53, 1, "conquest:sandstone_natural_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_53, 2, "conquest:mesastone_lightbrown_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_53, 3, "conquest:pahoehoe_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_54, 0, "conquest:endstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_54, 1, "conquest:stone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_54, 2, "conquest:granite_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_54, 3, "conquest:diorite_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_55, 0, "conquest:andesite_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_55, 1, "conquest:obsidian_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_55, 2, "conquest:hardclay_white_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_55, 3, "conquest:hardclay_orange_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_56, 0, "conquest:hardclay_yellow_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_56, 1, "conquest:hardclay_gray_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_56, 2, "conquest:hardclay_brown_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_56, 3, "conquest:hardclay_red_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_57, 0, "conquest:hardclay_normal_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_57, 1, "conquest:pile_bones_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_57, 2, "conquest:pile_skeletons_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_57, 3, "conquest:brick_romanvertical_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_58, 0, "conquest:architrave_polychrome_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_58, 1, "conquest:architrave_polychromelegionaries_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_58, 2, "conquest:architrave_polychrome2_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_58, 3, "conquest:capitalcorinthian_polychrome_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_59, 0, "conquest:marble_blackwhite_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_59, 1, "conquest:marble_smalldiagonalchecker_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_59, 2, "conquest:marble_smallchecker_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_59, 3, "conquest:marble_redwhite_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_60, 0, "conquest:marble_redwhite2_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_60, 1, "conquest:marble_blueyellow_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_60, 2, "conquest:marble_diagonalchecker_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_60, 3, "conquest:marble_checker_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_61, 0, "conquest:marble_black_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_61, 1, "conquest:marble_blue_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_61, 2, "conquest:marble_gray_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_61, 3, "conquest:marble_green_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_62, 0, "conquest:marble_pink_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_62, 1, "conquest:marble_red_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_62, 2, "conquest:brick_darkroman_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_62, 3, "conquest:brick_darkromansandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_63, 0, "conquest:brick_romansandstone_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_63, 1, "conquest:tudorframe_narrowslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_63, 2, "conquest:tudorframe_narrowbackslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_63, 3, "conquest:tudorframe_darknarrowslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_64, 0, "conquest:tudorframe_darknarrowbackslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_64, 1, "conquest:tudorframe_bricknarrowslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_64, 2, "conquest:tudorframe_bricknarrowbackslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_64, 3, "conquest:tudorframe_darkbricknarrowslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_65, 0, "conquest:tudorframe_darkbricknarrowbackslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_65, 1, "conquest:tudorframe_redbricknarrowslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_65, 2, "conquest:tudorframe_redbricknarrowbackslash_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_65, 3, "conquest:whiterock_mossy_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_66, 0, "conquest:cobblestone_dark_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_66, 1, "conquest:sandstone_roughnatural_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_66, 2, "conquest:stone_cliff_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_66, 3, "conquest:stone_coastal_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_67, 0, "conquest:wallpaper_blueyellow_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_67, 1, "conquest:wallpaper_blueyellowclean_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_67, 2, "conquest:wallpaper_green_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_67, 3, "conquest:wallpaper_greenclean_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_68, 0, "conquest:wallpaper_redyellow_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_68, 1, "conquest:wallpaper_redyellowclean_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_68, 2, "conquest:sandstone_darkcobble_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_68, 3, "conquest:slate_lightmortar_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_69, 0, "conquest:plasterwork_white_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_69, 1, "conquest:plasterwork_magentaclean_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_69, 2, "conquest:plasterwork_lightgrayclean_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_69, 3, "conquest:plasterwork_yellowclean_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_70, 0, "conquest:plasterwork_whiteclean_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_70, 1, "conquest:plasterwork_purpleclean_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_70, 2, "conquest:plasterwork_orange_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_70, 3, "conquest:cobblestone_hewntiles_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_71, 0, "conquest:cobblestone_plasteredtiles_verticalslab");
		registerRenderMetas(itemblock_stone_verticalslab_71, 1, "conquest:none");
		registerRenderMetas(itemblock_stone_verticalslab_71, 2, "conquest:none");
		registerRenderMetas(itemblock_stone_verticalslab_71, 3, "conquest:none");

	//Arrow Slit Stone Blocks
		registerRenderMetas(itemblock_stone_arrowslit_1, 0, "conquest:stone_plastered_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_1, 1, "conquest:cobblestone_normal_arrowslit");
		registerRenderMetas(itemblock_stone_arrowslit_1, 2, "conquest:stone_hewn_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_1, 3, "conquest:stone_colorfulslate_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_2, 0, "conquest:cobblestone_old_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_2, 1, "conquest:cobblestone_damaged_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_2, 2, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_arrowslit_2, 3, "conquest:cobblestone_mossy_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_3, 0, "conquest:cobblestone_overgrown_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_3, 1, "conquest:cobblestone_vines_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_3, 2, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_arrowslit_3, 3, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_arrowslit_4, 0, "conquest:groove_portculis_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_4, 1, "conquest:netherbrick_big_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_4, 2, "conquest:ironblock_dark_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_4, 3, "conquest:marble_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_5, 0, "conquest:marble_smoothwhite_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_5, 1, "conquest:bigslab_marble1_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_5, 2, "conquest:bigslab_sandstone1_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_5, 3, "conquest:bigslab_black1_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_6, 0, "conquest:brick_diorite_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_6, 1, "conquest:brick_travertine_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_6, 2, "conquest:brick_sandstonebig_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_6, 3, "conquest:sandstone_conglomerate_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_7, 0, "conquest:sandstone_mossy_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_7, 1, "conquest:brick_sandstone_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_7, 2, "conquest:tile_clay_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_7, 3, "conquest:tile_mixedclay_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_8, 0, "conquest:tile_lightclay_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_8, 1, "conquest:ironblock_rusty_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_8, 2, "conquest:brick_mossy_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_8, 3, "conquest:brick_darkred_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_9, 0, "conquest:brick_darkredmossy_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_9, 1, "conquest:brick_red_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_9, 2, "conquest:brick_redmossy_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_9, 3, "conquest:brick_roman_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_10, 0, "conquest:stucco_white_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_10, 1, "conquest:stucco_tan_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_10, 2, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_arrowslit_10, 3, "conquest:stucco_brown_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_11, 0, "conquest:stucco_purple_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_11, 1, "conquest:stucco_magenta_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_11, 2, "conquest:walldesign_red8_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_11, 3, "conquest:stucco_lightred_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_12, 0, "conquest:stucco_lime_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_12, 1, "conquest:stucco_green_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_12, 2, "conquest:stucco_cyan_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_12, 3, "conquest:stucco_lightblue_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_13, 0, "conquest:stucco_blue_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_13, 1, "conquest:stucco_black_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_13, 2, "conquest:granite_polished_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_13, 3, "conquest:diorite_polished_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_14, 0, "conquest:andesite_polished_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_14, 1, "conquest:sandstone_normal_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_14, 2, "conquest:iron_normal_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_14, 3, "conquest:bricks_normal_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_15, 0, "conquest:cobblestone_mossynormal_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_15, 1, "conquest:stonebrick_normal_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_15, 2, "conquest:stonebrick_mossy_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_15, 3, "conquest:stonebrick_cracked_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_16, 0, "conquest:netherbrick_normal_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_16, 1, "conquest:quartz_normal_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_16, 2, "conquest:plaster_magenta_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_16, 3, "conquest:plaster_lightgray_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_17, 0, "conquest:plaster_purple_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_17, 1, "conquest:prismarine_normal_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_17, 2, "conquest:prismarine_bricks_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_17, 3, "conquest:prismarine_dark_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_18, 0, "conquest:sandstone_red_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_18, 1, "conquest:purpur_normal_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_18, 2, "conquest:endstone_bricks_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_18, 3, "conquest:netherbrick_red_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_19, 0, "conquest:brick_romanvertical_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_19, 1, "conquest:marble_black_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_19, 2, "conquest:marble_blue_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_19, 3, "conquest:marble_gray_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_20, 0, "conquest:marble_green_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_20, 1, "conquest:marble_pink_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_20, 2, "conquest:marble_red_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_20, 3, "conquest:brick_darkroman_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_21, 0, "conquest:brick_darkromansandstone_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_21, 1, "conquest:brick_romansandstone_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_21, 2, "conquest:cobblestone_dark_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_21, 3, "conquest:sandstone_darkcobble_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_22, 0, "conquest:slate_lightmortar_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_22, 1, "conquest:plasterwork_white_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_22, 2, "conquest:plasterwork_magentaclean_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_22, 3, "conquest:plasterwork_lightgrayclean_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_23, 0, "conquest:plasterwork_yellowclean_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_23, 1, "conquest:plasterwork_whiteclean_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_23, 2, "conquest:plasterwork_purpleclean_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_23, 3, "conquest:plasterwork_orange_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_24, 0, "conquest:cobblestone_hewntiles_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_24, 1, "conquest:cobblestone_plasteredtiles_windowslit");
		registerRenderMetas(itemblock_stone_arrowslit_24, 2, "conquest:none");
		registerRenderMetas(itemblock_stone_arrowslit_24, 3, "conquest:none");
	//Full Slit Stone Blocks
		registerRenderMetas(itemblock_stone_fullslit_1, 0, "conquest:stone_plastered_slit");
		registerRenderMetas(itemblock_stone_fullslit_1, 1, "conquest:stone_slate_slit");
		registerRenderMetas(itemblock_stone_fullslit_1, 2, "conquest:stone_hewn_slit");
		registerRenderMetas(itemblock_stone_fullslit_1, 3, "conquest:cobblestone_normal_slit");
		registerRenderMetas(itemblock_stone_fullslit_2, 0, "conquest:stonebrick_slit");
		registerRenderMetas(itemblock_stone_fullslit_2, 1, "conquest:brick_slit");
		registerRenderMetas(itemblock_stone_fullslit_2, 2, "conquest:sandstone_dark_slit");
		registerRenderMetas(itemblock_stone_fullslit_2, 3, "conquest:sandstone_slit");
		registerRenderMetas(itemblock_stone_fullslit_3, 0, "conquest:stone_colorfulslate_slit");
		registerRenderMetas(itemblock_stone_fullslit_3, 1, "conquest:cobblestone_old_slit");
		registerRenderMetas(itemblock_stone_fullslit_3, 2, "conquest:cobblestone_damaged_slit");
		registerRenderMetas(itemblock_stone_fullslit_3, 3, "conquest:none"); //open slot
		registerRenderMetas(itemblock_stone_fullslit_4, 0, "conquest:cobblestone_mossy_slit");
		registerRenderMetas(itemblock_stone_fullslit_4, 1, "conquest:cobblestone_overgrown_slit");
		registerRenderMetas(itemblock_stone_fullslit_4, 2, "conquest:cobblestone_vines_slit");
		registerRenderMetas(itemblock_stone_fullslit_4, 3, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_fullslit_5, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_fullslit_5, 1, "conquest:groove_portculis_slit");
		registerRenderMetas(itemblock_stone_fullslit_5, 2, "conquest:netherbrick_big_slit");
		registerRenderMetas(itemblock_stone_fullslit_5, 3, "conquest:ironblock_dark_slit");
		registerRenderMetas(itemblock_stone_fullslit_6, 0, "conquest:marble_slit");
		registerRenderMetas(itemblock_stone_fullslit_6, 1, "conquest:marble_smoothwhite_slit");
		registerRenderMetas(itemblock_stone_fullslit_6, 2, "conquest:bigslab_marble1_slit");
		registerRenderMetas(itemblock_stone_fullslit_6, 3, "conquest:bigslab_sandstone1_slit");
		registerRenderMetas(itemblock_stone_fullslit_7, 0, "conquest:bigslab_black1_slit");
		registerRenderMetas(itemblock_stone_fullslit_7, 1, "conquest:brick_diorite_slit");
		registerRenderMetas(itemblock_stone_fullslit_7, 2, "conquest:brick_travertine_slit");
		registerRenderMetas(itemblock_stone_fullslit_7, 3, "conquest:sandstone_conglomerate_slit");
		registerRenderMetas(itemblock_stone_fullslit_8, 0, "conquest:sandstone_mossy_slit");
		registerRenderMetas(itemblock_stone_fullslit_8, 1, "conquest:brick_sandstone_slit");
		registerRenderMetas(itemblock_stone_fullslit_8, 2, "conquest:tile_clay_slit");
		registerRenderMetas(itemblock_stone_fullslit_8, 3, "conquest:tile_mixedclay_slit");
		registerRenderMetas(itemblock_stone_fullslit_9, 0, "conquest:tile_lightclay_slit");
		registerRenderMetas(itemblock_stone_fullslit_9, 1, "conquest:ironblock_rusty_slit");
		registerRenderMetas(itemblock_stone_fullslit_9, 2, "conquest:brick_mossy_slit");
		registerRenderMetas(itemblock_stone_fullslit_9, 3, "conquest:brick_darkred_slit");
		registerRenderMetas(itemblock_stone_fullslit_10, 0, "conquest:brick_darkredmossy_slit");
		registerRenderMetas(itemblock_stone_fullslit_10, 1, "conquest:brick_red_slit");
		registerRenderMetas(itemblock_stone_fullslit_10, 2, "conquest:brick_redmossy_slit");
		registerRenderMetas(itemblock_stone_fullslit_10, 3, "conquest:brick_roman_slit");
		registerRenderMetas(itemblock_stone_fullslit_11, 0, "conquest:stucco_white_slit");
		registerRenderMetas(itemblock_stone_fullslit_11, 1, "conquest:stucco_tan_slit");
		registerRenderMetas(itemblock_stone_fullslit_11, 2, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_fullslit_11, 3, "conquest:stucco_brown_slit");
		registerRenderMetas(itemblock_stone_fullslit_12, 0, "conquest:stucco_purple_slit");
		registerRenderMetas(itemblock_stone_fullslit_12, 1, "conquest:stucco_magenta_slit");
		registerRenderMetas(itemblock_stone_fullslit_12, 2, "conquest:walldesign_red8_slit");
		registerRenderMetas(itemblock_stone_fullslit_12, 3, "conquest:stucco_lightred_slit");
		registerRenderMetas(itemblock_stone_fullslit_13, 0, "conquest:stucco_lime_slit");
		registerRenderMetas(itemblock_stone_fullslit_13, 1, "conquest:stucco_green_slit");
		registerRenderMetas(itemblock_stone_fullslit_13, 2, "conquest:stucco_cyan_slit");
		registerRenderMetas(itemblock_stone_fullslit_13, 3, "conquest:stucco_lightblue_slit");
		registerRenderMetas(itemblock_stone_fullslit_14, 0, "conquest:stucco_blue_slit");
		registerRenderMetas(itemblock_stone_fullslit_14, 1, "conquest:stucco_black_slit");
		registerRenderMetas(itemblock_stone_fullslit_14, 2, "conquest:granite_polished_slit");
		registerRenderMetas(itemblock_stone_fullslit_14, 3, "conquest:diorite_polished_slit");
		registerRenderMetas(itemblock_stone_fullslit_15, 0, "conquest:andesite_polished_slit");
		registerRenderMetas(itemblock_stone_fullslit_15, 1, "conquest:iron_normal_slit");
		registerRenderMetas(itemblock_stone_fullslit_15, 2, "conquest:cobblestone_mossynormal_slit");
		registerRenderMetas(itemblock_stone_fullslit_15, 3, "conquest:stonebrick_mossy_slit");
		registerRenderMetas(itemblock_stone_fullslit_16, 0, "conquest:stonebrick_cracked_slit");
		registerRenderMetas(itemblock_stone_fullslit_16, 1, "conquest:netherbrick_normal_slit");
		registerRenderMetas(itemblock_stone_fullslit_16, 2, "conquest:quartz_normal_slit");
		registerRenderMetas(itemblock_stone_fullslit_16, 3, "conquest:plaster_magenta_slit");
		registerRenderMetas(itemblock_stone_fullslit_17, 0, "conquest:plaster_lightgray_slit");
		registerRenderMetas(itemblock_stone_fullslit_17, 1, "conquest:plaster_purple_slit");
		registerRenderMetas(itemblock_stone_fullslit_17, 2, "conquest:prismarine_normal_slit");
		registerRenderMetas(itemblock_stone_fullslit_17, 3, "conquest:prismarine_bricks_slit");
		registerRenderMetas(itemblock_stone_fullslit_18, 0, "conquest:prismarine_dark_slit");
		registerRenderMetas(itemblock_stone_fullslit_18, 1, "conquest:sandstone_red_slit");
		registerRenderMetas(itemblock_stone_fullslit_18, 2, "conquest:purpur_normal_slit");
		registerRenderMetas(itemblock_stone_fullslit_18, 3, "conquest:endstone_bricks_slit");
		registerRenderMetas(itemblock_stone_fullslit_19, 0, "conquest:netherbrick_red_slit");
		registerRenderMetas(itemblock_stone_fullslit_19, 1, "conquest:brick_romanvertical_slit");
		registerRenderMetas(itemblock_stone_fullslit_19, 2, "conquest:marble_black_slit");
		registerRenderMetas(itemblock_stone_fullslit_19, 3, "conquest:marble_blue_slit");
		registerRenderMetas(itemblock_stone_fullslit_20, 0, "conquest:marble_gray_slit");
		registerRenderMetas(itemblock_stone_fullslit_20, 1, "conquest:marble_green_slit");
		registerRenderMetas(itemblock_stone_fullslit_20, 2, "conquest:marble_pink_slit");
		registerRenderMetas(itemblock_stone_fullslit_20, 3, "conquest:marble_red_slit");
		registerRenderMetas(itemblock_stone_fullslit_21, 0, "conquest:brick_darkroman_slit");
		registerRenderMetas(itemblock_stone_fullslit_21, 1, "conquest:brick_darkromansandstone_slit");
		registerRenderMetas(itemblock_stone_fullslit_21, 2, "conquest:brick_romansandstone_slit");
		registerRenderMetas(itemblock_stone_fullslit_21, 3, "conquest:cobblestone_dark_slit");
		registerRenderMetas(itemblock_stone_fullslit_22, 0, "conquest:sandstone_darkcobble_slit");
		registerRenderMetas(itemblock_stone_fullslit_22, 1, "conquest:slate_lightmortar_slit");
		registerRenderMetas(itemblock_stone_fullslit_22, 2, "conquest:plasterwork_white_slit");
		registerRenderMetas(itemblock_stone_fullslit_22, 3, "conquest:plasterwork_magentaclean_slit");
		registerRenderMetas(itemblock_stone_fullslit_23, 0, "conquest:plasterwork_lightgrayclean_slit");
		registerRenderMetas(itemblock_stone_fullslit_23, 1, "conquest:plasterwork_yellowclean_slit");
		registerRenderMetas(itemblock_stone_fullslit_23, 2, "conquest:plasterwork_whiteclean_slit");
		registerRenderMetas(itemblock_stone_fullslit_23, 3, "conquest:plasterwork_purpleclean_slit");
		registerRenderMetas(itemblock_stone_fullslit_24, 0, "conquest:plasterwork_orange_slit");
		registerRenderMetas(itemblock_stone_fullslit_24, 1, "conquest:cobblestone_hewntiles_slit");
		registerRenderMetas(itemblock_stone_fullslit_24, 2, "conquest:cobblestone_plasteredtiles_slit");
		registerRenderMetas(itemblock_stone_fullslit_24, 3, "conquest:none");



	//Anvil Stone Blocks
		registerRenderMetas(itemblock_stone_anvil_1, 0, "conquest:stone_plastered_anvil");
		registerRenderMetas(itemblock_stone_anvil_1, 1, "conquest:stone_anvil");
		registerRenderMetas(itemblock_stone_anvil_1, 2, "conquest:marble_anvil");
		registerRenderMetas(itemblock_stone_anvil_1, 3, "conquest:none"); //open space for block
		registerRenderMetas(itemblock_stone_anvil_2, 0, "conquest:stone_hewn_anvil");
		registerRenderMetas(itemblock_stone_anvil_2, 1, "conquest:stone_colorfulslate_anvil");
		registerRenderMetas(itemblock_stone_anvil_2, 2, "conquest:cobblestone_old_anvil");
		registerRenderMetas(itemblock_stone_anvil_2, 3, "conquest:cobblestone_damaged_anvil");
		registerRenderMetas(itemblock_stone_anvil_3, 0, "conquest:none"); //open slot
		registerRenderMetas(itemblock_stone_anvil_3, 1, "conquest:cobblestone_mossy_anvil");
		registerRenderMetas(itemblock_stone_anvil_3, 2, "conquest:cobblestone_overgrown_anvil");
		registerRenderMetas(itemblock_stone_anvil_3, 3, "conquest:cobblestone_vines_anvil");
		registerRenderMetas(itemblock_stone_anvil_4, 0, "conquest:none"); //open slot
		registerRenderMetas(itemblock_stone_anvil_4, 1, "conquest:groove_portculis_anvil");
		registerRenderMetas(itemblock_stone_anvil_4, 2, "conquest:netherbrick_big_anvil");
		registerRenderMetas(itemblock_stone_anvil_4, 3, "conquest:netherbrick_carved_anvil");
		registerRenderMetas(itemblock_stone_anvil_5, 0, "conquest:tiles_red_anvil");
		registerRenderMetas(itemblock_stone_anvil_5, 1, "conquest:ironblock_dark_anvil");
		registerRenderMetas(itemblock_stone_anvil_5, 2, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_anvil_5, 3, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_anvil_6, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_anvil_6, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_anvil_6, 2, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_anvil_6, 3, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_anvil_7, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_anvil_7, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_anvil_7, 2, "conquest:cornice_sandstone_anvil");
		registerRenderMetas(itemblock_stone_anvil_7, 3, "conquest:plinth_sandstone_anvil");
		registerRenderMetas(itemblock_stone_anvil_8, 0, "conquest:base_marblesandstone_anvil");
		registerRenderMetas(itemblock_stone_anvil_8, 1, "conquest:cornice_marble_anvil");
		registerRenderMetas(itemblock_stone_anvil_8, 2, "conquest:baseplain_marble_anvil");
		registerRenderMetas(itemblock_stone_anvil_8, 3, "conquest:marble_anvil");
		registerRenderMetas(itemblock_stone_anvil_9, 0, "conquest:marble_smoothwhite_anvil");
		registerRenderMetas(itemblock_stone_anvil_9, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_anvil_9, 2, "conquest:bigslab_marble1_anvil");
		registerRenderMetas(itemblock_stone_anvil_9, 3, "conquest:bigslab_sandstone1_anvil");
		registerRenderMetas(itemblock_stone_anvil_10, 0, "conquest:bigslab_sandstoneinscribed1_anvil");
		registerRenderMetas(itemblock_stone_anvil_10, 1, "conquest:bigslab_black1_anvil");
		registerRenderMetas(itemblock_stone_anvil_10, 2, "conquest:bigslab_blackinscribed1_anvil");
		registerRenderMetas(itemblock_stone_anvil_10, 3, "conquest:brick_diorite_anvil");
		registerRenderMetas(itemblock_stone_anvil_11, 0, "conquest:brick_travertine_anvil");
		registerRenderMetas(itemblock_stone_anvil_11, 1, "conquest:brick_sandstonebig_anvil");
		registerRenderMetas(itemblock_stone_anvil_11, 2, "conquest:sandstone_conglomerate_anvil");
		registerRenderMetas(itemblock_stone_anvil_11, 3, "conquest:sandstone_mossy_anvil");
		registerRenderMetas(itemblock_stone_anvil_12, 0, "conquest:frieze_sandstone_anvil");
		registerRenderMetas(itemblock_stone_anvil_12, 1, "conquest:brick_sandstone_anvil");
		registerRenderMetas(itemblock_stone_anvil_12, 2, "conquest:tile_clay_anvil");
		registerRenderMetas(itemblock_stone_anvil_12, 3, "conquest:tile_mixedclay_anvil");
		registerRenderMetas(itemblock_stone_anvil_13, 0, "conquest:tile_lightclay_anvil");
		registerRenderMetas(itemblock_stone_anvil_13, 1, "conquest:ironblock_rusty_anvil");
		registerRenderMetas(itemblock_stone_anvil_13, 2, "conquest:brick_mossy_anvil");
		registerRenderMetas(itemblock_stone_anvil_13, 3, "conquest:brick_darkred_anvil");
		registerRenderMetas(itemblock_stone_anvil_14, 0, "conquest:brick_darkredmossy_anvil");
		registerRenderMetas(itemblock_stone_anvil_14, 1, "conquest:brick_red_anvil");
		registerRenderMetas(itemblock_stone_anvil_14, 2, "conquest:brick_redmossy_anvil");
		registerRenderMetas(itemblock_stone_anvil_14, 3, "conquest:brick_roman_anvil");
		registerRenderMetas(itemblock_stone_anvil_15, 0, "conquest:stucco_white_anvil");
		registerRenderMetas(itemblock_stone_anvil_15, 1, "conquest:stucco_tan_anvil");
		registerRenderMetas(itemblock_stone_anvil_15, 2, "conquest:stucco_brown_anvil");
		registerRenderMetas(itemblock_stone_anvil_15, 3, "conquest:stucco_purple_anvil");
		registerRenderMetas(itemblock_stone_anvil_16, 0, "conquest:walldesign_red8_anvil");
		registerRenderMetas(itemblock_stone_anvil_16, 1, "conquest:stucco_lightred_anvil");
		registerRenderMetas(itemblock_stone_anvil_16, 2, "conquest:stucco_lime_anvil");
		registerRenderMetas(itemblock_stone_anvil_16, 3, "conquest:stucco_green_anvil");
		registerRenderMetas(itemblock_stone_anvil_17, 0, "conquest:stucco_cyan_anvil");
		registerRenderMetas(itemblock_stone_anvil_17, 1, "conquest:stucco_lightblue_anvil");
		registerRenderMetas(itemblock_stone_anvil_17, 2, "conquest:stucco_blue_anvil");
		registerRenderMetas(itemblock_stone_anvil_17, 3, "conquest:stucco_black_anvil");
		registerRenderMetas(itemblock_stone_anvil_18, 0, "conquest:bronzeblock_gilded_anvil");
		registerRenderMetas(itemblock_stone_anvil_18, 1, "conquest:granite_polished_anvil");
		registerRenderMetas(itemblock_stone_anvil_18, 2, "conquest:diorite_polished_anvil");
		registerRenderMetas(itemblock_stone_anvil_18, 3, "conquest:andesite_polished_anvil");
		registerRenderMetas(itemblock_stone_anvil_19, 0, "conquest:cobblestone_normal_anvil");
		registerRenderMetas(itemblock_stone_anvil_19, 1, "conquest:sandstone_normal_anvil");
		registerRenderMetas(itemblock_stone_anvil_19, 2, "conquest:iron_normal_anvil");
		registerRenderMetas(itemblock_stone_anvil_19, 3, "conquest:bricks_normal_anvil");
		registerRenderMetas(itemblock_stone_anvil_20, 0, "conquest:cobblestone_mossynormal_anvil");
		registerRenderMetas(itemblock_stone_anvil_20, 1, "conquest:stonebrick_normal_anvil");
		registerRenderMetas(itemblock_stone_anvil_20, 2, "conquest:stonebrick_mossy_anvil");
		registerRenderMetas(itemblock_stone_anvil_20, 3, "conquest:stonebrick_cracked_anvil");
		registerRenderMetas(itemblock_stone_anvil_21, 0, "conquest:netherbrick_normal_anvil");
		registerRenderMetas(itemblock_stone_anvil_21, 1, "conquest:plaster_magenta_anvil");
		registerRenderMetas(itemblock_stone_anvil_21, 2, "conquest:plaster_lightgray_anvil");
		registerRenderMetas(itemblock_stone_anvil_21, 3, "conquest:plaster_purple_anvil");
		registerRenderMetas(itemblock_stone_anvil_22, 0, "conquest:prismarine_normal_anvil");
		registerRenderMetas(itemblock_stone_anvil_22, 1, "conquest:prismarine_bricks_anvil");
		registerRenderMetas(itemblock_stone_anvil_22, 2, "conquest:prismarine_dark_anvil");
		registerRenderMetas(itemblock_stone_anvil_22, 3, "conquest:sandstone_red_anvil");
		registerRenderMetas(itemblock_stone_anvil_23, 0, "conquest:purpur_normal_anvil");
		registerRenderMetas(itemblock_stone_anvil_23, 1, "conquest:endstone_bricks_anvil");
		registerRenderMetas(itemblock_stone_anvil_23, 2, "conquest:netherbrick_wart_anvil");
		registerRenderMetas(itemblock_stone_anvil_23, 3, "conquest:brick_romanvertical_anvil");
		registerRenderMetas(itemblock_stone_anvil_24, 0, "conquest:marble_black_anvil");
		registerRenderMetas(itemblock_stone_anvil_24, 1, "conquest:marble_blue_anvil");
		registerRenderMetas(itemblock_stone_anvil_24, 2, "conquest:marble_gray_anvil");
		registerRenderMetas(itemblock_stone_anvil_24, 3, "conquest:marble_green_anvil");
		registerRenderMetas(itemblock_stone_anvil_25, 0, "conquest:marble_pink_anvil");
		registerRenderMetas(itemblock_stone_anvil_25, 1, "conquest:marble_red_anvil");
		registerRenderMetas(itemblock_stone_anvil_25, 2, "conquest:brick_darkroman_anvil");
		registerRenderMetas(itemblock_stone_anvil_25, 3, "conquest:cobblestone_dark_anvil");
		registerRenderMetas(itemblock_stone_anvil_26, 0, "conquest:sandstone_darkcobble_anvil");
		registerRenderMetas(itemblock_stone_anvil_26, 1, "conquest:slate_lightmortar_anvil");
		registerRenderMetas(itemblock_stone_anvil_26, 2, "conquest:plasterwork_white_anvil");
		registerRenderMetas(itemblock_stone_anvil_26, 3, "conquest:plasterwork_magentaclean_anvil");
		registerRenderMetas(itemblock_stone_anvil_27, 0, "conquest:plasterwork_lightgrayclean_anvil");
		registerRenderMetas(itemblock_stone_anvil_27, 1, "conquest:plasterwork_yellowclean_anvil");
		registerRenderMetas(itemblock_stone_anvil_27, 2, "conquest:plasterwork_whiteclean_anvil");
		registerRenderMetas(itemblock_stone_anvil_27, 3, "conquest:plasterwork_purpleclean_anvil");
		registerRenderMetas(itemblock_stone_anvil_28, 0, "conquest:plasterwork_orange_anvil");
		registerRenderMetas(itemblock_stone_anvil_28, 1, "conquest:cobblestone_hewntiles_anvil");
		registerRenderMetas(itemblock_stone_anvil_28, 2, "conquest:cobblestone_plasteredtiles_anvil");
		registerRenderMetas(itemblock_stone_anvil_28, 3, "conquest:none");

	//Hopper Full Stone Blocks
		registerRenderMetas(itemblock_stone_hopperfull_1, 0, "conquest:stone_plastered_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_1, 1, "conquest:stone_hewn_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_1, 2, "conquest:stone_colorfulslate_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_1, 3, "conquest:cobblestone_old_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_1, 4, "conquest:cobblestone_damaged_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_1, 5, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_hopperfull_1, 6, "conquest:cobblestone_mossy_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_1, 7, "conquest:cobblestone_overgrown_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_1, 8, "conquest:cobblestone_vines_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_1, 9, "conquest:none"); //open slot
		registerRenderMetas(itemblock_stone_hopperfull_1, 10, "conquest:groove_portculis_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_1, 11, "conquest:netherbrick_big_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_1, 12, "conquest:netherbrick_carved_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_1, 13, "conquest:tiles_red_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_1, 14, "conquest:ironblock_dark_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_1, 15, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_hopperfull_2, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_hopperfull_2, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_hopperfull_2, 2, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_hopperfull_2, 3, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_hopperfull_2, 4, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_hopperfull_2, 5, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_hopperfull_2, 6, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_hopperfull_2, 7, "conquest:cornice_sandstone_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_2, 8, "conquest:base_sandstone_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_2, 9, "conquest:base_marblesandstone_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_2, 10, "conquest:cornice_marble_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_2, 11, "conquest:baseplain_marble_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_2, 12, "conquest:marble_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_2, 13, "conquest:marble_smoothwhite_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_2, 14, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_hopperfull_2, 15, "conquest:bigslab_marble1_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_3, 0, "conquest:bigslab_sandstone1_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_3, 1, "conquest:bigslab_sandstoneinscribed1_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_3, 2, "conquest:bigslab_black1_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_3, 3, "conquest:bigslab_blackinscribed1_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_3, 4, "conquest:brick_diorite_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_3, 5, "conquest:brick_travertine_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_3, 6, "conquest:brick_sandstonebig_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_3, 7, "conquest:sandstone_conglomerate_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_3, 8, "conquest:sandstone_mossy_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_3, 9, "conquest:frieze_sandstone_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_3, 10, "conquest:brick_sandstone_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_3, 11, "conquest:tile_clay_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_3, 12, "conquest:tile_mixedclay_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_3, 13, "conquest:tile_lightclay_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_3, 14, "conquest:ironblock_rusty_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_3, 15, "conquest:brick_mossy_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_4, 0, "conquest:brick_darkred_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_4, 1, "conquest:brick_darkredmossy_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_4, 2, "conquest:brick_red_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_4, 3, "conquest:brick_redmossy_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_4, 4, "conquest:brick_roman_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_4, 5, "conquest:stucco_white_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_4, 6, "conquest:stucco_tan_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_4, 7, "conquest:stucco_brown_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_4, 8, "conquest:stucco_purple_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_4, 9, "conquest:walldesign_red8_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_4, 10, "conquest:stucco_lightred_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_4, 11, "conquest:stucco_lime_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_4, 12, "conquest:stucco_green_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_4, 13, "conquest:stucco_cyan_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_4, 14, "conquest:stucco_lightblue_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_4, 15, "conquest:stucco_blue_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_5, 0, "conquest:stucco_black_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_5, 1, "conquest:bronzeblock_gilded_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_5, 2, "conquest:granite_polished_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_5, 3, "conquest:diorite_polished_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_5, 4, "conquest:andesite_polished_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_5, 5, "conquest:cobblestone_normal_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_5, 6, "conquest:sandstone_normal_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_5, 7, "conquest:iron_normal_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_5, 8, "conquest:bricks_normal_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_5, 9, "conquest:cobblestone_mossynormal_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_5, 10, "conquest:stonebrick_normal_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_5, 11, "conquest:stonebrick_mossy_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_5, 12, "conquest:stonebrick_cracked_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_5, 13, "conquest:netherbrick_normal_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_5, 14, "conquest:quartz_normal_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_5, 15, "conquest:plaster_magenta_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_6, 0, "conquest:plaster_lightgray_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_6, 1, "conquest:plaster_purple_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_6, 2, "conquest:prismarine_normal_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_6, 3, "conquest:prismarine_bricks_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_6, 4, "conquest:prismarine_dark_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_6, 5, "conquest:sandstone_red_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_6, 6, "conquest:purpur_normal_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_6, 7, "conquest:endstone_bricks_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_6, 8, "conquest:netherbrick_wart_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_6, 9, "conquest:brick_romanvertical_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_6, 10, "conquest:marble_black_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_6, 11, "conquest:marble_blue_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_6, 12, "conquest:marble_gray_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_6, 13, "conquest:marble_green_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_6, 14, "conquest:marble_pink_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_6, 15, "conquest:marble_red_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_7, 0, "conquest:brick_darkroman_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_7, 1, "conquest:cobblestone_dark_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_7, 2, "conquest:sandstone_darkcobble_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_7, 3, "conquest:slate_lightmortar_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_7, 4, "conquest:plasterwork_white_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_7, 5, "conquest:plasterwork_magentaclean_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_7, 6, "conquest:plasterwork_lightgrayclean_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_7, 7, "conquest:plasterwork_yellowclean_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_7, 8, "conquest:plasterwork_whiteclean_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_7, 9, "conquest:plasterwork_purpleclean_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_7, 10, "conquest:plasterwork_orange_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_7, 11, "conquest:cobblestone_hewntiles_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_7, 12, "conquest:cobblestone_plasteredtiles_hopperfull");
		registerRenderMetas(itemblock_stone_hopperfull_7, 13, "conquest:none");
		registerRenderMetas(itemblock_stone_hopperfull_7, 14, "conquest:none");
		registerRenderMetas(itemblock_stone_hopperfull_7, 15, "conquest:none");
	//Hopper Directional Stone Blocks
		registerRenderMetas(itemblock_stone_hopperdirectional_1, 0, "conquest:stone_plastered_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_1, 1, "conquest:stone_hewn_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_1, 2, "conquest:stone_colorfulslate_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_1, 3, "conquest:cobblestone_old_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_2, 0, "conquest:cobblestone_damaged_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_2, 1, "conquest:none"); //open slot
		registerRenderMetas(itemblock_stone_hopperdirectional_2, 2, "conquest:cobblestone_mossy_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_2, 3, "conquest:cobblestone_overgrown_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_3, 0, "conquest:cobblestone_vines_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_3, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_hopperdirectional_3, 2, "conquest:groove_portculis_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_3, 3, "conquest:netherbrick_big_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_4, 0, "conquest:netherbrick_carved_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_4, 1, "conquest:tiles_red_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_4, 2, "conquest:ironblock_dark_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_4, 3, "conquest:none"); //open slot
		registerRenderMetas(itemblock_stone_hopperdirectional_5, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_hopperdirectional_5, 1, "conquest:marble_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_5, 2, "conquest:marble_smoothwhite_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_5, 3, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_hopperdirectional_6, 0, "conquest:bigslab_marble1_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_6, 1, "conquest:bigslab_sandstone1_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_6, 2, "conquest:bigslab_sandstoneinscribed1_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_6, 3, "conquest:bigslab_black1_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_7, 0, "conquest:bigslab_blackinscribed1_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_7, 1, "conquest:brick_diorite_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_7, 2, "conquest:brick_travertine_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_7, 3, "conquest:brick_sandstonebig_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_8, 0, "conquest:sandstone_conglomerate_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_8, 1, "conquest:sandstone_mossy_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_8, 2, "conquest:frieze_sandstone_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_8, 3, "conquest:brick_sandstone_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_9, 0, "conquest:tile_clay_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_9, 1, "conquest:tile_mixedclay_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_9, 2, "conquest:tile_lightclay_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_9, 3, "conquest:ironblock_rusty_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_10, 0, "conquest:brick_mossy_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_10, 1, "conquest:brick_darkred_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_10, 2, "conquest:brick_darkredmossy_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_10, 3, "conquest:brick_red_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_11, 0, "conquest:brick_redmossy_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_11, 1, "conquest:brick_roman_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_11, 2, "conquest:stucco_white_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_11, 3, "conquest:stucco_tan_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_12, 0, "conquest:stucco_brown_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_12, 1, "conquest:stucco_purple_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_12, 2, "conquest:walldesign_red8_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_12, 3, "conquest:stucco_lightred_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_13, 0, "conquest:stucco_lime_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_13, 1, "conquest:stucco_green_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_13, 2, "conquest:stucco_cyan_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_13, 3, "conquest:stucco_lightblue_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_14, 0, "conquest:stucco_blue_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_14, 1, "conquest:stucco_black_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_14, 2, "conquest:bronzeblock_gilded_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_14, 3, "conquest:granite_polished_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_15, 0, "conquest:diorite_polished_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_15, 1, "conquest:andesite_polished_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_15, 2, "conquest:cobblestone_normal_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_15, 3, "conquest:sandstone_normal_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_16, 0, "conquest:iron_normal_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_16, 1, "conquest:bricks_normal_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_16, 2, "conquest:cobblestone_mossynormal_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_16, 3, "conquest:stonebrick_normal_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_17, 0, "conquest:stonebrick_mossy_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_17, 1, "conquest:stonebrick_cracked_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_17, 2, "conquest:netherbrick_normal_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_17, 3, "conquest:quartz_normal_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_18, 0, "conquest:plaster_magenta_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_18, 1, "conquest:plaster_lightgray_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_18, 2, "conquest:plaster_purple_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_18, 3, "conquest:prismarine_normal_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_19, 0, "conquest:prismarine_bricks_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_19, 1, "conquest:prismarine_dark_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_19, 2, "conquest:sandstone_red_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_19, 3, "conquest:purpur_normal_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_20, 0, "conquest:endstone_bricks_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_20, 1, "conquest:netherbrick_wart_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_20, 2, "conquest:brick_romanvertical_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_20, 3, "conquest:marble_black_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_21, 0, "conquest:marble_blue_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_21, 1, "conquest:marble_gray_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_21, 2, "conquest:marble_green_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_21, 3, "conquest:marble_pink_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_22, 0, "conquest:marble_red_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_22, 1, "conquest:brick_darkroman_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_22, 2, "conquest:cobblestone_dark_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_22, 3, "conquest:sandstone_darkcobble_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_23, 0, "conquest:slate_lightmortar_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_23, 1, "conquest:plasterwork_white_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_23, 2, "conquest:plasterwork_magentaclean_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_23, 3, "conquest:plasterwork_lightgrayclean_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_24, 0, "conquest:plasterwork_yellowclean_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_24, 1, "conquest:plasterwork_whiteclean_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_24, 2, "conquest:plasterwork_purpleclean_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_24, 3, "conquest:plasterwork_orange_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_25, 0, "conquest:cobblestone_hewntiles_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_25, 1, "conquest:cobblestone_plasteredtiles_hopperdirectional");
		registerRenderMetas(itemblock_stone_hopperdirectional_25, 2, "conquest:none");
		registerRenderMetas(itemblock_stone_hopperdirectional_25, 3, "conquest:none");
	//Directional Partial Full Stone Blocks
		registerRenderMetas(itemblock_stone_log_1, 0, "conquest:column_sandstone");
		registerRenderMetas(itemblock_stone_log_1, 1, "conquest:column_red");
		registerRenderMetas(itemblock_stone_log_1, 2, "conquest:column_blue");
		registerRenderMetas(itemblock_stone_log_1, 3, "conquest:column_gold");
		registerRenderMetas(itemblock_stone_log_2, 0, "conquest:column_marble");
		registerRenderMetas(itemblock_stone_log_2, 1, "conquest:none");
		registerRenderMetas(itemblock_stone_log_2, 2, "conquest:none");
		registerRenderMetas(itemblock_stone_log_2, 3, "conquest:none");
	//Wall Stone Blocks
		registerRenderMetas(itemblock_stone_wall_1, 0, "conquest:stone_plastered_wall");
		registerRenderMetas(itemblock_stone_wall_1, 1, "conquest:stone_hewn_wall");
		registerRenderMetas(itemblock_stone_wall_1, 2, "conquest:stonebrick_mossy_wall");
		registerRenderMetas(itemblock_stone_wall_1, 3, "conquest:stonebrick_wall");
		registerRenderMetas(itemblock_stone_wall_1, 4, "conquest:blackmarble_wall");
		registerRenderMetas(itemblock_stone_wall_1, 5, "conquest:netherbrick_wall");
		registerRenderMetas(itemblock_stone_wall_1, 6, "conquest:brick_wall");
		registerRenderMetas(itemblock_stone_wall_1, 7, "conquest:redbrick_dark_wall");
		registerRenderMetas(itemblock_stone_wall_1, 8, "conquest:redbrick_wall");
		registerRenderMetas(itemblock_stone_wall_1, 9, "conquest:brick_roman_wall");
		registerRenderMetas(itemblock_stone_wall_1, 10, "conquest:brick_travertine_wall");
		registerRenderMetas(itemblock_stone_wall_1, 11, "conquest:sandstone_big_wall");
		registerRenderMetas(itemblock_stone_wall_1, 12, "conquest:sandstone_wall");
		registerRenderMetas(itemblock_stone_wall_1, 13, "conquest:brick_sandstone_wall");
		registerRenderMetas(itemblock_stone_wall_1, 14, "conquest:quartz_pillar_wall");
		registerRenderMetas(itemblock_stone_wall_1, 15, "conquest:rooftile_red_wall");
		registerRenderMetas(itemblock_stone_wall_2, 0, "conquest:rooftile_pink_wall");
		registerRenderMetas(itemblock_stone_wall_2, 1, "conquest:rooftile_gray_wall");
		registerRenderMetas(itemblock_stone_wall_2, 2, "conquest:rooftile_brown_wall");
		registerRenderMetas(itemblock_stone_wall_2, 3, "conquest:rooftile_blue_wall");
		registerRenderMetas(itemblock_stone_wall_2, 4, "conquest:rooftile_cyan_wall");
		registerRenderMetas(itemblock_stone_wall_2, 5, "conquest:rooftile_lightblue_wall");
		registerRenderMetas(itemblock_stone_wall_2, 6, "conquest:rooftile_green_wall");
		registerRenderMetas(itemblock_stone_wall_2, 7, "conquest:rooftile_lightgreen_wall");
		registerRenderMetas(itemblock_stone_wall_2, 8, "conquest:oxidized_copper_wall");
		registerRenderMetas(itemblock_stone_wall_2, 9, "conquest:stone_colorfulslate_wall");
		registerRenderMetas(itemblock_stone_wall_2, 10, "conquest:cobblestone_old_wall");
		registerRenderMetas(itemblock_stone_wall_2, 11, "conquest:cobblestone_damaged_wall");
		registerRenderMetas(itemblock_stone_wall_2, 12, "conquest:iron_dark_wall");
		registerRenderMetas(itemblock_stone_wall_2, 13, "conquest:cobblestone_mossy_wall");
		registerRenderMetas(itemblock_stone_wall_2, 14, "conquest:cobblestone_overgrown_wall");
		registerRenderMetas(itemblock_stone_wall_2, 15, "conquest:cobblestone_vines_wall");
		registerRenderMetas(itemblock_stone_wall_3, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_wall_3, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_wall_3, 2, "conquest:groove_portculis_wall");
		registerRenderMetas(itemblock_stone_wall_3, 3, "conquest:pillar_stone_wall");
		registerRenderMetas(itemblock_stone_wall_3, 4, "conquest:netherbrick_wall_wall");
		registerRenderMetas(itemblock_stone_wall_3, 5, "conquest:netherbrick_big_wall");
		registerRenderMetas(itemblock_stone_wall_3, 6, "conquest:netherbrick_pillar_wall");
		registerRenderMetas(itemblock_stone_wall_3, 7, "conquest:tiles_red_wall");
		registerRenderMetas(itemblock_stone_wall_3, 8, "conquest:iron_normal_wall");
		registerRenderMetas(itemblock_stone_wall_3, 9, "conquest:column_sandstone_wall");
		registerRenderMetas(itemblock_stone_wall_3, 10, "conquest:capital_sandstone_wall");
		registerRenderMetas(itemblock_stone_wall_3, 11, "conquest:base_sandstone_wall");
		registerRenderMetas(itemblock_stone_wall_3, 12, "conquest:column_red_wall");
		registerRenderMetas(itemblock_stone_wall_3, 13, "conquest:capital_redmarble_wall");
		registerRenderMetas(itemblock_stone_wall_3, 14, "conquest:base_redmarble_wall");
		registerRenderMetas(itemblock_stone_wall_3, 15, "conquest:capital_redsandstone_wall");
		registerRenderMetas(itemblock_stone_wall_4, 0, "conquest:base_redsandstone_wall");
		registerRenderMetas(itemblock_stone_wall_4, 1, "conquest:column_blue_wall");
		registerRenderMetas(itemblock_stone_wall_4, 2, "conquest:capital_bluemarble_wall");
		registerRenderMetas(itemblock_stone_wall_4, 3, "conquest:base_bluemarble_wall");
		registerRenderMetas(itemblock_stone_wall_4, 4, "conquest:capital_bluesandstone_wall");
		registerRenderMetas(itemblock_stone_wall_4, 5, "conquest:base_bluesandstone_wall");
		registerRenderMetas(itemblock_stone_wall_4, 6, "conquest:column_gold_wall");
		registerRenderMetas(itemblock_stone_wall_4, 7, "conquest:capital_goldmarble_wall");
		registerRenderMetas(itemblock_stone_wall_4, 8, "conquest:base_goldmarble_wall");
		registerRenderMetas(itemblock_stone_wall_4, 9, "conquest:capital_goldsandstone_wall");
		registerRenderMetas(itemblock_stone_wall_4, 10, "conquest:base_goldsandstone_wall");
		registerRenderMetas(itemblock_stone_wall_4, 11, "conquest:column_marble_wall");
		registerRenderMetas(itemblock_stone_wall_4, 12, "conquest:capital_marble_wall");
		registerRenderMetas(itemblock_stone_wall_4, 13, "conquest:base_marble_wall");
		registerRenderMetas(itemblock_stone_wall_4, 14, "conquest:capitalcorinthian_wall");
		registerRenderMetas(itemblock_stone_wall_4, 15, "conquest:capitalcorinthian_sandstone_wall");
		registerRenderMetas(itemblock_stone_wall_5, 0, "conquest:cornice_sandstone_wall");
		registerRenderMetas(itemblock_stone_wall_5, 1, "conquest:plinth_sandstone_wall");
		registerRenderMetas(itemblock_stone_wall_5, 2, "conquest:base_marblesandstone_wall");
		registerRenderMetas(itemblock_stone_wall_5, 3, "conquest:cornice_marble_wall");
		registerRenderMetas(itemblock_stone_wall_5, 4, "conquest:baseplain_marble_wall");
		registerRenderMetas(itemblock_stone_wall_5, 5, "conquest:marble_wall");
		registerRenderMetas(itemblock_stone_wall_5, 6, "conquest:marble_smoothwhite_wall");
		registerRenderMetas(itemblock_stone_wall_5, 7, "conquest:base_parianmarble_wall");
		registerRenderMetas(itemblock_stone_wall_5, 8, "conquest:capital_parianmarble_wall");
		registerRenderMetas(itemblock_stone_wall_5, 9, "conquest:stucco_wall");
		registerRenderMetas(itemblock_stone_wall_5, 10, "conquest:brick_diorite_wall");
		registerRenderMetas(itemblock_stone_wall_5, 11, "conquest:sandstone_conglomerate_wall");
		registerRenderMetas(itemblock_stone_wall_5, 12, "conquest:sandstone_mossy_wall");
		registerRenderMetas(itemblock_stone_wall_5, 13, "conquest:frieze_sandstone_wall");
		registerRenderMetas(itemblock_stone_wall_5, 14, "conquest:ironblock_rusty_wall");
		registerRenderMetas(itemblock_stone_wall_5, 15, "conquest:brick_mossy_wall");
		registerRenderMetas(itemblock_stone_wall_6, 0, "conquest:brick_darkredmossy_wall");
		registerRenderMetas(itemblock_stone_wall_6, 1, "conquest:brick_redmossy_wall");
		registerRenderMetas(itemblock_stone_wall_6, 2, "conquest:stucco_white_wall");
		registerRenderMetas(itemblock_stone_wall_6, 3, "conquest:stucco_tan_wall");
		registerRenderMetas(itemblock_stone_wall_6, 4, "conquest:walldesign_brown_wall");
		registerRenderMetas(itemblock_stone_wall_6, 5, "conquest:stucco_brown_wall");
		registerRenderMetas(itemblock_stone_wall_6, 6, "conquest:stucco_purple_wall");
		registerRenderMetas(itemblock_stone_wall_6, 7, "conquest:walldesign_magenta_wall");
		registerRenderMetas(itemblock_stone_wall_6, 8, "conquest:stucco_magenta_wall");
		registerRenderMetas(itemblock_stone_wall_6, 9, "conquest:walldesign_red_wall");
		registerRenderMetas(itemblock_stone_wall_6, 10, "conquest:walldesign_red3_wall");
		registerRenderMetas(itemblock_stone_wall_6, 11, "conquest:walldesign_red4_wall");
		registerRenderMetas(itemblock_stone_wall_6, 12, "conquest:walldesign_red6_wall");
		registerRenderMetas(itemblock_stone_wall_6, 13, "conquest:walldesign_red7_wall");
		registerRenderMetas(itemblock_stone_wall_6, 14, "conquest:walldesign_red8_wall");
		registerRenderMetas(itemblock_stone_wall_6, 15, "conquest:stucco_lightred_wall");
		registerRenderMetas(itemblock_stone_wall_7, 0, "conquest:walldesign_orange_wall");
		registerRenderMetas(itemblock_stone_wall_7, 1, "conquest:walldesign_orange1_wall");
		registerRenderMetas(itemblock_stone_wall_7, 2, "conquest:stucco_lime_wall");
		registerRenderMetas(itemblock_stone_wall_7, 3, "conquest:walldesign_green_wall");
		registerRenderMetas(itemblock_stone_wall_7, 4, "conquest:stucco_green_wall");
		registerRenderMetas(itemblock_stone_wall_7, 5, "conquest:stucco_cyan_wall");
		registerRenderMetas(itemblock_stone_wall_7, 6, "conquest:stucco_lightblue_wall");
		registerRenderMetas(itemblock_stone_wall_7, 7, "conquest:stucco_blue_wall");
		registerRenderMetas(itemblock_stone_wall_7, 8, "conquest:stucco_black_wall");
		registerRenderMetas(itemblock_stone_wall_7, 9, "conquest:granite_polished_wall");
		registerRenderMetas(itemblock_stone_wall_7, 10, "conquest:diorite_polished_wall");
		registerRenderMetas(itemblock_stone_wall_7, 11, "conquest:andesite_polished_wall");
		registerRenderMetas(itemblock_stone_wall_7, 12, "conquest:iron_normal_wall");
		registerRenderMetas(itemblock_stone_wall_7, 13, "conquest:quartz_normal_wall");
		registerRenderMetas(itemblock_stone_wall_7, 14, "conquest:plaster_magenta_wall");
		registerRenderMetas(itemblock_stone_wall_7, 15, "conquest:plaster_lightgray_wall");
		registerRenderMetas(itemblock_stone_wall_8, 0, "conquest:plaster_purple_wall");
		registerRenderMetas(itemblock_stone_wall_8, 1, "conquest:prismarine_normal_wall");
		registerRenderMetas(itemblock_stone_wall_8, 2, "conquest:prismarine_bricks_wall");
		registerRenderMetas(itemblock_stone_wall_8, 3, "conquest:prismarine_dark_wall");
		registerRenderMetas(itemblock_stone_wall_8, 4, "conquest:sandstone_red_wall");
		registerRenderMetas(itemblock_stone_wall_8, 5, "conquest:sandstone_redchiseled_wall");
		registerRenderMetas(itemblock_stone_wall_8, 6, "conquest:sandstone_redsmooth_wall");
		registerRenderMetas(itemblock_stone_wall_8, 7, "conquest:purpur_normal_wall");
		registerRenderMetas(itemblock_stone_wall_8, 8, "conquest:purpur_pillar_wall");
		registerRenderMetas(itemblock_stone_wall_8, 9, "conquest:endstone_bricks_wall");
		registerRenderMetas(itemblock_stone_wall_8, 10, "conquest:netherbrick_red_wall");
		registerRenderMetas(itemblock_stone_wall_8, 11, "conquest:brick_romanvertical_wall");
		registerRenderMetas(itemblock_stone_wall_8, 12, "conquest:marble_black_wall");
		registerRenderMetas(itemblock_stone_wall_8, 13, "conquest:marble_blue_wall");
		registerRenderMetas(itemblock_stone_wall_8, 14, "conquest:marble_gray_wall");
		registerRenderMetas(itemblock_stone_wall_8, 15, "conquest:marble_green_wall");
		registerRenderMetas(itemblock_stone_wall_9, 0, "conquest:marble_pink_wall");
		registerRenderMetas(itemblock_stone_wall_9, 1, "conquest:marble_red_wall");
		registerRenderMetas(itemblock_stone_wall_9, 2, "conquest:brick_darkroman_wall");
		registerRenderMetas(itemblock_stone_wall_9, 3, "conquest:cobblestone_dark_wall");
		registerRenderMetas(itemblock_stone_wall_9, 4, "conquest:sandstone_darkcobble_wall");
		registerRenderMetas(itemblock_stone_wall_9, 5, "conquest:slate_lightmortar_wall");
		registerRenderMetas(itemblock_stone_wall_9, 6, "conquest:plasterwork_white_wall");
		registerRenderMetas(itemblock_stone_wall_9, 7, "conquest:plasterwork_magentaclean_wall");
		registerRenderMetas(itemblock_stone_wall_9, 8, "conquest:plasterwork_lightgrayclean_wall");
		registerRenderMetas(itemblock_stone_wall_9, 9, "conquest:plasterwork_yellowclean_wall");
		registerRenderMetas(itemblock_stone_wall_9, 10, "conquest:plasterwork_whiteclean_wall");
		registerRenderMetas(itemblock_stone_wall_9, 11, "conquest:plasterwork_purpleclean_wall");
		registerRenderMetas(itemblock_stone_wall_9, 12, "conquest:plasterwork_orange_wall");
		registerRenderMetas(itemblock_stone_wall_9, 13, "conquest:cobblestone_hewntiles_wall");
		registerRenderMetas(itemblock_stone_wall_9, 14, "conquest:cobblestone_plasteredtiles_wall");
		registerRenderMetas(itemblock_stone_wall_9, 15, "conquest:none");

	//Pillar Stone Blocks
		registerRenderMetas(itemblock_stone_pillar_1, 0, "conquest:stone_plastered_pillar");
		registerRenderMetas(itemblock_stone_pillar_1, 1, "conquest:stone_hewn_pillar");
		registerRenderMetas(itemblock_stone_pillar_1, 2, "conquest:stonebrick_mossy_pillar");
		registerRenderMetas(itemblock_stone_pillar_1, 3, "conquest:stonebrick_pillar");
		registerRenderMetas(itemblock_stone_pillar_1, 4, "conquest:blackmarble_pillar");
		registerRenderMetas(itemblock_stone_pillar_1, 5, "conquest:netherbrick_pillar");
		registerRenderMetas(itemblock_stone_pillar_1, 6, "conquest:brick_pillar");
		registerRenderMetas(itemblock_stone_pillar_1, 7, "conquest:redbrick_dark_pillar");
		registerRenderMetas(itemblock_stone_pillar_1, 8, "conquest:redbrick_pillar");
		registerRenderMetas(itemblock_stone_pillar_1, 9, "conquest:brick_roman_pillar");
		registerRenderMetas(itemblock_stone_pillar_1, 10, "conquest:brick_travertine_pillar");
		registerRenderMetas(itemblock_stone_pillar_1, 11, "conquest:sandstone_big_pillar");
		registerRenderMetas(itemblock_stone_pillar_1, 12, "conquest:sandstone_pillar");
		registerRenderMetas(itemblock_stone_pillar_1, 13, "conquest:brick_sandstone_pillar");
		registerRenderMetas(itemblock_stone_pillar_1, 14, "conquest:quartz_pillar_pillar");
		registerRenderMetas(itemblock_stone_pillar_1, 15, "conquest:rooftile_red_pillar");
		registerRenderMetas(itemblock_stone_pillar_2, 0, "conquest:rooftile_pink_pillar");
		registerRenderMetas(itemblock_stone_pillar_2, 1, "conquest:rooftile_gray_pillar");
		registerRenderMetas(itemblock_stone_pillar_2, 2, "conquest:rooftile_brown_pillar");
		registerRenderMetas(itemblock_stone_pillar_2, 3, "conquest:rooftile_blue_pillar");
		registerRenderMetas(itemblock_stone_pillar_2, 4, "conquest:rooftile_cyan_pillar");
		registerRenderMetas(itemblock_stone_pillar_2, 5, "conquest:rooftile_lightblue_pillar");
		registerRenderMetas(itemblock_stone_pillar_2, 6, "conquest:rooftile_green_pillar");
		registerRenderMetas(itemblock_stone_pillar_2, 7, "conquest:rooftile_lightgreen_pillar");
		registerRenderMetas(itemblock_stone_pillar_2, 8, "conquest:oxidized_copper_pillar");
		registerRenderMetas(itemblock_stone_pillar_2, 9, "conquest:stone_colorfulslate_pillar");
		registerRenderMetas(itemblock_stone_pillar_2, 10, "conquest:cobblestone_old_pillar");
		registerRenderMetas(itemblock_stone_pillar_2, 11, "conquest:cobblestone_damaged_pillar");
		registerRenderMetas(itemblock_stone_pillar_2, 12, "conquest:iron_dark_pillar");
		registerRenderMetas(itemblock_stone_pillar_2, 13, "conquest:cobblestone_mossy_pillar");
		registerRenderMetas(itemblock_stone_pillar_2, 14, "conquest:cobblestone_overgrown_pillar");
		registerRenderMetas(itemblock_stone_pillar_2, 15, "conquest:cobblestone_vines_pillar");
		registerRenderMetas(itemblock_stone_pillar_3, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_pillar_3, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_pillar_3, 2, "conquest:groove_portculis_pillar");
		registerRenderMetas(itemblock_stone_pillar_3, 3, "conquest:pillar_stone_pillar");
		registerRenderMetas(itemblock_stone_pillar_3, 4, "conquest:netherbrick_wall_pillar");
		registerRenderMetas(itemblock_stone_pillar_3, 5, "conquest:netherbrick_big_pillar");
		registerRenderMetas(itemblock_stone_pillar_3, 6, "conquest:netherbrick_pillar_pillar");
		registerRenderMetas(itemblock_stone_pillar_3, 7, "conquest:tiles_red_pillar");
		registerRenderMetas(itemblock_stone_pillar_3, 8, "conquest:iron_normal_pillar");
		registerRenderMetas(itemblock_stone_pillar_3, 9, "conquest:column_sandstone_pillar");
		registerRenderMetas(itemblock_stone_pillar_3, 10, "conquest:capital_sandstone_pillar");
		registerRenderMetas(itemblock_stone_pillar_3, 11, "conquest:base_sandstone_pillar");
		registerRenderMetas(itemblock_stone_pillar_3, 12, "conquest:column_red_pillar");
		registerRenderMetas(itemblock_stone_pillar_3, 13, "conquest:capital_redmarble_pillar");
		registerRenderMetas(itemblock_stone_pillar_3, 14, "conquest:base_redmarble_pillar");
		registerRenderMetas(itemblock_stone_pillar_3, 15, "conquest:capital_redsandstone_pillar");
		registerRenderMetas(itemblock_stone_pillar_4, 0, "conquest:base_redsandstone_pillar");
		registerRenderMetas(itemblock_stone_pillar_4, 1, "conquest:column_blue_pillar");
		registerRenderMetas(itemblock_stone_pillar_4, 2, "conquest:capital_bluemarble_pillar");
		registerRenderMetas(itemblock_stone_pillar_4, 3, "conquest:base_bluemarble_pillar");
		registerRenderMetas(itemblock_stone_pillar_4, 4, "conquest:capital_bluesandstone_pillar");
		registerRenderMetas(itemblock_stone_pillar_4, 5, "conquest:base_bluesandstone_pillar");
		registerRenderMetas(itemblock_stone_pillar_4, 6, "conquest:column_gold_pillar");
		registerRenderMetas(itemblock_stone_pillar_4, 7, "conquest:capital_goldmarble_pillar");
		registerRenderMetas(itemblock_stone_pillar_4, 8, "conquest:base_goldmarble_pillar");
		registerRenderMetas(itemblock_stone_pillar_4, 9, "conquest:capital_goldsandstone_pillar");
		registerRenderMetas(itemblock_stone_pillar_4, 10, "conquest:base_goldsandstone_pillar");
		registerRenderMetas(itemblock_stone_pillar_4, 11, "conquest:column_marble_pillar");
		registerRenderMetas(itemblock_stone_pillar_4, 12, "conquest:capital_marble_pillar");
		registerRenderMetas(itemblock_stone_pillar_4, 13, "conquest:base_marble_pillar");
		registerRenderMetas(itemblock_stone_pillar_4, 14, "conquest:capitalcorinthian_pillar");
		registerRenderMetas(itemblock_stone_pillar_4, 15, "conquest:capitalcorinthian_sandstone_pillar");
		registerRenderMetas(itemblock_stone_pillar_5, 0, "conquest:cornice_sandstone_pillar");
		registerRenderMetas(itemblock_stone_pillar_5, 1, "conquest:plinth_sandstone_pillar");
		registerRenderMetas(itemblock_stone_pillar_5, 2, "conquest:base_marblesandstone_pillar");
		registerRenderMetas(itemblock_stone_pillar_5, 3, "conquest:cornice_marble_pillar");
		registerRenderMetas(itemblock_stone_pillar_5, 4, "conquest:baseplain_marble_pillar");
		registerRenderMetas(itemblock_stone_pillar_5, 5, "conquest:marble_pillar");
		registerRenderMetas(itemblock_stone_pillar_5, 6, "conquest:marble_smoothwhite_pillar");
		registerRenderMetas(itemblock_stone_pillar_5, 7, "conquest:base_parianmarble_pillar");
		registerRenderMetas(itemblock_stone_pillar_5, 8, "conquest:capital_parianmarble_pillar");
		registerRenderMetas(itemblock_stone_pillar_5, 9, "conquest:stucco_pillar");
		registerRenderMetas(itemblock_stone_pillar_5, 10, "conquest:brick_diorite_pillar");
		registerRenderMetas(itemblock_stone_pillar_5, 11, "conquest:sandstone_conglomerate_pillar");
		registerRenderMetas(itemblock_stone_pillar_5, 12, "conquest:sandstone_mossy_pillar");
		registerRenderMetas(itemblock_stone_pillar_5, 13, "conquest:frieze_sandstone_pillar");
		registerRenderMetas(itemblock_stone_pillar_5, 14, "conquest:ironblock_rusty_pillar");
		registerRenderMetas(itemblock_stone_pillar_5, 15, "conquest:brick_mossy_pillar");
		registerRenderMetas(itemblock_stone_pillar_6, 0, "conquest:brick_darkredmossy_pillar");
		registerRenderMetas(itemblock_stone_pillar_6, 1, "conquest:brick_redmossy_pillar");
		registerRenderMetas(itemblock_stone_pillar_6, 2, "conquest:stucco_white_pillar");
		registerRenderMetas(itemblock_stone_pillar_6, 3, "conquest:stucco_tan_pillar");
		registerRenderMetas(itemblock_stone_pillar_6, 4, "conquest:walldesign_brown_pillar");
		registerRenderMetas(itemblock_stone_pillar_6, 5, "conquest:stucco_brown_pillar");
		registerRenderMetas(itemblock_stone_pillar_6, 6, "conquest:stucco_purple_pillar");
		registerRenderMetas(itemblock_stone_pillar_6, 7, "conquest:walldesign_magenta_pillar");
		registerRenderMetas(itemblock_stone_pillar_6, 8, "conquest:stucco_magenta_pillar");
		registerRenderMetas(itemblock_stone_pillar_6, 9, "conquest:walldesign_red_pillar");
		registerRenderMetas(itemblock_stone_pillar_6, 10, "conquest:walldesign_red3_pillar");
		registerRenderMetas(itemblock_stone_pillar_6, 11, "conquest:walldesign_red4_pillar");
		registerRenderMetas(itemblock_stone_pillar_6, 12, "conquest:walldesign_red6_pillar");
		registerRenderMetas(itemblock_stone_pillar_6, 13, "conquest:walldesign_red7_pillar");
		registerRenderMetas(itemblock_stone_pillar_6, 14, "conquest:walldesign_red8_pillar");
		registerRenderMetas(itemblock_stone_pillar_6, 15, "conquest:stucco_lightred_pillar");
		registerRenderMetas(itemblock_stone_pillar_7, 0, "conquest:walldesign_orange_pillar");
		registerRenderMetas(itemblock_stone_pillar_7, 1, "conquest:walldesign_orange1_pillar");
		registerRenderMetas(itemblock_stone_pillar_7, 2, "conquest:stucco_lime_pillar");
		registerRenderMetas(itemblock_stone_pillar_7, 3, "conquest:walldesign_green_pillar");
		registerRenderMetas(itemblock_stone_pillar_7, 4, "conquest:stucco_green_pillar");
		registerRenderMetas(itemblock_stone_pillar_7, 5, "conquest:stucco_cyan_pillar");
		registerRenderMetas(itemblock_stone_pillar_7, 6, "conquest:stucco_lightblue_pillar");
		registerRenderMetas(itemblock_stone_pillar_7, 7, "conquest:stucco_blue_pillar");
		registerRenderMetas(itemblock_stone_pillar_7, 8, "conquest:stucco_black_pillar");
		registerRenderMetas(itemblock_stone_pillar_7, 9, "conquest:granite_polished_pillar");
		registerRenderMetas(itemblock_stone_pillar_7, 10, "conquest:diorite_polished_pillar");
		registerRenderMetas(itemblock_stone_pillar_7, 11, "conquest:andesite_polished_pillar");
		registerRenderMetas(itemblock_stone_pillar_7, 12, "conquest:iron_normal_pillar");
		registerRenderMetas(itemblock_stone_pillar_7, 13, "conquest:quartz_normal_pillar");
		registerRenderMetas(itemblock_stone_pillar_7, 14, "conquest:plaster_magenta_pillar");
		registerRenderMetas(itemblock_stone_pillar_7, 15, "conquest:plaster_lightgray_pillar");
		registerRenderMetas(itemblock_stone_pillar_8, 0, "conquest:plaster_purple_pillar");
		registerRenderMetas(itemblock_stone_pillar_8, 1, "conquest:prismarine_normal_pillar");
		registerRenderMetas(itemblock_stone_pillar_8, 2, "conquest:prismarine_bricks_pillar");
		registerRenderMetas(itemblock_stone_pillar_8, 3, "conquest:prismarine_dark_pillar");
		registerRenderMetas(itemblock_stone_pillar_8, 4, "conquest:sandstone_red_pillar");
		registerRenderMetas(itemblock_stone_pillar_8, 5, "conquest:sandstone_redchiseled_pillar");
		registerRenderMetas(itemblock_stone_pillar_8, 6, "conquest:sandstone_redsmooth_pillar");
		registerRenderMetas(itemblock_stone_pillar_8, 7, "conquest:purpur_normal_pillar");
		registerRenderMetas(itemblock_stone_pillar_8, 8, "conquest:purpur_pillar_pillar");
		registerRenderMetas(itemblock_stone_pillar_8, 9, "conquest:endstone_bricks_pillar");
		registerRenderMetas(itemblock_stone_pillar_8, 10, "conquest:netherbrick_red_pillar");
		registerRenderMetas(itemblock_stone_pillar_8, 11, "conquest:brick_romanvertical_pillar");
		registerRenderMetas(itemblock_stone_pillar_8, 12, "conquest:marble_black_pillar");
		registerRenderMetas(itemblock_stone_pillar_8, 13, "conquest:marble_blue_pillar");
		registerRenderMetas(itemblock_stone_pillar_8, 14, "conquest:marble_gray_pillar");
		registerRenderMetas(itemblock_stone_pillar_8, 15, "conquest:marble_green_pillar");
		registerRenderMetas(itemblock_stone_pillar_9, 0, "conquest:marble_pink_pillar");
		registerRenderMetas(itemblock_stone_pillar_9, 1, "conquest:marble_red_pillar");
		registerRenderMetas(itemblock_stone_pillar_9, 2, "conquest:brick_darkroman_pillar");
		registerRenderMetas(itemblock_stone_pillar_9, 3, "conquest:cobblestone_dark_pillar");
		registerRenderMetas(itemblock_stone_pillar_9, 4, "conquest:sandstone_darkcobble_pillar");
		registerRenderMetas(itemblock_stone_pillar_9, 5, "conquest:slate_lightmortar_pillar");
		registerRenderMetas(itemblock_stone_pillar_9, 6, "conquest:plasterwork_white_pillar");
		registerRenderMetas(itemblock_stone_pillar_9, 7, "conquest:plasterwork_magentaclean_pillar");
		registerRenderMetas(itemblock_stone_pillar_9, 8, "conquest:plasterwork_lightgrayclean_pillar");
		registerRenderMetas(itemblock_stone_pillar_9, 9, "conquest:plasterwork_yellowclean_pillar");
		registerRenderMetas(itemblock_stone_pillar_9, 10, "conquest:plasterwork_whiteclean_pillar");
		registerRenderMetas(itemblock_stone_pillar_9, 11, "conquest:plasterwork_purpleclean_pillar");
		registerRenderMetas(itemblock_stone_pillar_9, 12, "conquest:plasterwork_orange_pillar");
		registerRenderMetas(itemblock_stone_pillar_9, 13, "conquest:cobblestone_hewntiles_pillar");
		registerRenderMetas(itemblock_stone_pillar_9, 14, "conquest:cobblestone_plasteredtiles_pillar");
		registerRenderMetas(itemblock_stone_pillar_9, 15, "conquest:none");

	//Fence Stone Blocks
		registerRenderMetas(itemblock_stone_fence_1, 0, "conquest:quartz_normal_fence");
		registerRenderMetas(itemblock_stone_fence_1, 1, "conquest:none");
		registerRenderMetas(itemblock_stone_fence_1, 2, "conquest:none");
		registerRenderMetas(itemblock_stone_fence_1, 3, "conquest:none");
		registerRenderMetas(itemblock_stone_fence_1, 4, "conquest:none");
		registerRenderMetas(itemblock_stone_fence_1, 5, "conquest:none");
		registerRenderMetas(itemblock_stone_fence_1, 6, "conquest:marble_rail_fence");
		registerRenderMetas(itemblock_stone_fence_1, 7, "conquest:terracota_fence");
		registerRenderMetas(itemblock_stone_fence_1, 8, "conquest:rooftile_red_fence");
		registerRenderMetas(itemblock_stone_fence_1, 9, "conquest:rooftile_pink_fence");
		registerRenderMetas(itemblock_stone_fence_1, 10, "conquest:rooftile_gray_fence");
		registerRenderMetas(itemblock_stone_fence_1, 11, "conquest:rooftile_brown_fence");
		registerRenderMetas(itemblock_stone_fence_1, 12, "conquest:rooftile_blue_fence");
		registerRenderMetas(itemblock_stone_fence_1, 13, "conquest:rooftile_cyan_fence");
		registerRenderMetas(itemblock_stone_fence_1, 14, "conquest:rooftile_lightblue_fence");
		registerRenderMetas(itemblock_stone_fence_1, 15, "conquest:rooftile_green_fence");
		registerRenderMetas(itemblock_stone_fence_2, 0, "conquest:rooftile_lightgreen_fence");
		registerRenderMetas(itemblock_stone_fence_2, 1, "conquest:oxidized_copper_fence");
		registerRenderMetas(itemblock_stone_fence_2, 2, "conquest:iron_dark_fence");
		registerRenderMetas(itemblock_stone_fence_2, 3, "conquest:none");
		registerRenderMetas(itemblock_stone_fence_2, 4, "conquest:none");
		registerRenderMetas(itemblock_stone_fence_2, 5, "conquest:none");
		registerRenderMetas(itemblock_stone_fence_2, 6, "conquest:none");
		registerRenderMetas(itemblock_stone_fence_2, 7, "conquest:none");
		registerRenderMetas(itemblock_stone_fence_2, 8, "conquest:none");
		registerRenderMetas(itemblock_stone_fence_2, 9, "conquest:none");
		registerRenderMetas(itemblock_stone_fence_2, 10, "conquest:none");
		registerRenderMetas(itemblock_stone_fence_2, 11, "conquest:none");
		registerRenderMetas(itemblock_stone_fence_2, 12, "conquest:none");
		registerRenderMetas(itemblock_stone_fence_2, 13, "conquest:none");
		registerRenderMetas(itemblock_stone_fence_2, 14, "conquest:none");
		registerRenderMetas(itemblock_stone_fence_2, 15, "conquest:none");
	//Fencegate Stone Blocks
		registerRenderMetas(itemblock_stone_fencegate_1, 0, "conquest:ironblock_dark_fencegate");
		registerRenderMetas(itemblock_stone_fencegate_1, 1, "conquest:quartz_normal_fencegate");

	//Small Pillar Stone Blocks
		registerRenderMetas(itemblock_stone_smallpillar_1, 0, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_1, 1, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_1, 2, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_1, 3, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_1, 4, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_1, 5, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_1, 6, "conquest:marble_rail_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_1, 7, "conquest:terracota_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_1, 8, "conquest:rooftile_red_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_1, 9, "conquest:rooftile_pink_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_1, 10, "conquest:rooftile_gray_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_1, 11, "conquest:rooftile_brown_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_1, 12, "conquest:rooftile_blue_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_1, 13, "conquest:rooftile_cyan_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_1, 14, "conquest:rooftile_lightblue_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_1, 15, "conquest:rooftile_green_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_2, 0, "conquest:rooftile_lightgreen_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_2, 1, "conquest:oxidized_copper_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_2, 2, "conquest:stone_colorfulslate_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_2, 3, "conquest:cobblestone_old_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_2, 4, "conquest:cobblestone_damaged_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_2, 5, "conquest:none"); //open slot
		registerRenderMetas(itemblock_stone_smallpillar_2, 6, "conquest:cobblestone_mossy_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_2, 7, "conquest:cobblestone_overgrown_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_2, 8, "conquest:cobblestone_vines_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_2, 9, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_smallpillar_2, 10, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_smallpillar_2, 11, "conquest:groove_portculis_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_2, 12, "conquest:pillar_stone_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_2, 13, "conquest:netherbrick_wall_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_2, 14, "conquest:netherbrick_big_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_2, 15, "conquest:netherbrick_pillar_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_3, 0, "conquest:tiles_red_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_3, 1, "conquest:iron_normal_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_3, 2, "conquest:column_sandstone_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_3, 3, "conquest:capital_sandstone_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_3, 4, "conquest:base_sandstone_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_3, 5, "conquest:column_red_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_3, 6, "conquest:capital_redmarble_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_3, 7, "conquest:base_redmarble_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_3, 8, "conquest:capital_redsandstone_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_3, 9, "conquest:base_redsandstone_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_3, 10, "conquest:column_blue_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_3, 11, "conquest:capital_bluemarble_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_3, 12, "conquest:base_bluemarble_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_3, 13, "conquest:capital_bluesandstone_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_3, 14, "conquest:base_bluesandstone_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_3, 15, "conquest:column_gold_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_4, 0, "conquest:capital_goldmarble_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_4, 1, "conquest:base_goldmarble_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_4, 2, "conquest:capital_goldsandstone_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_4, 3, "conquest:base_goldsandstone_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_4, 4, "conquest:column_marble_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_4, 5, "conquest:capital_marble_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_4, 6, "conquest:base_marble_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_4, 7, "conquest:capitalcorinthian_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_4, 8, "conquest:capitalcorinthian_sandstone_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_4, 9, "conquest:cornice_sandstone_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_4, 10, "conquest:plinth_sandstone_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_4, 11, "conquest:base_marblesandstone_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_4, 12, "conquest:cornice_marble_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_4, 13, "conquest:baseplain_marble_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_4, 14, "conquest:marble_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_4, 15, "conquest:marble_smoothwhite_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_5, 0, "conquest:base_parianmarble_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_5, 1, "conquest:capital_parianmarble_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_5, 2, "conquest:stucco_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_5, 3, "conquest:brick_diorite_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_5, 4, "conquest:sandstone_conglomerate_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_5, 5, "conquest:sandstone_mossy_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_5, 6, "conquest:ironblock_rusty_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_5, 7, "conquest:brick_mossy_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_5, 8, "conquest:brick_darkred_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_5, 9, "conquest:brick_darkredmossy_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_5, 10, "conquest:brick_red_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_5, 11, "conquest:brick_redmossy_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_5, 12, "conquest:stucco_white_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_5, 13, "conquest:stucco_tan_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_5, 14, "conquest:walldesign_brown_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_5, 15, "conquest:stucco_brown_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_6, 0, "conquest:stucco_purple_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_6, 1, "conquest:walldesign_magenta_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_6, 2, "conquest:stucco_magenta_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_6, 3, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_6, 4, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_6, 5, "conquest:walldesign_red4_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_6, 6, "conquest:walldesign_red6_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_6, 7, "conquest:walldesign_red7_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_6, 8, "conquest:walldesign_red8_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_6, 9, "conquest:stucco_lightred_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_6, 10, "conquest:walldesign_orange_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_6, 11, "conquest:walldesign_orange1_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_6, 12, "conquest:stucco_lime_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_6, 13, "conquest:walldesign_green_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_6, 14, "conquest:stucco_green_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_6, 15, "conquest:stucco_cyan_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_7, 0, "conquest:stucco_lightblue_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_7, 1, "conquest:stucco_blue_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_7, 2, "conquest:stucco_black_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_7, 3, "conquest:granite_polished_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_7, 4, "conquest:diorite_polished_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_7, 5, "conquest:andesite_polished_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_7, 6, "conquest:cobblestone_normal_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_7, 7, "conquest:sandstone_normal_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_7, 8, "conquest:iron_normal_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_7, 9, "conquest:bricks_normal_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_7, 10, "conquest:cobblestone_mossynormal_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_7, 11, "conquest:stonebrick_normal_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_7, 12, "conquest:stonebrick_mossy_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_7, 13, "conquest:stonebrick_cracked_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_7, 14, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_7, 15, "conquest:quartz_pillar_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_8, 0, "conquest:plaster_magenta_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_8, 1, "conquest:plaster_lightgray_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_8, 2, "conquest:plaster_purple_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_8, 3, "conquest:prismarine_normal_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_8, 4, "conquest:prismarine_bricks_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_8, 5, "conquest:prismarine_dark_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_8, 6, "conquest:sandstone_red_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_8, 7, "conquest:sandstone_redchiseled_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_8, 8, "conquest:sandstone_redsmooth_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_8, 9, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_8, 10, "conquest:purpur_pillar_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_8, 11, "conquest:endstone_bricks_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_8, 12, "conquest:netherbrick_red_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_8, 13, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_8, 14, "conquest:marble_black_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_8, 15, "conquest:marble_blue_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_9, 0, "conquest:marble_gray_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_9, 1, "conquest:marble_green_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_9, 2, "conquest:marble_pink_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_9, 3, "conquest:marble_red_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_9, 4, "conquest:brick_darkroman_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_9, 5, "conquest:cobblestone_dark_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_9, 6, "conquest:sandstone_darkcobble_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_9, 7, "conquest:slate_lightmortar_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_9, 8, "conquest:iron_dark_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_9, 9, "conquest:bronzelattice_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_9, 10, "conquest:darkironfence_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_9, 11, "conquest:rustedironfence_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_9, 12, "conquest:ironfence_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_9, 13, "conquest:plasterwork_white_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_9, 14, "conquest:plasterwork_magentaclean_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_9, 15, "conquest:plasterwork_lightgrayclean_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_10, 0, "conquest:plasterwork_yellowclean_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_10, 1, "conquest:plasterwork_whiteclean_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_10, 2, "conquest:plasterwork_purpleclean_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_10, 3, "conquest:plasterwork_orange_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_10, 4, "conquest:cobblestone_hewntiles_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_10, 5, "conquest:cobblestone_plasteredtiles_smallpillar");
		registerRenderMetas(itemblock_stone_smallpillar_10, 6, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_10, 7, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_10, 8, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_10, 9, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_10, 10, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_10, 11, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_10, 12, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_10, 13, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_10, 14, "conquest:none");
		registerRenderMetas(itemblock_stone_smallpillar_10, 15, "conquest:none");
	//Stairs Stone Blocks
		registerRenderMetas(itemblock_stone_stairs_1, 0, "conquest:stone_plastered_stairs");
		registerRenderMetas(itemblock_stone_stairs_1, 1, "conquest:stone_plastered_arch");
		registerRenderMetas(itemblock_stone_stairs_2, 0, "conquest:stone_slate_stairs");
		registerRenderMetas(itemblock_stone_stairs_2, 1, "conquest:stone_hewn_stairs");
		registerRenderMetas(itemblock_stone_stairs_3, 0, "conquest:cobblestone_normal_arch");
		registerRenderMetas(itemblock_stone_stairs_3, 1, "conquest:redbrick_dark_stairs");
		registerRenderMetas(itemblock_stone_stairs_4, 0, "conquest:redbrick_stairs");
		registerRenderMetas(itemblock_stone_stairs_4, 1, "conquest:brick_roman_stairs");
		registerRenderMetas(itemblock_stone_stairs_5, 0, "conquest:brick_travertine_stairs");
		registerRenderMetas(itemblock_stone_stairs_5, 1, "conquest:sandstone_big_stairs");
		registerRenderMetas(itemblock_stone_stairs_6, 0, "conquest:brick_sandstone_stairs");
		registerRenderMetas(itemblock_stone_stairs_6, 1, "conquest:cornice_sandstone_stairs");
		registerRenderMetas(itemblock_stone_stairs_7, 0, "conquest:base_sandstone_stairs");
		registerRenderMetas(itemblock_stone_stairs_7, 1, "conquest:base_marblesandstone_stairs");
		registerRenderMetas(itemblock_stone_stairs_8, 0, "conquest:cornice_marble_stairs");
		registerRenderMetas(itemblock_stone_stairs_8, 1, "conquest:baseplain_marble_stairs");
		registerRenderMetas(itemblock_stone_stairs_9, 0, "conquest:rooftile_red_stairs");
		registerRenderMetas(itemblock_stone_stairs_9, 1, "conquest:rooftile_pink_stairs");
		registerRenderMetas(itemblock_stone_stairs_10, 0, "conquest:rooftile_gray_stairs");
		registerRenderMetas(itemblock_stone_stairs_10, 1, "conquest:rooftile_brown_stairs");
		registerRenderMetas(itemblock_stone_stairs_11, 0, "conquest:rooftile_blue_stairs");
		registerRenderMetas(itemblock_stone_stairs_11, 1, "conquest:rooftile_cyan_stairs");
		registerRenderMetas(itemblock_stone_stairs_12, 0, "conquest:rooftile_lightblue_stairs");
		registerRenderMetas(itemblock_stone_stairs_12, 1, "conquest:rooftile_green_stairs");
		registerRenderMetas(itemblock_stone_stairs_13, 0, "conquest:rooftile_lightgreen_stairs");
		registerRenderMetas(itemblock_stone_stairs_13, 1, "conquest:oxidized_copper_stairs");
		registerRenderMetas(itemblock_stone_stairs_14, 0, "conquest:none"); //open block space
		registerRenderMetas(itemblock_stone_stairs_14, 1, "conquest:none"); //open block space
		registerRenderMetas(itemblock_stone_stairs_15, 0, "conquest:stone_colorfulslate_stairs");
		registerRenderMetas(itemblock_stone_stairs_15, 1, "conquest:cobblestone_old_stairs");
		registerRenderMetas(itemblock_stone_stairs_16, 0, "conquest:cobblestone_damaged_stairs");
		registerRenderMetas(itemblock_stone_stairs_16, 1, "conquest:cobblestone_fishscale_stairs");
		registerRenderMetas(itemblock_stone_stairs_17, 0, "conquest:cobblestone_fishscaledirty_stairs");
		registerRenderMetas(itemblock_stone_stairs_17, 1, "conquest:cobblestone_mossy_stairs");
		registerRenderMetas(itemblock_stone_stairs_18, 0, "conquest:cobblestone_overgrown_stairs");
		registerRenderMetas(itemblock_stone_stairs_18, 1, "conquest:cobblestone_vines_stairs");
		registerRenderMetas(itemblock_stone_stairs_19, 0, "conquest:cobblestone_reinforced_stairs");
		registerRenderMetas(itemblock_stone_stairs_19, 1, "conquest:cobblestone_reinforced_stairs");
		registerRenderMetas(itemblock_stone_stairs_20, 0, "conquest:groove_portculis_stairs");
		registerRenderMetas(itemblock_stone_stairs_20, 1, "conquest:netherbrick_big_stairs");
		registerRenderMetas(itemblock_stone_stairs_21, 0, "conquest:tiles_red_stairs");
		registerRenderMetas(itemblock_stone_stairs_21, 1, "conquest:ironblock_2_stairs");
		registerRenderMetas(itemblock_stone_stairs_22, 0, "conquest:marble_stairs");
		registerRenderMetas(itemblock_stone_stairs_22, 1, "conquest:marble_smoothwhite_stairs");
		registerRenderMetas(itemblock_stone_stairs_23, 0, "conquest:mosaic_roman_stairs");
		registerRenderMetas(itemblock_stone_stairs_23, 1, "conquest:mosaic_indian_stairs");
		registerRenderMetas(itemblock_stone_stairs_24, 0, "conquest:mosaic_andalusian_stairs");
		registerRenderMetas(itemblock_stone_stairs_24, 1, "conquest:stucco_stairs");
		registerRenderMetas(itemblock_stone_stairs_25, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_25, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_26, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_26, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_27, 0, "conquest:brick_diorite_stairs");
		registerRenderMetas(itemblock_stone_stairs_27, 1, "conquest:sandstone_chiseledtop_stairs");
		registerRenderMetas(itemblock_stone_stairs_28, 0, "conquest:sandstone_conglomerate_stairs");
		registerRenderMetas(itemblock_stone_stairs_28, 1, "conquest:sandstone_mossy_stairs");
		registerRenderMetas(itemblock_stone_stairs_29, 0, "conquest:tile_clay_stairs");
		registerRenderMetas(itemblock_stone_stairs_29, 1, "conquest:tile_mixedclay_stairs");
		registerRenderMetas(itemblock_stone_stairs_30, 0, "conquest:tile_lightclay_stairs");
		registerRenderMetas(itemblock_stone_stairs_30, 1, "conquest:ironblock_rusty_stairs");
		registerRenderMetas(itemblock_stone_stairs_31, 0, "conquest:brick_mossy_stairs");
		registerRenderMetas(itemblock_stone_stairs_31, 1, "conquest:brick_darkredmossy_stairs");
		registerRenderMetas(itemblock_stone_stairs_32, 0, "conquest:brick_redmossy_stairs");
		registerRenderMetas(itemblock_stone_stairs_32, 1, "conquest:stucco_white_stairs");
		registerRenderMetas(itemblock_stone_stairs_33, 0, "conquest:stucco_tan_stairs");
		registerRenderMetas(itemblock_stone_stairs_33, 1, "conquest:none"); //open slot
		registerRenderMetas(itemblock_stone_stairs_34, 0, "conquest:stucco_brown_stairs");
		registerRenderMetas(itemblock_stone_stairs_34, 1, "conquest:stucco_purple_stairs");
		registerRenderMetas(itemblock_stone_stairs_35, 0, "conquest:stucco_magenta_stairs");
		registerRenderMetas(itemblock_stone_stairs_35, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_36, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_36, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_37, 0, "conquest:stucco_lightred_stairs");
		registerRenderMetas(itemblock_stone_stairs_37, 1, "conquest:stucco_lime_stairs");
		registerRenderMetas(itemblock_stone_stairs_38, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_38, 1, "conquest:stucco_lightblue_stairs");
		registerRenderMetas(itemblock_stone_stairs_39, 0, "conquest:stucco_blue_stairs");
		registerRenderMetas(itemblock_stone_stairs_39, 1, "conquest:stucco_black_stairs");
		registerRenderMetas(itemblock_stone_stairs_40, 0, "conquest:none"); //open slot
		registerRenderMetas(itemblock_stone_stairs_40, 1, "conquest:bronzeblock_gilded_stairs");
		registerRenderMetas(itemblock_stone_stairs_41, 0, "conquest:none"); //open block slot
		registerRenderMetas(itemblock_stone_stairs_41, 1, "conquest:granite_polished_stairs");
		registerRenderMetas(itemblock_stone_stairs_42, 0, "conquest:diorite_polished_stairs");
		registerRenderMetas(itemblock_stone_stairs_42, 1, "conquest:andesite_polished_stairs");
		registerRenderMetas(itemblock_stone_stairs_43, 0, "conquest:iron_normal_stairs");
		registerRenderMetas(itemblock_stone_stairs_43, 1, "conquest:cobblestone_mossynormal_stairs");
		registerRenderMetas(itemblock_stone_stairs_44, 0, "conquest:stonebrick_mossy_stairs");
		registerRenderMetas(itemblock_stone_stairs_44, 1, "conquest:stonebrick_cracked_stairs");
		registerRenderMetas(itemblock_stone_stairs_45, 0, "conquest:plaster_magenta_stairs");
		registerRenderMetas(itemblock_stone_stairs_45, 1, "conquest:plaster_lightgray_stairs");
		registerRenderMetas(itemblock_stone_stairs_46, 0, "conquest:plaster_purple_stairs");
		registerRenderMetas(itemblock_stone_stairs_46, 1, "conquest:prismarine_normal_stairs");
		registerRenderMetas(itemblock_stone_stairs_47, 0, "conquest:prismarine_bricks_stairs");
		registerRenderMetas(itemblock_stone_stairs_47, 1, "conquest:prismarine_dark_stairs");
		registerRenderMetas(itemblock_stone_stairs_48, 0, "conquest:endstone_bricks_stairs");
		registerRenderMetas(itemblock_stone_stairs_48, 1, "conquest:netherbrick_red_stairs");

		registerRenderMetas(itemblock_stone_stairs_49, 0, "conquest:stone_hewn_arch");
		registerRenderMetas(itemblock_stone_stairs_49, 1, "conquest:stone_colorfulslate_arch");
		registerRenderMetas(itemblock_stone_stairs_50, 0, "conquest:cobblestone_old_arch");
		registerRenderMetas(itemblock_stone_stairs_50, 1, "conquest:cobblestone_damaged_arch");
		registerRenderMetas(itemblock_stone_stairs_51, 0, "conquest:cobblestone_mossy_arch");
		registerRenderMetas(itemblock_stone_stairs_51, 1, "conquest:cobblestone_overgrown_arch");
		registerRenderMetas(itemblock_stone_stairs_52, 0, "conquest:cobblestone_vines_arch");
		registerRenderMetas(itemblock_stone_stairs_52, 1, "conquest:cobblestone_reinforced_arch");
		registerRenderMetas(itemblock_stone_stairs_53, 0, "conquest:cobblestone_reinforced_arch");
		registerRenderMetas(itemblock_stone_stairs_53, 1, "conquest:netherbrick_big_arch");
		registerRenderMetas(itemblock_stone_stairs_54, 0, "conquest:tiles_red_arch");
		registerRenderMetas(itemblock_stone_stairs_54, 1, "conquest:marble_arch");
		registerRenderMetas(itemblock_stone_stairs_55, 0, "conquest:marble_smoothwhite_arch");
		registerRenderMetas(itemblock_stone_stairs_55, 1, "conquest:mosaic_roman_arch");
		registerRenderMetas(itemblock_stone_stairs_56, 0, "conquest:mosaic_indian_arch");
		registerRenderMetas(itemblock_stone_stairs_56, 1, "conquest:mosaic_andalusian_arch");
		registerRenderMetas(itemblock_stone_stairs_57, 0, "conquest:stucco_arch");
		registerRenderMetas(itemblock_stone_stairs_57, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_58, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_58, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_59, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_59, 1, "conquest:brick_diorite_arch");
		registerRenderMetas(itemblock_stone_stairs_60, 0, "conquest:brick_travertine_arch");
		registerRenderMetas(itemblock_stone_stairs_60, 1, "conquest:brick_sandstonebig_arch");
		registerRenderMetas(itemblock_stone_stairs_61, 0, "conquest:sandstone_conglomerate_arch");
		registerRenderMetas(itemblock_stone_stairs_61, 1, "conquest:sandstone_mossy_arch");
		registerRenderMetas(itemblock_stone_stairs_62, 0, "conquest:tile_clay_arch");
		registerRenderMetas(itemblock_stone_stairs_62, 1, "conquest:tile_mixedclay_arch");
		registerRenderMetas(itemblock_stone_stairs_63, 0, "conquest:tile_lightclay_arch");
		registerRenderMetas(itemblock_stone_stairs_63, 1, "conquest:brick_mossy_arch");
		registerRenderMetas(itemblock_stone_stairs_64, 0, "conquest:brick_darkred_arch");
		registerRenderMetas(itemblock_stone_stairs_64, 1, "conquest:brick_darkredmossy_arch");
		registerRenderMetas(itemblock_stone_stairs_65, 0, "conquest:brick_red_arch");
		registerRenderMetas(itemblock_stone_stairs_65, 1, "conquest:brick_redmossy_arch");
		registerRenderMetas(itemblock_stone_stairs_66, 0, "conquest:brick_roman_arch");
		registerRenderMetas(itemblock_stone_stairs_66, 1, "conquest:stucco_white_arch");
		registerRenderMetas(itemblock_stone_stairs_67, 0, "conquest:stucco_tan_arch");
		registerRenderMetas(itemblock_stone_stairs_67, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_68, 0, "conquest:stucco_brown_arch");
		registerRenderMetas(itemblock_stone_stairs_68, 1, "conquest:stucco_purple_arch");
		registerRenderMetas(itemblock_stone_stairs_69, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_69, 1, "conquest:stucco_magenta_arch");
		registerRenderMetas(itemblock_stone_stairs_70, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_70, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_71, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_71, 1, "conquest:stucco_lightred_arch");
		registerRenderMetas(itemblock_stone_stairs_72, 0, "conquest:stucco_lime_arch");
		registerRenderMetas(itemblock_stone_stairs_72, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_73, 0, "conquest:stucco_lightblue_arch");
		registerRenderMetas(itemblock_stone_stairs_73, 1, "conquest:stucco_blue_arch");
		registerRenderMetas(itemblock_stone_stairs_74, 0, "conquest:stucco_black_arch");
		registerRenderMetas(itemblock_stone_stairs_74, 1, "conquest:none"); //open slot
		registerRenderMetas(itemblock_stone_stairs_75, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_75, 1, "conquest:none"); //open block slot
		registerRenderMetas(itemblock_stone_stairs_76, 0, "conquest:granite_polished_arch");
		registerRenderMetas(itemblock_stone_stairs_76, 1, "conquest:diorite_polished_arch");
		registerRenderMetas(itemblock_stone_stairs_77, 0, "conquest:andesite_polished_arch");
		registerRenderMetas(itemblock_stone_stairs_77, 1, "conquest:sandstone_normal_arch");
		registerRenderMetas(itemblock_stone_stairs_78, 0, "conquest:bricks_normal_arch");
		registerRenderMetas(itemblock_stone_stairs_78, 1, "conquest:cobblestone_mossynormal_arch");
		registerRenderMetas(itemblock_stone_stairs_79, 0, "conquest:stonebrick_normal_arch");
		registerRenderMetas(itemblock_stone_stairs_79, 1, "conquest:stonebrick_mossy_arch");
		registerRenderMetas(itemblock_stone_stairs_80, 0, "conquest:stonebrick_cracked_arch");
		registerRenderMetas(itemblock_stone_stairs_80, 1, "conquest:netherbrick_normal_arch");
		registerRenderMetas(itemblock_stone_stairs_81, 0, "conquest:quartz_normal_arch");
		registerRenderMetas(itemblock_stone_stairs_81, 1, "conquest:plaster_magenta_arch");
		registerRenderMetas(itemblock_stone_stairs_82, 0, "conquest:plaster_lightgray_arch");
		registerRenderMetas(itemblock_stone_stairs_82, 1, "conquest:plaster_purple_arch");
		registerRenderMetas(itemblock_stone_stairs_83, 0, "conquest:prismarine_normal_arch");
		registerRenderMetas(itemblock_stone_stairs_83, 1, "conquest:prismarine_bricks_arch");
		registerRenderMetas(itemblock_stone_stairs_84, 0, "conquest:prismarine_dark_arch");
		registerRenderMetas(itemblock_stone_stairs_84, 1, "conquest:sandstone_red_arch");
		registerRenderMetas(itemblock_stone_stairs_85, 0, "conquest:purpur_normal_arch");
		registerRenderMetas(itemblock_stone_stairs_85, 1, "conquest:endstone_bricks_arch");
		registerRenderMetas(itemblock_stone_stairs_86, 0, "conquest:netherbrick_red_arch");
		registerRenderMetas(itemblock_stone_stairs_86, 1, "conquest:brick_sandstone_arch");
		registerRenderMetas(itemblock_stone_stairs_87, 0, "conquest:brick_romanvertical_stairs");
		registerRenderMetas(itemblock_stone_stairs_87, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_88, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_88, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_89, 0, "conquest:marble_black_stairs");
		registerRenderMetas(itemblock_stone_stairs_89, 1, "conquest:marble_blue_stairs");
		registerRenderMetas(itemblock_stone_stairs_90, 0, "conquest:marble_gray_stairs");
		registerRenderMetas(itemblock_stone_stairs_90, 1, "conquest:marble_green_stairs");
		registerRenderMetas(itemblock_stone_stairs_91, 0, "conquest:marble_pink_stairs");
		registerRenderMetas(itemblock_stone_stairs_91, 1, "conquest:marble_red_stairs");
		registerRenderMetas(itemblock_stone_stairs_92, 0, "conquest:brick_darkroman_stairs");
		registerRenderMetas(itemblock_stone_stairs_92, 1, "conquest:brick_romanvertical_arch");
		registerRenderMetas(itemblock_stone_stairs_93, 0, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_93, 1, "conquest:none");//open slot
		registerRenderMetas(itemblock_stone_stairs_94, 0, "conquest:cobblestone_plasteredtiles_arch");
		registerRenderMetas(itemblock_stone_stairs_94, 1, "conquest:marble_black_arch");
		registerRenderMetas(itemblock_stone_stairs_95, 0, "conquest:marble_blue_arch");
		registerRenderMetas(itemblock_stone_stairs_95, 1, "conquest:marble_gray_arch");
		registerRenderMetas(itemblock_stone_stairs_96, 0, "conquest:marble_green_arch");
		registerRenderMetas(itemblock_stone_stairs_96, 1, "conquest:marble_pink_arch");
		registerRenderMetas(itemblock_stone_stairs_97, 0, "conquest:marble_red_arch");
		registerRenderMetas(itemblock_stone_stairs_97, 1, "conquest:brick_darkroman_arch");
		registerRenderMetas(itemblock_stone_stairs_98, 0, "conquest:cobblestone_dark_stairs");
		registerRenderMetas(itemblock_stone_stairs_98, 1, "conquest:cobblestone_dark_arch");
		registerRenderMetas(itemblock_stone_stairs_99, 0, "conquest:sandstone_darkcobble_stairs");
		registerRenderMetas(itemblock_stone_stairs_99, 1, "conquest:sandstone_darkcobble_arch");
		registerRenderMetas(itemblock_stone_stairs_100, 0, "conquest:slate_lightmortar_stairs");
		registerRenderMetas(itemblock_stone_stairs_100, 1, "conquest:slate_lightmortar_arch");
		registerRenderMetas(itemblock_stone_stairs_101, 0, "conquest:walldesign_red8_stairs");
		registerRenderMetas(itemblock_stone_stairs_101, 1, "conquest:walldesign_red8_arch");
		registerRenderMetas(itemblock_stone_stairs_102, 0, "conquest:plasterwork_white_stairs");
		registerRenderMetas(itemblock_stone_stairs_102, 1, "conquest:plasterwork_white_arch");
		registerRenderMetas(itemblock_stone_stairs_103, 0, "conquest:plasterwork_magentaclean_stairs");
		registerRenderMetas(itemblock_stone_stairs_103, 1, "conquest:plasterwork_magentaclean_arch");
		registerRenderMetas(itemblock_stone_stairs_104, 0, "conquest:plasterwork_lightgrayclean_stairs");
		registerRenderMetas(itemblock_stone_stairs_104, 1, "conquest:plasterwork_lightgrayclean_arch");
		registerRenderMetas(itemblock_stone_stairs_105, 0, "conquest:plasterwork_yellowclean_stairs");
		registerRenderMetas(itemblock_stone_stairs_105, 1, "conquest:plasterwork_yellowclean_arch");
		registerRenderMetas(itemblock_stone_stairs_106, 0, "conquest:plasterwork_whiteclean_stairs");
		registerRenderMetas(itemblock_stone_stairs_106, 1, "conquest:plasterwork_whiteclean_arch");
		registerRenderMetas(itemblock_stone_stairs_107, 0, "conquest:plasterwork_purpleclean_stairs");
		registerRenderMetas(itemblock_stone_stairs_107, 1, "conquest:plasterwork_purpleclean_arch");
		registerRenderMetas(itemblock_stone_stairs_108, 0, "conquest:plasterwork_orange_stairs");
		registerRenderMetas(itemblock_stone_stairs_108, 1, "conquest:plasterwork_orange_arch");
		registerRenderMetas(itemblock_stone_stairs_109, 0, "conquest:cobblestone_hewntiles_stairs");
		registerRenderMetas(itemblock_stone_stairs_109, 1, "conquest:cobblestone_hewntiles_arch");

		//Slab Stone Blocks
		registerRenderMetas(itemblock_stone_slab_1, 0, "conquest:stone_plastered_slab");
		registerRenderMetas(itemblock_stone_slab_1, 1, "conquest:cobblestone_fishscale_slab");
		registerRenderMetas(itemblock_stone_slab_1, 2, "conquest:cobblestone_fishscaledirty_slab");
		registerRenderMetas(itemblock_stone_slab_1, 3, "conquest:stone_mossy_slab");
		registerRenderMetas(itemblock_stone_slab_1, 4, "conquest:redbrick_dark_slab");
		registerRenderMetas(itemblock_stone_slab_1, 5, "conquest:redbrick_slab");
		registerRenderMetas(itemblock_stone_slab_1, 6, "conquest:brick_roman_slab");
		registerRenderMetas(itemblock_stone_slab_1, 7, "conquest:brick_travertine_slab");
		registerRenderMetas(itemblock_stone_slab_2, 0, "conquest:sandstone_big_slab");
		registerRenderMetas(itemblock_stone_slab_2, 1, "conquest:brick_sandstone_slab");
		registerRenderMetas(itemblock_stone_slab_2, 2, "conquest:cornice_sandstone_slab");
		registerRenderMetas(itemblock_stone_slab_2, 3, "conquest:base_sandstone_slab");
		registerRenderMetas(itemblock_stone_slab_2, 4, "conquest:base_marblesandstone_slab");
		registerRenderMetas(itemblock_stone_slab_2, 5, "conquest:cornice_marble_slab");
		registerRenderMetas(itemblock_stone_slab_2, 6, "conquest:baseplain_marble_slab");
		registerRenderMetas(itemblock_stone_slab_2, 7, "conquest:stainedclay_yellow_slab");
		registerRenderMetas(itemblock_stone_slab_3, 0, "conquest:stainedclay_white_slab");
		registerRenderMetas(itemblock_stone_slab_3, 1, "conquest:stainedclay_orange_slab");
		registerRenderMetas(itemblock_stone_slab_3, 2, "conquest:stainedclay_magenta_slab");
		registerRenderMetas(itemblock_stone_slab_3, 3, "conquest:stainedclay_black_slab");
		registerRenderMetas(itemblock_stone_slab_3, 4, "conquest:rooftile_red_slab");
		registerRenderMetas(itemblock_stone_slab_3, 5, "conquest:rooftile_pink_slab");
		registerRenderMetas(itemblock_stone_slab_3, 6, "conquest:rooftile_gray_slab");
		registerRenderMetas(itemblock_stone_slab_3, 7, "conquest:rooftile_brown_slab");
		registerRenderMetas(itemblock_stone_slab_4, 0, "conquest:rooftile_blue_slab");
		registerRenderMetas(itemblock_stone_slab_4, 1, "conquest:rooftile_cyan_slab");
		registerRenderMetas(itemblock_stone_slab_4, 2, "conquest:rooftile_lightblue_slab");
		registerRenderMetas(itemblock_stone_slab_4, 3, "conquest:rooftile_green_slab");
		registerRenderMetas(itemblock_stone_slab_4, 4, "conquest:rooftile_lightgreen_slab");
		registerRenderMetas(itemblock_stone_slab_4, 5, "conquest:oxidized_copper_slab");
		registerRenderMetas(itemblock_stone_slab_4, 6, "conquest:endstone_slab");
		registerRenderMetas(itemblock_stone_slab_4, 7, "conquest:netherrack_slab");
		registerRenderMetas(itemblock_stone_slab_5, 0, "conquest:iron_dark_slab");
		registerRenderMetas(itemblock_stone_slab_5, 1, "conquest:stone_hewn_slab");
		registerRenderMetas(itemblock_stone_slab_5, 2, "conquest:stone_colorfulslate_slab");
		registerRenderMetas(itemblock_stone_slab_5, 3, "conquest:cobblestone_old_slab");
		registerRenderMetas(itemblock_stone_slab_5, 4, "conquest:cobblestone_damaged_slab");
		registerRenderMetas(itemblock_stone_slab_5, 5, "conquest:cobblestone_mossy_slab");
		registerRenderMetas(itemblock_stone_slab_5, 6, "conquest:cobblestone_overgrown_slab");
		registerRenderMetas(itemblock_stone_slab_5, 7, "conquest:cobblestone_vines_slab");
		registerRenderMetas(itemblock_stone_slab_6, 0, "conquest:cobblestone_reinforced_slab");
		registerRenderMetas(itemblock_stone_slab_6, 1, "conquest:cobblestone_reinforced_slab");
		registerRenderMetas(itemblock_stone_slab_6, 2, "conquest:groove_portculis_slab");
		registerRenderMetas(itemblock_stone_slab_6, 3, "conquest:netherbrick_big_slab");
		registerRenderMetas(itemblock_stone_slab_6, 4, "conquest:tiles_red_slab");
		registerRenderMetas(itemblock_stone_slab_6, 5, "conquest:ironblock_2_slab");
		registerRenderMetas(itemblock_stone_slab_6, 6, "conquest:marble_slab");
		registerRenderMetas(itemblock_stone_slab_6, 7, "conquest:marble_smoothwhite_slab");
		registerRenderMetas(itemblock_stone_slab_7, 0, "conquest:bigslab_marble_slab");
		registerRenderMetas(itemblock_stone_slab_7, 1, "conquest:bigslab_marble1_slab");
		registerRenderMetas(itemblock_stone_slab_7, 2, "conquest:bigslab_sandstone_slab");
		registerRenderMetas(itemblock_stone_slab_7, 3, "conquest:bigslab_sandstone1_slab");
		registerRenderMetas(itemblock_stone_slab_7, 4, "conquest:bigslab_sandstoneinscribed_slab");
		registerRenderMetas(itemblock_stone_slab_7, 5, "conquest:bigslab_sandstoneinscribed1_slab");
		registerRenderMetas(itemblock_stone_slab_7, 6, "conquest:bigslab_black_slab");
		registerRenderMetas(itemblock_stone_slab_7, 7, "conquest:bigslab_black1_slab");
		registerRenderMetas(itemblock_stone_slab_8, 0, "conquest:bigslab_blackinscribed_slab");
		registerRenderMetas(itemblock_stone_slab_8, 1, "conquest:bigslab_blackinscribed1_slab");
		registerRenderMetas(itemblock_stone_slab_8, 2, "conquest:mosaic_roman_slab");
		registerRenderMetas(itemblock_stone_slab_8, 3, "conquest:mosaic_indian_slab");
		registerRenderMetas(itemblock_stone_slab_8, 4, "conquest:mosaic_andalusian_slab");
		registerRenderMetas(itemblock_stone_slab_8, 5, "conquest:brick_diorite_slab");
		registerRenderMetas(itemblock_stone_slab_8, 6, "conquest:sandstone_chiseledtop_slab");
		registerRenderMetas(itemblock_stone_slab_8, 7, "conquest:sandstone_conglomerate_slab");
		registerRenderMetas(itemblock_stone_slab_9, 0, "conquest:sandstone_mossy_slab");
		registerRenderMetas(itemblock_stone_slab_9, 1, "conquest:tile_clay_slab");
		registerRenderMetas(itemblock_stone_slab_9, 2, "conquest:tile_mixedclay_slab");
		registerRenderMetas(itemblock_stone_slab_9, 3, "conquest:tile_lightclay_slab");
		registerRenderMetas(itemblock_stone_slab_9, 4, "conquest:ironblock_rusty_slab");
		registerRenderMetas(itemblock_stone_slab_9, 5, "conquest:brick_mossy_slab");
		registerRenderMetas(itemblock_stone_slab_9, 6, "conquest:brick_darkredmossy_slab");
		registerRenderMetas(itemblock_stone_slab_9, 7, "conquest:brick_redmossy_slab");
		registerRenderMetas(itemblock_stone_slab_10, 0, "conquest:stucco_white_slab");
		registerRenderMetas(itemblock_stone_slab_10, 1, "conquest:stucco_tan_slab");
		registerRenderMetas(itemblock_stone_slab_10, 2, "conquest:none"); //open block space
		registerRenderMetas(itemblock_stone_slab_10, 3, "conquest:stucco_purple_slab");
		registerRenderMetas(itemblock_stone_slab_10, 4, "conquest:none"); //open block space
		registerRenderMetas(itemblock_stone_slab_10, 5, "conquest:stucco_magenta_slab");
		registerRenderMetas(itemblock_stone_slab_10, 6, "conquest:none"); //open block space
		registerRenderMetas(itemblock_stone_slab_10, 7, "conquest:none"); //open block space
		registerRenderMetas(itemblock_stone_slab_11, 0, "conquest:none"); //open block space
		registerRenderMetas(itemblock_stone_slab_11, 1, "conquest:none"); //open block space
		registerRenderMetas(itemblock_stone_slab_11, 2, "conquest:none"); //open block space
		registerRenderMetas(itemblock_stone_slab_11, 3, "conquest:none"); //open block space
		registerRenderMetas(itemblock_stone_slab_11, 4, "conquest:none"); //open block space
		registerRenderMetas(itemblock_stone_slab_11, 5, "conquest:walldesign_red8_slab");
		registerRenderMetas(itemblock_stone_slab_11, 6, "conquest:stucco_lightred_slab");
		registerRenderMetas(itemblock_stone_slab_11, 7, "conquest:none");//open block space
		registerRenderMetas(itemblock_stone_slab_12, 0, "conquest:walldesign_orange1_slab");
		registerRenderMetas(itemblock_stone_slab_12, 1, "conquest:stucco_lime_slab");
		registerRenderMetas(itemblock_stone_slab_12, 2, "conquest:none");//open block space
		registerRenderMetas(itemblock_stone_slab_12, 3, "conquest:stucco_green_slab");
		registerRenderMetas(itemblock_stone_slab_12, 4, "conquest:stucco_cyan_slab");
		registerRenderMetas(itemblock_stone_slab_12, 5, "conquest:stucco_lightblue_slab");
		registerRenderMetas(itemblock_stone_slab_12, 6, "conquest:stucco_blue_slab");
		registerRenderMetas(itemblock_stone_slab_12, 7, "conquest:stucco_black_slab");
		registerRenderMetas(itemblock_stone_slab_13, 0, "conquest:bronzeblock_gilded_slab");
		registerRenderMetas(itemblock_stone_slab_13, 1, "conquest:bronzeblock_fancy_slab");
		registerRenderMetas(itemblock_stone_slab_13, 2, "conquest:goldblock_bars_slab");
		registerRenderMetas(itemblock_stone_slab_13, 3, "conquest:granite_polished_slab");
		registerRenderMetas(itemblock_stone_slab_13, 4, "conquest:diorite_polished_slab");
		registerRenderMetas(itemblock_stone_slab_13, 5, "conquest:andesite_polished_slab");
		registerRenderMetas(itemblock_stone_slab_13, 6, "conquest:iron_normal_slab");
		registerRenderMetas(itemblock_stone_slab_13, 7, "conquest:cobblestone_mossynormal_slab");
		registerRenderMetas(itemblock_stone_slab_14, 0, "conquest:stonebrick_mossy_slab");
		registerRenderMetas(itemblock_stone_slab_14, 1, "conquest:stonebrick_cracked_slab");
		registerRenderMetas(itemblock_stone_slab_14, 2, "conquest:plaster_magenta_slab");
		registerRenderMetas(itemblock_stone_slab_14, 3, "conquest:plaster_lightgray_slab");
		registerRenderMetas(itemblock_stone_slab_14, 4, "conquest:plaster_purple_slab");
		registerRenderMetas(itemblock_stone_slab_14, 5, "conquest:prismarine_normal_slab");
		registerRenderMetas(itemblock_stone_slab_14, 6, "conquest:prismarine_bricks_slab");
		registerRenderMetas(itemblock_stone_slab_14, 7, "conquest:prismarine_dark_slab");
		registerRenderMetas(itemblock_stone_slab_15, 0, "conquest:endstone_bricks_slab");
		registerRenderMetas(itemblock_stone_slab_15, 1, "conquest:netherbrick_red_slab");
		registerRenderMetas(itemblock_stone_slab_15, 2, "conquest:stone_icy_slab");
		registerRenderMetas(itemblock_stone_slab_15, 3, "conquest:stone_icytop_slab");
		registerRenderMetas(itemblock_stone_slab_15, 4, "conquest:stone_mossy_slab");
		registerRenderMetas(itemblock_stone_slab_15, 5, "conquest:stone_mossytop_slab");
		registerRenderMetas(itemblock_stone_slab_15, 6, "conquest:slate_wet_slab");
		registerRenderMetas(itemblock_stone_slab_15, 7, "conquest:slate_slab");
		registerRenderMetas(itemblock_stone_slab_16, 0, "conquest:marble_uncut_slab");
		registerRenderMetas(itemblock_stone_slab_16, 1, "conquest:granite_graypink_slab");
		registerRenderMetas(itemblock_stone_slab_16, 2, "conquest:sandstone_natural_slab");
		registerRenderMetas(itemblock_stone_slab_16, 3, "conquest:mesastone_lightbrown_slab");
		registerRenderMetas(itemblock_stone_slab_16, 4, "conquest:pahoehoe_slab");
		registerRenderMetas(itemblock_stone_slab_16, 5, "conquest:stone_slab");
		registerRenderMetas(itemblock_stone_slab_16, 6, "conquest:granite_slab");
		registerRenderMetas(itemblock_stone_slab_16, 7, "conquest:diorite_slab");
		registerRenderMetas(itemblock_stone_slab_17, 0, "conquest:andesite_slab");
		registerRenderMetas(itemblock_stone_slab_17, 1, "conquest:obsidian_slab");
		registerRenderMetas(itemblock_stone_slab_17, 2, "conquest:hardclay_white_slab");
		registerRenderMetas(itemblock_stone_slab_17, 3, "conquest:hardclay_orange_slab");
		registerRenderMetas(itemblock_stone_slab_17, 4, "conquest:hardclay_yellow_slab");
		registerRenderMetas(itemblock_stone_slab_17, 5, "conquest:hardclay_gray_slab");
		registerRenderMetas(itemblock_stone_slab_17, 6, "conquest:hardclay_brown_slab");
		registerRenderMetas(itemblock_stone_slab_17, 7, "conquest:hardclay_red_slab");
		registerRenderMetas(itemblock_stone_slab_18, 0, "conquest:hardclay_normal_slab");
		registerRenderMetas(itemblock_stone_slab_18, 1, "conquest:brick_romanvertical_slab");
		registerRenderMetas(itemblock_stone_slab_18, 2, "conquest:marble_black_slab");
		registerRenderMetas(itemblock_stone_slab_18, 3, "conquest:marble_blue_slab");
		registerRenderMetas(itemblock_stone_slab_18, 4, "conquest:marble_gray_slab");
		registerRenderMetas(itemblock_stone_slab_18, 5, "conquest:marble_green_slab");
		registerRenderMetas(itemblock_stone_slab_18, 6, "conquest:marble_pink_slab");
		registerRenderMetas(itemblock_stone_slab_18, 7, "conquest:marble_red_slab");
		registerRenderMetas(itemblock_stone_slab_19, 0, "conquest:brick_darkroman_slab");
		registerRenderMetas(itemblock_stone_slab_19, 1, "conquest:whiterock_mossy_slab");
		registerRenderMetas(itemblock_stone_slab_19, 2, "conquest:cobblestone_dark_slab");
		registerRenderMetas(itemblock_stone_slab_19, 3, "conquest:sandstone_roughnatural_slab");
		registerRenderMetas(itemblock_stone_slab_19, 4, "conquest:stone_cliff_slab");
		registerRenderMetas(itemblock_stone_slab_19, 5, "conquest:stone_coastal_slab");
		registerRenderMetas(itemblock_stone_slab_19, 6, "conquest:sandstone_darkcobble_slab");
		registerRenderMetas(itemblock_stone_slab_19, 7, "conquest:slate_lightmortar_slab");
		registerRenderMetas(itemblock_stone_slab_20, 0, "conquest:plasterwork_white_slab");
		registerRenderMetas(itemblock_stone_slab_20, 1, "conquest:plasterwork_magentaclean_slab");
		registerRenderMetas(itemblock_stone_slab_20, 2, "conquest:plasterwork_lightgrayclean_slab");
		registerRenderMetas(itemblock_stone_slab_20, 3, "conquest:plasterwork_yellowclean_slab");
		registerRenderMetas(itemblock_stone_slab_20, 4, "conquest:plasterwork_whiteclean_slab");
		registerRenderMetas(itemblock_stone_slab_20, 5, "conquest:plasterwork_purpleclean_slab");
		registerRenderMetas(itemblock_stone_slab_20, 6, "conquest:cobblestone_hewntiles_slab");
		registerRenderMetas(itemblock_stone_slab_20, 7, "conquest:cobblestone_plasteredtiles_slab");
	//Slab Stone Blocks
		registerRenderMetas(itemblock_stone_trapdoormodel_1, 0, "conquest:stone_plastered_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_1, 1, "conquest:stone_hewn_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_2, 0, "conquest:cobblestone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_2, 1, "conquest:stonebrick_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_3, 0, "conquest:stone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_3, 1, "conquest:brick_roman_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_4, 0, "conquest:brick_travertine_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_4, 1, "conquest:sandstone_big_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_5, 0, "conquest:brick_sandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_5, 1, "conquest:rooftile_red_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_6, 0, "conquest:rooftile_pink_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_6, 1, "conquest:rooftile_gray_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_7, 0, "conquest:rooftile_brown_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_7, 1, "conquest:rooftile_blue_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_8, 0, "conquest:rooftile_cyan_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_8, 1, "conquest:rooftile_lightblue_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_9, 0, "conquest:rooftile_green_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_9, 1, "conquest:rooftile_lightgreen_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_10, 0, "conquest:oxidized_copper_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_10, 1, "conquest:stone_colorfulslate_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_11, 0, "conquest:cobblestone_old_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_11, 1, "conquest:cobblestone_damaged_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_12, 0, "conquest:cobblestone_fishscale_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_12, 1, "conquest:cobblestone_fishscaledirty_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_13, 0, "conquest:cobblestone_mossy_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_13, 1, "conquest:cobblestone_overgrown_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_14, 0, "conquest:cobblestone_vines_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_14, 1, "conquest:cobblestone_reinforced_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_15, 0, "conquest:cobblestone_reinforced_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_15, 1, "conquest:groove_portculis_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_16, 0, "conquest:stone_chiseled_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_16, 1, "conquest:pillar_stone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_17, 0, "conquest:netherbrick_normal_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_17, 1, "conquest:netherbrick_big_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_18, 0, "conquest:netherbrick_pillar_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_18, 1, "conquest:netherbrick_carved_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_19, 0, "conquest:tiles_red_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_19, 1, "conquest:ironblock_dark_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_20, 0, "conquest:ironblock_2_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_20, 1, "conquest:column_sandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_21, 0, "conquest:capital_sandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_21, 1, "conquest:base_sandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_22, 0, "conquest:column_red_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_22, 1, "conquest:capital_redmarble_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_23, 0, "conquest:base_redmarble_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_23, 1, "conquest:capital_redsandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_24, 0, "conquest:base_redsandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_24, 1, "conquest:column_blue_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_25, 0, "conquest:capital_bluemarble_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_25, 1, "conquest:base_bluemarble_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_26, 0, "conquest:capital_bluesandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_26, 1, "conquest:base_bluesandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_27, 0, "conquest:column_gold_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_27, 1, "conquest:capital_goldmarble_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_28, 0, "conquest:base_goldmarble_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_28, 1, "conquest:capital_goldsandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_29, 0, "conquest:base_goldsandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_29, 1, "conquest:column_marble_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_30, 0, "conquest:capital_marble_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_30, 1, "conquest:base_marble_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_31, 0, "conquest:capitalcorinthian_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_31, 1, "conquest:capitalcorinthian_sandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_32, 0, "conquest:cornice_sandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_32, 1, "conquest:plinth_sandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_33, 0, "conquest:base_marblesandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_33, 1, "conquest:cornice_marble_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_34, 0, "conquest:baseplain_marble_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_34, 1, "conquest:marble_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_35, 0, "conquest:marble_smoothwhite_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_35, 1, "conquest:base_parianmarble_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_36, 0, "conquest:capital_parianmarble_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_36, 1, "conquest:bigslab_marble_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_37, 0, "conquest:bigslab_marble1_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_37, 1, "conquest:bigslab_sandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_38, 0, "conquest:bigslab_sandstone1_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_38, 1, "conquest:bigslab_sandstoneinscribed_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_39, 0, "conquest:bigslab_sandstoneinscribed1_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_39, 1, "conquest:bigslab_black_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_40, 0, "conquest:bigslab_black1_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_40, 1, "conquest:bigslab_blackinscribed_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_41, 0, "conquest:bigslab_blackinscribed1_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_41, 1, "conquest:mosaic_roman_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_42, 0, "conquest:mosaic_indian_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_42, 1, "conquest:mosaic_andalusian_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_43, 0, "conquest:stucco_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_43, 1, "conquest:mosaic_decorative_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_44, 0, "conquest:walldesign_white1_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_44, 1, "conquest:walldesign_white2_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_45, 0, "conquest:walldesign_white3_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_45, 1, "conquest:walldesign_white4_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_46, 0, "conquest:walldesign_white5_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_46, 1, "conquest:walldesign_white6_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_47, 0, "conquest:brick_diorite_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_47, 1, "conquest:sandstone_conglomerate_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_48, 0, "conquest:sandstone_mossy_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_48, 1, "conquest:frieze_sandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_49, 0, "conquest:sandstone_inscribed_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_49, 1, "conquest:tile_clay_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_50, 0, "conquest:tile_mixedclay_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_50, 1, "conquest:tile_lightclay_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_51, 0, "conquest:ironblock_rusty_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_51, 1, "conquest:tudorframe_slash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_52, 0, "conquest:tudorframe_backslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_52, 1, "conquest:tudorframe_x_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_53, 0, "conquest:tudorframe_up_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_53, 1, "conquest:tudorframe_down_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_54, 0, "conquest:tudorframe_box_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_54, 1, "conquest:tudorframe_slash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_55, 0, "conquest:tudorframe_backslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_55, 1, "conquest:tudorframe_box_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_56, 0, "conquest:tudorframe_darkslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_56, 1, "conquest:tudorframe_darkbackslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_57, 0, "conquest:tudorframe_darkx_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_57, 1, "conquest:tudorframe_darkup_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_58, 0, "conquest:tudorframe_darkdown_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_58, 1, "conquest:tudorframe_darkbox_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_59, 0, "conquest:tudorframe_darkslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_59, 1, "conquest:tudorframe_darkbackslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_60, 0, "conquest:tudorframe_darkbox_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_60, 1, "conquest:tudorframe_brickslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_61, 0, "conquest:tudorframe_brickbackslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_61, 1, "conquest:tudorframe_brickx_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_62, 0, "conquest:tudorframe_brickup_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_62, 1, "conquest:tudorframe_brickdown_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_63, 0, "conquest:tudorframe_brickbox_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_63, 1, "conquest:tudorframe_brickslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_64, 0, "conquest:tudorframe_brickbackslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_64, 1, "conquest:tudorframe_brickbox_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_65, 0, "conquest:tudorframe_darkbrickslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_65, 1, "conquest:tudorframe_darkbrickbackslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_66, 0, "conquest:tudorframe_darkbrickx_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_66, 1, "conquest:tudorframe_darkbrickup_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_67, 0, "conquest:tudorframe_darkbrickdown_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_67, 1, "conquest:tudorframe_darkbrickbox_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_68, 0, "conquest:tudorframe_darkbrickslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_68, 1, "conquest:tudorframe_darkbrickbackslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_69, 0, "conquest:tudorframe_darkbrickbox_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_69, 1, "conquest:tudorframe_redbrickslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_70, 0, "conquest:tudorframe_redbrickbackslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_70, 1, "conquest:tudorframe_redbrickx_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_71, 0, "conquest:tudorframe_redbrickup_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_71, 1, "conquest:tudorframe_redbrickdown_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_72, 0, "conquest:tudorframe_redbrickbox_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_72, 1, "conquest:tudorframe_redbrickslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_73, 0, "conquest:tudorframe_redbrickbackslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_73, 1, "conquest:tudorframe_redbrickbox_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_74, 0, "conquest:brick_mossy_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_74, 1, "conquest:brick_darkred_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_75, 0, "conquest:brick_darkredmossy_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_75, 1, "conquest:brick_red_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_76, 0, "conquest:brick_redmossy_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_76, 1, "conquest:stucco_white_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_77, 0, "conquest:stucco_tan_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_77, 1, "conquest:walldesign_brown_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_78, 0, "conquest:stucco_brown_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_78, 1, "conquest:stucco_purple_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_79, 0, "conquest:walldesign_magenta_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_79, 1, "conquest:stucco_magenta_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_80, 0, "conquest:walldesign_red_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_80, 1, "conquest:walldesign_red2_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_81, 0, "conquest:walldesign_red3_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_81, 1, "conquest:walldesign_red4_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_82, 0, "conquest:walldesign_red5_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_82, 1, "conquest:walldesign_red6_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_83, 0, "conquest:walldesign_red7_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_83, 1, "conquest:walldesign_red8_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_84, 0, "conquest:stucco_lightred_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_84, 1, "conquest:walldesign_orange_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_85, 0, "conquest:walldesign_orange1_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_85, 1, "conquest:stucco_lime_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_86, 0, "conquest:walldesign_green_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_86, 1, "conquest:stucco_green_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_87, 0, "conquest:stucco_cyan_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_87, 1, "conquest:stucco_lightblue_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_88, 0, "conquest:stucco_blue_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_88, 1, "conquest:stucco_black_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_89, 0, "conquest:door_ornateroman_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_89, 1, "conquest:door_ornateroman_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_90, 0, "conquest:bronzeblock_gilded_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_90, 1, "conquest:bronzeblock_fancy_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_91, 0, "conquest:goldblock_bars_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_91, 1, "conquest:granite_polished_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_92, 0, "conquest:diorite_polished_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_92, 1, "conquest:andesite_polished_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_93, 0, "conquest:sandstone_normal_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_93, 1, "conquest:sandstone_chiseled_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_94, 0, "conquest:sandstone_smooth_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_94, 1, "conquest:iron_normal_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_95, 0, "conquest:bricks_normal_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_95, 1, "conquest:cobblestone_mossynormal_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_96, 0, "conquest:stonebrick_mossy_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_96, 1, "conquest:stonebrick_cracked_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_97, 0, "conquest:stonebrick_chiseled_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_97, 1, "conquest:netherbrick_normal_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_98, 0, "conquest:quartz_normal_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_98, 1, "conquest:quartz_chiseled_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_99, 0, "conquest:quartz_pillar_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_99, 1, "conquest:plaster_magenta_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_100, 0, "conquest:plaster_lightgray_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_100, 1, "conquest:plaster_purple_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_101, 0, "conquest:prismarine_normal_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_101, 1, "conquest:prismarine_bricks_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_102, 0, "conquest:prismarine_dark_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_102, 1, "conquest:sandstone_red_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_103, 0, "conquest:sandstone_redchiseled_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_103, 1, "conquest:sandstone_redsmooth_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_104, 0, "conquest:purpur_normal_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_104, 1, "conquest:purpur_pillar_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_105, 0, "conquest:endstone_bricks_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_105, 1, "conquest:netherbrick_wart_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_106, 0, "conquest:netherbrick_red_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_106, 1, "conquest:stone_icy_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_107, 0, "conquest:stone_icytop_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_107, 1, "conquest:stone_mossy_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_108, 0, "conquest:stone_mossytop_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_108, 1, "conquest:slate_wet_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_109, 0, "conquest:slate_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_109, 1, "conquest:marble_uncut_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_110, 0, "conquest:granite_graypink_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_110, 1, "conquest:sandstone_natural_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_111, 0, "conquest:mesastone_lightbrown_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_111, 1, "conquest:pahoehoe_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_112, 0, "conquest:endstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_112, 1, "conquest:granite_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_113, 0, "conquest:diorite_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_113, 1, "conquest:andesite_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_114, 0, "conquest:obsidian_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_114, 1, "conquest:hardclay_white_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_115, 0, "conquest:hardclay_orange_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_115, 1, "conquest:hardclay_yellow_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_116, 0, "conquest:hardclay_gray_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_116, 1, "conquest:hardclay_brown_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_117, 0, "conquest:hardclay_red_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_117, 1, "conquest:hardclay_normal_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_118, 0, "conquest:brick_romanvertical_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_118, 1, "conquest:architrave_polychrome_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_119, 0, "conquest:architrave_polychromelegionaries_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_119, 1, "conquest:architrave_polychrome2_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_120, 0, "conquest:capitalcorinthian_polychrome_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_120, 1, "conquest:marble_blackwhite_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_121, 0, "conquest:marble_smalldiagonalchecker_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_121, 1, "conquest:marble_smallchecker_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_122, 0, "conquest:marble_redwhite_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_122, 1, "conquest:marble_redwhite2_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_123, 0, "conquest:marble_blueyellow_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_123, 1, "conquest:marble_diagonalchecker_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_124, 0, "conquest:marble_checker_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_124, 1, "conquest:marble_black_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_125, 0, "conquest:marble_blue_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_125, 1, "conquest:marble_gray_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_126, 0, "conquest:marble_green_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_126, 1, "conquest:marble_pink_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_127, 0, "conquest:marble_red_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_127, 1, "conquest:brick_darkroman_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_128, 0, "conquest:brick_darkromansandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_128, 1, "conquest:brick_romansandstone_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_129, 0, "conquest:tudorframe_narrowslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_129, 1, "conquest:tudorframe_narrowbackslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_130, 0, "conquest:tudorframe_darknarrowslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_130, 1, "conquest:tudorframe_darknarrowbackslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_131, 0, "conquest:tudorframe_bricknarrowslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_131, 1, "conquest:tudorframe_bricknarrowbackslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_132, 0, "conquest:tudorframe_darkbricknarrowslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_132, 1, "conquest:tudorframe_darkbricknarrowbackslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_133, 0, "conquest:tudorframe_redbricknarrowslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_133, 1, "conquest:tudorframe_redbricknarrowbackslash_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_134, 0, "conquest:whiterock_mossy_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_134, 1, "conquest:cobblestone_dark_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_135, 0, "conquest:sandstone_roughnatural_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_135, 1, "conquest:stone_cliff_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_136, 0, "conquest:stone_coastal_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_136, 1, "conquest:wallpaper_blueyellow_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_137, 0, "conquest:wallpaper_blueyellowclean_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_137, 1, "conquest:wallpaper_green_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_138, 0, "conquest:wallpaper_greenclean_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_138, 1, "conquest:wallpaper_redyellow_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_139, 0, "conquest:wallpaper_redyellowclean_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_139, 1, "conquest:sandstone_darkcobble_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_140, 0, "conquest:slate_lightmortar_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_140, 1, "conquest:plasterwork_white_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_141, 0, "conquest:plasterwork_magentaclean_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_141, 1, "conquest:plasterwork_lightgrayclean_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_142, 0, "conquest:plasterwork_yellowclean_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_142, 1, "conquest:plasterwork_whiteclean_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_143, 0, "conquest:plasterwork_purpleclean_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_143, 1, "conquest:plasterwork_orange_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_144, 0, "conquest:cobblestone_hewntiles_trapdoor");
		registerRenderMetas(itemblock_stone_trapdoormodel_144, 1, "conquest:cobblestone_plasteredtiles_trapdoor");


		//Carpet Stone Blocks
		registerRenderMetas(itemblock_stone_carpet_1, 0, "conquest:claytile");
		registerRenderMetas(itemblock_stone_carpet_1, 1, "conquest:mixedtile");
		registerRenderMetas(itemblock_stone_carpet_1, 2, "conquest:lighttile");
		registerRenderMetas(itemblock_stone_carpet_1, 3, "conquest:stonebrick_overgrown_carpet");
		registerRenderMetas(itemblock_stone_carpet_1, 4, "conquest:groove_portculis_carpet");
		registerRenderMetas(itemblock_stone_carpet_1, 5, "conquest:stone_chiseled_carpet");
		registerRenderMetas(itemblock_stone_carpet_1, 6, "conquest:pillar_stone_carpet");
		registerRenderMetas(itemblock_stone_carpet_1, 7, "conquest:ironblock_dark_carpet");
		registerRenderMetas(itemblock_stone_carpet_1, 8, "conquest:bigslab_marble_carpet");
		registerRenderMetas(itemblock_stone_carpet_1, 9, "conquest:bigslab_sandstone_carpet");
		registerRenderMetas(itemblock_stone_carpet_1, 10, "conquest:bigslab_black_carpet");
		registerRenderMetas(itemblock_stone_carpet_1, 11, "conquest:mosaic_roman_carpet");
		registerRenderMetas(itemblock_stone_carpet_1, 12, "conquest:mosaic_indian_carpet");
		registerRenderMetas(itemblock_stone_carpet_1, 13, "conquest:mosaic_andalusian_carpet");
		registerRenderMetas(itemblock_stone_carpet_1, 14, "conquest:none"); //open slot
		registerRenderMetas(itemblock_stone_carpet_1, 15, "conquest:bronzeblock_gilded_carpet");
		registerRenderMetas(itemblock_stone_carpet_2, 0, "conquest:bronzeblock_fancy_carpet");
		registerRenderMetas(itemblock_stone_carpet_2, 1, "conquest:iron_normal_carpet");
		registerRenderMetas(itemblock_stone_carpet_2, 2, "conquest:quartz_chiseled_carpet");
		registerRenderMetas(itemblock_stone_carpet_2, 3, "conquest:stone_icy_carpet");
		registerRenderMetas(itemblock_stone_carpet_2, 4, "conquest:stone_icytop_carpet");
		registerRenderMetas(itemblock_stone_carpet_2, 5, "conquest:stone_mossy_carpet");
		registerRenderMetas(itemblock_stone_carpet_2, 6, "conquest:stone_mossytop_carpet");
		registerRenderMetas(itemblock_stone_carpet_2, 7, "conquest:slate_wet_carpet");
		registerRenderMetas(itemblock_stone_carpet_2, 8, "conquest:slate_carpet");
		registerRenderMetas(itemblock_stone_carpet_2, 9, "conquest:marble_uncut_carpet");
		registerRenderMetas(itemblock_stone_carpet_2, 10, "conquest:granite_graypink_carpet");
		registerRenderMetas(itemblock_stone_carpet_2, 11, "conquest:sandstone_natural_carpet");
		registerRenderMetas(itemblock_stone_carpet_2, 12, "conquest:mesastone_lightbrown_carpet");
		registerRenderMetas(itemblock_stone_carpet_2, 13, "conquest:pahoehoe_carpet");
		registerRenderMetas(itemblock_stone_carpet_2, 14, "conquest:endstone_carpet");
		registerRenderMetas(itemblock_stone_carpet_2, 15, "conquest:stone_carpet");
		registerRenderMetas(itemblock_stone_carpet_3, 0, "conquest:granite_carpet");
		registerRenderMetas(itemblock_stone_carpet_3, 1, "conquest:diorite_carpet");
		registerRenderMetas(itemblock_stone_carpet_3, 2, "conquest:andesite_carpet");
		registerRenderMetas(itemblock_stone_carpet_3, 3, "conquest:obsidian_carpet");
		registerRenderMetas(itemblock_stone_carpet_3, 4, "conquest:hardclay_white_carpet");
		registerRenderMetas(itemblock_stone_carpet_3, 5, "conquest:hardclay_orange_carpet");
		registerRenderMetas(itemblock_stone_carpet_3, 6, "conquest:hardclay_yellow_carpet");
		registerRenderMetas(itemblock_stone_carpet_3, 7, "conquest:hardclay_gray_carpet");
		registerRenderMetas(itemblock_stone_carpet_3, 8, "conquest:hardclay_brown_carpet");
		registerRenderMetas(itemblock_stone_carpet_3, 9, "conquest:hardclay_red_carpet");
		registerRenderMetas(itemblock_stone_carpet_3, 10, "conquest:hardclay_normal_carpet");
		registerRenderMetas(itemblock_stone_carpet_3, 11, "conquest:marble_blackwhite_carpet");
		registerRenderMetas(itemblock_stone_carpet_3, 12, "conquest:marble_smalldiagonalchecker_carpet");
		registerRenderMetas(itemblock_stone_carpet_3, 13, "conquest:marble_smallchecker_carpet");
		registerRenderMetas(itemblock_stone_carpet_3, 14, "conquest:marble_redwhite_carpet");
		registerRenderMetas(itemblock_stone_carpet_3, 15, "conquest:marble_redwhite2_carpet");
		registerRenderMetas(itemblock_stone_carpet_4, 0, "conquest:marble_blueyellow_carpet");
		registerRenderMetas(itemblock_stone_carpet_4, 1, "conquest:marble_diagonalchecker_carpet");
		registerRenderMetas(itemblock_stone_carpet_4, 2, "conquest:marble_checker_carpet");
		registerRenderMetas(itemblock_stone_carpet_4, 3, "conquest:marble_black_carpet");
		registerRenderMetas(itemblock_stone_carpet_4, 4, "conquest:marble_blue_carpet");
		registerRenderMetas(itemblock_stone_carpet_4, 5, "conquest:marble_gray_carpet");
		registerRenderMetas(itemblock_stone_carpet_4, 6, "conquest:marble_green_carpet");
		registerRenderMetas(itemblock_stone_carpet_4, 7, "conquest:marble_pink_carpet");
		registerRenderMetas(itemblock_stone_carpet_4, 8, "conquest:marble_red_carpet");
		registerRenderMetas(itemblock_stone_carpet_4, 9, "conquest:brick_darkroman_carpet");
		registerRenderMetas(itemblock_stone_carpet_4, 10, "conquest:whiterock_mossy_carpet");
		registerRenderMetas(itemblock_stone_carpet_4, 11, "conquest:sandstone_roughnatural_carpet");
		registerRenderMetas(itemblock_stone_carpet_4, 12, "conquest:stone_cliff_carpet");
		registerRenderMetas(itemblock_stone_carpet_4, 13, "conquest:stone_coastal_carpet");
		registerRenderMetas(itemblock_stone_carpet_4, 14, "conquest:andesite_polished_carpet");
		registerRenderMetas(itemblock_stone_carpet_4, 15, "conquest:none");

	//Stone Layer Blocks
		registerRenderLayerMeta(itemblock_stone_layer_1, 0, "conquest:stone_plastered_snowlayer");
		registerRenderLayerMeta(itemblock_stone_layer_1, 1, "conquest:cobblestone_fishscale_snowlayer");
		registerRenderLayerMeta(itemblock_stone_layer_2, 0, "conquest:cobblestone_fishscaledirty_snowlayer");
		registerRenderLayerMeta(itemblock_stone_layer_2, 1, "conquest:cobblestone_mossy_snowlayer");
		registerRenderLayerMeta(itemblock_stone_layer_3, 0, "conquest:cobblestone_vines_snowlayer");
		registerRenderLayerMeta(itemblock_stone_layer_3, 1, "conquest:sandstone_conglomerate_snowlayer");
		registerRenderLayerMeta(itemblock_stone_layer_4, 0, "conquest:cobblestone_normal_snowlayer");
		registerRenderLayerMeta(itemblock_stone_layer_4, 1, "conquest:cobblestone_mossynormal_snowlayer");
		registerRenderLayerMeta(itemblock_stone_layer_5, 0, "conquest:sandstone_red_snowlayer");
		registerRenderLayerMeta(itemblock_stone_layer_5, 1, "conquest:clay_lightbrown_snowlayer");
		registerRenderLayerMeta(itemblock_stone_layer_6, 0, "conquest:clay_brown_snowlayer");
		registerRenderLayerMeta(itemblock_stone_layer_6, 1, "conquest:netherrack_snowlayer");
		registerRenderLayerMeta(itemblock_stone_layer_7, 0, "conquest:pile_bones_snowlayer");
		registerRenderLayerMeta(itemblock_stone_layer_7, 1, "conquest:pile_skeletons_snowlayer");
		registerRenderLayerMeta(itemblock_stone_layer_8, 0, "conquest:coins_gold_snowlayer");
		registerRenderLayerMeta(itemblock_stone_layer_8, 1, "conquest:none");
	//Directional No Collision Stone Blocks
		registerRenderMetas(itemblock_stone_directionalnocollision_1, 0, "conquest:scale");
		registerRenderMetas(itemblock_stone_directionalnocollision_1, 1, "conquest:abacus");
		registerRenderMetas(itemblock_stone_directionalnocollision_1, 2, "conquest:sextant");
		registerRenderMetas(itemblock_stone_directionalnocollision_1, 3, "conquest:telescope");
	//No Collision Stone Blocks
		registerRenderMetas(itemblock_stone_nocollision_1, 0, "conquest:bones_pressureplate");
		registerRenderMetas(itemblock_stone_nocollision_1, 1, "conquest:corpse_web");
		registerRenderMetas(itemblock_stone_nocollision_1, 2, "conquest:food_metal_plate");
		registerRenderMetas(itemblock_stone_nocollision_1, 3, "conquest:food_clay_plate");
		registerRenderMetas(itemblock_stone_nocollision_1, 4, "conquest:food_wood_plate");
		registerRenderMetas(itemblock_stone_nocollision_1, 5, "conquest:stone_stalactite");
		registerRenderMetas(itemblock_stone_nocollision_1, 6, "conquest:stone_stalagmite");
		registerRenderMetas(itemblock_stone_nocollision_1, 7, "conquest:rocks_sandstone");
		registerRenderMetas(itemblock_stone_nocollision_1, 8, "conquest:rocks_granite");
		registerRenderMetas(itemblock_stone_nocollision_1, 9, "conquest:rocks_limestone");
		registerRenderMetas(itemblock_stone_nocollision_1, 10, "conquest:charcoal_pressureplate");
		registerRenderMetas(itemblock_stone_nocollision_1, 11, "conquest:chains_iron");
		registerRenderMetas(itemblock_stone_nocollision_1, 12, "conquest:chains_rusty");
		registerRenderMetas(itemblock_stone_nocollision_1, 13, "conquest:chains_ironsmall");
		registerRenderMetas(itemblock_stone_nocollision_1, 14, "conquest:chains_gold");
		registerRenderMetas(itemblock_stone_nocollision_1, 15, "conquest:hook_rope");
		registerRenderMetas(itemblock_stone_nocollision_2, 0, "conquest:hook_metal");
		registerRenderMetas(itemblock_stone_nocollision_2, 1, "conquest:hourglass");
		registerRenderMetas(itemblock_stone_nocollision_2, 2, "conquest:globe");
		registerRenderMetas(itemblock_stone_nocollision_2, 3, "conquest:compass");
		registerRenderMetas(itemblock_stone_nocollision_2, 4, "conquest:coins_silver");
		registerRenderMetas(itemblock_stone_nocollision_2, 5, "conquest:coins_gold");
		registerRenderMetas(itemblock_stone_nocollision_2, 6, "conquest:pot_water");
		registerRenderMetas(itemblock_stone_nocollision_2, 7, "conquest:cannonball");
		registerRenderMetas(itemblock_stone_nocollision_2, 8, "conquest:eye_hell");
		registerRenderMetas(itemblock_stone_nocollision_2, 9, "conquest:amphora");
		registerRenderMetas(itemblock_stone_nocollision_2, 10, "conquest:none");
		registerRenderMetas(itemblock_stone_nocollision_2, 11, "conquest:none");
		registerRenderMetas(itemblock_stone_nocollision_2, 12, "conquest:none");
		registerRenderMetas(itemblock_stone_nocollision_2, 13, "conquest:none");
		registerRenderMetas(itemblock_stone_nocollision_2, 14, "conquest:none");
		registerRenderMetas(itemblock_stone_nocollision_2, 15, "conquest:none");
	//Full Wood Blocks
		registerRenderMetas(itemblock_wood_full_1, 0, "conquest:log_oak_full");
		registerRenderMetas(itemblock_wood_full_1, 1, "conquest:log_oakconnecting_full");
		registerRenderMetas(itemblock_wood_full_1, 2, "conquest:log_mossyoak_full");
		registerRenderMetas(itemblock_wood_full_1, 3, "conquest:log_spruce_full");
		registerRenderMetas(itemblock_wood_full_1, 4, "conquest:log_spruceconnecting_full");
		registerRenderMetas(itemblock_wood_full_1, 5, "conquest:log_mossyspruce_full");
		registerRenderMetas(itemblock_wood_full_1, 6, "conquest:log_birchnatural_full");
		registerRenderMetas(itemblock_wood_full_1, 7, "conquest:log_birchconnecting_full");
		registerRenderMetas(itemblock_wood_full_1, 8, "conquest:log_mossybirch_full");
		registerRenderMetas(itemblock_wood_full_1, 9, "conquest:log_jungle_full");
		registerRenderMetas(itemblock_wood_full_1, 10, "conquest:log_jungleconnecting_full");
		registerRenderMetas(itemblock_wood_full_1, 11, "conquest:log_mossyjungle_full");
		registerRenderMetas(itemblock_wood_full_1, 12, "conquest:log_acacia_full");
		registerRenderMetas(itemblock_wood_full_1, 13, "conquest:log_acaciaconnecting_full");
		registerRenderMetas(itemblock_wood_full_1, 14, "conquest:log_mossyacacia_full");
		registerRenderMetas(itemblock_wood_full_1, 15, "conquest:log_darkoak_full");
		registerRenderMetas(itemblock_wood_full_2, 0, "conquest:log_darkoakconnecting_full");
		registerRenderMetas(itemblock_wood_full_2, 1, "conquest:log_mossydarkoak_full");
		registerRenderMetas(itemblock_wood_full_2, 2, "conquest:none");
		registerRenderMetas(itemblock_wood_full_2, 3, "conquest:log_birch_full");
		registerRenderMetas(itemblock_wood_full_2, 4, "conquest:log_darkbirch_full");
		registerRenderMetas(itemblock_wood_full_2, 5, "conquest:wood_oakplatform_full");
		registerRenderMetas(itemblock_wood_full_2, 6, "conquest:wood_carvedoak_full");
		registerRenderMetas(itemblock_wood_full_2, 7, "conquest:wood_spruceplatform_full");
		registerRenderMetas(itemblock_wood_full_2, 8, "conquest:wood_spruceplanks_full");
		registerRenderMetas(itemblock_wood_full_2, 9, "conquest:wood_carvedspruce_full");
		registerRenderMetas(itemblock_wood_full_2, 10, "conquest:wood_carvedbirch_full");
		registerRenderMetas(itemblock_wood_full_2, 11, "conquest:wood_carvedjungle_full");
		registerRenderMetas(itemblock_wood_full_2, 12, "conquest:planks_redacacia_full");
		registerRenderMetas(itemblock_wood_full_2, 13, "conquest:wood_carvedacacia_full");
		registerRenderMetas(itemblock_wood_full_2, 14, "conquest:wood_carveddarkoak_full");
		registerRenderMetas(itemblock_wood_full_2, 15, "conquest:planks_driftwood_full");
		registerRenderMetas(itemblock_wood_full_3, 0, "conquest:planks_mossy_full");
		registerRenderMetas(itemblock_wood_full_3, 1, "conquest:planks_whiteweathered_full");
		registerRenderMetas(itemblock_wood_full_3, 2, "conquest:planks_whitepainted_full");
		registerRenderMetas(itemblock_wood_full_3, 3, "conquest:planks_redpainted_full");
		registerRenderMetas(itemblock_wood_full_3, 4, "conquest:planks_redweathered_full");
		registerRenderMetas(itemblock_wood_full_3, 5, "conquest:planks_lightredpainted_full");
		registerRenderMetas(itemblock_wood_full_3, 6, "conquest:planks_lightredweathered_full");
		registerRenderMetas(itemblock_wood_full_3, 7, "conquest:planks_orangepainted_full");
		registerRenderMetas(itemblock_wood_full_3, 8, "conquest:planks_yellowpainted_full");
		registerRenderMetas(itemblock_wood_full_3, 9, "conquest:planks_yellowweathered_full");
		registerRenderMetas(itemblock_wood_full_3, 10, "conquest:planks_greenpainted_full");
		registerRenderMetas(itemblock_wood_full_3, 11, "conquest:planks_greenweathered_full");
		registerRenderMetas(itemblock_wood_full_3, 12, "conquest:planks_lightgreenpainted_full");
		registerRenderMetas(itemblock_wood_full_3, 13, "conquest:planks_limeweathered_full");
		registerRenderMetas(itemblock_wood_full_3, 14, "conquest:planks_cyanpainted_full");
		registerRenderMetas(itemblock_wood_full_3, 15, "conquest:planks_cyanweathered_full");
		registerRenderMetas(itemblock_wood_full_4, 0, "conquest:planks_darkbluepainted_full");
		registerRenderMetas(itemblock_wood_full_4, 1, "conquest:planks_darkblueweathered_full");
		registerRenderMetas(itemblock_wood_full_4, 2, "conquest:planks_blueweathered_full");
		registerRenderMetas(itemblock_wood_full_4, 3, "conquest:planks_lightbluepainted_full");
		registerRenderMetas(itemblock_wood_full_4, 4, "conquest:planks_lightblueweathered_full");
		registerRenderMetas(itemblock_wood_full_4, 5, "conquest:planks_purplepainted_full");
		registerRenderMetas(itemblock_wood_full_4, 6, "conquest:planks_brownpainted_full");
		registerRenderMetas(itemblock_wood_full_4, 7, "conquest:planks_brownweathered_full");
		registerRenderMetas(itemblock_wood_full_4, 8, "conquest:barrel_apple_full");
		registerRenderMetas(itemblock_wood_full_4, 9, "conquest:sack_apple_full");
		registerRenderMetas(itemblock_wood_full_4, 10, "conquest:wickerbasket_apple_full");
		registerRenderMetas(itemblock_wood_full_4, 11, "conquest:barrel_bread_full");
		registerRenderMetas(itemblock_wood_full_4, 12, "conquest:sack_bread_full");
		registerRenderMetas(itemblock_wood_full_4, 13, "conquest:sack_flour_full");
		registerRenderMetas(itemblock_wood_full_4, 14, "conquest:barrel_cabbage_full");
		registerRenderMetas(itemblock_wood_full_4, 15, "conquest:sack_cabbage_full");
		registerRenderMetas(itemblock_wood_full_5, 0, "conquest:barrel_fish_full");
		registerRenderMetas(itemblock_wood_full_5, 1, "conquest:sack_fish_full");
		registerRenderMetas(itemblock_wood_full_5, 2, "conquest:barrel_cocoa_full");
		registerRenderMetas(itemblock_wood_full_5, 3, "conquest:sack_cocoa_full");
		registerRenderMetas(itemblock_wood_full_5, 4, "conquest:wickerbasket_cocoa_full");
		registerRenderMetas(itemblock_wood_full_5, 5, "conquest:barrel_potatoes_full");
		registerRenderMetas(itemblock_wood_full_5, 6, "conquest:sack_potatoes_full");
		registerRenderMetas(itemblock_wood_full_5, 7, "conquest:sack_hops_full");
		registerRenderMetas(itemblock_wood_full_5, 8, "conquest:sack_grapes_full");
		registerRenderMetas(itemblock_wood_full_5, 9, "conquest:keg_wine_full");
		registerRenderMetas(itemblock_wood_full_5, 10, "conquest:none"); //open slot
		registerRenderMetas(itemblock_wood_full_5, 11, "conquest:barrel_ironore_full");
		registerRenderMetas(itemblock_wood_full_5, 12, "conquest:barrel_diamondore_full");
		registerRenderMetas(itemblock_wood_full_5, 13, "conquest:barrel_emeralds_full");
		registerRenderMetas(itemblock_wood_full_5, 14, "conquest:barrel_lapis_full");
		registerRenderMetas(itemblock_wood_full_5, 15, "conquest:barrel_clay");
		registerRenderMetas(itemblock_wood_full_6, 0, "conquest:chest_gold");
		registerRenderMetas(itemblock_wood_full_6, 1, "conquest:barrel_goldnuggets");
		registerRenderMetas(itemblock_wood_full_6, 2, "conquest:sack_gold");
		registerRenderMetas(itemblock_wood_full_6, 3, "conquest:sack_ruby");
		registerRenderMetas(itemblock_wood_full_6, 4, "conquest:sack_pipeweed");
		registerRenderMetas(itemblock_wood_full_6, 5, "conquest:barrel_coal_full");
		registerRenderMetas(itemblock_wood_full_6, 6, "conquest:crate_wood_full");
		registerRenderMetas(itemblock_wood_full_6, 7, "conquest:crate_covered_full");
		registerRenderMetas(itemblock_wood_full_6, 8, "conquest:none"); //open slot
		registerRenderMetas(itemblock_wood_full_6, 9, "conquest:none"); //open slot
		registerRenderMetas(itemblock_wood_full_6, 10, "conquest:none"); //open slot
		registerRenderMetas(itemblock_wood_full_6, 11, "conquest:none");
		registerRenderMetas(itemblock_wood_full_6, 12, "conquest:cupboards_tools_full");
		registerRenderMetas(itemblock_wood_full_6, 13, "conquest:kitchentable_tools_full");
		registerRenderMetas(itemblock_wood_full_6, 14, "conquest:workbench_full");
		registerRenderMetas(itemblock_wood_full_6, 15, "conquest:ropepile_full");
		registerRenderMetas(itemblock_wood_full_7, 0, "conquest:chainpile_full");
		registerRenderMetas(itemblock_wood_full_7, 1, "conquest:wood_panel_full");
		registerRenderMetas(itemblock_wood_full_7, 2, "conquest:wardrobe_fancy_full");
		registerRenderMetas(itemblock_wood_full_7, 3, "conquest:wardrobe_poor_full");
		registerRenderMetas(itemblock_wood_full_7, 4, "conquest:none");
		registerRenderMetas(itemblock_wood_full_7, 5, "conquest:none");
		registerRenderMetas(itemblock_wood_full_7, 6, "conquest:none");
		registerRenderMetas(itemblock_wood_full_7, 7, "conquest:none");
		registerRenderMetas(itemblock_wood_full_7, 8, "conquest:none");
		registerRenderMetas(itemblock_wood_full_7, 9, "conquest:none");
		registerRenderMetas(itemblock_wood_full_7, 10, "conquest:none");
		registerRenderMetas(itemblock_wood_full_7, 11, "conquest:none");
		registerRenderMetas(itemblock_wood_full_7, 12, "conquest:none");
		registerRenderMetas(itemblock_wood_full_7, 13, "conquest:none");
		registerRenderMetas(itemblock_wood_full_7, 14, "conquest:none");
		registerRenderMetas(itemblock_wood_full_7, 15, "conquest:none");
	//Full Slit Wood Blocks
		registerRenderMetas(itemblock_wood_fullslit_1, 0, "conquest:planks_whiteweathered_slit");
		registerRenderMetas(itemblock_wood_fullslit_1, 1, "conquest:planks_redweathered_slit");
		registerRenderMetas(itemblock_wood_fullslit_1, 2, "conquest:planks_lightredweathered_slit");
		registerRenderMetas(itemblock_wood_fullslit_1, 3, "conquest:planks_orangepainted_slit");
		registerRenderMetas(itemblock_wood_fullslit_2, 0, "conquest:planks_yellowweathered_slit");
		registerRenderMetas(itemblock_wood_fullslit_2, 1, "conquest:planks_greenweathered_slit");
		registerRenderMetas(itemblock_wood_fullslit_2, 2, "conquest:planks_limeweathered_slit");
		registerRenderMetas(itemblock_wood_fullslit_2, 3, "conquest:planks_cyanweathered_slit");
		registerRenderMetas(itemblock_wood_fullslit_3, 0, "conquest:planks_darkblueweathered_slit");
		registerRenderMetas(itemblock_wood_fullslit_3, 1, "conquest:planks_blueweathered_slit");
		registerRenderMetas(itemblock_wood_fullslit_3, 2, "conquest:planks_lightblueweathered_slit");
		registerRenderMetas(itemblock_wood_fullslit_3, 3, "conquest:planks_purplepainted_slit");
		registerRenderMetas(itemblock_wood_fullslit_4, 0, "conquest:planks_brownweathered_slit");
		registerRenderMetas(itemblock_wood_fullslit_4, 1, "conquest:planks_oakplatform_slit");
		registerRenderMetas(itemblock_wood_fullslit_4, 2, "conquest:planks_spruceplatform_slit");
		registerRenderMetas(itemblock_wood_fullslit_4, 3, "conquest:planks_sprucevertical_slit");
		registerRenderMetas(itemblock_wood_fullslit_5, 0, "conquest:planks_redacacia_slit");
		registerRenderMetas(itemblock_wood_fullslit_5, 1, "conquest:planks_driftwood_slit");
		registerRenderMetas(itemblock_wood_fullslit_5, 2, "conquest:planks_mossy_slit");
		registerRenderMetas(itemblock_wood_fullslit_5, 3, "conquest:planks_oaknormal_slit");
		registerRenderMetas(itemblock_wood_fullslit_6, 0, "conquest:planks_sprucenormal_slit");
		registerRenderMetas(itemblock_wood_fullslit_6, 1, "conquest:planks_birchnormal_slit");
		registerRenderMetas(itemblock_wood_fullslit_6, 2, "conquest:planks_junglenormal_slit");
		registerRenderMetas(itemblock_wood_fullslit_6, 3, "conquest:planks_acacianormal_slit");
		registerRenderMetas(itemblock_wood_fullslit_7, 0, "conquest:planks_darkoaknormal_slit");
		registerRenderMetas(itemblock_wood_fullslit_7, 1, "conquest:none");
		registerRenderMetas(itemblock_wood_fullslit_7, 2, "conquest:none");
		registerRenderMetas(itemblock_wood_fullslit_7, 3, "conquest:none");

	//Wood Hopper Full Blocks
		registerRenderMetas(itemblock_wood_hopperfull_1, 0, "conquest:wood_oak_hopperfull");
		registerRenderMetas(itemblock_wood_hopperfull_1, 1, "conquest:wood_spruce_hopperfull");
		registerRenderMetas(itemblock_wood_hopperfull_1, 2, "conquest:planks_oakplatform_hopperfull");
		registerRenderMetas(itemblock_wood_hopperfull_1, 3, "conquest:planks_spruceplatform_hopperfull");
		registerRenderMetas(itemblock_wood_hopperfull_1, 4, "conquest:planks_sprucevertical_hopperfull");
		registerRenderMetas(itemblock_wood_hopperfull_1, 5, "conquest:planks_redacacia_hopperfull");
		registerRenderMetas(itemblock_wood_hopperfull_1, 6, "conquest:planks_driftwood_hopperfull");
		registerRenderMetas(itemblock_wood_hopperfull_1, 7, "conquest:planks_mossy_hopperfull");
		registerRenderMetas(itemblock_wood_hopperfull_1, 8, "conquest:planks_birchnormal_hopperfull");
		registerRenderMetas(itemblock_wood_hopperfull_1, 9, "conquest:planks_junglenormal_hopperfull");
		registerRenderMetas(itemblock_wood_hopperfull_1, 10, "conquest:planks_acacianormal_hopperfull");
		registerRenderMetas(itemblock_wood_hopperfull_1, 11, "conquest:planks_darkoaknormal_hopperfull");
		registerRenderMetas(itemblock_wood_hopperfull_1, 12, "conquest:none");
		registerRenderMetas(itemblock_wood_hopperfull_1, 13, "conquest:none");
		registerRenderMetas(itemblock_wood_hopperfull_1, 14, "conquest:none");
		registerRenderMetas(itemblock_wood_hopperfull_1, 15, "conquest:none");
	//Wood Hopper Directional Blocks
		registerRenderMetas(itemblock_wood_hopperdirectional_1, 0, "conquest:wood_oak_hopperdirectional");
		registerRenderMetas(itemblock_wood_hopperdirectional_1, 1, "conquest:wood_spruce_hopperdirectional");
		registerRenderMetas(itemblock_wood_hopperdirectional_1, 2, "conquest:planks_oakplatform_hopperdirectional");
		registerRenderMetas(itemblock_wood_hopperdirectional_1, 3, "conquest:planks_spruceplatform_hopperdirectional");
		registerRenderMetas(itemblock_wood_hopperdirectional_2, 0, "conquest:planks_sprucevertical_hopperdirectional");
		registerRenderMetas(itemblock_wood_hopperdirectional_2, 1, "conquest:planks_redacacia_hopperdirectional");
		registerRenderMetas(itemblock_wood_hopperdirectional_2, 2, "conquest:planks_driftwood_hopperdirectional");
		registerRenderMetas(itemblock_wood_hopperdirectional_2, 3, "conquest:planks_mossy_hopperdirectional");
		registerRenderMetas(itemblock_wood_hopperdirectional_3, 0, "conquest:planks_birchnormal_hopperdirectional");
		registerRenderMetas(itemblock_wood_hopperdirectional_3, 1, "conquest:planks_junglenormal_hopperdirectional");
		registerRenderMetas(itemblock_wood_hopperdirectional_3, 2, "conquest:planks_acacianormal_hopperdirectional");
		registerRenderMetas(itemblock_wood_hopperdirectional_3, 3, "conquest:planks_darkoaknormal_hopperdirectional");
		registerRenderMetas(itemblock_wood_hopperdirectional_4, 0, "conquest:spruce_chair");
		registerRenderMetas(itemblock_wood_hopperdirectional_4, 1, "conquest:none");
		registerRenderMetas(itemblock_wood_hopperdirectional_4, 2, "conquest:none");
		registerRenderMetas(itemblock_wood_hopperdirectional_4, 3, "conquest:none");

	//Full Wood Blocks
		registerRenderMetas(itemblock_wood_log_1, 0, "conquest:wood_mossy_oaklog");
		registerRenderMetas(itemblock_wood_log_1, 1, "conquest:wood_mossy_sprucelog");
		registerRenderMetas(itemblock_wood_log_1, 2, "conquest:wood_mossy_birchlog");
		registerRenderMetas(itemblock_wood_log_1, 3, "conquest:wood_mossy_junglelog");
		registerRenderMetas(itemblock_wood_log_2, 0, "conquest:wood_mossy_acacialog");
		registerRenderMetas(itemblock_wood_log_2, 1, "conquest:wood_mossy_darkoaklog");
		registerRenderMetas(itemblock_wood_log_2, 2, "conquest:wood_ornamentallog");
		registerRenderMetas(itemblock_wood_log_2, 3, "conquest:wood_darkornamentallog");
		registerRenderMetas(itemblock_wood_log_3, 0, "conquest:log_rope");
		registerRenderMetas(itemblock_wood_log_3, 1, "conquest:log_chains");
		registerRenderMetas(itemblock_wood_log_3, 2, "conquest:woodlogs");
		registerRenderMetas(itemblock_wood_log_3, 3, "conquest:barrel");
		registerRenderMetas(itemblock_wood_log_4, 0, "conquest:wickerbasket");
		registerRenderMetas(itemblock_wood_log_4, 1, "conquest:bookshelf_directional");
		registerRenderMetas(itemblock_wood_log_4, 2, "conquest:rack_wine_full");
		registerRenderMetas(itemblock_wood_log_4, 3, "conquest:shelves_full");
		registerRenderMetas(itemblock_wood_log_5, 0, "conquest:rack_spice_full");
		registerRenderMetas(itemblock_wood_log_5, 1, "conquest:rack_scroll_full");
		registerRenderMetas(itemblock_wood_log_5, 2, "conquest:cupboards_full");
		registerRenderMetas(itemblock_wood_log_5, 3, "conquest:beam_spruce_log");
		registerRenderMetas(itemblock_wood_log_6, 0, "conquest:beam_birch_log");
		registerRenderMetas(itemblock_wood_log_6, 1, "conquest:beam_jungle_log");
		registerRenderMetas(itemblock_wood_log_6, 2, "conquest:beam_acacia_log");
		registerRenderMetas(itemblock_wood_log_6, 3, "conquest:beam_darkoak_log");
	//Anvil Stone Blocks
		registerRenderMetas(itemblock_wood_anvil_1, 0, "conquest:spruce_anvil");
		registerRenderMetas(itemblock_wood_anvil_1, 1, "conquest:well_support");
		registerRenderMetas(itemblock_wood_anvil_1, 2, "conquest:planks_oakplatform_anvil");
		registerRenderMetas(itemblock_wood_anvil_1, 3, "conquest:planks_spruceplatform_anvil");
		registerRenderMetas(itemblock_wood_anvil_2, 0, "conquest:planks_sprucevertical_anvil");
		registerRenderMetas(itemblock_wood_anvil_2, 1, "conquest:planks_redacacia_anvil");
		registerRenderMetas(itemblock_wood_anvil_2, 2, "conquest:planks_driftwood_anvil");
		registerRenderMetas(itemblock_wood_anvil_2, 3, "conquest:planks_mossy_anvil");
		registerRenderMetas(itemblock_wood_anvil_3, 0, "conquest:planks_oaknormal_anvil");
		registerRenderMetas(itemblock_wood_anvil_3, 1, "conquest:planks_birchnormal_anvil");
		registerRenderMetas(itemblock_wood_anvil_3, 2, "conquest:planks_junglenormal_anvil");
		registerRenderMetas(itemblock_wood_anvil_3, 3, "conquest:planks_acacianormal_anvil");
		registerRenderMetas(itemblock_wood_anvil_4, 0, "conquest:planks_darkoaknormal_anvil");
		registerRenderMetas(itemblock_wood_anvil_4, 1, "conquest:none");
		registerRenderMetas(itemblock_wood_anvil_4, 2, "conquest:none");
		registerRenderMetas(itemblock_wood_anvil_4, 3, "conquest:none");
	//Wood Wall Blocks
		registerRenderMetas(itemblock_wood_wall_1, 0, "conquest:wood_smalloak_wall");
		registerRenderMetas(itemblock_wood_wall_1, 1, "conquest:wood_smallspruce_wall");
		registerRenderMetas(itemblock_wood_wall_1, 2, "conquest:wood_smallbirch_wall");
		registerRenderMetas(itemblock_wood_wall_1, 3, "conquest:wood_smalljungle_wall");
		registerRenderMetas(itemblock_wood_wall_1, 4, "conquest:wood_smallacacia_wall");
		registerRenderMetas(itemblock_wood_wall_1, 5, "conquest:wood_smalldarkoak_wall");
		registerRenderMetas(itemblock_wood_wall_1, 6, "conquest:wood_smallornamental_wall");
		registerRenderMetas(itemblock_wood_wall_1, 7, "conquest:wood_darkornamental_wall");
		registerRenderMetas(itemblock_wood_wall_1, 8, "conquest:wood_carvedoak_wall");
		registerRenderMetas(itemblock_wood_wall_1, 9, "conquest:wood_oak_wall");
		registerRenderMetas(itemblock_wood_wall_1, 10, "conquest:wood_carvedspruce_wall");
		registerRenderMetas(itemblock_wood_wall_1, 11, "conquest:wood_spruce_wall");
		registerRenderMetas(itemblock_wood_wall_1, 12, "conquest:wood_carvedbirch_wall");
		registerRenderMetas(itemblock_wood_wall_1, 13, "conquest:wood_birch_wall");
		registerRenderMetas(itemblock_wood_wall_1, 14, "conquest:wood_carvedjungle_wall");
		registerRenderMetas(itemblock_wood_wall_1, 15, "conquest:wood_jungle_wall");
		registerRenderMetas(itemblock_wood_wall_2, 0, "conquest:wood_asian_wall");
		registerRenderMetas(itemblock_wood_wall_2, 1, "conquest:wood_carvedacacia_wall");
		registerRenderMetas(itemblock_wood_wall_2, 2, "conquest:wood_acacia_wall");
		registerRenderMetas(itemblock_wood_wall_2, 3, "conquest:wood_carveddarkoak_wall");
		registerRenderMetas(itemblock_wood_wall_2, 4, "conquest:wood_darkoak_wall");
		registerRenderMetas(itemblock_wood_wall_2, 5, "conquest:oak_mossylog_wall");
		registerRenderMetas(itemblock_wood_wall_2, 6, "conquest:spruce_mossylog_wall");
		registerRenderMetas(itemblock_wood_wall_2, 7, "conquest:birch_mossylog_wall");
		registerRenderMetas(itemblock_wood_wall_2, 8, "conquest:jungle_mossylog_wall");
		registerRenderMetas(itemblock_wood_wall_2, 9, "conquest:acacia_mossylog_wall");
		registerRenderMetas(itemblock_wood_wall_2, 10, "conquest:oak_mossydark_wall");
		registerRenderMetas(itemblock_wood_wall_2, 11, "conquest:ash_burntlog_wall");
		registerRenderMetas(itemblock_wood_wall_2, 12, "conquest:planks_whiteweathered_wall");
		registerRenderMetas(itemblock_wood_wall_2, 13, "conquest:planks_redweathered_wall");
		registerRenderMetas(itemblock_wood_wall_2, 14, "conquest:planks_lightredweathered_wall");
		registerRenderMetas(itemblock_wood_wall_2, 15, "conquest:planks_orangepainted_wall");
		registerRenderMetas(itemblock_wood_wall_3, 0, "conquest:planks_yellowweathered_wall");
		registerRenderMetas(itemblock_wood_wall_3, 1, "conquest:planks_greenweathered_wall");
		registerRenderMetas(itemblock_wood_wall_3, 2, "conquest:planks_limeweathered_wall");
		registerRenderMetas(itemblock_wood_wall_3, 3, "conquest:planks_cyanweathered_wall");
		registerRenderMetas(itemblock_wood_wall_3, 4, "conquest:planks_darkblueweathered_wall");
		registerRenderMetas(itemblock_wood_wall_3, 5, "conquest:planks_blueweathered_wall");
		registerRenderMetas(itemblock_wood_wall_3, 6, "conquest:planks_lightblueweathered_wall");
		registerRenderMetas(itemblock_wood_wall_3, 7, "conquest:planks_purplepainted_wall");
		registerRenderMetas(itemblock_wood_wall_3, 8, "conquest:planks_brownweathered_wall");
		registerRenderMetas(itemblock_wood_wall_3, 9, "conquest:planks_oakplatform_wall");
		registerRenderMetas(itemblock_wood_wall_3, 10, "conquest:planks_spruceplatform_wall");
		registerRenderMetas(itemblock_wood_wall_3, 11, "conquest:planks_sprucevertical_wall");
		registerRenderMetas(itemblock_wood_wall_3, 12, "conquest:planks_redacacia_wall");
		registerRenderMetas(itemblock_wood_wall_3, 13, "conquest:planks_driftwood_wall");
		registerRenderMetas(itemblock_wood_wall_3, 14, "conquest:planks_mossy_wall");
		registerRenderMetas(itemblock_wood_wall_3, 15, "conquest:planks_oaknormal_wall");
		registerRenderMetas(itemblock_wood_wall_4, 0, "conquest:planks_sprucenormal_wall");
		registerRenderMetas(itemblock_wood_wall_4, 1, "conquest:planks_birchnormal_wall");
		registerRenderMetas(itemblock_wood_wall_4, 2, "conquest:planks_junglenormal_wall");
		registerRenderMetas(itemblock_wood_wall_4, 3, "conquest:planks_acacianormal_wall");
		registerRenderMetas(itemblock_wood_wall_4, 4, "conquest:planks_darkoaknormal_wall");
		registerRenderMetas(itemblock_wood_wall_4, 5, "conquest:none");
		registerRenderMetas(itemblock_wood_wall_4, 6, "conquest:none");
		registerRenderMetas(itemblock_wood_wall_4, 7, "conquest:none");
		registerRenderMetas(itemblock_wood_wall_4, 8, "conquest:none");
		registerRenderMetas(itemblock_wood_wall_4, 9, "conquest:none");
		registerRenderMetas(itemblock_wood_wall_4, 10, "conquest:none");
		registerRenderMetas(itemblock_wood_wall_4, 11, "conquest:none");
		registerRenderMetas(itemblock_wood_wall_4, 12, "conquest:none");
		registerRenderMetas(itemblock_wood_wall_4, 13, "conquest:none");
		registerRenderMetas(itemblock_wood_wall_4, 14, "conquest:none");
		registerRenderMetas(itemblock_wood_wall_4, 15, "conquest:none");
	//Wood Pillar Blocks
		registerRenderMetas(itemblock_wood_pillar_1, 0, "conquest:wood_smalloak_pillar");
		registerRenderMetas(itemblock_wood_pillar_1, 1, "conquest:wood_smallspruce_pillar");
		registerRenderMetas(itemblock_wood_pillar_1, 2, "conquest:wood_smallbirch_pillar");
		registerRenderMetas(itemblock_wood_pillar_1, 3, "conquest:wood_smalljungle_pillar");
		registerRenderMetas(itemblock_wood_pillar_1, 4, "conquest:wood_smallacacia_pillar");
		registerRenderMetas(itemblock_wood_pillar_1, 5, "conquest:wood_smalldarkoak_pillar");
		registerRenderMetas(itemblock_wood_pillar_1, 6, "conquest:wood_smallornamental_pillar");
		registerRenderMetas(itemblock_wood_pillar_1, 7, "conquest:wood_darkornamental_pillar");
		registerRenderMetas(itemblock_wood_pillar_1, 8, "conquest:wood_carvedoak_pillar");
		registerRenderMetas(itemblock_wood_pillar_1, 9, "conquest:wood_oak_pillar");
		registerRenderMetas(itemblock_wood_pillar_1, 10, "conquest:wood_carvedspruce_pillar");
		registerRenderMetas(itemblock_wood_pillar_1, 11, "conquest:wood_spruce_pillar");
		registerRenderMetas(itemblock_wood_pillar_1, 12, "conquest:wood_carvedbirch_pillar");
		registerRenderMetas(itemblock_wood_pillar_1, 13, "conquest:wood_birch_pillar");
		registerRenderMetas(itemblock_wood_pillar_1, 14, "conquest:wood_carvedjungle_pillar");
		registerRenderMetas(itemblock_wood_pillar_1, 15, "conquest:wood_jungle_pillar");
		registerRenderMetas(itemblock_wood_pillar_2, 0, "conquest:wood_asian_pillar");
		registerRenderMetas(itemblock_wood_pillar_2, 1, "conquest:wood_carvedacacia_pillar");
		registerRenderMetas(itemblock_wood_pillar_2, 2, "conquest:wood_acacia_pillar");
		registerRenderMetas(itemblock_wood_pillar_2, 3, "conquest:wood_carveddarkoak_pillar");
		registerRenderMetas(itemblock_wood_pillar_2, 4, "conquest:wood_darkoak_pillar");
		registerRenderMetas(itemblock_wood_pillar_2, 5, "conquest:oak_mossylog_pillar");
		registerRenderMetas(itemblock_wood_pillar_2, 6, "conquest:spruce_mossylog_pillar");
		registerRenderMetas(itemblock_wood_pillar_2, 7, "conquest:birch_mossylog_pillar");
		registerRenderMetas(itemblock_wood_pillar_2, 8, "conquest:jungle_mossylog_pillar");
		registerRenderMetas(itemblock_wood_pillar_2, 9, "conquest:acacia_mossylog_pillar");
		registerRenderMetas(itemblock_wood_pillar_2, 10, "conquest:oak_mossydark_pillar");
		registerRenderMetas(itemblock_wood_pillar_2, 11, "conquest:ash_burntlog_pillar");
		registerRenderMetas(itemblock_wood_pillar_2, 12, "conquest:planks_whiteweathered_pillar");
		registerRenderMetas(itemblock_wood_pillar_2, 13, "conquest:planks_redweathered_pillar");
		registerRenderMetas(itemblock_wood_pillar_2, 14, "conquest:planks_lightredweathered_pillar");
		registerRenderMetas(itemblock_wood_pillar_2, 15, "conquest:planks_orangepainted_pillar");
		registerRenderMetas(itemblock_wood_pillar_3, 0, "conquest:planks_yellowweathered_pillar");
		registerRenderMetas(itemblock_wood_pillar_3, 1, "conquest:planks_greenweathered_pillar");
		registerRenderMetas(itemblock_wood_pillar_3, 2, "conquest:planks_limeweathered_pillar");
		registerRenderMetas(itemblock_wood_pillar_3, 3, "conquest:planks_cyanweathered_pillar");
		registerRenderMetas(itemblock_wood_pillar_3, 4, "conquest:planks_darkblueweathered_pillar");
		registerRenderMetas(itemblock_wood_pillar_3, 5, "conquest:planks_blueweathered_pillar");
		registerRenderMetas(itemblock_wood_pillar_3, 6, "conquest:planks_lightblueweathered_pillar");
		registerRenderMetas(itemblock_wood_pillar_3, 7, "conquest:planks_purplepainted_pillar");
		registerRenderMetas(itemblock_wood_pillar_3, 8, "conquest:planks_brownweathered_pillar");
		registerRenderMetas(itemblock_wood_pillar_3, 9, "conquest:planks_oakplatform_pillar");
		registerRenderMetas(itemblock_wood_pillar_3, 10, "conquest:planks_spruceplatform_pillar");
		registerRenderMetas(itemblock_wood_pillar_3, 11, "conquest:planks_sprucevertical_pillar");
		registerRenderMetas(itemblock_wood_pillar_3, 12, "conquest:planks_redacacia_pillar");
		registerRenderMetas(itemblock_wood_pillar_3, 13, "conquest:planks_driftwood_pillar");
		registerRenderMetas(itemblock_wood_pillar_3, 14, "conquest:planks_mossy_pillar");
		registerRenderMetas(itemblock_wood_pillar_3, 15, "conquest:planks_oaknormal_pillar");
		registerRenderMetas(itemblock_wood_pillar_4, 0, "conquest:planks_sprucenormal_pillar");
		registerRenderMetas(itemblock_wood_pillar_4, 1, "conquest:planks_birchnormal_pillar");
		registerRenderMetas(itemblock_wood_pillar_4, 2, "conquest:planks_junglenormal_pillar");
		registerRenderMetas(itemblock_wood_pillar_4, 3, "conquest:planks_acacianormal_pillar");
		registerRenderMetas(itemblock_wood_pillar_4, 4, "conquest:planks_darkoaknormal_pillar");
		registerRenderMetas(itemblock_wood_pillar_4, 5, "conquest:log_rope_pillar");
		registerRenderMetas(itemblock_wood_pillar_4, 6, "conquest:log_chain_pillar");
		registerRenderMetas(itemblock_wood_pillar_4, 7, "conquest:none");
		registerRenderMetas(itemblock_wood_pillar_4, 8, "conquest:none");
		registerRenderMetas(itemblock_wood_pillar_4, 9, "conquest:none");
		registerRenderMetas(itemblock_wood_pillar_4, 10, "conquest:none");
		registerRenderMetas(itemblock_wood_pillar_4, 11, "conquest:none");
		registerRenderMetas(itemblock_wood_pillar_4, 12, "conquest:none");
		registerRenderMetas(itemblock_wood_pillar_4, 13, "conquest:none");
		registerRenderMetas(itemblock_wood_pillar_4, 14, "conquest:none");
		registerRenderMetas(itemblock_wood_pillar_4, 15, "conquest:none");
	//Fence Wood Blocks
		registerRenderMetas(itemblock_wood_fence_1, 0, "conquest:bamboo");
		registerRenderMetas(itemblock_wood_fence_1, 1, "conquest:wood_oaklog_fence");
		registerRenderMetas(itemblock_wood_fence_1, 2, "conquest:wood_sprucelog_fence");
		registerRenderMetas(itemblock_wood_fence_1, 3, "conquest:wood_birchlog_fence");
		registerRenderMetas(itemblock_wood_fence_1, 4, "conquest:wood_junglelog_fence");
		registerRenderMetas(itemblock_wood_fence_1, 5, "conquest:wood_acacia_fence");
		registerRenderMetas(itemblock_wood_fence_1, 6, "conquest:wood_darklog_fence");
		registerRenderMetas(itemblock_wood_fence_1, 7, "conquest:wood_plain_fence");
		registerRenderMetas(itemblock_wood_fence_1, 8, "conquest:wood_plain2_fence");
		registerRenderMetas(itemblock_wood_fence_1, 9, "conquest:wood_newelcapped_fence");
		registerRenderMetas(itemblock_wood_fence_1, 10, "conquest:oak_mossylog_fence");
		registerRenderMetas(itemblock_wood_fence_1, 11, "conquest:spruce_mossylog_fence");
		registerRenderMetas(itemblock_wood_fence_1, 12, "conquest:birch_mossylog_fence");
		registerRenderMetas(itemblock_wood_fence_1, 13, "conquest:jungle_mossylog_fence");
		registerRenderMetas(itemblock_wood_fence_1, 14, "conquest:acacia_mossylog_fence");
		registerRenderMetas(itemblock_wood_fence_1, 15, "conquest:oak_mossydark_fence");
		registerRenderMetas(itemblock_wood_fence_2, 0, "conquest:wood_spruce_fence");
		registerRenderMetas(itemblock_wood_fence_2, 1, "conquest:wood_birch_fence");
		registerRenderMetas(itemblock_wood_fence_2, 2, "conquest:wood_roped_fence");
		registerRenderMetas(itemblock_wood_fence_2, 3, "conquest:wood_asian_fence");
		registerRenderMetas(itemblock_wood_fence_2, 4, "conquest:wood_darkoak_fence");
		registerRenderMetas(itemblock_wood_fence_2, 5, "conquest:ash_burntlog_fence");
		registerRenderMetas(itemblock_wood_fence_2, 6, "conquest:planks_whiteweathered_fence");
		registerRenderMetas(itemblock_wood_fence_2, 7, "conquest:planks_redweathered_fence");
		registerRenderMetas(itemblock_wood_fence_2, 8, "conquest:planks_lightredweathered_fence");
		registerRenderMetas(itemblock_wood_fence_2, 9, "conquest:planks_orangepainted_fence");
		registerRenderMetas(itemblock_wood_fence_2, 10, "conquest:planks_yellowweathered_fence");
		registerRenderMetas(itemblock_wood_fence_2, 11, "conquest:planks_greenweathered_fence");
		registerRenderMetas(itemblock_wood_fence_2, 12, "conquest:planks_limeweathered_fence");
		registerRenderMetas(itemblock_wood_fence_2, 13, "conquest:planks_cyanweathered_fence");
		registerRenderMetas(itemblock_wood_fence_2, 14, "conquest:planks_darkblueweathered_fence");
		registerRenderMetas(itemblock_wood_fence_2, 15, "conquest:planks_blueweathered_fence");
		registerRenderMetas(itemblock_wood_fence_3, 0, "conquest:planks_lightblueweathered_fence");
		registerRenderMetas(itemblock_wood_fence_3, 1, "conquest:planks_purplepainted_fence");
		registerRenderMetas(itemblock_wood_fence_3, 2, "conquest:planks_brownweathered_fence");
		registerRenderMetas(itemblock_wood_fence_3, 3, "conquest:planks_oakplatform_fence");
		registerRenderMetas(itemblock_wood_fence_3, 4, "conquest:planks_spruceplatform_fence");
		registerRenderMetas(itemblock_wood_fence_3, 5, "conquest:planks_sprucevertical_fence");
		registerRenderMetas(itemblock_wood_fence_3, 6, "conquest:planks_redacacia_fence");
		registerRenderMetas(itemblock_wood_fence_3, 7, "conquest:planks_driftwood_fence");
		registerRenderMetas(itemblock_wood_fence_3, 8, "conquest:planks_mossy_fence");
		registerRenderMetas(itemblock_wood_fence_3, 9, "conquest:bamboo_dried_fence");
		registerRenderMetas(itemblock_wood_fence_3, 10, "conquest:none");
		registerRenderMetas(itemblock_wood_fence_3, 11, "conquest:none");
		registerRenderMetas(itemblock_wood_fence_3, 12, "conquest:none");
		registerRenderMetas(itemblock_wood_fence_3, 13, "conquest:none");
		registerRenderMetas(itemblock_wood_fence_3, 14, "conquest:none");
		registerRenderMetas(itemblock_wood_fence_3, 15, "conquest:none");
	//Fencegate Wood Blocks
		registerRenderMetas(itemblock_wood_fencegate_1, 0, "conquest:wood_oak_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_1, 1, "conquest:wood_spruce_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_2, 0, "conquest:wood_birch_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_2, 1, "conquest:wood_jungle_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_3, 0, "conquest:wood_asian_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_3, 1, "conquest:wood_darkoak_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_4, 0, "conquest:planks_whiteweathered_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_4, 1, "conquest:planks_redweathered_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_5, 0, "conquest:planks_lightredweathered_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_5, 1, "conquest:planks_orangepainted_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_6, 0, "conquest:planks_yellowweathered_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_6, 1, "conquest:planks_greenweathered_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_7, 0, "conquest:planks_limeweathered_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_7, 1, "conquest:planks_cyanweathered_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_8, 0, "conquest:planks_darkblueweathered_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_8, 1, "conquest:planks_blueweathered_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_9, 0, "conquest:planks_lightblueweathered_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_9, 1, "conquest:planks_purplepainted_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_10, 0, "conquest:planks_brownweathered_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_10, 1, "conquest:planks_redacacia_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_11, 0, "conquest:planks_driftwood_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_11, 1, "conquest:planks_mossy_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_12, 0, "conquest:wood_newelcapped_fencegate");
		registerRenderMetas(itemblock_wood_fencegate_12, 1, "conquest:none");

	//Small Pillar Wood Blocks
		registerRenderMetas(itemblock_wood_smallpillar_1, 0, "conquest:bamboo_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_1, 1, "conquest:wood_oaklog_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_1, 2, "conquest:wood_sprucelog_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_1, 3, "conquest:wood_birchlog_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_1, 4, "conquest:wood_junglelog_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_1, 5, "conquest:wood_acacia_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_1, 6, "conquest:wood_darklog_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_1, 7, "conquest:wood_plain_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_1, 8, "conquest:wood_plain2_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_1, 9, "conquest:wood_spruce_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_1, 10, "conquest:wood_birch_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_1, 11, "conquest:wood_roped_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_1, 12, "conquest:wood_asian_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_1, 13, "conquest:wood_darkoak_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_1, 14, "conquest:wood_newelcapped_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_1, 15, "conquest:oak_mossylog_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_2, 0, "conquest:spruce_mossylog_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_2, 1, "conquest:birch_mossylog_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_2, 2, "conquest:jungle_mossylog_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_2, 3, "conquest:acacia_mossylog_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_2, 4, "conquest:darkoak_mossylog_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_2, 5, "conquest:ash_burntlog_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_2, 6, "conquest:log_rope_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_2, 7, "conquest:log_chain_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_2, 8, "conquest:planks_whiteweathered_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_2, 9, "conquest:planks_redweathered_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_2, 10, "conquest:planks_lightredweathered_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_2, 11, "conquest:planks_orangepainted_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_2, 12, "conquest:planks_yellowweathered_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_2, 13, "conquest:planks_greenweathered_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_2, 14, "conquest:planks_limeweathered_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_2, 15, "conquest:planks_cyanweathered_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_3, 0, "conquest:planks_darkblueweathered_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_3, 1, "conquest:planks_blueweathered_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_3, 2, "conquest:planks_lightblueweathered_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_3, 3, "conquest:planks_purplepainted_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_3, 4, "conquest:planks_brownweathered_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_3, 5, "conquest:planks_oakplatform_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_3, 6, "conquest:planks_spruceplatform_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_3, 7, "conquest:planks_sprucevertical_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_3, 8, "conquest:planks_redacacia_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_3, 9, "conquest:planks_driftwood_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_3, 10, "conquest:planks_mossy_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_3, 11, "conquest:bamboo_dried_smallpillar");
		registerRenderMetas(itemblock_wood_smallpillar_3, 12, "conquest:none");
		registerRenderMetas(itemblock_wood_smallpillar_3, 13, "conquest:none");
		registerRenderMetas(itemblock_wood_smallpillar_3, 14, "conquest:none");
		registerRenderMetas(itemblock_wood_smallpillar_3, 15, "conquest:none");
	//Full Partial Wood Blocks
		registerRenderMetas(itemblock_wood_fullpartial_1, 0, "conquest:beehive");
		registerRenderMetas(itemblock_wood_fullpartial_1, 1, "conquest:hornetsnest");
		registerRenderMetas(itemblock_wood_fullpartial_1, 2, "conquest:palm");
		registerRenderMetas(itemblock_wood_fullpartial_1, 3, "conquest:wood_spruce_dragonegg");
		registerRenderMetas(itemblock_wood_fullpartial_1, 4, "conquest:planks_oakplatform_dragonegg");
		registerRenderMetas(itemblock_wood_fullpartial_1, 5, "conquest:planks_spruceplatform_dragonegg");
		registerRenderMetas(itemblock_wood_fullpartial_1, 6, "conquest:planks_sprucevertical_dragonegg");
		registerRenderMetas(itemblock_wood_fullpartial_1, 7, "conquest:planks_redacacia_dragonegg");
		registerRenderMetas(itemblock_wood_fullpartial_1, 8, "conquest:planks_driftwood_dragonegg");
		registerRenderMetas(itemblock_wood_fullpartial_1, 9, "conquest:planks_mossy_dragonegg");
		registerRenderMetas(itemblock_wood_fullpartial_1, 10, "conquest:planks_oaknormal_dragonegg");
		registerRenderMetas(itemblock_wood_fullpartial_1, 11, "conquest:planks_birchnormal_dragonegg");
		registerRenderMetas(itemblock_wood_fullpartial_1, 12, "conquest:planks_junglenormal_dragonegg");
		registerRenderMetas(itemblock_wood_fullpartial_1, 13, "conquest:planks_acacianormal_dragonegg");
		registerRenderMetas(itemblock_wood_fullpartial_1, 14, "conquest:planks_darkoaknormal_dragonegg");
		registerRenderMetas(itemblock_wood_fullpartial_1, 15, "conquest:plants_treestump");
		registerRenderMetas(itemblock_wood_fullpartial_2, 0, "conquest:basket_cauldron");
		registerRenderMetas(itemblock_wood_fullpartial_2, 1, "conquest:none");
		registerRenderMetas(itemblock_wood_fullpartial_2, 2, "conquest:none");
		registerRenderMetas(itemblock_wood_fullpartial_2, 3, "conquest:none");
		registerRenderMetas(itemblock_wood_fullpartial_2, 4, "conquest:none");
		registerRenderMetas(itemblock_wood_fullpartial_2, 5, "conquest:none");
		registerRenderMetas(itemblock_wood_fullpartial_2, 6, "conquest:none");
		registerRenderMetas(itemblock_wood_fullpartial_2, 7, "conquest:none");
		registerRenderMetas(itemblock_wood_fullpartial_2, 8, "conquest:none");
		registerRenderMetas(itemblock_wood_fullpartial_2, 9, "conquest:none");
		registerRenderMetas(itemblock_wood_fullpartial_2, 10, "conquest:none");
		registerRenderMetas(itemblock_wood_fullpartial_2, 11, "conquest:none");
		registerRenderMetas(itemblock_wood_fullpartial_2, 12, "conquest:none");
		registerRenderMetas(itemblock_wood_fullpartial_2, 13, "conquest:none");
		registerRenderMetas(itemblock_wood_fullpartial_2, 14, "conquest:none");
		registerRenderMetas(itemblock_wood_fullpartial_2, 15, "conquest:none");
	//ConnectedXZ Wood Blocks
		registerRenderMetas(itemblock_wood_connectedxz_1, 0, "conquest:table");
		registerRenderMetas(itemblock_wood_connectedxz_1, 1, "conquest:table2");
		registerRenderMetas(itemblock_wood_connectedxz_1, 2, "conquest:none");
		registerRenderMetas(itemblock_wood_connectedxz_1, 3, "conquest:none");
		registerRenderMetas(itemblock_wood_connectedxz_1, 4, "conquest:none");
		registerRenderMetas(itemblock_wood_connectedxz_1, 5, "conquest:none");
		registerRenderMetas(itemblock_wood_connectedxz_1, 6, "conquest:none");
		registerRenderMetas(itemblock_wood_connectedxz_1, 7, "conquest:none");
		registerRenderMetas(itemblock_wood_connectedxz_1, 8, "conquest:none");
		registerRenderMetas(itemblock_wood_connectedxz_1, 9, "conquest:none");
		registerRenderMetas(itemblock_wood_connectedxz_1, 10, "conquest:none");
		registerRenderMetas(itemblock_wood_connectedxz_1, 11, "conquest:none");
		registerRenderMetas(itemblock_wood_connectedxz_1, 12, "conquest:none");
		registerRenderMetas(itemblock_wood_connectedxz_1, 13, "conquest:none");
		registerRenderMetas(itemblock_wood_connectedxz_1, 14, "conquest:none");
		registerRenderMetas(itemblock_wood_connectedxz_1, 15, "conquest:none");
	//Half Wood Blocks
		registerRenderMetas(itemblock_wood_half_1, 0, "conquest:chair_wicker");
		registerRenderMetas(itemblock_wood_half_1, 1, "conquest:chair_oakwood");
		registerRenderMetas(itemblock_wood_half_1, 2, "conquest:chair_redcushion");
		registerRenderMetas(itemblock_wood_half_1, 3, "conquest:chair_bluecushion");
		registerRenderMetas(itemblock_wood_half_1, 4, "conquest:chair_blackcushion");
		registerRenderMetas(itemblock_wood_half_1, 5, "conquest:chair_greencushion");
		registerRenderMetas(itemblock_wood_half_1, 6, "conquest:chair_leather");
		registerRenderMetas(itemblock_wood_half_1, 7, "conquest:none");
		registerRenderMetas(itemblock_wood_half_1, 8, "conquest:none");
		registerRenderMetas(itemblock_wood_half_1, 9, "conquest:none");
		registerRenderMetas(itemblock_wood_half_1, 10, "conquest:none");
		registerRenderMetas(itemblock_wood_half_1, 11, "conquest:none");
		registerRenderMetas(itemblock_wood_half_1, 12, "conquest:none");
		registerRenderMetas(itemblock_wood_half_1, 13, "conquest:none");
		registerRenderMetas(itemblock_wood_half_1, 14, "conquest:none");
		registerRenderMetas(itemblock_wood_half_1, 15, "conquest:none");
	//Daylight Detector Wood Blocks
		registerRenderMetas(itemblock_wood_daylightdetector_1, 0, "conquest:chair_wicker_daylightdetector");
		registerRenderMetas(itemblock_wood_daylightdetector_1, 1, "conquest:chair_oakwood_daylightdetector");
		registerRenderMetas(itemblock_wood_daylightdetector_1, 2, "conquest:chair_redcushion_daylightdetector");
		registerRenderMetas(itemblock_wood_daylightdetector_1, 3, "conquest:chair_bluecushion_daylightdetector");
		registerRenderMetas(itemblock_wood_daylightdetector_1, 4, "conquest:chair_blackcushion_daylightdetector");
		registerRenderMetas(itemblock_wood_daylightdetector_1, 5, "conquest:chair_greencushion_daylightdetector");
		registerRenderMetas(itemblock_wood_daylightdetector_1, 6, "conquest:chair_leather_daylightdetector");
		registerRenderMetas(itemblock_wood_daylightdetector_1, 7, "conquest:none");
		registerRenderMetas(itemblock_wood_daylightdetector_1, 8, "conquest:none");
		registerRenderMetas(itemblock_wood_daylightdetector_1, 9, "conquest:none");
		registerRenderMetas(itemblock_wood_daylightdetector_1, 10, "conquest:none");
		registerRenderMetas(itemblock_wood_daylightdetector_1, 11, "conquest:none");
		registerRenderMetas(itemblock_wood_daylightdetector_1, 12, "conquest:none");
		registerRenderMetas(itemblock_wood_daylightdetector_1, 13, "conquest:none");
		registerRenderMetas(itemblock_wood_daylightdetector_1, 14, "conquest:none");
		registerRenderMetas(itemblock_wood_daylightdetector_1, 15, "conquest:none");
	//Stairs Wood Blocks
		registerRenderMetas(itemblock_wood_stairs_1, 0, "conquest:log_oak_stairs");
		registerRenderMetas(itemblock_wood_stairs_1, 1, "conquest:log_spruce_stairs");
		registerRenderMetas(itemblock_wood_stairs_2, 0, "conquest:log_birch_stairs");
		registerRenderMetas(itemblock_wood_stairs_2, 1, "conquest:log_jungle_stairs");
		registerRenderMetas(itemblock_wood_stairs_3, 0, "conquest:log_acacia_stairs");
		registerRenderMetas(itemblock_wood_stairs_3, 1, "conquest:log_darkoak_stairs");
		registerRenderMetas(itemblock_wood_stairs_4, 0, "conquest:planks_supports_stairs");
		registerRenderMetas(itemblock_wood_stairs_4, 1, "conquest:planks_oak_stairs");
		registerRenderMetas(itemblock_wood_stairs_5, 0, "conquest:planks_spruce_stairs");
		registerRenderMetas(itemblock_wood_stairs_5, 1, "conquest:planks_birch_stairs");
		registerRenderMetas(itemblock_wood_stairs_6, 0, "conquest:planks_jungle_stairs");
		registerRenderMetas(itemblock_wood_stairs_6, 1, "conquest:planks_redacacia_stairs");
		registerRenderMetas(itemblock_wood_stairs_7, 0, "conquest:planks_acacia_stairs");
		registerRenderMetas(itemblock_wood_stairs_7, 1, "conquest:planks_driftwood_stairs");
		registerRenderMetas(itemblock_wood_stairs_8, 0, "conquest:planks_mossy_stairs");
		registerRenderMetas(itemblock_wood_stairs_8, 1, "conquest:oak_mossylog_stairs");
		registerRenderMetas(itemblock_wood_stairs_9, 0, "conquest:spruce_mossylog_stairs");
		registerRenderMetas(itemblock_wood_stairs_9, 1, "conquest:birch_mossylog_stairs");
		registerRenderMetas(itemblock_wood_stairs_10, 0, "conquest:jungle_mossylog_stairs");
		registerRenderMetas(itemblock_wood_stairs_10, 1, "conquest:acacia_mossylog_stairs");
		registerRenderMetas(itemblock_wood_stairs_11, 0, "conquest:oak_mossydark_stairs");
		registerRenderMetas(itemblock_wood_stairs_11, 1, "conquest:none"); //open slot
		registerRenderMetas(itemblock_wood_stairs_12, 0, "conquest:ash_burntlog_stairs");
		registerRenderMetas(itemblock_wood_stairs_12, 1, "conquest:log_ornamental_stairs");
		registerRenderMetas(itemblock_wood_stairs_13, 0, "conquest:log_darkornamental_stairs");
		registerRenderMetas(itemblock_wood_stairs_13, 1, "conquest:birch_log_stairs");
		registerRenderMetas(itemblock_wood_stairs_14, 0, "conquest:darkbirch_log_stairs");
		registerRenderMetas(itemblock_wood_stairs_14, 1, "conquest:planks_whitepainted_stairs");
		registerRenderMetas(itemblock_wood_stairs_15, 0, "conquest:planks_whiteweathered_stairs");
		registerRenderMetas(itemblock_wood_stairs_15, 1, "conquest:planks_redpainted_stairs");
		registerRenderMetas(itemblock_wood_stairs_16, 0, "conquest:planks_redweathered_stairs");
		registerRenderMetas(itemblock_wood_stairs_16, 1, "conquest:planks_lightredpainted_stairs");
		registerRenderMetas(itemblock_wood_stairs_17, 0, "conquest:planks_lightredweathered_stairs");
		registerRenderMetas(itemblock_wood_stairs_17, 1, "conquest:planks_orangepainted_stairs");
		registerRenderMetas(itemblock_wood_stairs_18, 0, "conquest:planks_yellowpainted_stairs");
		registerRenderMetas(itemblock_wood_stairs_18, 1, "conquest:planks_yellowweathered_stairs");
		registerRenderMetas(itemblock_wood_stairs_19, 0, "conquest:planks_greenpainted_stairs");
		registerRenderMetas(itemblock_wood_stairs_19, 1, "conquest:planks_greenweathered_stairs");
		registerRenderMetas(itemblock_wood_stairs_20, 0, "conquest:planks_limepainted_stairs");
		registerRenderMetas(itemblock_wood_stairs_20, 1, "conquest:planks_limeweathered_stairs");
		registerRenderMetas(itemblock_wood_stairs_21, 0, "conquest:planks_cyanpainted_stairs");
		registerRenderMetas(itemblock_wood_stairs_21, 1, "conquest:planks_cyanweathered_stairs");
		registerRenderMetas(itemblock_wood_stairs_22, 0, "conquest:planks_bluepainted_stairs");
		registerRenderMetas(itemblock_wood_stairs_22, 1, "conquest:planks_darkblueweathered_stairs");
		registerRenderMetas(itemblock_wood_stairs_23, 0, "conquest:planks_blueweathered_stairs");
		registerRenderMetas(itemblock_wood_stairs_23, 1, "conquest:planks_lightbluepainted_stairs");
		registerRenderMetas(itemblock_wood_stairs_24, 0, "conquest:planks_lightblueweathered_stairs");
		registerRenderMetas(itemblock_wood_stairs_24, 1, "conquest:planks_purplepainted_stairs");
		registerRenderMetas(itemblock_wood_stairs_25, 0, "conquest:planks_brownpainted_stairs");
		registerRenderMetas(itemblock_wood_stairs_25, 1, "conquest:planks_brownweathered_stairs");
		registerRenderMetas(itemblock_wood_stairs_26, 0, "conquest:planks_oakplatform_stairs");
		registerRenderMetas(itemblock_wood_stairs_26, 1, "conquest:planks_spruceplatform_stairs");
		registerRenderMetas(itemblock_wood_stairs_27, 0, "conquest:planks_sprucevertical_stairs");
		registerRenderMetas(itemblock_wood_stairs_27, 1, "conquest:none");
	//Slab Wood Blocks
		registerRenderMetas(itemblock_wood_slab_1, 0, "conquest:planks_redacacia_slab");
		registerRenderMetas(itemblock_wood_slab_1, 1, "conquest:planks_driftwood_slab");
		registerRenderMetas(itemblock_wood_slab_1, 2, "conquest:planks_mossy_slab");
		registerRenderMetas(itemblock_wood_slab_1, 3, "conquest:table_map_slab");
		registerRenderMetas(itemblock_wood_slab_1, 4, "conquest:oak_mossylog_slab");
		registerRenderMetas(itemblock_wood_slab_1, 5, "conquest:spruce_mossylog_slab");
		registerRenderMetas(itemblock_wood_slab_1, 6, "conquest:birch_mossylog_slab");
		registerRenderMetas(itemblock_wood_slab_1, 7, "conquest:jungle_mossylog_slab");
		registerRenderMetas(itemblock_wood_slab_2, 0, "conquest:acacia_mossylog_slab");
		registerRenderMetas(itemblock_wood_slab_2, 1, "conquest:oak_mossydark_slab");
		registerRenderMetas(itemblock_wood_slab_2, 2, "conquest:none"); //open slot
		registerRenderMetas(itemblock_wood_slab_2, 3, "conquest:ash_burntlog_slab");
		registerRenderMetas(itemblock_wood_slab_2, 4, "conquest:log_ornamental_slab");
		registerRenderMetas(itemblock_wood_slab_2, 5, "conquest:log_darkornamental_slab");
		registerRenderMetas(itemblock_wood_slab_2, 6, "conquest:birch_log_slab");
		registerRenderMetas(itemblock_wood_slab_2, 7, "conquest:darkbirch_log_slab");
		registerRenderMetas(itemblock_wood_slab_3, 0, "conquest:log_rope_slab");
		registerRenderMetas(itemblock_wood_slab_3, 1, "conquest:log_chain_slab");
		registerRenderMetas(itemblock_wood_slab_3, 2, "conquest:woodlogs_slab");
		registerRenderMetas(itemblock_wood_slab_3, 3, "conquest:planks_whitepainted_slab");
		registerRenderMetas(itemblock_wood_slab_3, 4, "conquest:planks_whiteweathered_slab");
		registerRenderMetas(itemblock_wood_slab_3, 5, "conquest:planks_redpainted_slab");
		registerRenderMetas(itemblock_wood_slab_3, 6, "conquest:planks_redweathered_slab");
		registerRenderMetas(itemblock_wood_slab_3, 7, "conquest:planks_lightredpainted_slab");
		registerRenderMetas(itemblock_wood_slab_4, 0, "conquest:planks_lightredweathered_slab");
		registerRenderMetas(itemblock_wood_slab_4, 1, "conquest:planks_orangepainted_slab");
		registerRenderMetas(itemblock_wood_slab_4, 2, "conquest:planks_yellowpainted_slab");
		registerRenderMetas(itemblock_wood_slab_4, 3, "conquest:planks_yellowweathered_slab");
		registerRenderMetas(itemblock_wood_slab_4, 4, "conquest:planks_greenpainted_slab");
		registerRenderMetas(itemblock_wood_slab_4, 5, "conquest:planks_greenweathered_slab");
		registerRenderMetas(itemblock_wood_slab_4, 6, "conquest:planks_limepainted_slab");
		registerRenderMetas(itemblock_wood_slab_4, 7, "conquest:planks_limeweathered_slab");
		registerRenderMetas(itemblock_wood_slab_5, 0, "conquest:planks_cyanpainted_slab");
		registerRenderMetas(itemblock_wood_slab_5, 1, "conquest:planks_cyanweathered_slab");
		registerRenderMetas(itemblock_wood_slab_5, 2, "conquest:planks_bluepainted_slab");
		registerRenderMetas(itemblock_wood_slab_5, 3, "conquest:planks_darkblueweathered_slab");
		registerRenderMetas(itemblock_wood_slab_5, 4, "conquest:planks_blueweathered_slab");
		registerRenderMetas(itemblock_wood_slab_5, 5, "conquest:planks_lightbluepainted_slab");
		registerRenderMetas(itemblock_wood_slab_5, 6, "conquest:planks_lightblueweathered_slab");
		registerRenderMetas(itemblock_wood_slab_5, 7, "conquest:planks_purplepainted_slab");
		registerRenderMetas(itemblock_wood_slab_6, 0, "conquest:planks_brownpainted_slab");
		registerRenderMetas(itemblock_wood_slab_6, 1, "conquest:planks_brownweathered_slab");
		registerRenderMetas(itemblock_wood_slab_6, 2, "conquest:planks_oakplatform_slab");
		registerRenderMetas(itemblock_wood_slab_6, 3, "conquest:planks_spruceplatform_slab");
		registerRenderMetas(itemblock_wood_slab_6, 4, "conquest:planks_sprucevertical_slab");
		registerRenderMetas(itemblock_wood_slab_6, 5, "conquest:log_oaknormal_slab");
		registerRenderMetas(itemblock_wood_slab_6, 6, "conquest:log_sprucenormal_slab");
		registerRenderMetas(itemblock_wood_slab_6, 7, "conquest:log_birchnormal_slab");
		registerRenderMetas(itemblock_wood_slab_7, 0, "conquest:log_junglenormal_slab");
		registerRenderMetas(itemblock_wood_slab_7, 1, "conquest:log_acacianormal_slab");
		registerRenderMetas(itemblock_wood_slab_7, 2, "conquest:log_darkoaknormal_slab");
		registerRenderMetas(itemblock_wood_slab_7, 3, "conquest:basket_wicker_slab");
		registerRenderMetas(itemblock_wood_slab_7, 4, "conquest:wood_crate_slab");
		registerRenderMetas(itemblock_wood_slab_7, 5, "conquest:basket_wickerapple");
		registerRenderMetas(itemblock_wood_slab_7, 6, "conquest:basket_wickercocoa");
		registerRenderMetas(itemblock_wood_slab_7, 7, "conquest:none");
	//Wood Trapdoor Blocks
		registerRenderMetas(itemblock_wood_trapdoormodel_1, 0, "conquest:rooftile_thatch_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_1, 1, "conquest:rooftile_graythatch_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_2, 0, "conquest:wood_oaklog_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_2, 1, "conquest:wood_sprucelog_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_3, 0, "conquest:wood_birchlog_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_3, 1, "conquest:wood_junglelog_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_4, 0, "conquest:wood_acacialog_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_4, 1, "conquest:wood_darkoaklog_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_5, 0, "conquest:spruce_mossylog_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_5, 1, "conquest:birch_mossylog_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_6, 0, "conquest:jungle_mossylog_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_6, 1, "conquest:acacia_mossylog_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_7, 0, "conquest:oak_mossydark_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_7, 1, "conquest:oak_mossylog_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_8, 0, "conquest:ash_burntlog_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_8, 1, "conquest:log_ornamental_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_9, 0, "conquest:log_darkornamental_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_9, 1, "conquest:birch_log_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_10, 0, "conquest:darkbirch_log_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_10, 1, "conquest:log_rope_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_11, 0, "conquest:log_chain_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_11, 1, "conquest:wood_carvedoak_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_12, 0, "conquest:wood_carvedspruce_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_12, 1, "conquest:birch_carvedwood_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_13, 0, "conquest:jungle_carvedwood_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_13, 1, "conquest:acacia_carvedwood_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_14, 0, "conquest:darkoak_carvedwood_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_14, 1, "conquest:planks_whitepainted_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_15, 0, "conquest:planks_whiteweathered_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_15, 1, "conquest:planks_redpainted_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_16, 0, "conquest:planks_redweathered_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_16, 1, "conquest:planks_lightredpainted_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_17, 0, "conquest:planks_lightredweathered_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_17, 1, "conquest:planks_orangepainted_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_18, 0, "conquest:planks_yellowpainted_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_18, 1, "conquest:planks_yellowweathered_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_19, 0, "conquest:planks_greenpainted_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_19, 1, "conquest:planks_greenweathered_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_20, 0, "conquest:planks_limepainted_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_20, 1, "conquest:planks_limeweathered_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_21, 0, "conquest:planks_cyanpainted_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_21, 1, "conquest:planks_cyanweathered_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_22, 0, "conquest:planks_bluepainted_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_22, 1, "conquest:planks_darkblueweathered_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_23, 0, "conquest:planks_blueweathered_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_23, 1, "conquest:planks_lightbluepainted_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_24, 0, "conquest:planks_lightblueweathered_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_24, 1, "conquest:planks_purplepainted_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_25, 0, "conquest:planks_brownpainted_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_25, 1, "conquest:planks_brownweathered_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_26, 0, "conquest:planks_oakplatform_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_26, 1, "conquest:planks_spruceplatform_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_27, 0, "conquest:planks_sprucevertical_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_27, 1, "conquest:planks_redacacia_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_28, 0, "conquest:planks_driftwood_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_28, 1, "conquest:planks_mossy_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_29, 0, "conquest:planks_oaknormal_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_29, 1, "conquest:planks_sprucenormal_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_30, 0, "conquest:planks_birchnormal_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_30, 1, "conquest:planks_junglenormal_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_31, 0, "conquest:planks_acacianormal_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_31, 1, "conquest:planks_darkoaknormal_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_32, 0, "conquest:wood_crate_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_32, 1, "conquest:crate_clothcovered_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_33, 0, "conquest:shelves_wooden_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_33, 1, "conquest:rack_spices_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_34, 0, "conquest:rack_scrolls_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_34, 1, "conquest:cupboards_wooden_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_35, 0, "conquest:wardrobe_fancy_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_35, 1, "conquest:wardrobe_poor_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_36, 0, "conquest:bookshelf_normal_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_36, 1, "conquest:rack_wine_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_37, 0, "conquest:wood_panel_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_37, 1, "conquest:beam_spruce_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_38, 0, "conquest:beam_birch_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_38, 1, "conquest:beam_jungle_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_39, 0, "conquest:beam_acacia_trapdoor");
		registerRenderMetas(itemblock_wood_trapdoormodel_39, 1, "conquest:beam_darkoak_trapdoor");

	//Wood Vertical Slab Blocks
		registerRenderMetas(itemblock_wood_verticalslab_1, 0, "conquest:spruce_mossylog_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_1, 1, "conquest:birch_mossylog_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_1, 2, "conquest:jungle_mossylog_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_1, 3, "conquest:acacia_mossylog_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_2, 0, "conquest:oak_mossydark_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_2, 1, "conquest:oak_mossylog_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_2, 2, "conquest:ash_burntlog_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_2, 3, "conquest:log_ornamental_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_3, 0, "conquest:log_darkornamental_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_3, 1, "conquest:birch_log_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_3, 2, "conquest:darkbirch_log_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_3, 3, "conquest:log_rope_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_4, 0, "conquest:log_chain_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_4, 1, "conquest:wood_carvedoak_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_4, 2, "conquest:wood_carvedspruce_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_4, 3, "conquest:birch_carvedwood_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_5, 0, "conquest:jungle_carvedwood_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_5, 1, "conquest:acacia_carvedwood_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_5, 2, "conquest:darkoak_carvedwood_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_5, 3, "conquest:planks_whitepainted_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_6, 0, "conquest:planks_whiteweathered_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_6, 1, "conquest:planks_redpainted_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_6, 2, "conquest:planks_redweathered_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_6, 3, "conquest:planks_lightredpainted_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_7, 0, "conquest:planks_lightredweathered_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_7, 1, "conquest:planks_orangepainted_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_7, 2, "conquest:planks_yellowpainted_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_7, 3, "conquest:planks_yellowweathered_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_8, 0, "conquest:planks_greenpainted_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_8, 1, "conquest:planks_greenweathered_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_8, 2, "conquest:planks_limepainted_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_8, 3, "conquest:planks_limeweathered_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_9, 0, "conquest:planks_cyanpainted_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_9, 1, "conquest:planks_cyanweathered_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_9, 2, "conquest:planks_bluepainted_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_9, 3, "conquest:planks_darkblueweathered_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_10, 0, "conquest:planks_blueweathered_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_10, 1, "conquest:planks_lightbluepainted_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_10, 2, "conquest:planks_lightblueweathered_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_10, 3, "conquest:planks_purplepainted_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_11, 0, "conquest:planks_brownpainted_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_11, 1, "conquest:planks_brownweathered_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_11, 2, "conquest:planks_oakplatform_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_11, 3, "conquest:planks_spruceplatform_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_12, 0, "conquest:planks_sprucevertical_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_12, 1, "conquest:planks_redacacia_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_12, 2, "conquest:planks_driftwood_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_12, 3, "conquest:planks_mossy_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_13, 0, "conquest:planks_oaknormal_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_13, 1, "conquest:planks_sprucenormal_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_13, 2, "conquest:planks_birchnormal_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_13, 3, "conquest:planks_junglenormal_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_14, 0, "conquest:planks_acacianormal_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_14, 1, "conquest:planks_darkoaknormal_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_14, 2, "conquest:log_oaknormal_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_14, 3, "conquest:log_sprucenormal_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_15, 0, "conquest:log_birchnormal_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_15, 1, "conquest:log_junglenormal_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_15, 2, "conquest:log_acacianormal_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_15, 3, "conquest:log_darkoaknormal_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_16, 0, "conquest:wood_crate_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_16, 1, "conquest:crate_clothcovered_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_16, 2, "conquest:shelves_wooden_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_16, 3, "conquest:rack_spices_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_17, 0, "conquest:rack_scrolls_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_17, 1, "conquest:cupboards_wooden_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_17, 2, "conquest:wood_panel_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_17, 3, "conquest:wardrobe_fancy_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_18, 0, "conquest:bookshelf_normal_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_18, 1, "conquest:rack_wine_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_18, 2, "conquest:wardrobe_poor_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_18, 3, "conquest:beam_spruce_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_19, 0, "conquest:beam_birch_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_19, 1, "conquest:beam_jungle_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_19, 2, "conquest:beam_acacia_verticalslab");
		registerRenderMetas(itemblock_wood_verticalslab_19, 3, "conquest:beam_darkoak_verticalslab");
//Wood Corner Blocks
		registerRenderMetas(itemblock_wood_corner_1, 0, "conquest:spruce_mossylog_corner");
		registerRenderMetas(itemblock_wood_corner_1, 1, "conquest:birch_mossylog_corner");
		registerRenderMetas(itemblock_wood_corner_1, 2, "conquest:jungle_mossylog_corner");
		registerRenderMetas(itemblock_wood_corner_1, 3, "conquest:acacia_mossylog_corner");
		registerRenderMetas(itemblock_wood_corner_2, 0, "conquest:oak_mossydark_corner");
		registerRenderMetas(itemblock_wood_corner_2, 1, "conquest:oak_mossylog_corner");
		registerRenderMetas(itemblock_wood_corner_2, 2, "conquest:ash_burntlog_corner");
		registerRenderMetas(itemblock_wood_corner_2, 3, "conquest:log_ornamental_corner");
		registerRenderMetas(itemblock_wood_corner_3, 0, "conquest:log_darkornamental_corner");
		registerRenderMetas(itemblock_wood_corner_3, 1, "conquest:birch_log_corner");
		registerRenderMetas(itemblock_wood_corner_3, 2, "conquest:darkbirch_log_corner");
		registerRenderMetas(itemblock_wood_corner_3, 3, "conquest:log_rope_corner");
		registerRenderMetas(itemblock_wood_corner_4, 0, "conquest:log_chain_corner");
		registerRenderMetas(itemblock_wood_corner_4, 1, "conquest:wood_carvedoak_corner");
		registerRenderMetas(itemblock_wood_corner_4, 2, "conquest:wood_carvedspruce_corner");
		registerRenderMetas(itemblock_wood_corner_4, 3, "conquest:birch_carvedwood_corner");
		registerRenderMetas(itemblock_wood_corner_5, 0, "conquest:jungle_carvedwood_corner");
		registerRenderMetas(itemblock_wood_corner_5, 1, "conquest:acacia_carvedwood_corner");
		registerRenderMetas(itemblock_wood_corner_5, 2, "conquest:darkoak_carvedwood_corner");
		registerRenderMetas(itemblock_wood_corner_5, 3, "conquest:planks_whitepainted_corner");
		registerRenderMetas(itemblock_wood_corner_6, 0, "conquest:planks_whiteweathered_corner");
		registerRenderMetas(itemblock_wood_corner_6, 1, "conquest:planks_redpainted_corner");
		registerRenderMetas(itemblock_wood_corner_6, 2, "conquest:planks_redweathered_corner");
		registerRenderMetas(itemblock_wood_corner_6, 3, "conquest:planks_lightredpainted_corner");
		registerRenderMetas(itemblock_wood_corner_7, 0, "conquest:planks_lightredweathered_corner");
		registerRenderMetas(itemblock_wood_corner_7, 1, "conquest:planks_orangepainted_corner");
		registerRenderMetas(itemblock_wood_corner_7, 2, "conquest:planks_yellowpainted_corner");
		registerRenderMetas(itemblock_wood_corner_7, 3, "conquest:planks_yellowweathered_corner");
		registerRenderMetas(itemblock_wood_corner_8, 0, "conquest:planks_greenpainted_corner");
		registerRenderMetas(itemblock_wood_corner_8, 1, "conquest:planks_greenweathered_corner");
		registerRenderMetas(itemblock_wood_corner_8, 2, "conquest:planks_limepainted_corner");
		registerRenderMetas(itemblock_wood_corner_8, 3, "conquest:planks_limeweathered_corner");
		registerRenderMetas(itemblock_wood_corner_9, 0, "conquest:planks_cyanpainted_corner");
		registerRenderMetas(itemblock_wood_corner_9, 1, "conquest:planks_cyanweathered_corner");
		registerRenderMetas(itemblock_wood_corner_9, 2, "conquest:planks_bluepainted_corner");
		registerRenderMetas(itemblock_wood_corner_9, 3, "conquest:planks_darkblueweathered_corner");
		registerRenderMetas(itemblock_wood_corner_10, 0, "conquest:planks_blueweathered_corner");
		registerRenderMetas(itemblock_wood_corner_10, 1, "conquest:planks_lightbluepainted_corner");
		registerRenderMetas(itemblock_wood_corner_10, 2, "conquest:planks_lightblueweathered_corner");
		registerRenderMetas(itemblock_wood_corner_10, 3, "conquest:planks_purplepainted_corner");
		registerRenderMetas(itemblock_wood_corner_11, 0, "conquest:planks_brownpainted_corner");
		registerRenderMetas(itemblock_wood_corner_11, 1, "conquest:planks_brownweathered_corner");
		registerRenderMetas(itemblock_wood_corner_11, 2, "conquest:planks_oakplatform_corner");
		registerRenderMetas(itemblock_wood_corner_11, 3, "conquest:planks_spruceplatform_corner");
		registerRenderMetas(itemblock_wood_corner_12, 0, "conquest:planks_sprucevertical_corner");
		registerRenderMetas(itemblock_wood_corner_12, 1, "conquest:planks_redacacia_corner");
		registerRenderMetas(itemblock_wood_corner_12, 2, "conquest:planks_driftwood_corner");
		registerRenderMetas(itemblock_wood_corner_12, 3, "conquest:planks_mossy_corner");
		registerRenderMetas(itemblock_wood_corner_13, 0, "conquest:planks_oaknormal_corner");
		registerRenderMetas(itemblock_wood_corner_13, 1, "conquest:planks_sprucenormal_corner");
		registerRenderMetas(itemblock_wood_corner_13, 2, "conquest:planks_birchnormal_corner");
		registerRenderMetas(itemblock_wood_corner_13, 3, "conquest:planks_junglenormal_corner");
		registerRenderMetas(itemblock_wood_corner_14, 0, "conquest:planks_acacianormal_corner");
		registerRenderMetas(itemblock_wood_corner_14, 1, "conquest:planks_darkoaknormal_corner");
		registerRenderMetas(itemblock_wood_corner_14, 2, "conquest:log_oaknormal_corner");
		registerRenderMetas(itemblock_wood_corner_14, 3, "conquest:log_sprucenormal_corner");
		registerRenderMetas(itemblock_wood_corner_15, 0, "conquest:log_birchnormal_corner");
		registerRenderMetas(itemblock_wood_corner_15, 1, "conquest:log_junglenormal_corner");
		registerRenderMetas(itemblock_wood_corner_15, 2, "conquest:log_acacianormal_corner");
		registerRenderMetas(itemblock_wood_corner_15, 3, "conquest:log_darkoaknormal_corner");
		registerRenderMetas(itemblock_wood_corner_16, 0, "conquest:wood_crate_corner");
		registerRenderMetas(itemblock_wood_corner_16, 1, "conquest:crate_clothcovered_corner");
		registerRenderMetas(itemblock_wood_corner_16, 2, "conquest:shelves_wooden_corner");
		registerRenderMetas(itemblock_wood_corner_16, 3, "conquest:rack_spices_corner");
		registerRenderMetas(itemblock_wood_corner_17, 0, "conquest:rack_scrolls_corner");
		registerRenderMetas(itemblock_wood_corner_17, 1, "conquest:cupboards_wooden_corner");
		registerRenderMetas(itemblock_wood_corner_17, 2, "conquest:wood_panel_corner");
		registerRenderMetas(itemblock_wood_corner_17, 3, "conquest:none");
		registerRenderMetas(itemblock_wood_corner_18, 0, "conquest:bookshelf_normal_corner");
		registerRenderMetas(itemblock_wood_corner_18, 1, "conquest:rack_wine_corner");
		registerRenderMetas(itemblock_wood_corner_18, 2, "conquest:beam_spruce_corner");
		registerRenderMetas(itemblock_wood_corner_18, 3, "conquest:beam_birch_corner");
		registerRenderMetas(itemblock_wood_corner_19, 0, "conquest:beam_jungle_corner");
		registerRenderMetas(itemblock_wood_corner_19, 1, "conquest:beam_acacia_corner");
		registerRenderMetas(itemblock_wood_corner_19, 2, "conquest:beam_darkoak_corner");
		registerRenderMetas(itemblock_wood_corner_19, 3, "conquest:none");

	//Wood Ironbar Blocks
		registerRenderMetas(itemblock_wood_ironbar_1, 0, "conquest:wattlefence");
		registerRenderMetas(itemblock_wood_ironbar_1, 1, "conquest:woodfence");
		registerRenderMetas(itemblock_wood_ironbar_1, 2, "conquest:paintedwoodfence");
		registerRenderMetas(itemblock_wood_ironbar_1, 3, "conquest:woodframing");
		registerRenderMetas(itemblock_wood_ironbar_1, 4, "conquest:pigonaspit");
		registerRenderMetas(itemblock_wood_ironbar_1, 5, "conquest:hangingsausages");
		registerRenderMetas(itemblock_wood_ironbar_1, 6, "conquest:none");
		registerRenderMetas(itemblock_wood_ironbar_1, 7, "conquest:none");
		registerRenderMetas(itemblock_wood_ironbar_1, 8, "conquest:none");
		registerRenderMetas(itemblock_wood_ironbar_1, 9, "conquest:none");
		registerRenderMetas(itemblock_wood_ironbar_1, 10, "conquest:none");
		registerRenderMetas(itemblock_wood_ironbar_1, 11, "conquest:none");
		registerRenderMetas(itemblock_wood_ironbar_1, 12, "conquest:none");
		registerRenderMetas(itemblock_wood_ironbar_1, 13, "conquest:none");
		registerRenderMetas(itemblock_wood_ironbar_1, 14, "conquest:none");
		registerRenderMetas(itemblock_wood_ironbar_1, 15, "conquest:none");
	//Wood Carpet Blocks
		registerRenderMetas(itemblock_wood_carpet_1, 0, "conquest:spruce_carpet");
		registerRenderMetas(itemblock_wood_carpet_1, 1, "conquest:planks_spruceplatform_carpet");
		registerRenderMetas(itemblock_wood_carpet_1, 2, "conquest:planks_sprucevertical_carpet");
		registerRenderMetas(itemblock_wood_carpet_1, 3, "conquest:planks_redacacia_carpet");
		registerRenderMetas(itemblock_wood_carpet_1, 4, "conquest:planks_driftwood_carpet");
		registerRenderMetas(itemblock_wood_carpet_1, 5, "conquest:planks_mossy_carpet");
		registerRenderMetas(itemblock_wood_carpet_1, 6, "conquest:planks_oaknormal_carpet");
		registerRenderMetas(itemblock_wood_carpet_1, 7, "conquest:planks_birchnormal_carpet");
		registerRenderMetas(itemblock_wood_carpet_1, 8, "conquest:planks_junglenormal_carpet");
		registerRenderMetas(itemblock_wood_carpet_1, 9, "conquest:planks_acacianormal_carpet");
		registerRenderMetas(itemblock_wood_carpet_1, 10, "conquest:planks_darkoaknormal_carpet");
		registerRenderMetas(itemblock_wood_carpet_1, 11, "conquest:wood_crate_carpet");
		registerRenderMetas(itemblock_wood_carpet_1, 12, "conquest:none");
		registerRenderMetas(itemblock_wood_carpet_1, 13, "conquest:none");
		registerRenderMetas(itemblock_wood_carpet_1, 14, "conquest:none");
		registerRenderMetas(itemblock_wood_carpet_1, 15, "conquest:none");
	//No Collision Wood Blocks
		registerRenderMetas(itemblock_wood_nocollision_1, 0, "conquest:checkerboard");
		registerRenderMetas(itemblock_wood_nocollision_1, 1, "conquest:quillandparchment");
		registerRenderMetas(itemblock_wood_nocollision_1, 2, "conquest:playingcards");
		registerRenderMetas(itemblock_wood_nocollision_1, 3, "conquest:arrowbundle");
		registerRenderMetas(itemblock_wood_nocollision_1, 4, "conquest:none");
		registerRenderMetas(itemblock_wood_nocollision_1, 5, "conquest:none");
		registerRenderMetas(itemblock_wood_nocollision_1, 6, "conquest:none");
		registerRenderMetas(itemblock_wood_nocollision_1, 7, "conquest:none");
		registerRenderMetas(itemblock_wood_nocollision_1, 8, "conquest:none");
		registerRenderMetas(itemblock_wood_nocollision_1, 9, "conquest:none");
		registerRenderMetas(itemblock_wood_nocollision_1, 10, "conquest:none");
		registerRenderMetas(itemblock_wood_nocollision_1, 11, "conquest:none");
		registerRenderMetas(itemblock_wood_nocollision_1, 12, "conquest:none");
		registerRenderMetas(itemblock_wood_nocollision_1, 13, "conquest:none");
		registerRenderMetas(itemblock_wood_nocollision_1, 14, "conquest:none");
		registerRenderMetas(itemblock_wood_nocollision_1, 15, "conquest:none");
	//Directional No Collision Wood Blocks
		registerRenderMetas(itemblock_wood_directionalnocollision_1, 0, "conquest:bow");
		registerRenderMetas(itemblock_wood_directionalnocollision_1, 1, "conquest:arrow");
		registerRenderMetas(itemblock_wood_directionalnocollision_1, 2, "conquest:fishingrod");
		registerRenderMetas(itemblock_wood_directionalnocollision_1, 3, "conquest:bowsaw");
	//Directional Collision Trapdoor Wood Blocks
		registerRenderMetas(itemblock_wood_directionalcollisiontrapdoor_1, 0, "conquest:weaponrack_swords");
		registerRenderMetas(itemblock_wood_directionalcollisiontrapdoor_1, 1, "conquest:weaponrack_axes");
		registerRenderMetas(itemblock_wood_directionalcollisiontrapdoor_1, 2, "conquest:weaponrack_maces");
		registerRenderMetas(itemblock_wood_directionalcollisiontrapdoor_1, 3, "conquest:weaponrack_crossbow");
		registerRenderMetas(itemblock_wood_directionalcollisiontrapdoor_2, 0, "conquest:weaponrack_daggers");
		registerRenderMetas(itemblock_wood_directionalcollisiontrapdoor_2, 1, "conquest:weaponrack_shortswords");
		registerRenderMetas(itemblock_wood_directionalcollisiontrapdoor_2, 2, "conquest:weaponrack_pistol");
		registerRenderMetas(itemblock_wood_directionalcollisiontrapdoor_2, 3, "conquest:weaponrack_halberds");
		registerRenderMetas(itemblock_wood_directionalcollisiontrapdoor_3, 0, "conquest:weaponrack_muskets");
		registerRenderMetas(itemblock_wood_directionalcollisiontrapdoor_3, 1, "conquest:farmingtools_small");
		registerRenderMetas(itemblock_wood_directionalcollisiontrapdoor_3, 2, "conquest:brooms");
		registerRenderMetas(itemblock_wood_directionalcollisiontrapdoor_3, 3, "conquest:farmingtools");
	//Enchanted Wood Blocks
		registerRenderMetas(itemblock_wood_enchantedbook_1, 0, "conquest:enchantingtable");
		registerRenderMetas(itemblock_wood_enchantedbook_1, 1, "conquest:floatingbook");
		registerRenderMetas(itemblock_wood_enchantedbook_1, 2, "conquest:none");
		registerRenderMetas(itemblock_wood_enchantedbook_1, 3, "conquest:none");
		registerRenderMetas(itemblock_wood_enchantedbook_1, 4, "conquest:none");
		registerRenderMetas(itemblock_wood_enchantedbook_1, 5, "conquest:none");
		registerRenderMetas(itemblock_wood_enchantedbook_1, 6, "conquest:none");
		registerRenderMetas(itemblock_wood_enchantedbook_1, 7, "conquest:none");
		registerRenderMetas(itemblock_wood_enchantedbook_1, 8, "conquest:none");
		registerRenderMetas(itemblock_wood_enchantedbook_1, 9, "conquest:none");
		registerRenderMetas(itemblock_wood_enchantedbook_1, 10, "conquest:none");
		registerRenderMetas(itemblock_wood_enchantedbook_1, 11, "conquest:none");
		registerRenderMetas(itemblock_wood_enchantedbook_1, 12, "conquest:none");
		registerRenderMetas(itemblock_wood_enchantedbook_1, 13, "conquest:none");
		registerRenderMetas(itemblock_wood_enchantedbook_1, 14, "conquest:none");
		registerRenderMetas(itemblock_wood_enchantedbook_1, 15, "conquest:none");
	//Full Iron Blocks
		registerRenderMetas(itemblock_iron_full_1, 0, "conquest:iron_dark_full");
		registerRenderMetas(itemblock_iron_full_1, 1, "conquest:iron_2_full");
		registerRenderMetas(itemblock_iron_full_1, 2, "conquest:iron_2_full");
		registerRenderMetas(itemblock_iron_full_1, 3, "conquest:iron_rusty_full");
		registerRenderMetas(itemblock_iron_full_1, 4, "conquest:none");
		registerRenderMetas(itemblock_iron_full_1, 5, "conquest:none");
		registerRenderMetas(itemblock_iron_full_1, 6, "conquest:none");
		registerRenderMetas(itemblock_iron_full_1, 7, "conquest:none");
		registerRenderMetas(itemblock_iron_full_1, 8, "conquest:none");
		registerRenderMetas(itemblock_iron_full_1, 9, "conquest:none");
		registerRenderMetas(itemblock_iron_full_1, 10, "conquest:none");
		registerRenderMetas(itemblock_iron_full_1, 11, "conquest:none");
		registerRenderMetas(itemblock_iron_full_1, 12, "conquest:none");
		registerRenderMetas(itemblock_iron_full_1, 13, "conquest:none");
		registerRenderMetas(itemblock_iron_full_1, 14, "conquest:none");
		registerRenderMetas(itemblock_iron_full_1, 15, "conquest:none");
	//Full Partial Light 1.0 Iron Blocks
		registerRenderMetas(itemblock_iron_fullpartiallight10_1, 0, "conquest:hanging_brazier");
		registerRenderMetas(itemblock_iron_fullpartiallight10_1, 1, "conquest:lantern_small");
		registerRenderMetas(itemblock_iron_fullpartiallight10_1, 2, "conquest:lantern_big_beacon");
		registerRenderMetas(itemblock_iron_fullpartiallight10_1, 3, "conquest:greenscreen");
		registerRenderMetas(itemblock_iron_fullpartiallight10_1, 4, "conquest:none");
		registerRenderMetas(itemblock_iron_fullpartiallight10_1, 5, "conquest:none");
		registerRenderMetas(itemblock_iron_fullpartiallight10_1, 6, "conquest:none");
		registerRenderMetas(itemblock_iron_fullpartiallight10_1, 7, "conquest:none");
		registerRenderMetas(itemblock_iron_fullpartiallight10_1, 8, "conquest:none");
		registerRenderMetas(itemblock_iron_fullpartiallight10_1, 9, "conquest:none");
		registerRenderMetas(itemblock_iron_fullpartiallight10_1, 10, "conquest:none");
		registerRenderMetas(itemblock_iron_fullpartiallight10_1, 11, "conquest:none");
		registerRenderMetas(itemblock_iron_fullpartiallight10_1, 12, "conquest:none");
		registerRenderMetas(itemblock_iron_fullpartiallight10_1, 13, "conquest:none");
		registerRenderMetas(itemblock_iron_fullpartiallight10_1, 14, "conquest:none");
		registerRenderMetas(itemblock_iron_fullpartiallight10_1, 15, "conquest:none");
	//Iron Anvil Blocks
		registerRenderMetas(itemblock_iron_anvil_1, 0, "conquest:anvil");
		registerRenderMetas(itemblock_iron_anvil_1, 1, "conquest:none");
		registerRenderMetas(itemblock_iron_anvil_1, 2, "conquest:none");
		registerRenderMetas(itemblock_iron_anvil_1, 3, "conquest:none");
	//Iron Ironbar Blocks
		registerRenderMetas(itemblock_iron_ironbar_1, 0, "conquest:bronzelattice_ironbar");
		registerRenderMetas(itemblock_iron_ironbar_1, 1, "conquest:darkironfence_ironbar");
		registerRenderMetas(itemblock_iron_ironbar_1, 2, "conquest:rustedironfence_ironbar");
		registerRenderMetas(itemblock_iron_ironbar_1, 3, "conquest:ironfence_ironbar");
		registerRenderMetas(itemblock_iron_ironbar_1, 4, "conquest:cellbars");
		registerRenderMetas(itemblock_iron_ironbar_1, 5, "conquest:irongrille");
		registerRenderMetas(itemblock_iron_ironbar_1, 6, "conquest:weathervane");
		registerRenderMetas(itemblock_iron_ironbar_1, 7, "conquest:straightsaw");
		registerRenderMetas(itemblock_iron_ironbar_1, 8, "conquest:none");
		registerRenderMetas(itemblock_iron_ironbar_1, 9, "conquest:none");
		registerRenderMetas(itemblock_iron_ironbar_1, 10, "conquest:none");
		registerRenderMetas(itemblock_iron_ironbar_1, 11, "conquest:none");
		registerRenderMetas(itemblock_iron_ironbar_1, 12, "conquest:none");
		registerRenderMetas(itemblock_iron_ironbar_1, 13, "conquest:none");
		registerRenderMetas(itemblock_iron_ironbar_1, 14, "conquest:none");
		registerRenderMetas(itemblock_iron_ironbar_1, 15, "conquest:none");
	//Stairs Iron Blocks
		registerRenderMetas(itemblock_iron_stairs_1, 0, "conquest:iron_dark_stairs");
		registerRenderMetas(itemblock_iron_stairs_1, 1, "conquest:iron_cannon_stairs");
		registerRenderMetas(itemblock_iron_stairs_2, 0, "conquest:iron_stairs");
		registerRenderMetas(itemblock_iron_stairs_2, 1, "conquest:none");
	//Iron Trapdoor Blocks
		registerRenderMetas(itemblock_iron_trapdoormodel_1, 0, "conquest:metalgrate");
		registerRenderMetas(itemblock_iron_trapdoormodel_1, 1, "conquest:cagebars");
		registerRenderMetas(itemblock_iron_trapdoormodel_2, 0, "conquest:circularsaw");
		registerRenderMetas(itemblock_iron_trapdoormodel_2, 1, "conquest:none");
	//Iron Directional No Collision Blocks
		registerRenderMetas(itemblock_iron_directionalnocollision_1, 0, "conquest:axe");
		registerRenderMetas(itemblock_iron_directionalnocollision_1, 1, "conquest:sword");
		registerRenderMetas(itemblock_iron_directionalnocollision_1, 2, "conquest:shovel");
		registerRenderMetas(itemblock_iron_directionalnocollision_1, 3, "conquest:hammer");
		registerRenderMetas(itemblock_iron_directionalnocollision_2, 0, "conquest:pickaxe");
		registerRenderMetas(itemblock_iron_directionalnocollision_2, 1, "conquest:sickle");
		registerRenderMetas(itemblock_iron_directionalnocollision_2, 2, "conquest:none");
		registerRenderMetas(itemblock_iron_directionalnocollision_2, 3, "conquest:none");
	//Plants Log Blocks
		registerRenderMetas(itemblock_plants_log_1, 0, "conquest:rooftile_thatch");
		registerRenderMetas(itemblock_plants_log_1, 1, "conquest:rooftile_graythatch");
		registerRenderMetas(itemblock_plants_log_1, 2, "conquest:rooftile_wovengraythatch");
		registerRenderMetas(itemblock_plants_log_1, 3, "conquest:none");
	//Stairs Plants Blocks
		registerRenderMetas(itemblock_plants_stairs_1, 0, "conquest:rooftile_thatch_stairs");
		registerRenderMetas(itemblock_plants_stairs_1, 1, "conquest:rooftile_graythatch_stairs");
	//Slab Plants Blocks
		registerRenderMetas(itemblock_plants_slab_1, 0, "conquest:rooftile_thatch_slab");
		registerRenderMetas(itemblock_plants_slab_1, 1, "conquest:rooftile_graythatch_slab");
		registerRenderMetas(itemblock_plants_slab_1, 2, "conquest:none");
		registerRenderMetas(itemblock_plants_slab_1, 3, "conquest:none");
		registerRenderMetas(itemblock_plants_slab_1, 4, "conquest:none");
		registerRenderMetas(itemblock_plants_slab_1, 5, "conquest:none");
		registerRenderMetas(itemblock_plants_slab_1, 6, "conquest:none");
		registerRenderMetas(itemblock_plants_slab_1, 7, "conquest:none");
	//No Collision Plants Blocks
		registerRenderMetas(itemblock_plants_nocollision_1, 0, "conquest:plants_sapling_oak");
		registerRenderMetas(itemblock_plants_nocollision_1, 1, "conquest:plants_sapling_birch");
		registerRenderMetas(itemblock_plants_nocollision_1, 2, "conquest:plants_sapling_jungle");
		registerRenderMetas(itemblock_plants_nocollision_1, 3, "conquest:plants_sapling_acacia");
		registerRenderMetas(itemblock_plants_nocollision_1, 4, "conquest:plants_sapling_spruce");
		registerRenderMetas(itemblock_plants_nocollision_1, 5, "conquest:plants_sapling_darkoak");
		registerRenderMetas(itemblock_plants_nocollision_1, 6, "conquest:plants_smalltree");
		registerRenderMetas(itemblock_plants_nocollision_1, 7, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollision_1, 8, "conquest:plants_bush_beautyberry");
		registerRenderMetas(itemblock_plants_nocollision_1, 9, "conquest:plants_bush_raspberry");
		registerRenderMetas(itemblock_plants_nocollision_1, 10, "conquest:plants_bush_blackberry");
		registerRenderMetas(itemblock_plants_nocollision_1, 11, "conquest:plants_ivy_hanging");
		registerRenderMetas(itemblock_plants_nocollision_1, 12, "conquest:plants_moss_hanging");
		registerRenderMetas(itemblock_plants_nocollision_1, 13, "conquest:plants_wheat_mature");
		registerRenderMetas(itemblock_plants_nocollision_1, 14, "conquest:plants_wheat_small");
		registerRenderMetas(itemblock_plants_nocollision_1, 15, "conquest:plants_wheat");
		registerRenderMetas(itemblock_plants_nocollision_2, 0, "conquest:plants_corn");
		registerRenderMetas(itemblock_plants_nocollision_2, 1, "conquest:plants_beans");
		registerRenderMetas(itemblock_plants_nocollision_2, 2, "conquest:plants_bamboo");
		registerRenderMetas(itemblock_plants_nocollision_2, 3, "conquest:plants_darkbamboo");
		registerRenderMetas(itemblock_plants_nocollision_2, 4, "conquest:plants_deadbushes");
		registerRenderMetas(itemblock_plants_nocollision_2, 5, "conquest:plants_tallcowparsley");
		registerRenderMetas(itemblock_plants_nocollision_2, 6, "conquest:plants_lilyofthevalley");
		registerRenderMetas(itemblock_plants_nocollision_2, 7, "conquest:plants_euphorbiaesula");
		registerRenderMetas(itemblock_plants_nocollision_2, 8, "conquest:plants_pulsatillavulgaris");
		registerRenderMetas(itemblock_plants_nocollision_2, 9, "conquest:plants_knabenkraut");
		registerRenderMetas(itemblock_plants_nocollision_2, 10, "conquest:plants_cicerbitaalpinae");
		registerRenderMetas(itemblock_plants_nocollision_2, 11, "conquest:plants_cicerbitaalpinae");
		registerRenderMetas(itemblock_plants_nocollision_2, 12, "conquest:plants_nettles");
		registerRenderMetas(itemblock_plants_nocollision_2, 13, "conquest:plants_heather");
		registerRenderMetas(itemblock_plants_nocollision_2, 14, "conquest:plants_cottongrass");
		registerRenderMetas(itemblock_plants_nocollision_2, 15, "conquest:plants_hemp");
		registerRenderMetas(itemblock_plants_nocollision_3, 0, "conquest:plants_cattails");
		registerRenderMetas(itemblock_plants_nocollision_3, 1, "conquest:plants_hangingflowers");
		registerRenderMetas(itemblock_plants_nocollision_3, 2, "conquest:plants_hangingflowers2");
		registerRenderMetas(itemblock_plants_nocollision_3, 3, "conquest:plants_brownmushrooms");
		registerRenderMetas(itemblock_plants_nocollision_3, 4, "conquest:plants_redmushrooms");
		registerRenderMetas(itemblock_plants_nocollision_3, 5, "conquest:plants_mossonground");
		registerRenderMetas(itemblock_plants_nocollision_3, 6, "conquest:plants_birdnest");
		registerRenderMetas(itemblock_plants_nocollision_3, 7, "conquest:plants_birdnest_small");
		registerRenderMetas(itemblock_plants_nocollision_3, 8, "conquest:plants_cowpatty");
		registerRenderMetas(itemblock_plants_nocollision_3, 9, "conquest:plants_apple");
		registerRenderMetas(itemblock_plants_nocollision_3, 10, "conquest:food_hangingrabbit_brown");
		registerRenderMetas(itemblock_plants_nocollision_3, 11, "conquest:food_hangingrabbbit_white");
		registerRenderMetas(itemblock_plants_nocollision_3, 12, "conquest:food_hangingrabbit");
		registerRenderMetas(itemblock_plants_nocollision_3, 13, "conquest:food_beefcut");
		registerRenderMetas(itemblock_plants_nocollision_3, 14, "conquest:food_bigsausages");
		registerRenderMetas(itemblock_plants_nocollision_3, 15, "conquest:food_smallsausages");
		registerRenderMetas(itemblock_plants_nocollision_4, 0, "conquest:food_hangingfish");
		registerRenderMetas(itemblock_plants_nocollision_4, 1, "conquest:food_hangingswordfish");
		registerRenderMetas(itemblock_plants_nocollision_4, 2, "conquest:food_hangingexoticfish");
		registerRenderMetas(itemblock_plants_nocollision_4, 3, "conquest:food_hangingclownfish");
		registerRenderMetas(itemblock_plants_nocollision_4, 4, "conquest:food_hangingsardines");
		registerRenderMetas(itemblock_plants_nocollision_4, 5, "conquest:food_carrotbundle");
		registerRenderMetas(itemblock_plants_nocollision_4, 6, "conquest:food_onionbundle");
		registerRenderMetas(itemblock_plants_nocollision_4, 7, "conquest:food_bananabundle");
		registerRenderMetas(itemblock_plants_nocollision_4, 8, "conquest:food_herbs");
		registerRenderMetas(itemblock_plants_nocollision_4, 9, "conquest:food_cypress");
		registerRenderMetas(itemblock_plants_nocollision_4, 10, "conquest:plants_autumnreeds");
		registerRenderMetas(itemblock_plants_nocollision_4, 11, "conquest:plants_bogasphode");
		registerRenderMetas(itemblock_plants_nocollision_4, 12, "conquest:plants_cowwheat");
		registerRenderMetas(itemblock_plants_nocollision_4, 13, "conquest:plants_buttercup");
		registerRenderMetas(itemblock_plants_nocollision_4, 14, "conquest:plants_bilberry");
		registerRenderMetas(itemblock_plants_nocollision_4, 15, "conquest:plants_rushes");
		registerRenderMetas(itemblock_plants_nocollision_5, 0, "conquest:plants_fireweed");
		registerRenderMetas(itemblock_plants_nocollision_5, 1, "conquest:plants_tumbleweed");
		registerRenderMetas(itemblock_plants_nocollision_5, 2, "conquest:plants_summerreeds");
		registerRenderMetas(itemblock_plants_nocollision_5, 3, "conquest:plants_treefrozen");
		registerRenderMetas(itemblock_plants_nocollision_5, 4, "conquest:dragonflies");
		registerRenderMetas(itemblock_plants_nocollision_5, 5, "conquest:plants_whiteclematis");
		registerRenderMetas(itemblock_plants_nocollision_5, 6, "conquest:plants_hangingroots");
		registerRenderMetas(itemblock_plants_nocollision_5, 7, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollision_5, 8, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollision_5, 9, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollision_5, 10, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollision_5, 11, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollision_5, 12, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollision_5, 13, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollision_5, 14, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollision_5, 15, "conquest:none");

		//No Collision Biome Plants Blocks
		registerRenderMetas(itemblock_plants_nocollisionbiome_1, 0, "conquest:plants_junglefern");
		registerRenderMetas(itemblock_plants_nocollisionbiome_1, 1, "conquest:plants_desertshrub");
		registerRenderMetas(itemblock_plants_nocollisionbiome_1, 2, "conquest:plants_thickfern");
		registerRenderMetas(itemblock_plants_nocollisionbiome_1, 3, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollisionbiome_1, 4, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollisionbiome_1, 5, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollisionbiome_1, 6, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollisionbiome_1, 7, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollisionbiome_1, 8, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollisionbiome_1, 9, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollisionbiome_1, 10, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollisionbiome_1, 11, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollisionbiome_1, 12, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollisionbiome_1, 13, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollisionbiome_1, 14, "conquest:none");
		registerRenderMetas(itemblock_plants_nocollisionbiome_1, 15, "conquest:none");

		//Full Leaves Blocks
		registerRenderMetas(itemblock_leaves_full_1, 0, "conquest:leaves_cherryblossoms");
		registerRenderMetas(itemblock_leaves_full_1, 1, "conquest:leaves_appletree");
		registerRenderMetas(itemblock_leaves_full_1, 2, "conquest:leaves_hollyberrybush");
		registerRenderMetas(itemblock_leaves_full_1, 3, "conquest:leaves_citrus");
		registerRenderMetas(itemblock_leaves_full_1, 4, "conquest:leaves_peartree");
		registerRenderMetas(itemblock_leaves_full_1, 5, "conquest:leaves_blueberrybush");
		registerRenderMetas(itemblock_leaves_full_1, 6, "conquest:leaves_grapevines");
		registerRenderMetas(itemblock_leaves_full_1, 7, "conquest:leaves_oak_extremehills");
		registerRenderMetas(itemblock_leaves_full_1, 8, "conquest:leaves_oak_frozenocean");
		registerRenderMetas(itemblock_leaves_full_1, 9, "conquest:leaves_darkoak_frozenocean");
		registerRenderMetas(itemblock_leaves_full_1, 10, "conquest:leaves_jungle_taiga");
		registerRenderMetas(itemblock_leaves_full_1, 11, "conquest:leaves_jungle_frozenocean");
		registerRenderMetas(itemblock_leaves_full_1, 12, "conquest:leaves_birch_frozenocean");
		registerRenderMetas(itemblock_leaves_full_1, 13, "conquest:leaves_birch_extremehills");
		registerRenderMetas(itemblock_leaves_full_1, 14, "conquest:none");
		registerRenderMetas(itemblock_leaves_full_1, 15, "conquest:none");

		//Full Sand Blocks
		registerRenderMetas(itemblock_sand_full_1, 0, "conquest:sand_grass_full");
		registerRenderMetas(itemblock_sand_full_1, 1, "conquest:sand_gravel_full");
		registerRenderMetas(itemblock_sand_full_1, 2, "conquest:sand_veg_full");
		registerRenderMetas(itemblock_sand_full_1, 3, "conquest:wetsand_full");
		registerRenderMetas(itemblock_sand_full_1, 4, "conquest:wetsand_gravel_full");
		registerRenderMetas(itemblock_sand_full_1, 5, "conquest:wetsand_veg_full");
		registerRenderMetas(itemblock_sand_full_1, 6, "conquest:redsand_gravel_full");
		registerRenderMetas(itemblock_sand_full_1, 7, "conquest:redsand_veg_full");
		registerRenderMetas(itemblock_sand_full_1, 8, "conquest:none");
		registerRenderMetas(itemblock_sand_full_1, 9, "conquest:none");
		registerRenderMetas(itemblock_sand_full_1, 10, "conquest:none");
		registerRenderMetas(itemblock_sand_full_1, 11, "conquest:none");
		registerRenderMetas(itemblock_sand_full_1, 12, "conquest:none");
		registerRenderMetas(itemblock_sand_full_1, 13, "conquest:none");
		registerRenderMetas(itemblock_sand_full_1, 14, "conquest:none");
		registerRenderMetas(itemblock_sand_full_1, 15, "conquest:none");

	//Sand Layer Blocks
		registerRenderLayerMeta(itemblock_sand_layer_1, 0, "conquest:sand_snowlayer");
		registerRenderLayerMeta(itemblock_sand_layer_1, 1, "conquest:wetsand_snowlayer");
		registerRenderLayerMeta(itemblock_sand_layer_2, 0, "conquest:redsand_snowlayer");
		registerRenderLayerMeta(itemblock_sand_layer_2, 1, "conquest:ash_snowlayer");
		registerRenderLayerMeta(itemblock_sand_layer_3, 0, "conquest:sand_grass_snowlayer");
		registerRenderLayerMeta(itemblock_sand_layer_3, 1, "conquest:sand_gravel_snowlayer");
		registerRenderLayerMeta(itemblock_sand_layer_4, 0, "conquest:sand_veg_snowlayer");
		registerRenderLayerMeta(itemblock_sand_layer_4, 1, "conquest:sand_wetgravel_snowlayer");
		registerRenderLayerMeta(itemblock_sand_layer_5, 0, "conquest:sand_wetveg_snowlayer");
		registerRenderLayerMeta(itemblock_sand_layer_5, 1, "conquest:sand_redgravel_snowlayer");
		registerRenderLayerMeta(itemblock_sand_layer_6, 0, "conquest:sand_redveg_snowlayer");
		registerRenderLayerMeta(itemblock_sand_layer_6, 1, "conquest:none");


		//Full Ground Blocks
		registerRenderMetas(itemblock_ground_full_1, 0, "conquest:dirt_full");
		registerRenderMetas(itemblock_ground_full_1, 1, "conquest:dirt_bones_full");
		registerRenderMetas(itemblock_ground_full_1, 2, "conquest:dirt_frozen_full");
		registerRenderMetas(itemblock_ground_full_1, 3, "conquest:dirt_gravel_full");
		registerRenderMetas(itemblock_ground_full_1, 4, "conquest:dirt_mossy_full");
		registerRenderMetas(itemblock_ground_full_1, 5, "conquest:dirt_light_full");
		registerRenderMetas(itemblock_ground_full_1, 6, "conquest:dirt_lightpath_full");
		registerRenderMetas(itemblock_ground_full_1, 7, "conquest:dirt_darkpath_full");
		registerRenderMetas(itemblock_ground_full_1, 8, "conquest:dirt_burnt_full");
		registerRenderMetas(itemblock_ground_full_1, 9, "conquest:ants_full");
		registerRenderMetas(itemblock_ground_full_1, 10, "conquest:forestfloor_taiga_full");
		registerRenderMetas(itemblock_ground_full_1, 11, "conquest:forestfloor_zautum_full");
		registerRenderMetas(itemblock_ground_full_1, 12, "conquest:podzol_overgrown_full");
		registerRenderMetas(itemblock_ground_full_1, 13, "conquest:podzol_old_full");
		registerRenderMetas(itemblock_ground_full_1, 14, "conquest:smallstones_full");
		registerRenderMetas(itemblock_ground_full_1, 15, "conquest:gravel_grassy_full");
		registerRenderMetas(itemblock_ground_full_2, 0, "conquest:smallstones_grassy_full");
		registerRenderMetas(itemblock_ground_full_2, 1, "conquest:gravel_white_full");
		registerRenderMetas(itemblock_ground_full_2, 2, "conquest:none");
		registerRenderMetas(itemblock_ground_full_2, 3, "conquest:none");
		registerRenderMetas(itemblock_ground_full_2, 4, "conquest:none");
		registerRenderMetas(itemblock_ground_full_2, 5, "conquest:none");
		registerRenderMetas(itemblock_ground_full_2, 6, "conquest:none");
		registerRenderMetas(itemblock_ground_full_2, 7, "conquest:none");
		registerRenderMetas(itemblock_ground_full_2, 8, "conquest:none");
		registerRenderMetas(itemblock_ground_full_2, 9, "conquest:none");
		registerRenderMetas(itemblock_ground_full_2, 10, "conquest:none");
		registerRenderMetas(itemblock_ground_full_2, 11, "conquest:none");
		registerRenderMetas(itemblock_ground_full_2, 12, "conquest:none");
		registerRenderMetas(itemblock_ground_full_2, 13, "conquest:none");
		registerRenderMetas(itemblock_ground_full_2, 14, "conquest:none");
		registerRenderMetas(itemblock_ground_full_2, 15, "conquest:none");
	//Soulsand Ground Blocks
		registerRenderMetas(itemblock_ground_soulsand_1, 0, "conquest:mud");
		registerRenderMetas(itemblock_ground_soulsand_1, 1, "conquest:dirt_muddy");
		registerRenderMetas(itemblock_ground_soulsand_1, 2, "conquest:none");
		registerRenderMetas(itemblock_ground_soulsand_1, 3, "conquest:none");
		registerRenderMetas(itemblock_ground_soulsand_1, 4, "conquest:none");
		registerRenderMetas(itemblock_ground_soulsand_1, 5, "conquest:none");
		registerRenderMetas(itemblock_ground_soulsand_1, 6, "conquest:none");
		registerRenderMetas(itemblock_ground_soulsand_1, 7, "conquest:none");
		registerRenderMetas(itemblock_ground_soulsand_1, 8, "conquest:none");
		registerRenderMetas(itemblock_ground_soulsand_1, 9, "conquest:none");
		registerRenderMetas(itemblock_ground_soulsand_1, 10, "conquest:none");
		registerRenderMetas(itemblock_ground_soulsand_1, 11, "conquest:none");
		registerRenderMetas(itemblock_ground_soulsand_1, 12, "conquest:none");
		registerRenderMetas(itemblock_ground_soulsand_1, 13, "conquest:none");
		registerRenderMetas(itemblock_ground_soulsand_1, 14, "conquest:none");
		registerRenderMetas(itemblock_ground_soulsand_1, 15, "conquest:none");
	//Directional Full Partial Ground Blocks
		registerRenderMetas(itemblock_ground_directionalfullpartial_1, 0, "conquest:farmland");
		registerRenderMetas(itemblock_ground_directionalfullpartial_1, 1, "conquest:farmland_diagonal");
		registerRenderMetas(itemblock_ground_directionalfullpartial_1, 2, "conquest:pinata");
		registerRenderMetas(itemblock_ground_directionalfullpartial_1, 3, "conquest:none");
	//Ground Layer Blocks
		registerRenderLayerMeta(itemblock_ground_layer_1, 0, "conquest:dirt_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_1, 1, "conquest:dirt_gravel_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_2, 0, "conquest:gravel_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_2, 1, "conquest:smallstones_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_3, 0, "conquest:dirt_bones_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_3, 1, "conquest:dirt_frozen_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_4, 0, "conquest:dirt_mossy_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_4, 1, "conquest:dirt_light_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_5, 0, "conquest:dirt_lightpath_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_5, 1, "conquest:dirt_darkpath_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_6, 0, "conquest:dirt_burnt_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_6, 1, "conquest:dirt_muddy_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_7, 0, "conquest:mud_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_7, 1, "conquest:ants_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_8, 0, "conquest:forestfloor_taiga_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_8, 1, "conquest:zautum_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_9, 0, "conquest:podzol_overgrown_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_9, 1, "conquest:podzol_old_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_10, 0, "conquest:gravel_grassy_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_10, 1, "conquest:smallstones_grassy_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_11, 0, "conquest:gravel_white_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_11, 1, "conquest:soulsand_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_12, 0, "conquest:clay_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_12, 1, "conquest:podzol_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_13, 0, "conquest:dirt_coarse_snowlayer");
		registerRenderLayerMeta(itemblock_ground_layer_13, 1, "conquest:mycelium_snowlayer");
	//Translucent Ice Blocks
		registerRenderMetas(itemblock_ice_translucent_1, 0, "conquest:brick_icetranslucent");
		registerRenderMetas(itemblock_ice_translucent_1, 1, "conquest:crystal_red");
		registerRenderMetas(itemblock_ice_translucent_1, 2, "conquest:crystal_blue");
		registerRenderMetas(itemblock_ice_translucent_1, 3, "conquest:crystal_green");
		registerRenderMetas(itemblock_ice_translucent_1, 4, "conquest:crystal_purple");
		registerRenderMetas(itemblock_ice_translucent_1, 5, "conquest:crystal_lightgreen");
		registerRenderMetas(itemblock_ice_translucent_1, 6, "conquest:ice_crystallized");
		registerRenderMetas(itemblock_ice_translucent_1, 7, "conquest:none");
		registerRenderMetas(itemblock_ice_translucent_1, 8, "conquest:none");
		registerRenderMetas(itemblock_ice_translucent_1, 9, "conquest:none");
		registerRenderMetas(itemblock_ice_translucent_1, 10, "conquest:none");
		registerRenderMetas(itemblock_ice_translucent_1, 11, "conquest:none");
		registerRenderMetas(itemblock_ice_translucent_1, 12, "conquest:none");
		registerRenderMetas(itemblock_ice_translucent_1, 13, "conquest:none");
		registerRenderMetas(itemblock_ice_translucent_1, 14, "conquest:none");
		registerRenderMetas(itemblock_ice_translucent_1, 15, "conquest:none");

	//Slab Ice Blocks
		registerRenderMetas(itemblock_ice_slab_1, 0, "conquest:ice_packed_slab");
		registerRenderMetas(itemblock_ice_slab_1, 1, "conquest:none");
		registerRenderMetas(itemblock_ice_slab_1, 2, "conquest:none");
		registerRenderMetas(itemblock_ice_slab_1, 3, "conquest:none");
		registerRenderMetas(itemblock_ice_slab_1, 4, "conquest:none");
		registerRenderMetas(itemblock_ice_slab_1, 5, "conquest:none");
		registerRenderMetas(itemblock_ice_slab_1, 6, "conquest:none");
		registerRenderMetas(itemblock_ice_slab_1, 7, "conquest:none");

	//No Collision Ice Blocks
		registerRenderMetas(itemblock_ice_nocollision_1, 0, "conquest:icicles");
		registerRenderMetas(itemblock_ice_nocollision_1, 1, "conquest:brokenbottle");
		registerRenderMetas(itemblock_ice_nocollision_1, 2, "conquest:bottle");
		registerRenderMetas(itemblock_ice_nocollision_1, 3, "conquest:potionglass");
		registerRenderMetas(itemblock_ice_nocollision_1, 4, "conquest:goblet");
		registerRenderMetas(itemblock_ice_nocollision_1, 5, "conquest:none");
		registerRenderMetas(itemblock_ice_nocollision_1, 6, "conquest:none");
		registerRenderMetas(itemblock_ice_nocollision_1, 7, "conquest:none");
		registerRenderMetas(itemblock_ice_nocollision_1, 8, "conquest:none");
		registerRenderMetas(itemblock_ice_nocollision_1, 9, "conquest:none");
		registerRenderMetas(itemblock_ice_nocollision_1, 10, "conquest:none");
		registerRenderMetas(itemblock_ice_nocollision_1, 11, "conquest:none");
		registerRenderMetas(itemblock_ice_nocollision_1, 12, "conquest:none");
		registerRenderMetas(itemblock_ice_nocollision_1, 13, "conquest:none");
		registerRenderMetas(itemblock_ice_nocollision_1, 14, "conquest:none");
		registerRenderMetas(itemblock_ice_nocollision_1, 15, "conquest:none");
	//Stairs Snow Blocks
		registerRenderMetas(itemblock_snow_stairs_1, 0, "conquest:snow_stairs");
		registerRenderMetas(itemblock_snow_stairs_1, 1, "conquest:none");

	//Slab Cloth Blocks
		registerRenderMetas(itemblock_cloth_slab_1, 0, "conquest:wool_slab");
		registerRenderMetas(itemblock_cloth_slab_1, 1, "conquest:wool_orange_slab");
		registerRenderMetas(itemblock_cloth_slab_1, 2, "conquest:wool_magenta_slab");
		registerRenderMetas(itemblock_cloth_slab_1, 3, "conquest:wool_lightblue_slab");
		registerRenderMetas(itemblock_cloth_slab_1, 4, "conquest:wool_yellow_slab");
		registerRenderMetas(itemblock_cloth_slab_1, 5, "conquest:wool_lime_slab");
		registerRenderMetas(itemblock_cloth_slab_1, 6, "conquest:wool_pink_slab");
		registerRenderMetas(itemblock_cloth_slab_1, 7, "conquest:wool_gray_slab");
		registerRenderMetas(itemblock_cloth_slab_2, 0, "conquest:wool_lightgray_slab");
		registerRenderMetas(itemblock_cloth_slab_2, 1, "conquest:wool_cyan_slab");
		registerRenderMetas(itemblock_cloth_slab_2, 2, "conquest:wool_purple_slab");
		registerRenderMetas(itemblock_cloth_slab_2, 3, "conquest:wool_blue_slab");
		registerRenderMetas(itemblock_cloth_slab_2, 4, "conquest:wool_brown_slab");
		registerRenderMetas(itemblock_cloth_slab_2, 5, "conquest:wool_green_slab");
		registerRenderMetas(itemblock_cloth_slab_2, 6, "conquest:wool_red_slab");
		registerRenderMetas(itemblock_cloth_slab_2, 7, "conquest:wool_black_slab");
	//Pane Cloth Blocks
		registerRenderMetas(itemblock_cloth_pane_1, 0, "conquest:rope_wall");
		registerRenderMetas(itemblock_cloth_pane_1, 1, "conquest:fishingnet");
		registerRenderMetas(itemblock_cloth_pane_1, 2, "conquest:bearhide");
		registerRenderMetas(itemblock_cloth_pane_1, 3, "conquest:rope");
		registerRenderMetas(itemblock_cloth_pane_1, 4, "conquest:none");
		registerRenderMetas(itemblock_cloth_pane_1, 5, "conquest:none");
		registerRenderMetas(itemblock_cloth_pane_1, 6, "conquest:none");
		registerRenderMetas(itemblock_cloth_pane_1, 7, "conquest:none");
		registerRenderMetas(itemblock_cloth_pane_1, 8, "conquest:none");
		registerRenderMetas(itemblock_cloth_pane_1, 9, "conquest:none");
		registerRenderMetas(itemblock_cloth_pane_1, 10, "conquest:none");
		registerRenderMetas(itemblock_cloth_pane_1, 11, "conquest:none");
		registerRenderMetas(itemblock_cloth_pane_1, 12, "conquest:none");
		registerRenderMetas(itemblock_cloth_pane_1, 13, "conquest:none");
		registerRenderMetas(itemblock_cloth_pane_1, 14, "conquest:none");
		registerRenderMetas(itemblock_cloth_pane_1, 15, "conquest:none");
	//Climbable Iron Bar Cloth Blocks
		registerRenderMetas(itemblock_cloth_climbironbar_1, 0, "conquest:curtain_black");
		registerRenderMetas(itemblock_cloth_climbironbar_1, 1, "conquest:curtain_blue");
		registerRenderMetas(itemblock_cloth_climbironbar_1, 2, "conquest:curtain_brown");
		registerRenderMetas(itemblock_cloth_climbironbar_1, 3, "conquest:curtain_green");
		registerRenderMetas(itemblock_cloth_climbironbar_1, 4, "conquest:curtain_purple");
		registerRenderMetas(itemblock_cloth_climbironbar_1, 5, "conquest:curtain_red");
		registerRenderMetas(itemblock_cloth_climbironbar_1, 6, "conquest:curtain_white");
		registerRenderMetas(itemblock_cloth_climbironbar_1, 7, "conquest:none");
		registerRenderMetas(itemblock_cloth_climbironbar_1, 8, "conquest:none");
		registerRenderMetas(itemblock_cloth_climbironbar_1, 9, "conquest:none");
		registerRenderMetas(itemblock_cloth_climbironbar_1, 10, "conquest:none");
		registerRenderMetas(itemblock_cloth_climbironbar_1, 11, "conquest:none");
		registerRenderMetas(itemblock_cloth_climbironbar_1, 12, "conquest:none");
		registerRenderMetas(itemblock_cloth_climbironbar_1, 13, "conquest:none");
		registerRenderMetas(itemblock_cloth_climbironbar_1, 14, "conquest:none");
		registerRenderMetas(itemblock_cloth_climbironbar_1, 15, "conquest:none");
	//Climbable Iron Bar Iron/Cloth Blocks
		registerRenderMetas(itemblock_cloth_ironclimbironbar_1, 0, "conquest:clothesline");
		registerRenderMetas(itemblock_cloth_ironclimbironbar_1, 1, "conquest:decorative_flags");
		registerRenderMetas(itemblock_cloth_ironclimbironbar_1, 2, "conquest:flag_cool");
		registerRenderMetas(itemblock_cloth_ironclimbironbar_1, 3, "conquest:none");
		registerRenderMetas(itemblock_cloth_ironclimbironbar_1, 4, "conquest:none");
		registerRenderMetas(itemblock_cloth_ironclimbironbar_1, 5, "conquest:none");
		registerRenderMetas(itemblock_cloth_ironclimbironbar_1, 6, "conquest:none");
		registerRenderMetas(itemblock_cloth_ironclimbironbar_1, 7, "conquest:none");
		registerRenderMetas(itemblock_cloth_ironclimbironbar_1, 8, "conquest:none");
		registerRenderMetas(itemblock_cloth_ironclimbironbar_1, 9, "conquest:none");
		registerRenderMetas(itemblock_cloth_ironclimbironbar_1, 10, "conquest:none");
		registerRenderMetas(itemblock_cloth_ironclimbironbar_1, 11, "conquest:none");
		registerRenderMetas(itemblock_cloth_ironclimbironbar_1, 12, "conquest:none");
		registerRenderMetas(itemblock_cloth_ironclimbironbar_1, 13, "conquest:none");
		registerRenderMetas(itemblock_cloth_ironclimbironbar_1, 14, "conquest:none");
		registerRenderMetas(itemblock_cloth_ironclimbironbar_1, 15, "conquest:none");

	//Full Light 1.0 Cloth Blocks
		registerRenderMetas(itemblock_cloth_fulllight10_1, 0, "conquest:lantern_whitepaper");
		registerRenderMetas(itemblock_cloth_fulllight10_1, 1, "conquest:lantern_yellowpaper");
		registerRenderMetas(itemblock_cloth_fulllight10_1, 2, "conquest:none");
		registerRenderMetas(itemblock_cloth_fulllight10_1, 3, "conquest:none");
		registerRenderMetas(itemblock_cloth_fulllight10_1, 4, "conquest:none");
		registerRenderMetas(itemblock_cloth_fulllight10_1, 5, "conquest:none");
		registerRenderMetas(itemblock_cloth_fulllight10_1, 6, "conquest:none");
		registerRenderMetas(itemblock_cloth_fulllight10_1, 7, "conquest:none");
		registerRenderMetas(itemblock_cloth_fulllight10_1, 8, "conquest:none");
		registerRenderMetas(itemblock_cloth_fulllight10_1, 9, "conquest:none");
		registerRenderMetas(itemblock_cloth_fulllight10_1, 10, "conquest:none");
		registerRenderMetas(itemblock_cloth_fulllight10_1, 11, "conquest:none");
		registerRenderMetas(itemblock_cloth_fulllight10_1, 12, "conquest:none");
		registerRenderMetas(itemblock_cloth_fulllight10_1, 13, "conquest:none");
		registerRenderMetas(itemblock_cloth_fulllight10_1, 14, "conquest:none");
		registerRenderMetas(itemblock_cloth_fulllight10_1, 15, "conquest:none");
	//No Collision Cloth Blocks
		registerRenderMetas(itemblock_cloth_nocollision_1, 0, "conquest:paperpile");
		registerRenderMetas(itemblock_cloth_nocollision_1, 1, "conquest:book");
		registerRenderMetas(itemblock_cloth_nocollision_1, 2, "conquest:net");
		registerRenderMetas(itemblock_cloth_nocollision_1, 3, "conquest:carpet_net");
		registerRenderMetas(itemblock_cloth_nocollision_1, 4, "conquest:carpet_bearskin");
		registerRenderMetas(itemblock_cloth_nocollision_1, 5, "conquest:bearskin");
		registerRenderMetas(itemblock_cloth_nocollision_1, 6, "conquest:carpet_wolfskin");
		registerRenderMetas(itemblock_cloth_nocollision_1, 7, "conquest:carpet_sheepskin");
		registerRenderMetas(itemblock_cloth_nocollision_1, 8, "conquest:carpet_hay");
		registerRenderMetas(itemblock_cloth_nocollision_1, 9, "conquest:rope_web");
		registerRenderMetas(itemblock_cloth_nocollision_1, 10, "conquest:hangingbags");
		registerRenderMetas(itemblock_cloth_nocollision_1, 11, "conquest:none");
		registerRenderMetas(itemblock_cloth_nocollision_1, 12, "conquest:none");
		registerRenderMetas(itemblock_cloth_nocollision_1, 13, "conquest:none");
		registerRenderMetas(itemblock_cloth_nocollision_1, 14, "conquest:none");
		registerRenderMetas(itemblock_cloth_nocollision_1, 15, "conquest:none");
	//Directionl No Collision Cloth Blocks
		registerRenderMetas(itemblock_cloth_directionalnocollision_1, 0, "conquest:noose");
		registerRenderMetas(itemblock_cloth_directionalnocollision_1, 1, "conquest:none");
		registerRenderMetas(itemblock_cloth_directionalnocollision_1, 2, "conquest:none");
		registerRenderMetas(itemblock_cloth_directionalnocollision_1, 3, "conquest:none");
		registerRenderMetas(itemblock_cloth_directionalnocollision_1, 4, "conquest:none");
		registerRenderMetas(itemblock_cloth_directionalnocollision_1, 5, "conquest:none");
		registerRenderMetas(itemblock_cloth_directionalnocollision_1, 6, "conquest:none");
		registerRenderMetas(itemblock_cloth_directionalnocollision_1, 7, "conquest:none");
		registerRenderMetas(itemblock_cloth_directionalnocollision_1, 8, "conquest:none");
		registerRenderMetas(itemblock_cloth_directionalnocollision_1, 9, "conquest:none");
		registerRenderMetas(itemblock_cloth_directionalnocollision_1, 10, "conquest:none");
		registerRenderMetas(itemblock_cloth_directionalnocollision_1, 11, "conquest:none");
		registerRenderMetas(itemblock_cloth_directionalnocollision_1, 12, "conquest:none");
		registerRenderMetas(itemblock_cloth_directionalnocollision_1, 13, "conquest:none");
		registerRenderMetas(itemblock_cloth_directionalnocollision_1, 14, "conquest:none");
		registerRenderMetas(itemblock_cloth_directionalnocollision_1, 15, "conquest:none");

	//No Collision No Material Blocks
		registerRenderMetas(itemblock_nomaterial_nocollision_1, 0, "conquest:animal_raven");
		registerRenderMetas(itemblock_nomaterial_nocollision_1, 1, "conquest:animal_hawk");
		registerRenderMetas(itemblock_nomaterial_nocollision_1, 2, "conquest:animal_owl");
		registerRenderMetas(itemblock_nomaterial_nocollision_1, 3, "conquest:animal_seagull");
		registerRenderMetas(itemblock_nomaterial_nocollision_1, 4, "conquest:animal_duck");
		registerRenderMetas(itemblock_nomaterial_nocollision_1, 5, "conquest:animal_pigeon");
		registerRenderMetas(itemblock_nomaterial_nocollision_1, 6, "conquest:animal_bluejay");
		registerRenderMetas(itemblock_nomaterial_nocollision_1, 7, "conquest:animal_bat");
		registerRenderMetas(itemblock_nomaterial_nocollision_1, 8, "conquest:animal_rat");
		registerRenderMetas(itemblock_nomaterial_nocollision_1, 9, "conquest:animal_toad");
		registerRenderMetas(itemblock_nomaterial_nocollision_1, 10, "conquest:animal_flies");
		registerRenderMetas(itemblock_nomaterial_nocollision_1, 11, "conquest:steam");
		registerRenderMetas(itemblock_nomaterial_nocollision_1, 12, "conquest:smoke");
		registerRenderMetas(itemblock_nomaterial_nocollision_1, 13, "conquest:none");
		registerRenderMetas(itemblock_nomaterial_nocollision_1, 14, "conquest:none");
		registerRenderMetas(itemblock_nomaterial_nocollision_1, 15, "conquest:none");
	//Full Glass Blocks
		registerRenderMetas(itemblock_glass_full_1, 0, "conquest:mirror");
		registerRenderMetas(itemblock_glass_full_1, 1, "conquest:mirror");
		registerRenderMetas(itemblock_glass_full_1, 2, "conquest:none");
		registerRenderMetas(itemblock_glass_full_1, 3, "conquest:none");
		registerRenderMetas(itemblock_glass_full_1, 4, "conquest:none");
		registerRenderMetas(itemblock_glass_full_1, 5, "conquest:none");
		registerRenderMetas(itemblock_glass_full_1, 6, "conquest:none");
		registerRenderMetas(itemblock_glass_full_1, 7, "conquest:none");
		registerRenderMetas(itemblock_glass_full_1, 8, "conquest:none");
		registerRenderMetas(itemblock_glass_full_1, 9, "conquest:none");
		registerRenderMetas(itemblock_glass_full_1, 10, "conquest:none");
		registerRenderMetas(itemblock_glass_full_1, 11, "conquest:none");
		registerRenderMetas(itemblock_glass_full_1, 12, "conquest:none");
		registerRenderMetas(itemblock_glass_full_1, 13, "conquest:none");
		registerRenderMetas(itemblock_glass_full_1, 14, "conquest:none");
		registerRenderMetas(itemblock_glass_full_1, 15, "conquest:none");
	//Glass Glass Blocks
		registerRenderMetas(itemblock_glass_glass_1, 0, "conquest:glassblock_noctm");
		registerRenderMetas(itemblock_glass_glass_1, 1, "conquest:glassblock_straight");
		registerRenderMetas(itemblock_glass_glass_1, 2, "conquest:glassblock_straight2");
		registerRenderMetas(itemblock_glass_glass_1, 3, "conquest:glassblock_fancy");
		registerRenderMetas(itemblock_glass_glass_1, 4, "conquest:glassblock_fancy2");
		registerRenderMetas(itemblock_glass_glass_1, 5, "conquest:glassblock_dragon");
		registerRenderMetas(itemblock_glass_glass_1, 6, "conquest:glassblock_dragon2");
		registerRenderMetas(itemblock_glass_glass_1, 7, "conquest:glassblock_window");
		registerRenderMetas(itemblock_glass_glass_1, 8, "conquest:glassblock_brown_elongated");
		registerRenderMetas(itemblock_glass_glass_1, 9, "conquest:glassblock_brown_round");
		registerRenderMetas(itemblock_glass_glass_1, 10, "conquest:glassblock_yellow");
		registerRenderMetas(itemblock_glass_glass_1, 11, "conquest:glassblock_yellow_round");
		registerRenderMetas(itemblock_glass_glass_1, 12, "conquest:glassblock_yellow_elongated");
		registerRenderMetas(itemblock_glass_glass_1, 13, "conquest:glassblock_green");
		registerRenderMetas(itemblock_glass_glass_1, 14, "conquest:glassblock_green_round");
		registerRenderMetas(itemblock_glass_glass_1, 15, "conquest:glassblock_green_elongated");
		registerRenderMetas(itemblock_glass_glass_2, 0, "conquest:glassblock_white");
		registerRenderMetas(itemblock_glass_glass_2, 1, "conquest:glassblock_white_round");
		registerRenderMetas(itemblock_glass_glass_2, 2, "conquest:glassblock_white_elongated");
		registerRenderMetas(itemblock_glass_glass_2, 3, "conquest:glassblock_shoji");
		registerRenderMetas(itemblock_glass_glass_2, 4, "conquest:ironbars");
		registerRenderMetas(itemblock_glass_glass_2, 5, "conquest:window_asian");
		registerRenderMetas(itemblock_glass_glass_2, 6, "conquest:glassblock_brown_round2");
		registerRenderMetas(itemblock_glass_glass_2, 7, "conquest:glassblock_yellow_round2");
		registerRenderMetas(itemblock_glass_glass_2, 8, "conquest:glassblock_green_round2");
		registerRenderMetas(itemblock_glass_glass_2, 9, "conquest:glassblock_white_round2");
		registerRenderMetas(itemblock_glass_glass_2, 10, "conquest:window_gothiccarved_full");
		registerRenderMetas(itemblock_glass_glass_2, 11, "conquest:none");
		registerRenderMetas(itemblock_glass_glass_2, 12, "conquest:none");
		registerRenderMetas(itemblock_glass_glass_2, 13, "conquest:none");
		registerRenderMetas(itemblock_glass_glass_2, 14, "conquest:none");
		registerRenderMetas(itemblock_glass_glass_2, 15, "conquest:none");
	//Glass Vertical Slab Blocks
		registerRenderMetas(itemblock_glass_verticalslab_1, 0, "conquest:glassblock_straight_verticalslab");
		registerRenderMetas(itemblock_glass_verticalslab_1, 1, "conquest:glassblock_straight2_verticalslab");
		registerRenderMetas(itemblock_glass_verticalslab_1, 2, "conquest:none");
		registerRenderMetas(itemblock_glass_verticalslab_1, 3, "conquest:none");
	//Glass Trapdoor Blocks
		registerRenderMetas(itemblock_glass_trapdoormodel_1, 0, "conquest:glassblock_noctm_trapdoor");
		registerRenderMetas(itemblock_glass_trapdoormodel_1, 1, "conquest:glassblock_straight_trapdoor");
		registerRenderMetas(itemblock_glass_trapdoormodel_2, 0, "conquest:glassblock_window_trapdoor");
		registerRenderMetas(itemblock_glass_trapdoormodel_2, 1, "conquest:glassblock_yellow_trapdoor");
		registerRenderMetas(itemblock_glass_trapdoormodel_3, 0, "conquest:glassblock_green_trapdoor");
		registerRenderMetas(itemblock_glass_trapdoormodel_3, 1, "conquest:glassblock_white_trapdoor");
		registerRenderMetas(itemblock_glass_trapdoormodel_4, 0, "conquest:window_asian_trapdoor");
		registerRenderMetas(itemblock_glass_trapdoormodel_4, 1, "conquest:shoji_trapdoor");

	//Glass Pane Blocks
		registerRenderMetas(itemblock_glass_pane_1, 0, "conquest:glasspane_straight");
		registerRenderMetas(itemblock_glass_pane_1, 1, "conquest:glasspane_fancy");
		registerRenderMetas(itemblock_glass_pane_1, 2, "conquest:glasspane_dragon");
		registerRenderMetas(itemblock_glass_pane_1, 3, "conquest:glasspane_brown_round");
		registerRenderMetas(itemblock_glass_pane_1, 4, "conquest:glasspane_brown");
		registerRenderMetas(itemblock_glass_pane_1, 5, "conquest:glasspane_brown_elongated");
		registerRenderMetas(itemblock_glass_pane_1, 6, "conquest:glasspane_yellow");
		registerRenderMetas(itemblock_glass_pane_1, 7, "conquest:glasspane_yellow_round");
		registerRenderMetas(itemblock_glass_pane_1, 8, "conquest:glasspane_yellow_elongated");
		registerRenderMetas(itemblock_glass_pane_1, 9, "conquest:glasspane_green");
		registerRenderMetas(itemblock_glass_pane_1, 10, "conquest:glasspane_green_roundctm");
		registerRenderMetas(itemblock_glass_pane_1, 11, "conquest:glasspane_green_elongated");
		registerRenderMetas(itemblock_glass_pane_1, 12, "conquest:glasspane_white");
		registerRenderMetas(itemblock_glass_pane_1, 13, "conquest:glasspane_white_roundctm");
		registerRenderMetas(itemblock_glass_pane_1, 14, "conquest:glasspane_white_elongated");
		registerRenderMetas(itemblock_glass_pane_1, 15, "conquest:glasspane_shoji");
		registerRenderMetas(itemblock_glass_pane_2, 0, "conquest:glasspane_invisisble");
		registerRenderMetas(itemblock_glass_pane_2, 1, "conquest:glasspane_window_asian");
		registerRenderMetas(itemblock_glass_pane_2, 2, "conquest:glasspane_brown_round2");
		registerRenderMetas(itemblock_glass_pane_2, 3, "conquest:glasspane_yellow_round2");
		registerRenderMetas(itemblock_glass_pane_2, 4, "conquest:glasspane_green_round2");
		registerRenderMetas(itemblock_glass_pane_2, 5, "conquest:glasspane_white_round2");
		registerRenderMetas(itemblock_glass_pane_2, 6, "conquest:window_gothiccarved_pane");
		registerRenderMetas(itemblock_glass_pane_2, 7, "conquest:none");
		registerRenderMetas(itemblock_glass_pane_2, 8, "conquest:none");
		registerRenderMetas(itemblock_glass_pane_2, 9, "conquest:none");
		registerRenderMetas(itemblock_glass_pane_2, 10, "conquest:none");
		registerRenderMetas(itemblock_glass_pane_2, 11, "conquest:none");
		registerRenderMetas(itemblock_glass_pane_2, 12, "conquest:none");
		registerRenderMetas(itemblock_glass_pane_2, 13, "conquest:none");
		registerRenderMetas(itemblock_glass_pane_2, 14, "conquest:none");
		registerRenderMetas(itemblock_glass_pane_2, 15, "conquest:none");

	}


	public static void registerRendersNoMeta()
	{
		/**
		 * FOR RENDERING BLOCKS WITHOUT METAS
		 registerRender(block static name)
		 */
		//Stone Damage Light 0.6 Block
		registerRenderNoMetas(stone_fulldamagelight6_1);
		registerRenderNoMetas(stone_layerdamagelight6_1);
		//Stone Damage Light 0.8 Block
		registerRenderNoMetas(stone_layerdamagelight8_1);
		//End Frame Stone Block
		registerRenderNoMetas(base_marblesandstone_endframe);
		registerRenderNoMetas(urn_terracotta_endframe);
		//Stone Ladder Block
		registerRenderNoMetas(climbingrocks);
		//Wood Door Blocks
		registerRenderNoMetas(planks_whiteweathered_door);
		registerRenderNoMetas(planks_redweathered_door);
		registerRenderNoMetas(planks_lightredweathered_door);
		registerRenderNoMetas(planks_orangepainted_door);
		registerRenderNoMetas(planks_yellowweathered_door);
		registerRenderNoMetas(planks_greenweathered_door);
		registerRenderNoMetas(planks_limeweathered_door);
		registerRenderNoMetas(planks_cyanweathered_door);
		registerRenderNoMetas(planks_darkblueweathered_door);
		registerRenderNoMetas(planks_blueweathered_door);
		registerRenderNoMetas(planks_lightblueweathered_door);
		registerRenderNoMetas(planks_purplepainted_door);
		registerRenderNoMetas(planks_brownweathered_door);
		//Wood Ladder Blocks
		registerRenderNoMetas(ladder);
		//Wood Functional Trapdoor Blocks
		registerRenderNoMetas(woodenboard);
		registerRenderNoMetas(wheel);
		//Wood Shutters Blocks
		registerRenderNoMetas(shutters_light);
		registerRenderNoMetas(shutters_dark);
		//Wood Trapdoor Railing Blocks
		registerRenderNoMetas(wood_railing);
		registerRenderNoMetas(wood_railing_straight);
		registerRenderNoMetas(spruce_railing);
		registerRenderNoMetas(spruce_railing_straight);
		registerRenderNoMetas(birch_railing);
		registerRenderNoMetas(birch_railing_straight);
		registerRenderNoMetas(jungle_railing);
		registerRenderNoMetas(jungle_railing_straight);
		registerRenderNoMetas(acacia_railing);
		registerRenderNoMetas(acacia_railing_straight);
		//No Collision Damage Wood Blocks
		registerRenderNoMetas(wood_nocollisiondamage_1);
		//Bed Wood Blocks
		registerRenderNoMetas(bed_wolf);
		registerRenderNoMetas(bed_rustic);
		registerRenderNoMetas(bed_bear);
		registerRenderNoMetas(bed_fancygreen);
		//Cauldron Wood Blocks
		registerRenderNoMetas(barrel_grille_cauldron);
		registerRenderNoMetas(barrel_cauldron);
		//Wood Rail Blocks
		registerRenderNoMetas(jungle_rail);
		registerRenderNoMetas(spruce_floor_rail);
		//Cauldron Iron Blocks
		registerRenderNoMetas(cauldron_irongrille);
		registerRenderNoMetas(cauldron);
		//Brewingstand Iron Blocks
		registerRenderNoMetas(iron_brewingstand_1);
		registerRenderNoMetas(iron_brewingstandlight10_1);
		//Iron Trapdoor Railing Blocks
		registerRenderNoMetas(iron_railing);
		registerRenderNoMetas(iron_railing_straight);
		//Iron Rail Blocks
		registerRenderNoMetas(ironchains_rail);
		//Vine Cloth Blocks
		registerRenderNoMetas(curtain_black_vine);
		registerRenderNoMetas(curtain_blue_vine);
		registerRenderNoMetas(curtain_brown_vine);
		registerRenderNoMetas(curtain_green_vine);
		registerRenderNoMetas(curtain_purple_vine);
		registerRenderNoMetas(curtain_red_vine);
		registerRenderNoMetas(curtain_white_vine);
		//Cloth Ladder Blocks
		registerRenderNoMetas(rope_ladder);
		registerRenderNoMetas(rope_climbing_ladder);
		//No Collision Cloth Light 1.0 Blocks
		registerRenderNoMetas(cloth_nocollisionlight10_1);
		//Cloth Rail Blocks
		registerRenderNoMetas(rope_rail);
		//Lilypad Plants Blocks
		registerRenderNoMetas(plants_lilypad_1);
		registerRenderNoMetas(plants_lilypad_2);
		//Vine Vine Blocks
		registerRenderNoMetas(vine_jungle);
		registerRenderNoMetas(vine_ivy);
		registerRenderNoMetas(vine_moss);
		//Snowlayer Biome Grass Blocks
		registerRenderNoMetas(grass_snowlayer_1);
		//Translucent No Collision Ice Blocks
		registerRenderNoMetas(ice_translucentnocollision_1);
		//No Collision Damage Ice Blocks
		registerRenderNoMetas(ice_nocollisiondamage_1);
		//Lilypad Ice Blocks
		registerRenderNoMetas(ice_lilypad_1);
		//No Collision No Material Light 0.5 Blocks
		registerRenderNoMetas(nomaterial_nocollisionlight5_1);
		//No Collision No Material Light 0.6 Blocks
		registerRenderNoMetas(invisible_light6);
		//No Collision No Material Light 0.8 Blocks
		registerRenderNoMetas(invisible_light8);
		// No Collision No Material Light 1.0 Blocks
		registerRenderNoMetas(invisible_light10);
		// Cake Cake Blocks
		registerRenderNoMetas(cake_cheesewheel);
		registerRenderNoMetas(cake_applepie);
		registerRenderNoMetas(cake_icingfruit);
		registerRenderNoMetas(cake_icingchocolate);
		registerRenderNoMetas(cake_bread);
		//Furnace Blocks
		registerRenderNoMetas(furnace_iron);
		registerRenderNoMetas(furnace_sandstone);
		//Lit Furnace Blocks
		registerRenderNoMetas(furnace_ironlit);
		registerRenderNoMetas(furnace_cobblelit);
		registerRenderNoMetas(furnace_sandstonelit);
		//Dispenser Blocks
		registerRenderNoMetas(dispenser_aztec);
		registerRenderNoMetas(dispenser_iron);
		registerRenderNoMetas(dispenser_egyptian);
		//Dropper Blocks
		registerRenderNoMetas(dropper_sandstone);
		registerRenderNoMetas(dropper_iron);
		//Flower Pot Blocks
		registerRenderNoMetas(flowerpot);
		registerRenderNoMetas(flowerpot_black);
		//Button Blocks
		registerRenderNoMetas(wood_button);
		registerRenderNoMetas(stone_button);
		//Lever Blocks
		registerRenderNoMetas(lever);
		//Hook Blocks
		registerRenderNoMetas(hook);
	}

	private static void registerRenderMetas (ItemBlockMeta block, int meta, String variant)
	{
		ModelResourceLocation
		itemModelResourceLocation = new ModelResourceLocation(variant, "inventory");
	    ModelLoader.setCustomModelResourceLocation(block, (int)meta, itemModelResourceLocation);
	}

	/**
	 * Special registerRender for Layer Block
	 */
	private static void registerRenderLayerMeta (ItemLayerMeta block, int meta, String variant)
	{
		ModelResourceLocation
		itemModelResourceLocation = new ModelResourceLocation(variant, "inventory");
	    ModelLoader.setCustomModelResourceLocation(block, (int)meta, itemModelResourceLocation);
	}


	private static void registerRenderNoMetas (Block block)
	{
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(Item.getItemFromBlock(block), 0, new ModelResourceLocation(block.getRegistryName(), "inventory"));
	}

	/**
	 * Adding a new ItemBlock for registered block without metas (no need to make one for each block).
	 */
	private static void registerBlock(Block block)
	{
		GameRegistry.register(block);
		ItemBlock item = new ItemBlock(block);
		item.setRegistryName(block.getRegistryName());
		GameRegistry.register(item);
	}

	/**
	 * Adding a new ItemBlock (BED) for registered block without metas (no need to make one for each block).
	 */
	private static void registerBedBlock(Block block)
	{
		GameRegistry.register(block);
		BedItem item = new BedItem(block);
		item.setRegistryName(block.getRegistryName());
		GameRegistry.register(item);
	}

	/**
	 * Adding a new ItemBlock (BED) for registered block without metas (no need to make one for each block).
	 */
	private static void registerDoorBlock(Block block)
	{
		GameRegistry.register(block);
		DoorItem item = new DoorItem(block);
		item.setRegistryName(block.getRegistryName());
		GameRegistry.register(item);
	}

	/**
	 * Adding a new ItemBlock (Snowlayer) for registered block without metas (no need to make one for each block).
	 */
	private static void registerSnowlayerBlock(Block block)
	{
		GameRegistry.register(block);
		SnowItem item = new SnowItem(block);
		item.setRegistryName(block.getRegistryName());
		GameRegistry.register(item);
	}

	/**
	 * Adding a new ItemBlock (Lilypad) for registered block without metas (no need to make one for each block).
	 */
	private static void registerLilypadBlock(Block block)
	{
		GameRegistry.register(block);
		LilyPadItem item = new LilyPadItem(block);
		item.setRegistryName(block.getRegistryName());
		GameRegistry.register(item);
	}

	/**
	 * Adding a new ItemBlockMeta for each registered block with metas.
	 */
	//Full Stone Blocks
	public static ItemBlockMeta itemblock_stone_full_1;
	public static ItemBlockMeta itemblock_stone_full_2;
	public static ItemBlockMeta itemblock_stone_full_3;
	public static ItemBlockMeta itemblock_stone_full_4;
	public static ItemBlockMeta itemblock_stone_full_5;
	public static ItemBlockMeta itemblock_stone_full_6;
	public static ItemBlockMeta itemblock_stone_full_7;
	public static ItemBlockMeta itemblock_stone_full_8;
	public static ItemBlockMeta itemblock_stone_full_9;
	public static ItemBlockMeta itemblock_stone_full_10;
	public static ItemBlockMeta itemblock_stone_full_11;
	public static ItemBlockMeta itemblock_stone_full_12;
	public static ItemBlockMeta itemblock_stone_full_13;
	public static ItemBlockMeta itemblock_stone_full_14;
	public static ItemBlockMeta itemblock_stone_full_15;
	public static ItemBlockMeta itemblock_stone_full_16;
	public static ItemBlockMeta itemblock_stone_full_17;
	//Partial Full Stone Blocks
	public static ItemBlockMeta itemblock_stone_fullpartial_1;
	public static ItemBlockMeta itemblock_stone_fullpartial_2;
	public static ItemBlockMeta itemblock_stone_fullpartial_3;
	public static ItemBlockMeta itemblock_stone_fullpartial_4;
	public static ItemBlockMeta itemblock_stone_fullpartial_5;
	public static ItemBlockMeta itemblock_stone_fullpartial_6;
	//Directional Full Partial Stone Blocks
	public static ItemBlockMeta itemblock_stone_directionalfullpartial_1;
	//Corner Stone Blocks
	public static ItemBlockMeta itemblock_stone_corner_1;
	public static ItemBlockMeta itemblock_stone_corner_2;
	public static ItemBlockMeta itemblock_stone_corner_3;
	public static ItemBlockMeta itemblock_stone_corner_4;
	public static ItemBlockMeta itemblock_stone_corner_5;
	public static ItemBlockMeta itemblock_stone_corner_6;
	public static ItemBlockMeta itemblock_stone_corner_7;
	public static ItemBlockMeta itemblock_stone_corner_8;
	public static ItemBlockMeta itemblock_stone_corner_9;
	public static ItemBlockMeta itemblock_stone_corner_10;
	public static ItemBlockMeta itemblock_stone_corner_11;
	public static ItemBlockMeta itemblock_stone_corner_12;
	public static ItemBlockMeta itemblock_stone_corner_13;
	public static ItemBlockMeta itemblock_stone_corner_14;
	public static ItemBlockMeta itemblock_stone_corner_15;
	public static ItemBlockMeta itemblock_stone_corner_16;
	public static ItemBlockMeta itemblock_stone_corner_17;
	public static ItemBlockMeta itemblock_stone_corner_18;
	public static ItemBlockMeta itemblock_stone_corner_19;
	public static ItemBlockMeta itemblock_stone_corner_20;
	public static ItemBlockMeta itemblock_stone_corner_21;
	public static ItemBlockMeta itemblock_stone_corner_22;
	public static ItemBlockMeta itemblock_stone_corner_23;
	public static ItemBlockMeta itemblock_stone_corner_24;
	public static ItemBlockMeta itemblock_stone_corner_25;
	public static ItemBlockMeta itemblock_stone_corner_26;
	public static ItemBlockMeta itemblock_stone_corner_27;
	public static ItemBlockMeta itemblock_stone_corner_28;
	public static ItemBlockMeta itemblock_stone_corner_29;
	public static ItemBlockMeta itemblock_stone_corner_30;
	public static ItemBlockMeta itemblock_stone_corner_31;
	public static ItemBlockMeta itemblock_stone_corner_32;
	public static ItemBlockMeta itemblock_stone_corner_33;
	public static ItemBlockMeta itemblock_stone_corner_34;
	public static ItemBlockMeta itemblock_stone_corner_35;
	public static ItemBlockMeta itemblock_stone_corner_36;
	public static ItemBlockMeta itemblock_stone_corner_37;
	public static ItemBlockMeta itemblock_stone_corner_38;
	public static ItemBlockMeta itemblock_stone_corner_39;
	public static ItemBlockMeta itemblock_stone_corner_40;
	public static ItemBlockMeta itemblock_stone_corner_41;
	public static ItemBlockMeta itemblock_stone_corner_42;
	public static ItemBlockMeta itemblock_stone_corner_43;
	public static ItemBlockMeta itemblock_stone_corner_44;
	public static ItemBlockMeta itemblock_stone_corner_45;
	public static ItemBlockMeta itemblock_stone_corner_46;
	public static ItemBlockMeta itemblock_stone_corner_47;
	public static ItemBlockMeta itemblock_stone_corner_48;
	public static ItemBlockMeta itemblock_stone_corner_49;
	public static ItemBlockMeta itemblock_stone_corner_50;
	public static ItemBlockMeta itemblock_stone_corner_51;
	public static ItemBlockMeta itemblock_stone_corner_52;
	public static ItemBlockMeta itemblock_stone_corner_53;
	public static ItemBlockMeta itemblock_stone_corner_54;
	public static ItemBlockMeta itemblock_stone_corner_55;
	public static ItemBlockMeta itemblock_stone_corner_56;
	public static ItemBlockMeta itemblock_stone_corner_57;
	public static ItemBlockMeta itemblock_stone_corner_58;
	public static ItemBlockMeta itemblock_stone_corner_59;
	public static ItemBlockMeta itemblock_stone_corner_60;
	public static ItemBlockMeta itemblock_stone_corner_61;
	public static ItemBlockMeta itemblock_stone_corner_62;
	public static ItemBlockMeta itemblock_stone_corner_63;
	public static ItemBlockMeta itemblock_stone_corner_64;
	//Vertical Slab Stone Blocks
	public static ItemBlockMeta itemblock_stone_verticalslab_1;
	public static ItemBlockMeta itemblock_stone_verticalslab_2;
	public static ItemBlockMeta itemblock_stone_verticalslab_3;
	public static ItemBlockMeta itemblock_stone_verticalslab_4;
	public static ItemBlockMeta itemblock_stone_verticalslab_5;
	public static ItemBlockMeta itemblock_stone_verticalslab_6;
	public static ItemBlockMeta itemblock_stone_verticalslab_7;
	public static ItemBlockMeta itemblock_stone_verticalslab_8;
	public static ItemBlockMeta itemblock_stone_verticalslab_9;
	public static ItemBlockMeta itemblock_stone_verticalslab_10;
	public static ItemBlockMeta itemblock_stone_verticalslab_11;
	public static ItemBlockMeta itemblock_stone_verticalslab_12;
	public static ItemBlockMeta itemblock_stone_verticalslab_13;
	public static ItemBlockMeta itemblock_stone_verticalslab_14;
	public static ItemBlockMeta itemblock_stone_verticalslab_15;
	public static ItemBlockMeta itemblock_stone_verticalslab_16;
	public static ItemBlockMeta itemblock_stone_verticalslab_17;
	public static ItemBlockMeta itemblock_stone_verticalslab_18;
	public static ItemBlockMeta itemblock_stone_verticalslab_19;
	public static ItemBlockMeta itemblock_stone_verticalslab_20;
	public static ItemBlockMeta itemblock_stone_verticalslab_21;
	public static ItemBlockMeta itemblock_stone_verticalslab_22;
	public static ItemBlockMeta itemblock_stone_verticalslab_23;
	public static ItemBlockMeta itemblock_stone_verticalslab_24;
	public static ItemBlockMeta itemblock_stone_verticalslab_25;
	public static ItemBlockMeta itemblock_stone_verticalslab_26;
	public static ItemBlockMeta itemblock_stone_verticalslab_27;
	public static ItemBlockMeta itemblock_stone_verticalslab_28;
	public static ItemBlockMeta itemblock_stone_verticalslab_29;
	public static ItemBlockMeta itemblock_stone_verticalslab_30;
	public static ItemBlockMeta itemblock_stone_verticalslab_31;
	public static ItemBlockMeta itemblock_stone_verticalslab_32;
	public static ItemBlockMeta itemblock_stone_verticalslab_33;
	public static ItemBlockMeta itemblock_stone_verticalslab_34;
	public static ItemBlockMeta itemblock_stone_verticalslab_35;
	public static ItemBlockMeta itemblock_stone_verticalslab_36;
	public static ItemBlockMeta itemblock_stone_verticalslab_37;
	public static ItemBlockMeta itemblock_stone_verticalslab_38;
	public static ItemBlockMeta itemblock_stone_verticalslab_39;
	public static ItemBlockMeta itemblock_stone_verticalslab_40;
	public static ItemBlockMeta itemblock_stone_verticalslab_41;
	public static ItemBlockMeta itemblock_stone_verticalslab_42;
	public static ItemBlockMeta itemblock_stone_verticalslab_43;
	public static ItemBlockMeta itemblock_stone_verticalslab_44;
	public static ItemBlockMeta itemblock_stone_verticalslab_45;
	public static ItemBlockMeta itemblock_stone_verticalslab_46;
	public static ItemBlockMeta itemblock_stone_verticalslab_47;
	public static ItemBlockMeta itemblock_stone_verticalslab_48;
	public static ItemBlockMeta itemblock_stone_verticalslab_49;
	public static ItemBlockMeta itemblock_stone_verticalslab_50;
	public static ItemBlockMeta itemblock_stone_verticalslab_51;
	public static ItemBlockMeta itemblock_stone_verticalslab_52;
	public static ItemBlockMeta itemblock_stone_verticalslab_53;
	public static ItemBlockMeta itemblock_stone_verticalslab_54;
	public static ItemBlockMeta itemblock_stone_verticalslab_55;
	public static ItemBlockMeta itemblock_stone_verticalslab_56;
	public static ItemBlockMeta itemblock_stone_verticalslab_57;
	public static ItemBlockMeta itemblock_stone_verticalslab_58;
	public static ItemBlockMeta itemblock_stone_verticalslab_59;
	public static ItemBlockMeta itemblock_stone_verticalslab_60;
	public static ItemBlockMeta itemblock_stone_verticalslab_61;
	public static ItemBlockMeta itemblock_stone_verticalslab_62;
	public static ItemBlockMeta itemblock_stone_verticalslab_63;
	public static ItemBlockMeta itemblock_stone_verticalslab_64;
	public static ItemBlockMeta itemblock_stone_verticalslab_65;
	public static ItemBlockMeta itemblock_stone_verticalslab_66;
	public static ItemBlockMeta itemblock_stone_verticalslab_67;
	public static ItemBlockMeta itemblock_stone_verticalslab_68;
	public static ItemBlockMeta itemblock_stone_verticalslab_69;
	public static ItemBlockMeta itemblock_stone_verticalslab_70;
	public static ItemBlockMeta itemblock_stone_verticalslab_71;
	//Arrow Slit Stone Blocks
	public static ItemBlockMeta itemblock_stone_arrowslit_1;
	public static ItemBlockMeta itemblock_stone_arrowslit_2;
	public static ItemBlockMeta itemblock_stone_arrowslit_3;
	public static ItemBlockMeta itemblock_stone_arrowslit_4;
	public static ItemBlockMeta itemblock_stone_arrowslit_5;
	public static ItemBlockMeta itemblock_stone_arrowslit_6;
	public static ItemBlockMeta itemblock_stone_arrowslit_7;
	public static ItemBlockMeta itemblock_stone_arrowslit_8;
	public static ItemBlockMeta itemblock_stone_arrowslit_9;
	public static ItemBlockMeta itemblock_stone_arrowslit_10;
	public static ItemBlockMeta itemblock_stone_arrowslit_11;
	public static ItemBlockMeta itemblock_stone_arrowslit_12;
	public static ItemBlockMeta itemblock_stone_arrowslit_13;
	public static ItemBlockMeta itemblock_stone_arrowslit_14;
	public static ItemBlockMeta itemblock_stone_arrowslit_15;
	public static ItemBlockMeta itemblock_stone_arrowslit_16;
	public static ItemBlockMeta itemblock_stone_arrowslit_17;
	public static ItemBlockMeta itemblock_stone_arrowslit_18;
	public static ItemBlockMeta itemblock_stone_arrowslit_19;
	public static ItemBlockMeta itemblock_stone_arrowslit_20;
	public static ItemBlockMeta itemblock_stone_arrowslit_21;
	public static ItemBlockMeta itemblock_stone_arrowslit_22;
	public static ItemBlockMeta itemblock_stone_arrowslit_23;
	public static ItemBlockMeta itemblock_stone_arrowslit_24;

	//Full Slit Stone Blocks
	public static ItemBlockMeta itemblock_stone_fullslit_1;
	public static ItemBlockMeta itemblock_stone_fullslit_2;
	public static ItemBlockMeta itemblock_stone_fullslit_3;
	public static ItemBlockMeta itemblock_stone_fullslit_4;
	public static ItemBlockMeta itemblock_stone_fullslit_5;
	public static ItemBlockMeta itemblock_stone_fullslit_6;
	public static ItemBlockMeta itemblock_stone_fullslit_7;
	public static ItemBlockMeta itemblock_stone_fullslit_8;
	public static ItemBlockMeta itemblock_stone_fullslit_9;
	public static ItemBlockMeta itemblock_stone_fullslit_10;
	public static ItemBlockMeta itemblock_stone_fullslit_11;
	public static ItemBlockMeta itemblock_stone_fullslit_12;
	public static ItemBlockMeta itemblock_stone_fullslit_13;
	public static ItemBlockMeta itemblock_stone_fullslit_14;
	public static ItemBlockMeta itemblock_stone_fullslit_15;
	public static ItemBlockMeta itemblock_stone_fullslit_16;
	public static ItemBlockMeta itemblock_stone_fullslit_17;
	public static ItemBlockMeta itemblock_stone_fullslit_18;
	public static ItemBlockMeta itemblock_stone_fullslit_19;
	public static ItemBlockMeta itemblock_stone_fullslit_20;
	public static ItemBlockMeta itemblock_stone_fullslit_21;
	public static ItemBlockMeta itemblock_stone_fullslit_22;
	public static ItemBlockMeta itemblock_stone_fullslit_23;
	public static ItemBlockMeta itemblock_stone_fullslit_24;
	//Anvil Stone Blocks
	public static ItemBlockMeta itemblock_stone_anvil_1;
	public static ItemBlockMeta itemblock_stone_anvil_2;
	public static ItemBlockMeta itemblock_stone_anvil_3;
	public static ItemBlockMeta itemblock_stone_anvil_4;
	public static ItemBlockMeta itemblock_stone_anvil_5;
	public static ItemBlockMeta itemblock_stone_anvil_6;
	public static ItemBlockMeta itemblock_stone_anvil_7;
	public static ItemBlockMeta itemblock_stone_anvil_8;
	public static ItemBlockMeta itemblock_stone_anvil_9;
	public static ItemBlockMeta itemblock_stone_anvil_10;
	public static ItemBlockMeta itemblock_stone_anvil_11;
	public static ItemBlockMeta itemblock_stone_anvil_12;
	public static ItemBlockMeta itemblock_stone_anvil_13;
	public static ItemBlockMeta itemblock_stone_anvil_14;
	public static ItemBlockMeta itemblock_stone_anvil_15;
	public static ItemBlockMeta itemblock_stone_anvil_16;
	public static ItemBlockMeta itemblock_stone_anvil_17;
	public static ItemBlockMeta itemblock_stone_anvil_18;
	public static ItemBlockMeta itemblock_stone_anvil_19;
	public static ItemBlockMeta itemblock_stone_anvil_20;
	public static ItemBlockMeta itemblock_stone_anvil_21;
	public static ItemBlockMeta itemblock_stone_anvil_22;
	public static ItemBlockMeta itemblock_stone_anvil_23;
	public static ItemBlockMeta itemblock_stone_anvil_24;
	public static ItemBlockMeta itemblock_stone_anvil_25;
	public static ItemBlockMeta itemblock_stone_anvil_26;
	public static ItemBlockMeta itemblock_stone_anvil_27;
	public static ItemBlockMeta itemblock_stone_anvil_28;
	//Hopper Full Stone Blocks
	public static ItemBlockMeta itemblock_stone_hopperfull_1;
	public static ItemBlockMeta itemblock_stone_hopperfull_2;
	public static ItemBlockMeta itemblock_stone_hopperfull_3;
	public static ItemBlockMeta itemblock_stone_hopperfull_4;
	public static ItemBlockMeta itemblock_stone_hopperfull_5;
	public static ItemBlockMeta itemblock_stone_hopperfull_6;
	public static ItemBlockMeta itemblock_stone_hopperfull_7;
	//Hopper Directional Stone Blocks
	public static ItemBlockMeta itemblock_stone_hopperdirectional_1;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_2;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_3;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_4;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_5;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_6;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_7;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_8;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_9;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_10;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_11;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_12;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_13;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_14;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_15;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_16;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_17;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_18;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_19;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_20;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_21;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_22;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_23;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_24;
	public static ItemBlockMeta itemblock_stone_hopperdirectional_25;
	//Log Stone Blocks
	public static ItemBlockMeta itemblock_stone_log_1;
	public static ItemBlockMeta itemblock_stone_log_2;
	//Wall Stone Blocks
	public static ItemBlockMeta itemblock_stone_wall_1;
	public static ItemBlockMeta itemblock_stone_wall_2;
	public static ItemBlockMeta itemblock_stone_wall_3;
	public static ItemBlockMeta itemblock_stone_wall_4;
	public static ItemBlockMeta itemblock_stone_wall_5;
	public static ItemBlockMeta itemblock_stone_wall_6;
	public static ItemBlockMeta itemblock_stone_wall_7;
	public static ItemBlockMeta itemblock_stone_wall_8;
	public static ItemBlockMeta itemblock_stone_wall_9;

	//Pillar Stone Blocks
	public static ItemBlockMeta itemblock_stone_pillar_1;
	public static ItemBlockMeta itemblock_stone_pillar_2;
	public static ItemBlockMeta itemblock_stone_pillar_3;
	public static ItemBlockMeta itemblock_stone_pillar_4;
	public static ItemBlockMeta itemblock_stone_pillar_5;
	public static ItemBlockMeta itemblock_stone_pillar_6;
	public static ItemBlockMeta itemblock_stone_pillar_7;
	public static ItemBlockMeta itemblock_stone_pillar_8;
	public static ItemBlockMeta itemblock_stone_pillar_9;
	//Fence Stone Blocks
	public static ItemBlockMeta itemblock_stone_fence_1;
	public static ItemBlockMeta itemblock_stone_fence_2;
	//Fencegate Stone Blocks
	public static ItemBlockMeta itemblock_stone_fencegate_1;

	//Small Pillar Stone Blocks
	public static ItemBlockMeta itemblock_stone_smallpillar_1;
	public static ItemBlockMeta itemblock_stone_smallpillar_2;
	public static ItemBlockMeta itemblock_stone_smallpillar_3;
	public static ItemBlockMeta itemblock_stone_smallpillar_4;
	public static ItemBlockMeta itemblock_stone_smallpillar_5;
	public static ItemBlockMeta itemblock_stone_smallpillar_6;
	public static ItemBlockMeta itemblock_stone_smallpillar_7;
	public static ItemBlockMeta itemblock_stone_smallpillar_8;
	public static ItemBlockMeta itemblock_stone_smallpillar_9;
	public static ItemBlockMeta itemblock_stone_smallpillar_10;
	//Stairs Stone Blocks
	public static ItemBlockMeta itemblock_stone_stairs_1;
	public static ItemBlockMeta itemblock_stone_stairs_2;
	public static ItemBlockMeta itemblock_stone_stairs_3;
	public static ItemBlockMeta itemblock_stone_stairs_4;
	public static ItemBlockMeta itemblock_stone_stairs_5;
	public static ItemBlockMeta itemblock_stone_stairs_6;
	public static ItemBlockMeta itemblock_stone_stairs_7;
	public static ItemBlockMeta itemblock_stone_stairs_8;
	public static ItemBlockMeta itemblock_stone_stairs_9;
	public static ItemBlockMeta itemblock_stone_stairs_10;
	public static ItemBlockMeta itemblock_stone_stairs_11;
	public static ItemBlockMeta itemblock_stone_stairs_12;
	public static ItemBlockMeta itemblock_stone_stairs_13;
	public static ItemBlockMeta itemblock_stone_stairs_14;
	public static ItemBlockMeta itemblock_stone_stairs_15;
	public static ItemBlockMeta itemblock_stone_stairs_16;
	public static ItemBlockMeta itemblock_stone_stairs_17;
	public static ItemBlockMeta itemblock_stone_stairs_18;
	public static ItemBlockMeta itemblock_stone_stairs_19;
	public static ItemBlockMeta itemblock_stone_stairs_20;
	public static ItemBlockMeta itemblock_stone_stairs_21;
	public static ItemBlockMeta itemblock_stone_stairs_22;
	public static ItemBlockMeta itemblock_stone_stairs_23;
	public static ItemBlockMeta itemblock_stone_stairs_24;
	public static ItemBlockMeta itemblock_stone_stairs_25;
	public static ItemBlockMeta itemblock_stone_stairs_26;
	public static ItemBlockMeta itemblock_stone_stairs_27;
	public static ItemBlockMeta itemblock_stone_stairs_28;
	public static ItemBlockMeta itemblock_stone_stairs_29;
	public static ItemBlockMeta itemblock_stone_stairs_30;
	public static ItemBlockMeta itemblock_stone_stairs_31;
	public static ItemBlockMeta itemblock_stone_stairs_32;
	public static ItemBlockMeta itemblock_stone_stairs_33;
	public static ItemBlockMeta itemblock_stone_stairs_34;
	public static ItemBlockMeta itemblock_stone_stairs_35;
	public static ItemBlockMeta itemblock_stone_stairs_36;
	public static ItemBlockMeta itemblock_stone_stairs_37;
	public static ItemBlockMeta itemblock_stone_stairs_38;
	public static ItemBlockMeta itemblock_stone_stairs_39;
	public static ItemBlockMeta itemblock_stone_stairs_40;
	public static ItemBlockMeta itemblock_stone_stairs_41;
	public static ItemBlockMeta itemblock_stone_stairs_42;
	public static ItemBlockMeta itemblock_stone_stairs_43;
	public static ItemBlockMeta itemblock_stone_stairs_44;
	public static ItemBlockMeta itemblock_stone_stairs_45;
	public static ItemBlockMeta itemblock_stone_stairs_46;
	public static ItemBlockMeta itemblock_stone_stairs_47;
	public static ItemBlockMeta itemblock_stone_stairs_48;
	public static ItemBlockMeta itemblock_stone_stairs_49;
	public static ItemBlockMeta itemblock_stone_stairs_50;
	public static ItemBlockMeta itemblock_stone_stairs_51;
	public static ItemBlockMeta itemblock_stone_stairs_52;
	public static ItemBlockMeta itemblock_stone_stairs_53;
	public static ItemBlockMeta itemblock_stone_stairs_54;
	public static ItemBlockMeta itemblock_stone_stairs_55;
	public static ItemBlockMeta itemblock_stone_stairs_56;
	public static ItemBlockMeta itemblock_stone_stairs_57;
	public static ItemBlockMeta itemblock_stone_stairs_58;
	public static ItemBlockMeta itemblock_stone_stairs_59;
	public static ItemBlockMeta itemblock_stone_stairs_60;
	public static ItemBlockMeta itemblock_stone_stairs_61;
	public static ItemBlockMeta itemblock_stone_stairs_62;
	public static ItemBlockMeta itemblock_stone_stairs_63;
	public static ItemBlockMeta itemblock_stone_stairs_64;
	public static ItemBlockMeta itemblock_stone_stairs_65;
	public static ItemBlockMeta itemblock_stone_stairs_66;
	public static ItemBlockMeta itemblock_stone_stairs_67;
	public static ItemBlockMeta itemblock_stone_stairs_68;
	public static ItemBlockMeta itemblock_stone_stairs_69;
	public static ItemBlockMeta itemblock_stone_stairs_70;
	public static ItemBlockMeta itemblock_stone_stairs_71;
	public static ItemBlockMeta itemblock_stone_stairs_72;
	public static ItemBlockMeta itemblock_stone_stairs_73;
	public static ItemBlockMeta itemblock_stone_stairs_74;
	public static ItemBlockMeta itemblock_stone_stairs_75;
	public static ItemBlockMeta itemblock_stone_stairs_76;
	public static ItemBlockMeta itemblock_stone_stairs_77;
	public static ItemBlockMeta itemblock_stone_stairs_78;
	public static ItemBlockMeta itemblock_stone_stairs_79;
	public static ItemBlockMeta itemblock_stone_stairs_80;
	public static ItemBlockMeta itemblock_stone_stairs_81;
	public static ItemBlockMeta itemblock_stone_stairs_82;
	public static ItemBlockMeta itemblock_stone_stairs_83;
	public static ItemBlockMeta itemblock_stone_stairs_84;
	public static ItemBlockMeta itemblock_stone_stairs_85;
	public static ItemBlockMeta itemblock_stone_stairs_86;
	public static ItemBlockMeta itemblock_stone_stairs_87;
	public static ItemBlockMeta itemblock_stone_stairs_88;
	public static ItemBlockMeta itemblock_stone_stairs_89;
	public static ItemBlockMeta itemblock_stone_stairs_90;
	public static ItemBlockMeta itemblock_stone_stairs_91;
	public static ItemBlockMeta itemblock_stone_stairs_92;
	public static ItemBlockMeta itemblock_stone_stairs_93;
	public static ItemBlockMeta itemblock_stone_stairs_94;
	public static ItemBlockMeta itemblock_stone_stairs_95;
	public static ItemBlockMeta itemblock_stone_stairs_96;
	public static ItemBlockMeta itemblock_stone_stairs_97;
	public static ItemBlockMeta itemblock_stone_stairs_98;
	public static ItemBlockMeta itemblock_stone_stairs_99;
	public static ItemBlockMeta itemblock_stone_stairs_100;
	public static ItemBlockMeta itemblock_stone_stairs_101;
	public static ItemBlockMeta itemblock_stone_stairs_102;
	public static ItemBlockMeta itemblock_stone_stairs_103;
	public static ItemBlockMeta itemblock_stone_stairs_104;
	public static ItemBlockMeta itemblock_stone_stairs_105;
	public static ItemBlockMeta itemblock_stone_stairs_106;
	public static ItemBlockMeta itemblock_stone_stairs_107;
	public static ItemBlockMeta itemblock_stone_stairs_108;
	public static ItemBlockMeta itemblock_stone_stairs_109;
	//Slab Stone Blocks
	public static ItemBlockMeta itemblock_stone_slab_1;
	public static ItemBlockMeta itemblock_stone_slab_2;
	public static ItemBlockMeta itemblock_stone_slab_3;
	public static ItemBlockMeta itemblock_stone_slab_4;
	public static ItemBlockMeta itemblock_stone_slab_5;
	public static ItemBlockMeta itemblock_stone_slab_6;
	public static ItemBlockMeta itemblock_stone_slab_7;
	public static ItemBlockMeta itemblock_stone_slab_8;
	public static ItemBlockMeta itemblock_stone_slab_9;
	public static ItemBlockMeta itemblock_stone_slab_10;
	public static ItemBlockMeta itemblock_stone_slab_11;
	public static ItemBlockMeta itemblock_stone_slab_12;
	public static ItemBlockMeta itemblock_stone_slab_13;
	public static ItemBlockMeta itemblock_stone_slab_14;
	public static ItemBlockMeta itemblock_stone_slab_15;
	public static ItemBlockMeta itemblock_stone_slab_16;
	public static ItemBlockMeta itemblock_stone_slab_17;
	public static ItemBlockMeta itemblock_stone_slab_18;
	public static ItemBlockMeta itemblock_stone_slab_19;
	public static ItemBlockMeta itemblock_stone_slab_20;
	//Stone Trapdoor Blocks
	public static ItemBlockMeta itemblock_stone_trapdoormodel_1;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_2;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_3;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_4;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_5;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_6;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_7;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_8;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_9;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_10;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_11;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_12;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_13;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_14;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_15;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_16;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_17;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_18;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_19;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_20;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_21;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_22;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_23;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_24;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_25;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_26;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_27;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_28;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_29;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_30;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_31;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_32;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_33;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_34;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_35;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_36;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_37;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_38;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_39;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_40;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_41;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_42;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_43;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_44;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_45;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_46;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_47;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_48;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_49;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_50;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_51;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_52;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_53;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_54;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_55;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_56;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_57;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_58;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_59;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_60;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_61;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_62;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_63;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_64;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_65;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_66;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_67;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_68;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_69;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_70;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_71;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_72;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_73;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_74;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_75;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_76;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_77;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_78;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_79;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_80;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_81;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_82;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_83;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_84;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_85;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_86;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_87;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_88;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_89;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_90;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_91;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_92;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_93;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_94;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_95;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_96;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_97;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_98;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_99;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_100;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_101;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_102;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_103;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_104;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_105;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_106;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_107;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_108;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_109;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_110;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_111;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_112;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_113;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_114;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_115;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_116;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_117;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_118;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_119;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_120;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_121;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_122;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_123;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_124;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_125;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_126;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_127;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_128;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_129;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_130;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_131;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_132;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_133;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_134;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_135;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_136;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_137;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_138;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_139;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_140;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_141;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_142;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_143;
	public static ItemBlockMeta itemblock_stone_trapdoormodel_144;

	//Carpet Stone Blocks
	public static ItemBlockMeta itemblock_stone_carpet_1;
	public static ItemBlockMeta itemblock_stone_carpet_2;
	public static ItemBlockMeta itemblock_stone_carpet_3;
	public static ItemBlockMeta itemblock_stone_carpet_4;
	//Stone Layer Blocks
	public static ItemLayerMeta itemblock_stone_layer_1;
	public static ItemLayerMeta itemblock_stone_layer_2;
	public static ItemLayerMeta itemblock_stone_layer_3;
	public static ItemLayerMeta itemblock_stone_layer_4;
	public static ItemLayerMeta itemblock_stone_layer_5;
	public static ItemLayerMeta itemblock_stone_layer_6;
	public static ItemLayerMeta itemblock_stone_layer_7;
	public static ItemLayerMeta itemblock_stone_layer_8;

	//Directional No Collision Stone Blocks
	public static ItemBlockMeta itemblock_stone_directionalnocollision_1;
	//No Collision Stone Blocks
	public static ItemBlockMeta itemblock_stone_nocollision_1;
	public static ItemBlockMeta itemblock_stone_nocollision_2;
	//Full Wood Blocks
	public static ItemBlockMeta itemblock_wood_full_1;
	public static ItemBlockMeta itemblock_wood_full_2;
	public static ItemBlockMeta itemblock_wood_full_3;
	public static ItemBlockMeta itemblock_wood_full_4;
	public static ItemBlockMeta itemblock_wood_full_5;
	public static ItemBlockMeta itemblock_wood_full_6;
	public static ItemBlockMeta itemblock_wood_full_7;
	//Full Slit Wood Blocks
	public static ItemBlockMeta itemblock_wood_fullslit_1;
	public static ItemBlockMeta itemblock_wood_fullslit_2;
	public static ItemBlockMeta itemblock_wood_fullslit_3;
	public static ItemBlockMeta itemblock_wood_fullslit_4;
	public static ItemBlockMeta itemblock_wood_fullslit_5;
	public static ItemBlockMeta itemblock_wood_fullslit_6;
	public static ItemBlockMeta itemblock_wood_fullslit_7;
	//Wood Hopper Full Blocks
	public static ItemBlockMeta itemblock_wood_hopperfull_1;
	//Wood Hopper Directional Blocks
	public static ItemBlockMeta itemblock_wood_hopperdirectional_1;
	public static ItemBlockMeta itemblock_wood_hopperdirectional_2;
	public static ItemBlockMeta itemblock_wood_hopperdirectional_3;
	public static ItemBlockMeta itemblock_wood_hopperdirectional_4;
	//Wood Log Blocks
	public static ItemBlockMeta itemblock_wood_log_1;
	public static ItemBlockMeta itemblock_wood_log_2;
	public static ItemBlockMeta itemblock_wood_log_3;
	public static ItemBlockMeta itemblock_wood_log_4;
	public static ItemBlockMeta itemblock_wood_log_5;
	public static ItemBlockMeta itemblock_wood_log_6;
	//Wood Anvil Blocks
	public static ItemBlockMeta itemblock_wood_anvil_1;
	public static ItemBlockMeta itemblock_wood_anvil_2;
	public static ItemBlockMeta itemblock_wood_anvil_3;
	public static ItemBlockMeta itemblock_wood_anvil_4;
	//Wood Wall Blocks
	public static ItemBlockMeta itemblock_wood_wall_1;
	public static ItemBlockMeta itemblock_wood_wall_2;
	public static ItemBlockMeta itemblock_wood_wall_3;
	public static ItemBlockMeta itemblock_wood_wall_4;
	//Wood Pillar Blocks
	public static ItemBlockMeta itemblock_wood_pillar_1;
	public static ItemBlockMeta itemblock_wood_pillar_2;
	public static ItemBlockMeta itemblock_wood_pillar_3;
	public static ItemBlockMeta itemblock_wood_pillar_4;
	//Fence Wood Blocks
	public static ItemBlockMeta itemblock_wood_fence_1;
	public static ItemBlockMeta itemblock_wood_fence_2;
	public static ItemBlockMeta itemblock_wood_fence_3;
	//Fencegate Wood Blocks
	public static ItemBlockMeta itemblock_wood_fencegate_1;
	public static ItemBlockMeta itemblock_wood_fencegate_2;
	public static ItemBlockMeta itemblock_wood_fencegate_3;
	public static ItemBlockMeta itemblock_wood_fencegate_4;
	public static ItemBlockMeta itemblock_wood_fencegate_5;
	public static ItemBlockMeta itemblock_wood_fencegate_6;
	public static ItemBlockMeta itemblock_wood_fencegate_7;
	public static ItemBlockMeta itemblock_wood_fencegate_8;
	public static ItemBlockMeta itemblock_wood_fencegate_9;
	public static ItemBlockMeta itemblock_wood_fencegate_10;
	public static ItemBlockMeta itemblock_wood_fencegate_11;
	public static ItemBlockMeta itemblock_wood_fencegate_12;
	//Small Pillar Wood Blocks
	public static ItemBlockMeta itemblock_wood_smallpillar_1;
	public static ItemBlockMeta itemblock_wood_smallpillar_2;
	public static ItemBlockMeta itemblock_wood_smallpillar_3;
	//Full Partial Wood Blocks
	public static ItemBlockMeta itemblock_wood_fullpartial_1;
	public static ItemBlockMeta itemblock_wood_fullpartial_2;
	//ConnectedXZ Wood Blocks
	public static ItemBlockMeta itemblock_wood_connectedxz_1;
	//Half Wood Blocks
	public static ItemBlockMeta itemblock_wood_half_1;
	//Daylight Detector Wood Blocks
	public static ItemBlockMeta itemblock_wood_daylightdetector_1;
	//Stairs Wood Blocks
	public static ItemBlockMeta itemblock_wood_stairs_1;
	public static ItemBlockMeta itemblock_wood_stairs_2;
	public static ItemBlockMeta itemblock_wood_stairs_3;
	public static ItemBlockMeta itemblock_wood_stairs_4;
	public static ItemBlockMeta itemblock_wood_stairs_5;
	public static ItemBlockMeta itemblock_wood_stairs_6;
	public static ItemBlockMeta itemblock_wood_stairs_7;
	public static ItemBlockMeta itemblock_wood_stairs_8;
	public static ItemBlockMeta itemblock_wood_stairs_9;
	public static ItemBlockMeta itemblock_wood_stairs_10;
	public static ItemBlockMeta itemblock_wood_stairs_11;
	public static ItemBlockMeta itemblock_wood_stairs_12;
	public static ItemBlockMeta itemblock_wood_stairs_13;
	public static ItemBlockMeta itemblock_wood_stairs_14;
	public static ItemBlockMeta itemblock_wood_stairs_15;
	public static ItemBlockMeta itemblock_wood_stairs_16;
	public static ItemBlockMeta itemblock_wood_stairs_17;
	public static ItemBlockMeta itemblock_wood_stairs_18;
	public static ItemBlockMeta itemblock_wood_stairs_19;
	public static ItemBlockMeta itemblock_wood_stairs_20;
	public static ItemBlockMeta itemblock_wood_stairs_21;
	public static ItemBlockMeta itemblock_wood_stairs_22;
	public static ItemBlockMeta itemblock_wood_stairs_23;
	public static ItemBlockMeta itemblock_wood_stairs_24;
	public static ItemBlockMeta itemblock_wood_stairs_25;
	public static ItemBlockMeta itemblock_wood_stairs_26;
	public static ItemBlockMeta itemblock_wood_stairs_27;
	//Slab Wood Blocks
	public static ItemBlockMeta itemblock_wood_slab_1;
	public static ItemBlockMeta itemblock_wood_slab_2;
	public static ItemBlockMeta itemblock_wood_slab_3;
	public static ItemBlockMeta itemblock_wood_slab_4;
	public static ItemBlockMeta itemblock_wood_slab_5;
	public static ItemBlockMeta itemblock_wood_slab_6;
	public static ItemBlockMeta itemblock_wood_slab_7;
	//Wood Trapdoor Blocks
	public static ItemBlockMeta itemblock_wood_trapdoormodel_1;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_2;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_3;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_4;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_5;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_6;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_7;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_8;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_9;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_10;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_11;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_12;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_13;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_14;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_15;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_16;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_17;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_18;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_19;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_20;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_21;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_22;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_23;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_24;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_25;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_26;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_27;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_28;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_29;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_30;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_31;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_32;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_33;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_34;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_35;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_36;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_37;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_38;
	public static ItemBlockMeta itemblock_wood_trapdoormodel_39;
	//Wood Vertical Slab Blocks
	public static ItemBlockMeta itemblock_wood_verticalslab_1;
	public static ItemBlockMeta itemblock_wood_verticalslab_2;
	public static ItemBlockMeta itemblock_wood_verticalslab_3;
	public static ItemBlockMeta itemblock_wood_verticalslab_4;
	public static ItemBlockMeta itemblock_wood_verticalslab_5;
	public static ItemBlockMeta itemblock_wood_verticalslab_6;
	public static ItemBlockMeta itemblock_wood_verticalslab_7;
	public static ItemBlockMeta itemblock_wood_verticalslab_8;
	public static ItemBlockMeta itemblock_wood_verticalslab_9;
	public static ItemBlockMeta itemblock_wood_verticalslab_10;
	public static ItemBlockMeta itemblock_wood_verticalslab_11;
	public static ItemBlockMeta itemblock_wood_verticalslab_12;
	public static ItemBlockMeta itemblock_wood_verticalslab_13;
	public static ItemBlockMeta itemblock_wood_verticalslab_14;
	public static ItemBlockMeta itemblock_wood_verticalslab_15;
	public static ItemBlockMeta itemblock_wood_verticalslab_16;
	public static ItemBlockMeta itemblock_wood_verticalslab_17;
	public static ItemBlockMeta itemblock_wood_verticalslab_18;
	public static ItemBlockMeta itemblock_wood_verticalslab_19;
	//Wood Corner Blocks
	public static ItemBlockMeta itemblock_wood_corner_1;
	public static ItemBlockMeta itemblock_wood_corner_2;
	public static ItemBlockMeta itemblock_wood_corner_3;
	public static ItemBlockMeta itemblock_wood_corner_4;
	public static ItemBlockMeta itemblock_wood_corner_5;
	public static ItemBlockMeta itemblock_wood_corner_6;
	public static ItemBlockMeta itemblock_wood_corner_7;
	public static ItemBlockMeta itemblock_wood_corner_8;
	public static ItemBlockMeta itemblock_wood_corner_9;
	public static ItemBlockMeta itemblock_wood_corner_10;
	public static ItemBlockMeta itemblock_wood_corner_11;
	public static ItemBlockMeta itemblock_wood_corner_12;
	public static ItemBlockMeta itemblock_wood_corner_13;
	public static ItemBlockMeta itemblock_wood_corner_14;
	public static ItemBlockMeta itemblock_wood_corner_15;
	public static ItemBlockMeta itemblock_wood_corner_16;
	public static ItemBlockMeta itemblock_wood_corner_17;
	public static ItemBlockMeta itemblock_wood_corner_18;
	public static ItemBlockMeta itemblock_wood_corner_19;
	//Wood Ironbar Blocks
	public static ItemBlockMeta itemblock_wood_ironbar_1;
	//Wood Carpet Blocks
	public static ItemBlockMeta itemblock_wood_carpet_1;
	//No Collision Wood Blocks
	public static ItemBlockMeta itemblock_wood_nocollision_1;
	//Directional No Collision Wood Blocks
	public static ItemBlockMeta itemblock_wood_directionalnocollision_1;
	//Directional Collision Trapdoor Wood Blocks
	public static ItemBlockMeta itemblock_wood_directionalcollisiontrapdoor_1;
	public static ItemBlockMeta itemblock_wood_directionalcollisiontrapdoor_2;
	public static ItemBlockMeta itemblock_wood_directionalcollisiontrapdoor_3;
	//Enchanted Wood Blocks
	public static ItemBlockMeta itemblock_wood_enchantedbook_1;
	//Full Iron Blocks
	public static ItemBlockMeta itemblock_iron_full_1;
	//Full Partial Light 1.0 Iron Blocks
	public static ItemBlockMeta itemblock_iron_fullpartiallight10_1;
	//Iron Anvil Blocks
	public static ItemBlockMeta itemblock_iron_anvil_1;
	//Iron Ironbar Blocks
	public static ItemBlockMeta itemblock_iron_ironbar_1;
	//Stairs Iron Blocks
	public static ItemBlockMeta itemblock_iron_stairs_1;
	public static ItemBlockMeta itemblock_iron_stairs_2;
	//Iron Trapdoor Blocks
	public static ItemBlockMeta itemblock_iron_trapdoormodel_1;
	public static ItemBlockMeta itemblock_iron_trapdoormodel_2;
	//Directional No Collision Iron Blocks
	public static ItemBlockMeta itemblock_iron_directionalnocollision_1;
	public static ItemBlockMeta itemblock_iron_directionalnocollision_2;
	//Plants Log Blocks
	public static ItemBlockMeta itemblock_plants_log_1;
	//Stairs Plants Blocks
	public static ItemBlockMeta itemblock_plants_stairs_1;
	//Slab Plants Block
	public static ItemBlockMeta itemblock_plants_slab_1;
	//No Collision Plants Block
	public static ItemBlockMeta itemblock_plants_nocollision_1;
	public static ItemBlockMeta itemblock_plants_nocollision_2;
	public static ItemBlockMeta itemblock_plants_nocollision_3;
	public static ItemBlockMeta itemblock_plants_nocollision_4;
	public static ItemBlockMeta itemblock_plants_nocollision_5;
	//No Collision Biome Plants Block
	public static ItemBlockMeta itemblock_plants_nocollisionbiome_1;
	//Full Leaves Block
	public static ItemBlockMeta itemblock_leaves_full_1;
	//Full Sand Blocks
	public static ItemBlockMeta itemblock_sand_full_1;
	//Sand Layer Blocks
	public static ItemLayerMeta itemblock_sand_layer_1;
	public static ItemLayerMeta itemblock_sand_layer_2;
	public static ItemLayerMeta itemblock_sand_layer_3;
	public static ItemLayerMeta itemblock_sand_layer_4;
	public static ItemLayerMeta itemblock_sand_layer_5;
	public static ItemLayerMeta itemblock_sand_layer_6;
	//Full Ground Blocks
	public static ItemBlockMeta itemblock_ground_full_1;
	public static ItemBlockMeta itemblock_ground_full_2;
	//Soulsand Ground Blocks
	public static ItemBlockMeta itemblock_ground_soulsand_1;
	//Directional Full Partial Ground Blocks
	public static ItemBlockMeta itemblock_ground_directionalfullpartial_1;
	//Ground Layer Blocks
	public static ItemLayerMeta itemblock_ground_layer_1;
	public static ItemLayerMeta itemblock_ground_layer_2;
	public static ItemLayerMeta itemblock_ground_layer_3;
	public static ItemLayerMeta itemblock_ground_layer_4;
	public static ItemLayerMeta itemblock_ground_layer_5;
	public static ItemLayerMeta itemblock_ground_layer_6;
	public static ItemLayerMeta itemblock_ground_layer_7;
	public static ItemLayerMeta itemblock_ground_layer_8;
	public static ItemLayerMeta itemblock_ground_layer_9;
	public static ItemLayerMeta itemblock_ground_layer_10;
	public static ItemLayerMeta itemblock_ground_layer_11;
	public static ItemLayerMeta itemblock_ground_layer_12;
	public static ItemLayerMeta itemblock_ground_layer_13;
	//Translucent Ice Blocks
	public static ItemBlockMeta itemblock_ice_translucent_1;
	//Slab Ice Blocks
	public static ItemBlockMeta itemblock_ice_slab_1;
	//No Collision Ice Blocks
	public static ItemBlockMeta itemblock_ice_nocollision_1;
	//Stairs Snow Blocks
	public static ItemBlockMeta itemblock_snow_stairs_1;
	//Slab Cloth Blocks
	public static ItemBlockMeta itemblock_cloth_slab_1;
	public static ItemBlockMeta itemblock_cloth_slab_2;
	//Pane Cloth Blocks
	public static ItemBlockMeta itemblock_cloth_pane_1;
	//Climbable Iron Bar Cloth Blocks
	public static ItemBlockMeta itemblock_cloth_climbironbar_1;
	//Climbable Iron Bar Iron/Cloth Blocks
	public static ItemBlockMeta itemblock_cloth_ironclimbironbar_1;
	//Full Light 1.0 Cloth Blocks
	public static ItemBlockMeta itemblock_cloth_fulllight10_1;
	//Directional No Collision Cloth Blocks
	public static ItemBlockMeta itemblock_cloth_directionalnocollision_1;
	//No Collision Cloth Blocks
	public static ItemBlockMeta itemblock_cloth_nocollision_1;
	//No Collision No Material Blocks
	public static ItemBlockMeta itemblock_nomaterial_nocollision_1;
	//Glass Full Blocks
	public static ItemBlockMeta itemblock_glass_full_1;
	//Glass Glass Blocks
	public static ItemBlockMeta itemblock_glass_glass_1;
	public static ItemBlockMeta itemblock_glass_glass_2;
	//Glass Vertical Slab Blocks
	public static ItemBlockMeta itemblock_glass_verticalslab_1;
	//Glass Trapdoor Blocks
	public static ItemBlockMeta itemblock_glass_trapdoormodel_1;
	public static ItemBlockMeta itemblock_glass_trapdoormodel_2;
	public static ItemBlockMeta itemblock_glass_trapdoormodel_3;
	public static ItemBlockMeta itemblock_glass_trapdoormodel_4;
	//Glass Pane Blocks
	public static ItemBlockMeta itemblock_glass_pane_1;
	public static ItemBlockMeta itemblock_glass_pane_2;

	//Full Stone Blocks
	private static void registerBlockStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_full_1 = new ItemBlockMeta(block);
		itemblock_stone_full_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_full_1);
    }

	private static void registerBlockStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_full_2 = new ItemBlockMeta(block);
		itemblock_stone_full_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_full_2);
	}

	private static void registerBlockStone3(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_full_3 = new ItemBlockMeta(block);
		itemblock_stone_full_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_full_3);
	}

	private static void registerBlockStone4(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_full_4 = new ItemBlockMeta(block);
		itemblock_stone_full_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_full_4);
	}

	private static void registerBlockStone5(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_full_5 = new ItemBlockMeta(block);
		itemblock_stone_full_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_full_5);
	}

	private static void registerBlockStone6(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_full_6 = new ItemBlockMeta(block);
		itemblock_stone_full_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_full_6);
	}

	private static void registerBlockStone7(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_full_7 = new ItemBlockMeta(block);
		itemblock_stone_full_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_full_7);
	}

	private static void registerBlockStone8(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_full_8 = new ItemBlockMeta(block);
		itemblock_stone_full_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_full_8);
	}

	private static void registerBlockStone9(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_full_9 = new ItemBlockMeta(block);
		itemblock_stone_full_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_full_9);
	}

	private static void registerBlockStone10(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_full_10 = new ItemBlockMeta(block);
		itemblock_stone_full_10.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_full_10);
	}

	private static void registerBlockStone11(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_full_11 = new ItemBlockMeta(block);
		itemblock_stone_full_11.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_full_11);
	}

	private static void registerBlockStone12(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_full_12 = new ItemBlockMeta(block);
		itemblock_stone_full_12.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_full_12);
	}

	private static void registerBlockStone13(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_full_13 = new ItemBlockMeta(block);
		itemblock_stone_full_13.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_full_13);
	}

	private static void registerBlockStone14(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_full_14 = new ItemBlockMeta(block);
		itemblock_stone_full_14.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_full_14);
	}

	private static void registerBlockStone15(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_full_15 = new ItemBlockMeta(block);
		itemblock_stone_full_15.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_full_15);
	}

	private static void registerBlockStone16(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_full_16 = new ItemBlockMeta(block);
		itemblock_stone_full_16.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_full_16);
	}

	private static void registerBlockStone17(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_full_17 = new ItemBlockMeta(block);
		itemblock_stone_full_17.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_full_17);
	}

	//Partial Full Stone Blocks
	private static void registerBlockFullPartialStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullpartial_1 = new ItemBlockMeta(block);
		itemblock_stone_fullpartial_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullpartial_1);
	}

	private static void registerBlockFullPartialStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullpartial_2 = new ItemBlockMeta(block);
		itemblock_stone_fullpartial_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullpartial_2);
	}

	private static void registerBlockFullPartialStone3(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullpartial_3 = new ItemBlockMeta(block);
		itemblock_stone_fullpartial_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullpartial_3);
	}

	private static void registerBlockFullPartialStone4(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullpartial_4 = new ItemBlockMeta(block);
		itemblock_stone_fullpartial_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullpartial_4);
	}

	private static void registerBlockFullPartialStone5(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullpartial_5 = new ItemBlockMeta(block);
		itemblock_stone_fullpartial_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullpartial_5);
	}

	private static void registerBlockFullPartialStone6(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullpartial_6 = new ItemBlockMeta(block);
		itemblock_stone_fullpartial_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullpartial_6);
	}
	//Directional Partial Full Stone Blocks
	private static void registerBlockDirectionalFullPartialStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_directionalfullpartial_1 = new ItemBlockMeta(block);
		itemblock_stone_directionalfullpartial_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_directionalfullpartial_1);
	}
	//Corner Stone Blocks
	private static void registerBlockCornerStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_1 = new ItemBlockMeta(block);
		itemblock_stone_corner_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_1);
	}

	private static void registerBlockCornerStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_2 = new ItemBlockMeta(block);
		itemblock_stone_corner_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_2);
	}

	private static void registerBlockCornerStone3(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_3 = new ItemBlockMeta(block);
		itemblock_stone_corner_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_3);
	}

	private static void registerBlockCornerStone4(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_4 = new ItemBlockMeta(block);
		itemblock_stone_corner_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_4);
	}

	private static void registerBlockCornerStone5(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_5 = new ItemBlockMeta(block);
		itemblock_stone_corner_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_5);
	}

	private static void registerBlockCornerStone6(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_6 = new ItemBlockMeta(block);
		itemblock_stone_corner_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_6);
	}

	private static void registerBlockCornerStone7(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_7 = new ItemBlockMeta(block);
		itemblock_stone_corner_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_7);
	}

	private static void registerBlockCornerStone8(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_8 = new ItemBlockMeta(block);
		itemblock_stone_corner_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_8);
	}

	private static void registerBlockCornerStone9(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_9 = new ItemBlockMeta(block);
		itemblock_stone_corner_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_9);
	}

	private static void registerBlockCornerStone10(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_10 = new ItemBlockMeta(block);
		itemblock_stone_corner_10.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_10);
	}

	private static void registerBlockCornerStone11(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_11 = new ItemBlockMeta(block);
		itemblock_stone_corner_11.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_11);
	}

	private static void registerBlockCornerStone12(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_12 = new ItemBlockMeta(block);
		itemblock_stone_corner_12.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_12);
	}

	private static void registerBlockCornerStone13(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_13 = new ItemBlockMeta(block);
		itemblock_stone_corner_13.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_13);
	}

	private static void registerBlockCornerStone14(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_14 = new ItemBlockMeta(block);
		itemblock_stone_corner_14.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_14);
	}

	private static void registerBlockCornerStone15(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_15 = new ItemBlockMeta(block);
		itemblock_stone_corner_15.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_15);
	}

	private static void registerBlockCornerStone16(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_16 = new ItemBlockMeta(block);
		itemblock_stone_corner_16.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_16);
	}

	private static void registerBlockCornerStone17(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_17 = new ItemBlockMeta(block);
		itemblock_stone_corner_17.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_17);
	}

	private static void registerBlockCornerStone18(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_18 = new ItemBlockMeta(block);
		itemblock_stone_corner_18.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_18);
	}

	private static void registerBlockCornerStone19(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_19 = new ItemBlockMeta(block);
		itemblock_stone_corner_19.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_19);
	}

	private static void registerBlockCornerStone20(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_20 = new ItemBlockMeta(block);
		itemblock_stone_corner_20.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_20);
	}

	private static void registerBlockCornerStone21(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_21 = new ItemBlockMeta(block);
		itemblock_stone_corner_21.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_21);
	}

	private static void registerBlockCornerStone22(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_22 = new ItemBlockMeta(block);
		itemblock_stone_corner_22.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_22);
	}

	private static void registerBlockCornerStone23(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_23 = new ItemBlockMeta(block);
		itemblock_stone_corner_23.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_23);
	}

	private static void registerBlockCornerStone24(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_24 = new ItemBlockMeta(block);
		itemblock_stone_corner_24.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_24);
	}

	private static void registerBlockCornerStone25(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_25 = new ItemBlockMeta(block);
		itemblock_stone_corner_25.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_25);
	}

	private static void registerBlockCornerStone26(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_26 = new ItemBlockMeta(block);
		itemblock_stone_corner_26.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_26);
	}

	private static void registerBlockCornerStone27(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_27 = new ItemBlockMeta(block);
		itemblock_stone_corner_27.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_27);
	}

	private static void registerBlockCornerStone28(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_28 = new ItemBlockMeta(block);
		itemblock_stone_corner_28.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_28);
	}

	private static void registerBlockCornerStone29(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_29 = new ItemBlockMeta(block);
		itemblock_stone_corner_29.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_29);
	}

	private static void registerBlockCornerStone30(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_30 = new ItemBlockMeta(block);
		itemblock_stone_corner_30.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_30);
	}

	private static void registerBlockCornerStone31(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_31 = new ItemBlockMeta(block);
		itemblock_stone_corner_31.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_31);
	}

	private static void registerBlockCornerStone32(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_32 = new ItemBlockMeta(block);
		itemblock_stone_corner_32.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_32);
	}

	private static void registerBlockCornerStone33(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_33 = new ItemBlockMeta(block);
		itemblock_stone_corner_33.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_33);
	}

	private static void registerBlockCornerStone34(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_34 = new ItemBlockMeta(block);
		itemblock_stone_corner_34.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_34);
	}

	private static void registerBlockCornerStone35(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_35 = new ItemBlockMeta(block);
		itemblock_stone_corner_35.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_35);
	}

	private static void registerBlockCornerStone36(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_36 = new ItemBlockMeta(block);
		itemblock_stone_corner_36.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_36);
	}

	private static void registerBlockCornerStone37(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_37 = new ItemBlockMeta(block);
		itemblock_stone_corner_37.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_37);
	}

	private static void registerBlockCornerStone38(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_38 = new ItemBlockMeta(block);
		itemblock_stone_corner_38.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_38);
	}

	private static void registerBlockCornerStone39(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_39 = new ItemBlockMeta(block);
		itemblock_stone_corner_39.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_39);
	}

	private static void registerBlockCornerStone40(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_40 = new ItemBlockMeta(block);
		itemblock_stone_corner_40.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_40);
	}

	private static void registerBlockCornerStone41(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_41 = new ItemBlockMeta(block);
		itemblock_stone_corner_41.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_41);
	}

	private static void registerBlockCornerStone42(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_42 = new ItemBlockMeta(block);
		itemblock_stone_corner_42.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_42);
	}

	private static void registerBlockCornerStone43(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_43 = new ItemBlockMeta(block);
		itemblock_stone_corner_43.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_43);
	}

	private static void registerBlockCornerStone44(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_44 = new ItemBlockMeta(block);
		itemblock_stone_corner_44.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_44);
	}

	private static void registerBlockCornerStone45(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_45 = new ItemBlockMeta(block);
		itemblock_stone_corner_45.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_45);
	}

	private static void registerBlockCornerStone46(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_46 = new ItemBlockMeta(block);
		itemblock_stone_corner_46.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_46);
	}

	private static void registerBlockCornerStone47(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_47 = new ItemBlockMeta(block);
		itemblock_stone_corner_47.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_47);
	}

	private static void registerBlockCornerStone48(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_48 = new ItemBlockMeta(block);
		itemblock_stone_corner_48.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_48);
	}

	private static void registerBlockCornerStone49(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_49 = new ItemBlockMeta(block);
		itemblock_stone_corner_49.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_49);
	}

	private static void registerBlockCornerStone50(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_50 = new ItemBlockMeta(block);
		itemblock_stone_corner_50.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_50);
	}

	private static void registerBlockCornerStone51(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_51 = new ItemBlockMeta(block);
		itemblock_stone_corner_51.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_51);
	}

	private static void registerBlockCornerStone52(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_52 = new ItemBlockMeta(block);
		itemblock_stone_corner_52.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_52);
	}

	private static void registerBlockCornerStone53(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_53 = new ItemBlockMeta(block);
		itemblock_stone_corner_53.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_53);
	}

	private static void registerBlockCornerStone54(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_54 = new ItemBlockMeta(block);
		itemblock_stone_corner_54.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_54);
	}

	private static void registerBlockCornerStone55(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_55 = new ItemBlockMeta(block);
		itemblock_stone_corner_55.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_55);
	}

	private static void registerBlockCornerStone56(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_56 = new ItemBlockMeta(block);
		itemblock_stone_corner_56.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_56);
	}

	private static void registerBlockCornerStone57(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_57 = new ItemBlockMeta(block);
		itemblock_stone_corner_57.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_57);
	}

	private static void registerBlockCornerStone58(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_58 = new ItemBlockMeta(block);
		itemblock_stone_corner_58.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_58);
	}

	private static void registerBlockCornerStone59(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_59 = new ItemBlockMeta(block);
		itemblock_stone_corner_59.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_59);
	}

	private static void registerBlockCornerStone60(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_60 = new ItemBlockMeta(block);
		itemblock_stone_corner_60.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_60);
	}

	private static void registerBlockCornerStone61(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_61 = new ItemBlockMeta(block);
		itemblock_stone_corner_61.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_61);
	}

	private static void registerBlockCornerStone62(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_62 = new ItemBlockMeta(block);
		itemblock_stone_corner_62.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_62);
	}

	private static void registerBlockCornerStone63(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_63 = new ItemBlockMeta(block);
		itemblock_stone_corner_63.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_63);
	}

	private static void registerBlockCornerStone64(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_corner_64 = new ItemBlockMeta(block);
		itemblock_stone_corner_64.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_corner_64);
	}



	//Vertical Slab Stone Blocks
	private static void registerBlockVerticalSlabStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_1 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_1);
	}

	private static void registerBlockVerticalSlabStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_2 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_2);
	}

	private static void registerBlockVerticalSlabStone3(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_3 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_3);
	}

	private static void registerBlockVerticalSlabStone4(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_4 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_4);
	}

	private static void registerBlockVerticalSlabStone5(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_5 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_5);
	}

	private static void registerBlockVerticalSlabStone6(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_6 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_6);
	}

	private static void registerBlockVerticalSlabStone7(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_7 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_7);
	}

	private static void registerBlockVerticalSlabStone8(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_8 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_8);
	}

	private static void registerBlockVerticalSlabStone9(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_9 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_9);
	}

	private static void registerBlockVerticalSlabStone10(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_10 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_10.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_10);
	}

	private static void registerBlockVerticalSlabStone11(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_11 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_11.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_11);
	}

	private static void registerBlockVerticalSlabStone12(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_12 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_12.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_12);
	}

	private static void registerBlockVerticalSlabStone13(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_13 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_13.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_13);
	}

	private static void registerBlockVerticalSlabStone14(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_14 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_14.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_14);
	}

	private static void registerBlockVerticalSlabStone15(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_15 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_15.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_15);
	}

	private static void registerBlockVerticalSlabStone16(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_16 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_16.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_16);
	}

	private static void registerBlockVerticalSlabStone17(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_17 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_17.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_17);
	}

	private static void registerBlockVerticalSlabStone18(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_18 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_18.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_18);
	}

	private static void registerBlockVerticalSlabStone19(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_19 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_19.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_19);
	}

	private static void registerBlockVerticalSlabStone20(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_20 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_20.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_20);
	}

	private static void registerBlockVerticalSlabStone21(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_21 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_21.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_21);
	}

	private static void registerBlockVerticalSlabStone22(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_22 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_22.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_22);
	}

	private static void registerBlockVerticalSlabStone23(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_23 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_23.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_23);
	}

	private static void registerBlockVerticalSlabStone24(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_24 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_24.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_24);
	}

	private static void registerBlockVerticalSlabStone25(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_25 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_25.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_25);
	}

	private static void registerBlockVerticalSlabStone26(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_26 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_26.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_26);
	}

	private static void registerBlockVerticalSlabStone27(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_27 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_27.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_27);
	}

	private static void registerBlockVerticalSlabStone28(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_28 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_28.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_28);
	}

	private static void registerBlockVerticalSlabStone29(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_29 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_29.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_29);
	}

	private static void registerBlockVerticalSlabStone30(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_30 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_30.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_30);
	}

	private static void registerBlockVerticalSlabStone31(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_31 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_31.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_31);
	}

	private static void registerBlockVerticalSlabStone32(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_32 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_32.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_32);
	}

	private static void registerBlockVerticalSlabStone33(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_33 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_33.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_33);
	}

	private static void registerBlockVerticalSlabStone34(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_34 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_34.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_34);
	}

	private static void registerBlockVerticalSlabStone35(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_35 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_35.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_35);
	}

	private static void registerBlockVerticalSlabStone36(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_36 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_36.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_36);
	}

	private static void registerBlockVerticalSlabStone37(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_37 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_37.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_37);
	}

	private static void registerBlockVerticalSlabStone38(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_38 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_38.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_38);
	}

	private static void registerBlockVerticalSlabStone39(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_39 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_39.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_39);
	}

	private static void registerBlockVerticalSlabStone40(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_40 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_40.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_40);
	}

	private static void registerBlockVerticalSlabStone41(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_41 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_41.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_41);
	}

	private static void registerBlockVerticalSlabStone42(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_42 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_42.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_42);
	}

	private static void registerBlockVerticalSlabStone43(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_43 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_43.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_43);
	}

	private static void registerBlockVerticalSlabStone44(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_44 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_44.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_44);
	}

	private static void registerBlockVerticalSlabStone45(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_45 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_45.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_45);
	}

	private static void registerBlockVerticalSlabStone46(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_46 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_46.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_46);
	}

	private static void registerBlockVerticalSlabStone47(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_47 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_47.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_47);
	}

	private static void registerBlockVerticalSlabStone48(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_48 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_48.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_48);
	}

	private static void registerBlockVerticalSlabStone49(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_49 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_49.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_49);
	}

	private static void registerBlockVerticalSlabStone50(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_50 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_50.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_50);
	}

	private static void registerBlockVerticalSlabStone51(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_51 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_51.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_51);
	}

	private static void registerBlockVerticalSlabStone52(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_52 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_52.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_52);
	}

	private static void registerBlockVerticalSlabStone53(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_53 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_53.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_53);
	}

	private static void registerBlockVerticalSlabStone54(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_54 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_54.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_54);
	}

	private static void registerBlockVerticalSlabStone55(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_55 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_55.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_55);
	}

	private static void registerBlockVerticalSlabStone56(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_56 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_56.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_56);
	}

	private static void registerBlockVerticalSlabStone57(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_57 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_57.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_57);
	}

	private static void registerBlockVerticalSlabStone58(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_58 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_58.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_58);
	}

	private static void registerBlockVerticalSlabStone59(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_59 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_59.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_59);
	}

	private static void registerBlockVerticalSlabStone60(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_60 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_60.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_60);
	}

	private static void registerBlockVerticalSlabStone61(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_61 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_61.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_61);
	}

	private static void registerBlockVerticalSlabStone62(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_62 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_62.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_62);
	}

	private static void registerBlockVerticalSlabStone63(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_63 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_63.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_63);
	}

	private static void registerBlockVerticalSlabStone64(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_64 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_64.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_64);
	}

	private static void registerBlockVerticalSlabStone65(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_65 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_65.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_65);
	}

	private static void registerBlockVerticalSlabStone66(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_66 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_66.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_66);
	}

	private static void registerBlockVerticalSlabStone67(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_67 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_67.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_67);
	}

	private static void registerBlockVerticalSlabStone68(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_68 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_68.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_68);
	}

	private static void registerBlockVerticalSlabStone69(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_69 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_69.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_69);
	}

	private static void registerBlockVerticalSlabStone70(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_70 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_70.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_70);
	}

	private static void registerBlockVerticalSlabStone71(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_verticalslab_71 = new ItemBlockMeta(block);
		itemblock_stone_verticalslab_71.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_verticalslab_71);
	}

	//Arrow Slit Stone Blocks
	private static void registerBlockArrowSlitStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_1 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_1);
	}

	private static void registerBlockArrowSlitStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_2 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_2);
	}

	private static void registerBlockArrowSlitStone3(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_3 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_3);
	}

	private static void registerBlockArrowSlitStone4(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_4 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_4);
	}

	private static void registerBlockArrowSlitStone5(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_5 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_5);
	}

	private static void registerBlockArrowSlitStone6(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_6 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_6);
	}

	private static void registerBlockArrowSlitStone7(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_7 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_7);
	}

	private static void registerBlockArrowSlitStone8(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_8 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_8);
	}

	private static void registerBlockArrowSlitStone9(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_9 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_9);
	}

	private static void registerBlockArrowSlitStone10(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_10 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_10.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_10);
	}

	private static void registerBlockArrowSlitStone11(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_11 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_11.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_11);
	}

	private static void registerBlockArrowSlitStone12(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_12 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_12.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_12);
	}

	private static void registerBlockArrowSlitStone13(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_13 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_13.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_13);
	}

	private static void registerBlockArrowSlitStone14(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_14 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_14.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_14);
	}

	private static void registerBlockArrowSlitStone15(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_15 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_15.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_15);
	}

	private static void registerBlockArrowSlitStone16(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_16 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_16.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_16);
	}

	private static void registerBlockArrowSlitStone17(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_17 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_17.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_17);
	}

	private static void registerBlockArrowSlitStone18(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_18 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_18.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_18);
	}

	private static void registerBlockArrowSlitStone19(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_19 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_19.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_19);
	}

	private static void registerBlockArrowSlitStone20(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_20 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_20.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_20);
	}

	private static void registerBlockArrowSlitStone21(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_21 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_21.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_21);
	}

	private static void registerBlockArrowSlitStone22(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_22 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_22.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_22);
	}

	private static void registerBlockArrowSlitStone23(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_23 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_23.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_23);
	}

	private static void registerBlockArrowSlitStone24(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_arrowslit_24 = new ItemBlockMeta(block);
		itemblock_stone_arrowslit_24.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_arrowslit_24);
	}

	//Full Slit Stone Blocks
	private static void registerBlockFullSlitStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_1 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_1);
	}

	private static void registerBlockFullSlitStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_2 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_2);
	}

	private static void registerBlockFullSlitStone3(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_3 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_3);
	}

	private static void registerBlockFullSlitStone4(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_4 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_4);
	}

	private static void registerBlockFullSlitStone5(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_5 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_5);
	}

	private static void registerBlockFullSlitStone6(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_6 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_6);
	}

	private static void registerBlockFullSlitStone7(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_7 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_7);
	}

	private static void registerBlockFullSlitStone8(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_8 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_8);
	}

	private static void registerBlockFullSlitStone9(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_9 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_9);
	}

	private static void registerBlockFullSlitStone10(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_10 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_10.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_10);
	}

	private static void registerBlockFullSlitStone11(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_11 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_11.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_11);
	}

	private static void registerBlockFullSlitStone12(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_12 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_12.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_12);
	}

	private static void registerBlockFullSlitStone13(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_13 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_13.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_13);
	}

	private static void registerBlockFullSlitStone14(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_14 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_14.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_14);
	}

	private static void registerBlockFullSlitStone15(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_15 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_15.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_15);
	}

	private static void registerBlockFullSlitStone16(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_16 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_16.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_16);
	}

	private static void registerBlockFullSlitStone17(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_17 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_17.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_17);
	}

	private static void registerBlockFullSlitStone18(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_18 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_18.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_18);
	}

	private static void registerBlockFullSlitStone19(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_19 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_19.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_19);
	}

	private static void registerBlockFullSlitStone20(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_20 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_20.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_20);
	}

	private static void registerBlockFullSlitStone21(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_21 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_21.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_21);
	}

	private static void registerBlockFullSlitStone22(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_22 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_22.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_22);
	}

	private static void registerBlockFullSlitStone23(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_23 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_23.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_23);
	}

	private static void registerBlockFullSlitStone24(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fullslit_24 = new ItemBlockMeta(block);
		itemblock_stone_fullslit_24.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fullslit_24);
	}

	//Anvil Stone Blocks
	private static void registerBlockAnvilStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_1 = new ItemBlockMeta(block);
		itemblock_stone_anvil_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_1);
	}

	private static void registerBlockAnvilStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_2 = new ItemBlockMeta(block);
		itemblock_stone_anvil_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_2);
	}

	private static void registerBlockAnvilStone3(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_3 = new ItemBlockMeta(block);
		itemblock_stone_anvil_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_3);
	}

	private static void registerBlockAnvilStone4(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_4 = new ItemBlockMeta(block);
		itemblock_stone_anvil_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_4);
	}

	private static void registerBlockAnvilStone5(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_5 = new ItemBlockMeta(block);
		itemblock_stone_anvil_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_5);
	}

	private static void registerBlockAnvilStone6(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_6 = new ItemBlockMeta(block);
		itemblock_stone_anvil_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_6);
	}

	private static void registerBlockAnvilStone7(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_7 = new ItemBlockMeta(block);
		itemblock_stone_anvil_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_7);
	}

	private static void registerBlockAnvilStone8(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_8 = new ItemBlockMeta(block);
		itemblock_stone_anvil_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_8);
	}

	private static void registerBlockAnvilStone9(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_9 = new ItemBlockMeta(block);
		itemblock_stone_anvil_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_9);
	}

	private static void registerBlockAnvilStone10(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_10 = new ItemBlockMeta(block);
		itemblock_stone_anvil_10.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_10);
	}

	private static void registerBlockAnvilStone11(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_11 = new ItemBlockMeta(block);
		itemblock_stone_anvil_11.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_11);
	}

	private static void registerBlockAnvilStone12(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_12 = new ItemBlockMeta(block);
		itemblock_stone_anvil_12.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_12);
	}

	private static void registerBlockAnvilStone13(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_13 = new ItemBlockMeta(block);
		itemblock_stone_anvil_13.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_13);
	}

	private static void registerBlockAnvilStone14(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_14 = new ItemBlockMeta(block);
		itemblock_stone_anvil_14.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_14);
	}

	private static void registerBlockAnvilStone15(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_15 = new ItemBlockMeta(block);
		itemblock_stone_anvil_15.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_15);
	}

	private static void registerBlockAnvilStone16(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_16 = new ItemBlockMeta(block);
		itemblock_stone_anvil_16.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_16);
	}

	private static void registerBlockAnvilStone17(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_17 = new ItemBlockMeta(block);
		itemblock_stone_anvil_17.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_17);
	}

	private static void registerBlockAnvilStone18(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_18 = new ItemBlockMeta(block);
		itemblock_stone_anvil_18.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_18);
	}

	private static void registerBlockAnvilStone19(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_19 = new ItemBlockMeta(block);
		itemblock_stone_anvil_19.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_19);
	}

	private static void registerBlockAnvilStone20(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_20 = new ItemBlockMeta(block);
		itemblock_stone_anvil_20.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_20);
	}

	private static void registerBlockAnvilStone21(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_21 = new ItemBlockMeta(block);
		itemblock_stone_anvil_21.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_21);
	}

	private static void registerBlockAnvilStone22(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_22 = new ItemBlockMeta(block);
		itemblock_stone_anvil_22.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_22);
	}

	private static void registerBlockAnvilStone23(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_23 = new ItemBlockMeta(block);
		itemblock_stone_anvil_23.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_23);
	}

	private static void registerBlockAnvilStone24(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_24 = new ItemBlockMeta(block);
		itemblock_stone_anvil_24.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_24);
	}

	private static void registerBlockAnvilStone25(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_25 = new ItemBlockMeta(block);
		itemblock_stone_anvil_25.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_25);
	}

	private static void registerBlockAnvilStone26(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_26 = new ItemBlockMeta(block);
		itemblock_stone_anvil_26.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_26);
	}

	private static void registerBlockAnvilStone27(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_27 = new ItemBlockMeta(block);
		itemblock_stone_anvil_27.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_27);
	}

	private static void registerBlockAnvilStone28(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_anvil_28 = new ItemBlockMeta(block);
		itemblock_stone_anvil_28.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_anvil_28);
	}

	//Hopper Full Stone Blocks
	private static void registerBlockHopperFullStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperfull_1 = new ItemBlockMeta(block);
		itemblock_stone_hopperfull_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperfull_1);
	}
	private static void registerBlockHopperFullStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperfull_2 = new ItemBlockMeta(block);
		itemblock_stone_hopperfull_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperfull_2);
	}
	private static void registerBlockHopperFullStone3(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperfull_3 = new ItemBlockMeta(block);
		itemblock_stone_hopperfull_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperfull_3);
	}
	private static void registerBlockHopperFullStone4(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperfull_4 = new ItemBlockMeta(block);
		itemblock_stone_hopperfull_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperfull_4);
	}
	private static void registerBlockHopperFullStone5(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperfull_5 = new ItemBlockMeta(block);
		itemblock_stone_hopperfull_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperfull_5);
	}
	private static void registerBlockHopperFullStone6(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperfull_6 = new ItemBlockMeta(block);
		itemblock_stone_hopperfull_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperfull_6);
	}

	private static void registerBlockHopperFullStone7(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperfull_7 = new ItemBlockMeta(block);
		itemblock_stone_hopperfull_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperfull_7);
	}

	//Hopper Directional Stone Blocks
	private static void registerBlockHopperDirectionalStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_1 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_1);
	}

	private static void registerBlockHopperDirectionalStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_2 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_2);
	}

	private static void registerBlockHopperDirectionalStone3(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_3 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_3);
	}

	private static void registerBlockHopperDirectionalStone4(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_4 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_4);
	}

	private static void registerBlockHopperDirectionalStone5(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_5 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_5);
	}

	private static void registerBlockHopperDirectionalStone6(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_6 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_6);
	}

	private static void registerBlockHopperDirectionalStone7(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_7 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_7);
	}

	private static void registerBlockHopperDirectionalStone8(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_8 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_8);
	}

	private static void registerBlockHopperDirectionalStone9(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_9 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_9);
	}

	private static void registerBlockHopperDirectionalStone10(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_10 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_10.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_10);
	}

	private static void registerBlockHopperDirectionalStone11(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_11 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_11.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_11);
	}

	private static void registerBlockHopperDirectionalStone12(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_12 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_12.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_12);
	}

	private static void registerBlockHopperDirectionalStone13(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_13 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_13.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_13);
	}

	private static void registerBlockHopperDirectionalStone14(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_14 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_14.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_14);
	}

	private static void registerBlockHopperDirectionalStone15(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_15 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_15.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_15);
	}

	private static void registerBlockHopperDirectionalStone16(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_16 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_16.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_16);
	}

	private static void registerBlockHopperDirectionalStone17(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_17 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_17.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_17);
	}

	private static void registerBlockHopperDirectionalStone18(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_18 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_18.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_18);
	}

	private static void registerBlockHopperDirectionalStone19(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_19 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_19.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_19);
	}

	private static void registerBlockHopperDirectionalStone20(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_20 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_20.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_20);
	}

	private static void registerBlockHopperDirectionalStone21(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_21 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_21.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_21);
	}

	private static void registerBlockHopperDirectionalStone22(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_22 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_22.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_22);
	}

	private static void registerBlockHopperDirectionalStone23(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_23 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_23.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_23);
	}

	private static void registerBlockHopperDirectionalStone24(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_24 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_24.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_24);
	}

	private static void registerBlockHopperDirectionalStone25(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_hopperdirectional_25 = new ItemBlockMeta(block);
		itemblock_stone_hopperdirectional_25.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_hopperdirectional_25);
	}

	//Log Stone Blocks
	private static void registerBlockLogStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_log_1 = new ItemBlockMeta(block);
		itemblock_stone_log_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_log_1);
	}

	private static void registerBlockLogStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_log_2 = new ItemBlockMeta(block);
		itemblock_stone_log_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_log_2);
	}

	//Wall Stone Blocks
	private static void registerBlockWallStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_wall_1 = new ItemBlockMeta(block);
		itemblock_stone_wall_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_wall_1);
	}

	private static void registerBlockWallStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_wall_2 = new ItemBlockMeta(block);
		itemblock_stone_wall_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_wall_2);
	}

	private static void registerBlockWallStone3(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_wall_3 = new ItemBlockMeta(block);
		itemblock_stone_wall_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_wall_3);
	}

	private static void registerBlockWallStone4(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_wall_4 = new ItemBlockMeta(block);
		itemblock_stone_wall_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_wall_4);
	}

	private static void registerBlockWallStone5(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_wall_5 = new ItemBlockMeta(block);
		itemblock_stone_wall_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_wall_5);
	}

	private static void registerBlockWallStone6(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_wall_6 = new ItemBlockMeta(block);
		itemblock_stone_wall_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_wall_6);
	}

	private static void registerBlockWallStone7(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_wall_7 = new ItemBlockMeta(block);
		itemblock_stone_wall_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_wall_7);
	}

	private static void registerBlockWallStone8(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_wall_8 = new ItemBlockMeta(block);
		itemblock_stone_wall_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_wall_8);
	}

	private static void registerBlockWallStone9(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_wall_9 = new ItemBlockMeta(block);
		itemblock_stone_wall_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_wall_9);
	}

	//Pillar Stone Blocks
	private static void registerBlockPillarStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_pillar_1 = new ItemBlockMeta(block);
		itemblock_stone_pillar_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_pillar_1);
	}

	private static void registerBlockPillarStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_pillar_2 = new ItemBlockMeta(block);
		itemblock_stone_pillar_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_pillar_2);
	}

	private static void registerBlockPillarStone3(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_pillar_3 = new ItemBlockMeta(block);
		itemblock_stone_pillar_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_pillar_3);
	}

	private static void registerBlockPillarStone4(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_pillar_4 = new ItemBlockMeta(block);
		itemblock_stone_pillar_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_pillar_4);
	}

	private static void registerBlockPillarStone5(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_pillar_5 = new ItemBlockMeta(block);
		itemblock_stone_pillar_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_pillar_5);
	}

	private static void registerBlockPillarStone6(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_pillar_6 = new ItemBlockMeta(block);
		itemblock_stone_pillar_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_pillar_6);
	}

	private static void registerBlockPillarStone7(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_pillar_7 = new ItemBlockMeta(block);
		itemblock_stone_pillar_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_pillar_7);
	}

	private static void registerBlockPillarStone8(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_pillar_8 = new ItemBlockMeta(block);
		itemblock_stone_pillar_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_pillar_8);
	}

	private static void registerBlockPillarStone9(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_pillar_9 = new ItemBlockMeta(block);
		itemblock_stone_pillar_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_pillar_9);
	}

	//Fence Stone Blocks
	private static void registerBlockFenceStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fence_1 = new ItemBlockMeta(block);
		itemblock_stone_fence_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fence_1);
	}

	private static void registerBlockFenceStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fence_2 = new ItemBlockMeta(block);
		itemblock_stone_fence_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fence_2);
	}

	//Fencegate Stone Blocks
	private static void registerBlockFencegateStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_fencegate_1 = new ItemBlockMeta(block);
		itemblock_stone_fencegate_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_fencegate_1);
	}

	//Small Pillar Stone Blocks
	private static void registerBlockSmallPillarStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_smallpillar_1 = new ItemBlockMeta(block);
		itemblock_stone_smallpillar_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_smallpillar_1);
	}

	private static void registerBlockSmallPillarStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_smallpillar_2 = new ItemBlockMeta(block);
		itemblock_stone_smallpillar_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_smallpillar_2);
	}

	private static void registerBlockSmallPillarStone3(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_smallpillar_3 = new ItemBlockMeta(block);
		itemblock_stone_smallpillar_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_smallpillar_3);
	}

	private static void registerBlockSmallPillarStone4(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_smallpillar_4 = new ItemBlockMeta(block);
		itemblock_stone_smallpillar_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_smallpillar_4);
	}

	private static void registerBlockSmallPillarStone5(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_smallpillar_5 = new ItemBlockMeta(block);
		itemblock_stone_smallpillar_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_smallpillar_5);
	}

	private static void registerBlockSmallPillarStone6(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_smallpillar_6 = new ItemBlockMeta(block);
		itemblock_stone_smallpillar_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_smallpillar_6);
	}

	private static void registerBlockSmallPillarStone7(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_smallpillar_7 = new ItemBlockMeta(block);
		itemblock_stone_smallpillar_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_smallpillar_7);
	}

	private static void registerBlockSmallPillarStone8(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_smallpillar_8 = new ItemBlockMeta(block);
		itemblock_stone_smallpillar_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_smallpillar_8);
	}

	private static void registerBlockSmallPillarStone9(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_smallpillar_9 = new ItemBlockMeta(block);
		itemblock_stone_smallpillar_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_smallpillar_9);
	}

	private static void registerBlockSmallPillarStone10(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_smallpillar_10 = new ItemBlockMeta(block);
		itemblock_stone_smallpillar_10.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_smallpillar_10);
	}

	//Stairs Stone Blocks
	private static void registerBlockStairsStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_1 = new ItemBlockMeta(block);
		itemblock_stone_stairs_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_1);
	}
	private static void registerBlockStairsStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_2 = new ItemBlockMeta(block);
		itemblock_stone_stairs_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_2);
	}
	private static void registerBlockStairsStone3(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_3 = new ItemBlockMeta(block);
		itemblock_stone_stairs_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_3);
	}
	private static void registerBlockStairsStone4(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_4 = new ItemBlockMeta(block);
		itemblock_stone_stairs_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_4);
	}
	private static void registerBlockStairsStone5(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_5 = new ItemBlockMeta(block);
		itemblock_stone_stairs_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_5);
	}
	private static void registerBlockStairsStone6(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_6 = new ItemBlockMeta(block);
		itemblock_stone_stairs_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_6);
	}
	private static void registerBlockStairsStone7(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_7 = new ItemBlockMeta(block);
		itemblock_stone_stairs_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_7);
	}
	private static void registerBlockStairsStone8(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_8 = new ItemBlockMeta(block);
		itemblock_stone_stairs_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_8);
	}
	private static void registerBlockStairsStone9(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_9 = new ItemBlockMeta(block);
		itemblock_stone_stairs_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_9);
	}
	private static void registerBlockStairsStone10(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_10 = new ItemBlockMeta(block);
		itemblock_stone_stairs_10.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_10);
	}
	private static void registerBlockStairsStone11(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_11 = new ItemBlockMeta(block);
		itemblock_stone_stairs_11.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_11);
	}
	private static void registerBlockStairsStone12(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_12 = new ItemBlockMeta(block);
		itemblock_stone_stairs_12.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_12);
	}
	private static void registerBlockStairsStone13(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_13 = new ItemBlockMeta(block);
		itemblock_stone_stairs_13.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_13);
	}
	private static void registerBlockStairsStone14(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_14 = new ItemBlockMeta(block);
		itemblock_stone_stairs_14.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_14);
	}
	private static void registerBlockStairsStone15(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_15 = new ItemBlockMeta(block);
		itemblock_stone_stairs_15.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_15);
	}
	private static void registerBlockStairsStone16(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_16 = new ItemBlockMeta(block);
		itemblock_stone_stairs_16.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_16);
	}
	private static void registerBlockStairsStone17(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_17 = new ItemBlockMeta(block);
		itemblock_stone_stairs_17.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_17);
	}
	private static void registerBlockStairsStone18(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_18 = new ItemBlockMeta(block);
		itemblock_stone_stairs_18.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_18);
	}
	private static void registerBlockStairsStone19(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_19 = new ItemBlockMeta(block);
		itemblock_stone_stairs_19.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_19);
	}
	private static void registerBlockStairsStone20(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_20 = new ItemBlockMeta(block);
		itemblock_stone_stairs_20.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_20);
	}
	private static void registerBlockStairsStone21(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_21 = new ItemBlockMeta(block);
		itemblock_stone_stairs_21.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_21);
	}
	private static void registerBlockStairsStone22(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_22 = new ItemBlockMeta(block);
		itemblock_stone_stairs_22.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_22);
	}
	private static void registerBlockStairsStone23(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_23 = new ItemBlockMeta(block);
		itemblock_stone_stairs_23.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_23);
	}
	private static void registerBlockStairsStone24(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_24 = new ItemBlockMeta(block);
		itemblock_stone_stairs_24.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_24);
	}

	private static void registerBlockStairsStone25(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_25 = new ItemBlockMeta(block);
		itemblock_stone_stairs_25.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_25);
	}
	private static void registerBlockStairsStone26(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_26 = new ItemBlockMeta(block);
		itemblock_stone_stairs_26.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_26);
	}
	private static void registerBlockStairsStone27(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_27 = new ItemBlockMeta(block);
		itemblock_stone_stairs_27.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_27);
	}
	private static void registerBlockStairsStone28(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_28 = new ItemBlockMeta(block);
		itemblock_stone_stairs_28.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_28);
	}
	private static void registerBlockStairsStone29(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_29 = new ItemBlockMeta(block);
		itemblock_stone_stairs_29.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_29);
	}
	private static void registerBlockStairsStone30(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_30 = new ItemBlockMeta(block);
		itemblock_stone_stairs_30.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_30);
	}
	private static void registerBlockStairsStone31(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_31 = new ItemBlockMeta(block);
		itemblock_stone_stairs_31.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_31);
	}
	private static void registerBlockStairsStone32(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_32 = new ItemBlockMeta(block);
		itemblock_stone_stairs_32.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_32);
	}
	private static void registerBlockStairsStone33(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_33 = new ItemBlockMeta(block);
		itemblock_stone_stairs_33.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_33);
	}
	private static void registerBlockStairsStone34(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_34 = new ItemBlockMeta(block);
		itemblock_stone_stairs_34.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_34);
	}
	private static void registerBlockStairsStone35(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_35 = new ItemBlockMeta(block);
		itemblock_stone_stairs_35.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_35);
	}
	private static void registerBlockStairsStone36(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_36 = new ItemBlockMeta(block);
		itemblock_stone_stairs_36.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_36);
	}
	private static void registerBlockStairsStone37(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_37 = new ItemBlockMeta(block);
		itemblock_stone_stairs_37.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_37);
	}
	private static void registerBlockStairsStone38(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_38 = new ItemBlockMeta(block);
		itemblock_stone_stairs_38.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_38);
	}
	private static void registerBlockStairsStone39(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_39 = new ItemBlockMeta(block);
		itemblock_stone_stairs_39.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_39);
	}
	private static void registerBlockStairsStone40(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_40 = new ItemBlockMeta(block);
		itemblock_stone_stairs_40.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_40);
	}
	private static void registerBlockStairsStone41(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_41 = new ItemBlockMeta(block);
		itemblock_stone_stairs_41.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_41);
	}
	private static void registerBlockStairsStone42(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_42 = new ItemBlockMeta(block);
		itemblock_stone_stairs_42.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_42);
	}
	private static void registerBlockStairsStone43(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_43 = new ItemBlockMeta(block);
		itemblock_stone_stairs_43.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_43);
	}
	private static void registerBlockStairsStone44(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_44 = new ItemBlockMeta(block);
		itemblock_stone_stairs_44.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_44);
	}
	private static void registerBlockStairsStone45(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_45 = new ItemBlockMeta(block);
		itemblock_stone_stairs_45.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_45);
	}
	private static void registerBlockStairsStone46(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_46 = new ItemBlockMeta(block);
		itemblock_stone_stairs_46.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_46);
	}
	private static void registerBlockStairsStone47(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_47 = new ItemBlockMeta(block);
		itemblock_stone_stairs_47.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_47);
	}
	private static void registerBlockStairsStone48(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_48 = new ItemBlockMeta(block);
		itemblock_stone_stairs_48.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_48);
	}

	private static void registerBlockStairsStone49(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_49 = new ItemBlockMeta(block);
		itemblock_stone_stairs_49.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_49);
	}
	private static void registerBlockStairsStone50(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_50 = new ItemBlockMeta(block);
		itemblock_stone_stairs_50.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_50);
	}
	private static void registerBlockStairsStone51(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_51 = new ItemBlockMeta(block);
		itemblock_stone_stairs_51.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_51);
	}
	private static void registerBlockStairsStone52(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_52 = new ItemBlockMeta(block);
		itemblock_stone_stairs_52.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_52);
	}
	private static void registerBlockStairsStone53(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_53 = new ItemBlockMeta(block);
		itemblock_stone_stairs_53.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_53);
	}
	private static void registerBlockStairsStone54(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_54 = new ItemBlockMeta(block);
		itemblock_stone_stairs_54.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_54);
	}
	private static void registerBlockStairsStone55(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_55 = new ItemBlockMeta(block);
		itemblock_stone_stairs_55.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_55);
	}
	private static void registerBlockStairsStone56(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_56 = new ItemBlockMeta(block);
		itemblock_stone_stairs_56.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_56);
	}
	private static void registerBlockStairsStone57(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_57 = new ItemBlockMeta(block);
		itemblock_stone_stairs_57.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_57);
	}
	private static void registerBlockStairsStone58(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_58 = new ItemBlockMeta(block);
		itemblock_stone_stairs_58.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_58);
	}
	private static void registerBlockStairsStone59(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_59 = new ItemBlockMeta(block);
		itemblock_stone_stairs_59.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_59);
	}
	private static void registerBlockStairsStone60(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_60 = new ItemBlockMeta(block);
		itemblock_stone_stairs_60.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_60);
	}
	private static void registerBlockStairsStone61(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_61 = new ItemBlockMeta(block);
		itemblock_stone_stairs_61.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_61);
	}
	private static void registerBlockStairsStone62(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_62 = new ItemBlockMeta(block);
		itemblock_stone_stairs_62.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_62);
	}
	private static void registerBlockStairsStone63(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_63 = new ItemBlockMeta(block);
		itemblock_stone_stairs_63.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_63);
	}
	private static void registerBlockStairsStone64(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_64 = new ItemBlockMeta(block);
		itemblock_stone_stairs_64.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_64);
	}

	private static void registerBlockStairsStone65(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_65 = new ItemBlockMeta(block);
		itemblock_stone_stairs_65.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_65);
	}
	private static void registerBlockStairsStone66(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_66 = new ItemBlockMeta(block);
		itemblock_stone_stairs_66.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_66);
	}
	private static void registerBlockStairsStone67(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_67 = new ItemBlockMeta(block);
		itemblock_stone_stairs_67.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_67);
	}
	private static void registerBlockStairsStone68(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_68 = new ItemBlockMeta(block);
		itemblock_stone_stairs_68.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_68);
	}
	private static void registerBlockStairsStone69(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_69 = new ItemBlockMeta(block);
		itemblock_stone_stairs_69.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_69);
	}
	private static void registerBlockStairsStone70(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_70 = new ItemBlockMeta(block);
		itemblock_stone_stairs_70.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_70);
	}
	private static void registerBlockStairsStone71(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_71 = new ItemBlockMeta(block);
		itemblock_stone_stairs_71.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_71);
	}
	private static void registerBlockStairsStone72(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_72 = new ItemBlockMeta(block);
		itemblock_stone_stairs_72.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_72);
	}
	private static void registerBlockStairsStone73(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_73 = new ItemBlockMeta(block);
		itemblock_stone_stairs_73.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_73);
	}
	private static void registerBlockStairsStone74(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_74 = new ItemBlockMeta(block);
		itemblock_stone_stairs_74.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_74);
	}
	private static void registerBlockStairsStone75(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_75 = new ItemBlockMeta(block);
		itemblock_stone_stairs_75.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_75);
	}
	private static void registerBlockStairsStone76(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_76 = new ItemBlockMeta(block);
		itemblock_stone_stairs_76.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_76);
	}
	private static void registerBlockStairsStone77(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_77 = new ItemBlockMeta(block);
		itemblock_stone_stairs_77.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_77);
	}
	private static void registerBlockStairsStone78(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_78 = new ItemBlockMeta(block);
		itemblock_stone_stairs_78.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_78);
	}
	private static void registerBlockStairsStone79(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_79 = new ItemBlockMeta(block);
		itemblock_stone_stairs_79.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_79);
	}
	private static void registerBlockStairsStone80(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_80 = new ItemBlockMeta(block);
		itemblock_stone_stairs_80.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_80);
	}
	private static void registerBlockStairsStone81(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_81 = new ItemBlockMeta(block);
		itemblock_stone_stairs_81.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_81);
	}
	private static void registerBlockStairsStone82(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_82 = new ItemBlockMeta(block);
		itemblock_stone_stairs_82.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_82);
	}
	private static void registerBlockStairsStone83(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_83 = new ItemBlockMeta(block);
		itemblock_stone_stairs_83.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_83);
	}
	private static void registerBlockStairsStone84(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_84 = new ItemBlockMeta(block);
		itemblock_stone_stairs_84.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_84);
	}
	private static void registerBlockStairsStone85(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_85 = new ItemBlockMeta(block);
		itemblock_stone_stairs_85.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_85);
	}
	private static void registerBlockStairsStone86(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_86 = new ItemBlockMeta(block);
		itemblock_stone_stairs_86.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_86);
	}

	private static void registerBlockStairsStone87(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_87 = new ItemBlockMeta(block);
		itemblock_stone_stairs_87.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_87);
	}
	private static void registerBlockStairsStone88(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_88 = new ItemBlockMeta(block);
		itemblock_stone_stairs_88.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_88);
	}
	private static void registerBlockStairsStone89(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_89 = new ItemBlockMeta(block);
		itemblock_stone_stairs_89.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_89);
	}
	private static void registerBlockStairsStone90(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_90 = new ItemBlockMeta(block);
		itemblock_stone_stairs_90.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_90);
	}
	private static void registerBlockStairsStone91(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_91 = new ItemBlockMeta(block);
		itemblock_stone_stairs_91.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_91);
	}
	private static void registerBlockStairsStone92(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_92 = new ItemBlockMeta(block);
		itemblock_stone_stairs_92.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_92);
	}
	private static void registerBlockStairsStone93(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_93 = new ItemBlockMeta(block);
		itemblock_stone_stairs_93.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_93);
	}
	private static void registerBlockStairsStone94(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_94 = new ItemBlockMeta(block);
		itemblock_stone_stairs_94.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_94);
	}
	private static void registerBlockStairsStone95(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_95 = new ItemBlockMeta(block);
		itemblock_stone_stairs_95.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_95);
	}
	private static void registerBlockStairsStone96(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_96 = new ItemBlockMeta(block);
		itemblock_stone_stairs_96.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_96);
	}
	private static void registerBlockStairsStone97(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_97 = new ItemBlockMeta(block);
		itemblock_stone_stairs_97.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_97);
	}

	private static void registerBlockStairsStone98(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_98 = new ItemBlockMeta(block);
		itemblock_stone_stairs_98.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_98);
	}

	private static void registerBlockStairsStone99(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_99 = new ItemBlockMeta(block);
		itemblock_stone_stairs_99.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_99);
	}

	private static void registerBlockStairsStone100(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_100 = new ItemBlockMeta(block);
		itemblock_stone_stairs_100.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_100);
	}

	private static void registerBlockStairsStone101(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_101 = new ItemBlockMeta(block);
		itemblock_stone_stairs_101.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_101);
	}

	private static void registerBlockStairsStone102(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_102 = new ItemBlockMeta(block);
		itemblock_stone_stairs_102.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_102);
	}

	private static void registerBlockStairsStone103(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_103 = new ItemBlockMeta(block);
		itemblock_stone_stairs_103.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_103);
	}

	private static void registerBlockStairsStone104(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_104 = new ItemBlockMeta(block);
		itemblock_stone_stairs_104.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_104);
	}

	private static void registerBlockStairsStone105(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_105 = new ItemBlockMeta(block);
		itemblock_stone_stairs_105.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_105);
	}

	private static void registerBlockStairsStone106(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_106 = new ItemBlockMeta(block);
		itemblock_stone_stairs_106.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_106);
	}

	private static void registerBlockStairsStone107(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_107 = new ItemBlockMeta(block);
		itemblock_stone_stairs_107.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_107);
	}

	private static void registerBlockStairsStone108(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_108 = new ItemBlockMeta(block);
		itemblock_stone_stairs_108.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_108);
	}

	private static void registerBlockStairsStone109(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_stairs_109 = new ItemBlockMeta(block);
		itemblock_stone_stairs_109.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_stairs_109);
	}

	//Slab Stone Blocks
	private static void registerBlockSlabStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_1 = new ItemBlockMeta(block);
		itemblock_stone_slab_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_1);
	}

	private static void registerBlockSlabStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_2 = new ItemBlockMeta(block);
		itemblock_stone_slab_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_2);
	}

	private static void registerBlockSlabStone3(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_3 = new ItemBlockMeta(block);
		itemblock_stone_slab_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_3);
	}

	private static void registerBlockSlabStone4(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_4 = new ItemBlockMeta(block);
		itemblock_stone_slab_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_4);
	}

	private static void registerBlockSlabStone5(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_5 = new ItemBlockMeta(block);
		itemblock_stone_slab_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_5);
	}

	private static void registerBlockSlabStone6(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_6 = new ItemBlockMeta(block);
		itemblock_stone_slab_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_6);
	}

	private static void registerBlockSlabStone7(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_7 = new ItemBlockMeta(block);
		itemblock_stone_slab_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_7);
	}

	private static void registerBlockSlabStone8(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_8 = new ItemBlockMeta(block);
		itemblock_stone_slab_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_8);
	}

	private static void registerBlockSlabStone9(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_9 = new ItemBlockMeta(block);
		itemblock_stone_slab_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_9);
	}

	private static void registerBlockSlabStone10(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_10 = new ItemBlockMeta(block);
		itemblock_stone_slab_10.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_10);
	}

	private static void registerBlockSlabStone11(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_11 = new ItemBlockMeta(block);
		itemblock_stone_slab_11.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_11);
	}

	private static void registerBlockSlabStone12(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_12 = new ItemBlockMeta(block);
		itemblock_stone_slab_12.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_12);
	}

	private static void registerBlockSlabStone13(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_13 = new ItemBlockMeta(block);
		itemblock_stone_slab_13.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_13);
	}

	private static void registerBlockSlabStone14(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_14 = new ItemBlockMeta(block);
		itemblock_stone_slab_14.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_14);
	}

	private static void registerBlockSlabStone15(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_15 = new ItemBlockMeta(block);
		itemblock_stone_slab_15.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_15);
	}

	private static void registerBlockSlabStone16(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_16 = new ItemBlockMeta(block);
		itemblock_stone_slab_16.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_16);
	}

	private static void registerBlockSlabStone17(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_17 = new ItemBlockMeta(block);
		itemblock_stone_slab_17.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_17);
	}

	private static void registerBlockSlabStone18(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_18 = new ItemBlockMeta(block);
		itemblock_stone_slab_18.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_18);
	}

	private static void registerBlockSlabStone19(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_19 = new ItemBlockMeta(block);
		itemblock_stone_slab_19.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_19);
	}

	private static void registerBlockSlabStone20(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_slab_20 = new ItemBlockMeta(block);
		itemblock_stone_slab_20.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_slab_20);
	}

	//Stone Trapdoor Blocks
	private static void registerBlockTrapdoorStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_1 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_1);
	}

	private static void registerBlockTrapdoorStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_2 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_2);
	}

	private static void registerBlockTrapdoorStone3(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_3 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_3);
	}

	private static void registerBlockTrapdoorStone4(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_4 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_4);
	}

	private static void registerBlockTrapdoorStone5(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_5 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_5);
	}

	private static void registerBlockTrapdoorStone6(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_6 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_6);
	}

	private static void registerBlockTrapdoorStone7(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_7 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_7);
	}

	private static void registerBlockTrapdoorStone8(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_8 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_8);
	}

	private static void registerBlockTrapdoorStone9(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_9 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_9);
	}

	private static void registerBlockTrapdoorStone10(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_10 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_10.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_10);
	}

	private static void registerBlockTrapdoorStone11(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_11 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_11.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_11);
	}

	private static void registerBlockTrapdoorStone12(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_12 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_12.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_12);
	}

	private static void registerBlockTrapdoorStone13(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_13 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_13.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_13);
	}

	private static void registerBlockTrapdoorStone14(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_14 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_14.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_14);
	}

	private static void registerBlockTrapdoorStone15(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_15 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_15.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_15);
	}

	private static void registerBlockTrapdoorStone16(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_16 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_16.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_16);
	}

	private static void registerBlockTrapdoorStone17(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_17 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_17.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_17);
	}

	private static void registerBlockTrapdoorStone18(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_18 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_18.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_18);
	}

	private static void registerBlockTrapdoorStone19(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_19 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_19.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_19);
	}

	private static void registerBlockTrapdoorStone20(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_20 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_20.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_20);
	}

	private static void registerBlockTrapdoorStone21(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_21 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_21.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_21);
	}

	private static void registerBlockTrapdoorStone22(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_22 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_22.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_22);
	}

	private static void registerBlockTrapdoorStone23(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_23 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_23.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_23);
	}

	private static void registerBlockTrapdoorStone24(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_24 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_24.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_24);
	}

	private static void registerBlockTrapdoorStone25(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_25 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_25.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_25);
	}

	private static void registerBlockTrapdoorStone26(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_26 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_26.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_26);
	}

	private static void registerBlockTrapdoorStone27(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_27 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_27.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_27);
	}

	private static void registerBlockTrapdoorStone28(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_28 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_28.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_28);
	}

	private static void registerBlockTrapdoorStone29(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_29 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_29.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_29);
	}

	private static void registerBlockTrapdoorStone30(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_30 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_30.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_30);
	}

	private static void registerBlockTrapdoorStone31(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_31 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_31.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_31);
	}

	private static void registerBlockTrapdoorStone32(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_32 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_32.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_32);
	}

	private static void registerBlockTrapdoorStone33(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_33 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_33.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_33);
	}

	private static void registerBlockTrapdoorStone34(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_34 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_34.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_34);
	}

	private static void registerBlockTrapdoorStone35(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_35 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_35.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_35);
	}

	private static void registerBlockTrapdoorStone36(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_36 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_36.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_36);
	}

	private static void registerBlockTrapdoorStone37(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_37 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_37.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_37);
	}

	private static void registerBlockTrapdoorStone38(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_38 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_38.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_38);
	}

	private static void registerBlockTrapdoorStone39(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_39 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_39.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_39);
	}

	private static void registerBlockTrapdoorStone40(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_40 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_40.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_40);
	}

	private static void registerBlockTrapdoorStone41(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_41 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_41.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_41);
	}

	private static void registerBlockTrapdoorStone42(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_42 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_42.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_42);
	}

	private static void registerBlockTrapdoorStone43(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_43 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_43.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_43);
	}

	private static void registerBlockTrapdoorStone44(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_44 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_44.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_44);
	}

	private static void registerBlockTrapdoorStone45(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_45 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_45.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_45);
	}

	private static void registerBlockTrapdoorStone46(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_46 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_46.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_46);
	}

	private static void registerBlockTrapdoorStone47(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_47 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_47.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_47);
	}

	private static void registerBlockTrapdoorStone48(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_48 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_48.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_48);
	}

	private static void registerBlockTrapdoorStone49(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_49 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_49.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_49);
	}

	private static void registerBlockTrapdoorStone50(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_50 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_50.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_50);
	}

	private static void registerBlockTrapdoorStone51(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_51 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_51.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_51);
	}

	private static void registerBlockTrapdoorStone52(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_52 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_52.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_52);
	}

	private static void registerBlockTrapdoorStone53(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_53 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_53.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_53);
	}

	private static void registerBlockTrapdoorStone54(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_54 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_54.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_54);
	}

	private static void registerBlockTrapdoorStone55(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_55 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_55.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_55);
	}

	private static void registerBlockTrapdoorStone56(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_56 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_56.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_56);
	}

	private static void registerBlockTrapdoorStone57(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_57 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_57.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_57);
	}

	private static void registerBlockTrapdoorStone58(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_58 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_58.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_58);
	}

	private static void registerBlockTrapdoorStone59(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_59 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_59.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_59);
	}

	private static void registerBlockTrapdoorStone60(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_60 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_60.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_60);
	}

	private static void registerBlockTrapdoorStone61(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_61 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_61.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_61);
	}

	private static void registerBlockTrapdoorStone62(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_62 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_62.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_62);
	}

	private static void registerBlockTrapdoorStone63(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_63 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_63.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_63);
	}

	private static void registerBlockTrapdoorStone64(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_64 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_64.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_64);
	}

	private static void registerBlockTrapdoorStone65(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_65 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_65.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_65);
	}

	private static void registerBlockTrapdoorStone66(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_66 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_66.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_66);
	}

	private static void registerBlockTrapdoorStone67(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_67 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_67.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_67);
	}

	private static void registerBlockTrapdoorStone68(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_68 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_68.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_68);
	}

	private static void registerBlockTrapdoorStone69(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_69 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_69.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_69);
	}

	private static void registerBlockTrapdoorStone70(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_70 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_70.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_70);
	}

	private static void registerBlockTrapdoorStone71(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_71 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_71.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_71);
	}

	private static void registerBlockTrapdoorStone72(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_72 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_72.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_72);
	}

	private static void registerBlockTrapdoorStone73(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_73 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_73.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_73);
	}

	private static void registerBlockTrapdoorStone74(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_74 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_74.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_74);
	}

	private static void registerBlockTrapdoorStone75(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_75 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_75.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_75);
	}

	private static void registerBlockTrapdoorStone76(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_76 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_76.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_76);
	}

	private static void registerBlockTrapdoorStone77(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_77 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_77.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_77);
	}

	private static void registerBlockTrapdoorStone78(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_78 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_78.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_78);
	}

	private static void registerBlockTrapdoorStone79(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_79 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_79.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_79);
	}

	private static void registerBlockTrapdoorStone80(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_80 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_80.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_80);
	}

	private static void registerBlockTrapdoorStone81(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_81 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_81.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_81);
	}

	private static void registerBlockTrapdoorStone82(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_82 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_82.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_82);
	}

	private static void registerBlockTrapdoorStone83(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_83 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_83.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_83);
	}

	private static void registerBlockTrapdoorStone84(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_84 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_84.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_84);
	}

	private static void registerBlockTrapdoorStone85(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_85 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_85.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_85);
	}

	private static void registerBlockTrapdoorStone86(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_86 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_86.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_86);
	}

	private static void registerBlockTrapdoorStone87(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_87 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_87.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_87);
	}

	private static void registerBlockTrapdoorStone88(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_88 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_88.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_88);
	}

	private static void registerBlockTrapdoorStone89(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_89 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_89.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_89);
	}

	private static void registerBlockTrapdoorStone90(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_90 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_90.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_90);
	}

	private static void registerBlockTrapdoorStone91(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_91 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_91.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_91);
	}

	private static void registerBlockTrapdoorStone92(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_92 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_92.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_92);
	}

	private static void registerBlockTrapdoorStone93(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_93 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_93.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_93);
	}

	private static void registerBlockTrapdoorStone94(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_94 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_94.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_94);
	}

	private static void registerBlockTrapdoorStone95(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_95 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_95.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_95);
	}

	private static void registerBlockTrapdoorStone96(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_96 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_96.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_96);
	}

	private static void registerBlockTrapdoorStone97(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_97 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_97.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_97);
	}

	private static void registerBlockTrapdoorStone98(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_98 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_98.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_98);
	}

	private static void registerBlockTrapdoorStone99(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_99 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_99.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_99);
	}

	private static void registerBlockTrapdoorStone100(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_100 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_100.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_100);
	}

	private static void registerBlockTrapdoorStone101(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_101 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_101.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_101);
	}

	private static void registerBlockTrapdoorStone102(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_102 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_102.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_102);
	}

	private static void registerBlockTrapdoorStone103(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_103 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_103.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_103);
	}

	private static void registerBlockTrapdoorStone104(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_104 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_104.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_104);
	}

	private static void registerBlockTrapdoorStone105(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_105 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_105.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_105);
	}

	private static void registerBlockTrapdoorStone106(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_106 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_106.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_106);
	}

	private static void registerBlockTrapdoorStone107(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_107 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_107.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_107);
	}

	private static void registerBlockTrapdoorStone108(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_108 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_108.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_108);
	}

	private static void registerBlockTrapdoorStone109(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_109 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_109.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_109);
	}

	private static void registerBlockTrapdoorStone110(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_110 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_110.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_110);
	}

	private static void registerBlockTrapdoorStone111(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_111 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_111.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_111);
	}

	private static void registerBlockTrapdoorStone112(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_112 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_112.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_112);
	}

	private static void registerBlockTrapdoorStone113(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_113 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_113.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_113);
	}

	private static void registerBlockTrapdoorStone114(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_114 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_114.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_114);
	}

	private static void registerBlockTrapdoorStone115(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_115 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_115.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_115);
	}

	private static void registerBlockTrapdoorStone116(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_116 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_116.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_116);
	}

	private static void registerBlockTrapdoorStone117(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_117 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_117.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_117);
	}

	private static void registerBlockTrapdoorStone118(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_118 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_118.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_118);
	}

	private static void registerBlockTrapdoorStone119(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_119 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_119.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_119);
	}

	private static void registerBlockTrapdoorStone120(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_120 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_120.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_120);
	}

	private static void registerBlockTrapdoorStone121(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_121 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_121.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_121);
	}

	private static void registerBlockTrapdoorStone122(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_122 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_122.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_122);
	}

	private static void registerBlockTrapdoorStone123(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_123 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_123.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_123);
	}

	private static void registerBlockTrapdoorStone124(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_124 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_124.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_124);
	}

	private static void registerBlockTrapdoorStone125(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_125 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_125.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_125);
	}

	private static void registerBlockTrapdoorStone126(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_126 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_126.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_126);
	}

	private static void registerBlockTrapdoorStone127(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_127 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_127.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_127);
	}

	private static void registerBlockTrapdoorStone128(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_128 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_128.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_128);
	}

	private static void registerBlockTrapdoorStone129(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_129 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_129.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_129);
	}

	private static void registerBlockTrapdoorStone130(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_130 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_130.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_130);
	}

	private static void registerBlockTrapdoorStone131(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_131 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_131.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_131);
	}

	private static void registerBlockTrapdoorStone132(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_132 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_132.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_132);
	}

	private static void registerBlockTrapdoorStone133(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_133 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_133.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_133);
	}

	private static void registerBlockTrapdoorStone134(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_134 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_134.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_134);
	}

	private static void registerBlockTrapdoorStone135(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_135 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_135.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_135);
	}

	private static void registerBlockTrapdoorStone136(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_136 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_136.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_136);
	}

	private static void registerBlockTrapdoorStone137(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_137 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_137.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_137);
	}

	private static void registerBlockTrapdoorStone138(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_138 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_138.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_138);
	}

	private static void registerBlockTrapdoorStone139(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_139 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_139.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_139);
	}

	private static void registerBlockTrapdoorStone140(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_140 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_140.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_140);
	}

	private static void registerBlockTrapdoorStone141(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_141 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_141.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_141);
	}

	private static void registerBlockTrapdoorStone142(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_142 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_142.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_142);
	}

	private static void registerBlockTrapdoorStone143(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_143 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_143.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_143);
	}

	private static void registerBlockTrapdoorStone144(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_trapdoormodel_144 = new ItemBlockMeta(block);
		itemblock_stone_trapdoormodel_144.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_trapdoormodel_144);
	}

	//Carpet Stone Blocks
	private static void registerBlockCarpetStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_carpet_1 = new ItemBlockMeta(block);
		itemblock_stone_carpet_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_carpet_1);
	}

	private static void registerBlockCarpetStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_carpet_2 = new ItemBlockMeta(block);
		itemblock_stone_carpet_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_carpet_2);
	}

	private static void registerBlockCarpetStone3(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_carpet_3 = new ItemBlockMeta(block);
		itemblock_stone_carpet_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_carpet_3);
	}

	private static void registerBlockCarpetStone4(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_carpet_4 = new ItemBlockMeta(block);
		itemblock_stone_carpet_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_carpet_4);
	}

	//Stone Layer Blocks
	private static void registerBlockLayerStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_layer_1 = new ItemLayerMeta(block);
		itemblock_stone_layer_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_layer_1);
	}

	private static void registerBlockLayerStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_layer_2 = new ItemLayerMeta(block);
		itemblock_stone_layer_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_layer_2);
	}

	private static void registerBlockLayerStone3(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_layer_3 = new ItemLayerMeta(block);
		itemblock_stone_layer_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_layer_3);
	}

	private static void registerBlockLayerStone4(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_layer_4 = new ItemLayerMeta(block);
		itemblock_stone_layer_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_layer_4);
	}

	private static void registerBlockLayerStone5(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_layer_5 = new ItemLayerMeta(block);
		itemblock_stone_layer_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_layer_5);
	}

	private static void registerBlockLayerStone6(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_layer_6 = new ItemLayerMeta(block);
		itemblock_stone_layer_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_layer_6);
	}

	private static void registerBlockLayerStone7(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_layer_7 = new ItemLayerMeta(block);
		itemblock_stone_layer_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_layer_7);
	}

	private static void registerBlockLayerStone8(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_layer_8 = new ItemLayerMeta(block);
		itemblock_stone_layer_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_layer_8);
	}

	//Directional No Collision Stone Blocks
	private static void registerBlockDirectionalNoCollisionStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_directionalnocollision_1 = new ItemBlockMeta(block);
		itemblock_stone_directionalnocollision_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_directionalnocollision_1);
	}

	//No Collision Stone Blocks
	private static void registerBlockNoCollisionStone1(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_nocollision_1 = new ItemBlockMeta(block);
		itemblock_stone_nocollision_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_nocollision_1);
	}

	private static void registerBlockNoCollisionStone2(Block block)
	{
		GameRegistry.register(block);
		itemblock_stone_nocollision_2 = new ItemBlockMeta(block);
		itemblock_stone_nocollision_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_stone_nocollision_2);
	}

	//Full Wood Blocks
	private static void registerBlockWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_full_1 = new ItemBlockMeta(block);
		itemblock_wood_full_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_full_1);
	}

	private static void registerBlockWood2(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_full_2 = new ItemBlockMeta(block);
		itemblock_wood_full_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_full_2);
	}

	private static void registerBlockWood3(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_full_3 = new ItemBlockMeta(block);
		itemblock_wood_full_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_full_3);
	}

	private static void registerBlockWood4(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_full_4 = new ItemBlockMeta(block);
		itemblock_wood_full_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_full_4);
	}

	private static void registerBlockWood5(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_full_5 = new ItemBlockMeta(block);
		itemblock_wood_full_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_full_5);
	}

	private static void registerBlockWood6(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_full_6 = new ItemBlockMeta(block);
		itemblock_wood_full_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_full_6);
	}

	private static void registerBlockWood7(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_full_7 = new ItemBlockMeta(block);
		itemblock_wood_full_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_full_7);
	}

	//Full Slit Wood Blocks
	private static void registerBlockFullSlitWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fullslit_1 = new ItemBlockMeta(block);
		itemblock_wood_fullslit_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fullslit_1);
	}

	private static void registerBlockFullSlitWood2(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fullslit_2 = new ItemBlockMeta(block);
		itemblock_wood_fullslit_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fullslit_2);
	}

	private static void registerBlockFullSlitWood3(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fullslit_3 = new ItemBlockMeta(block);
		itemblock_wood_fullslit_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fullslit_3);
	}

	private static void registerBlockFullSlitWood4(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fullslit_4 = new ItemBlockMeta(block);
		itemblock_wood_fullslit_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fullslit_4);
	}

	private static void registerBlockFullSlitWood5(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fullslit_5 = new ItemBlockMeta(block);
		itemblock_wood_fullslit_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fullslit_5);
	}

	private static void registerBlockFullSlitWood6(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fullslit_6 = new ItemBlockMeta(block);
		itemblock_wood_fullslit_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fullslit_6);
	}

	private static void registerBlockFullSlitWood7(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fullslit_7 = new ItemBlockMeta(block);
		itemblock_wood_fullslit_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fullslit_7);
	}

	//Wood Hopper Full Blocks
	private static void registerBlockHopperFullWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_hopperfull_1 = new ItemBlockMeta(block);
		itemblock_wood_hopperfull_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_hopperfull_1);
	}

	//Wood Hopper Directional Blocks
	private static void registerBlockHopperDirectionalWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_hopperdirectional_1 = new ItemBlockMeta(block);
		itemblock_wood_hopperdirectional_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_hopperdirectional_1);
	}
	private static void registerBlockHopperDirectionalWood2(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_hopperdirectional_2 = new ItemBlockMeta(block);
		itemblock_wood_hopperdirectional_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_hopperdirectional_2);
	}
	private static void registerBlockHopperDirectionalWood3(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_hopperdirectional_3 = new ItemBlockMeta(block);
		itemblock_wood_hopperdirectional_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_hopperdirectional_3);
	}

	private static void registerBlockHopperDirectionalWood4(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_hopperdirectional_4 = new ItemBlockMeta(block);
		itemblock_wood_hopperdirectional_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_hopperdirectional_4);
	}

	//Wood Log Blocks
	private static void registerBlockLogWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_log_1 = new ItemBlockMeta(block);
		itemblock_wood_log_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_log_1);
	}

	private static void registerBlockLogWood2(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_log_2 = new ItemBlockMeta(block);
		itemblock_wood_log_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_log_2);
	}

	private static void registerBlockLogWood3(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_log_3 = new ItemBlockMeta(block);
		itemblock_wood_log_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_log_3);
	}

	private static void registerBlockLogWood4(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_log_4 = new ItemBlockMeta(block);
		itemblock_wood_log_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_log_4);
	}

	private static void registerBlockLogWood5(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_log_5 = new ItemBlockMeta(block);
		itemblock_wood_log_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_log_5);
	}

	private static void registerBlockLogWood6(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_log_6 = new ItemBlockMeta(block);
		itemblock_wood_log_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_log_6);
	}

	//Wood Anvil Blocks
	private static void registerBlockAnvilWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_anvil_1 = new ItemBlockMeta(block);
		itemblock_wood_anvil_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_anvil_1);
	}

	private static void registerBlockAnvilWood2(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_anvil_2 = new ItemBlockMeta(block);
		itemblock_wood_anvil_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_anvil_2);
	}

	private static void registerBlockAnvilWood3(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_anvil_3 = new ItemBlockMeta(block);
		itemblock_wood_anvil_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_anvil_3);
	}

	private static void registerBlockAnvilWood4(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_anvil_4 = new ItemBlockMeta(block);
		itemblock_wood_anvil_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_anvil_4);
	}

	//Wood Wall Blocks
	private static void registerBlockWallWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_wall_1 = new ItemBlockMeta(block);
		itemblock_wood_wall_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_wall_1);
	}

	private static void registerBlockWallWood2(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_wall_2 = new ItemBlockMeta(block);
		itemblock_wood_wall_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_wall_2);
	}

	private static void registerBlockWallWood3(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_wall_3 = new ItemBlockMeta(block);
		itemblock_wood_wall_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_wall_3);
	}

	private static void registerBlockWallWood4(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_wall_4 = new ItemBlockMeta(block);
		itemblock_wood_wall_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_wall_4);
	}

	//Wood Pillar Blocks
	private static void registerBlockPillarWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_pillar_1 = new ItemBlockMeta(block);
		itemblock_wood_pillar_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_pillar_1);
	}

	private static void registerBlockPillarWood2(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_pillar_2 = new ItemBlockMeta(block);
		itemblock_wood_pillar_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_pillar_2);
	}

	private static void registerBlockPillarWood3(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_pillar_3 = new ItemBlockMeta(block);
		itemblock_wood_pillar_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_pillar_3);
	}

	private static void registerBlockPillarWood4(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_pillar_4 = new ItemBlockMeta(block);
		itemblock_wood_pillar_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_pillar_4);
	}

	//Fence Wood Blocks
	private static void registerBlockFenceWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fence_1 = new ItemBlockMeta(block);
		itemblock_wood_fence_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fence_1);
	}
	private static void registerBlockFenceWood2(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fence_2 = new ItemBlockMeta(block);
		itemblock_wood_fence_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fence_2);
	}
	private static void registerBlockFenceWood3(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fence_3 = new ItemBlockMeta(block);
		itemblock_wood_fence_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fence_3);
	}

	//Fencegate Wood Blocks
	private static void registerBlockFencegateWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fencegate_1 = new ItemBlockMeta(block);
		itemblock_wood_fencegate_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fencegate_1);
	}
	private static void registerBlockFencegateWood2(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fencegate_2 = new ItemBlockMeta(block);
		itemblock_wood_fencegate_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fencegate_2);
	}
	private static void registerBlockFencegateWood3(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fencegate_3 = new ItemBlockMeta(block);
		itemblock_wood_fencegate_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fencegate_3);
	}
	private static void registerBlockFencegateWood4(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fencegate_4 = new ItemBlockMeta(block);
		itemblock_wood_fencegate_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fencegate_4);
	}
	private static void registerBlockFencegateWood5(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fencegate_5 = new ItemBlockMeta(block);
		itemblock_wood_fencegate_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fencegate_5);
	}
	private static void registerBlockFencegateWood6(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fencegate_6 = new ItemBlockMeta(block);
		itemblock_wood_fencegate_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fencegate_6);
	}
	private static void registerBlockFencegateWood7(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fencegate_7 = new ItemBlockMeta(block);
		itemblock_wood_fencegate_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fencegate_7);
	}
	private static void registerBlockFencegateWood8(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fencegate_8 = new ItemBlockMeta(block);
		itemblock_wood_fencegate_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fencegate_8);
	}
	private static void registerBlockFencegateWood9(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fencegate_9 = new ItemBlockMeta(block);
		itemblock_wood_fencegate_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fencegate_9);
	}
	private static void registerBlockFencegateWood10(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fencegate_10 = new ItemBlockMeta(block);
		itemblock_wood_fencegate_10.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fencegate_10);
	}
	private static void registerBlockFencegateWood11(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fencegate_11 = new ItemBlockMeta(block);
		itemblock_wood_fencegate_11.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fencegate_11);
	}

	private static void registerBlockFencegateWood12(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fencegate_12 = new ItemBlockMeta(block);
		itemblock_wood_fencegate_12.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fencegate_12);
	}

	//Small Pillar Wood Blocks
	private static void registerBlockSmallPillarWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_smallpillar_1 = new ItemBlockMeta(block);
		itemblock_wood_smallpillar_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_smallpillar_1);
	}
	private static void registerBlockSmallPillarWood2(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_smallpillar_2 = new ItemBlockMeta(block);
		itemblock_wood_smallpillar_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_smallpillar_2);
	}
	private static void registerBlockSmallPillarWood3(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_smallpillar_3 = new ItemBlockMeta(block);
		itemblock_wood_smallpillar_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_smallpillar_3);
	}

	//Full Partial Wood Blocks
	private static void registerBlockFullPartialWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fullpartial_1 = new ItemBlockMeta(block);
		itemblock_wood_fullpartial_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fullpartial_1);
	}

	private static void registerBlockFullPartialWood2(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_fullpartial_2 = new ItemBlockMeta(block);
		itemblock_wood_fullpartial_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_fullpartial_2);
	}

	//ConnectedXZ Wood Blocks
	private static void registerBlockConnectedXZWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_connectedxz_1 = new ItemBlockMeta(block);
		itemblock_wood_connectedxz_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_connectedxz_1);
	}

	//Stairs Wood Blocks
	private static void registerBlockStairsWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_1 = new ItemBlockMeta(block);
		itemblock_wood_stairs_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_1);
	}

	private static void registerBlockStairsWood2(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_2 = new ItemBlockMeta(block);
		itemblock_wood_stairs_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_2);
	}

	private static void registerBlockStairsWood3(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_3 = new ItemBlockMeta(block);
		itemblock_wood_stairs_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_3);
	}

	private static void registerBlockStairsWood4(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_4 = new ItemBlockMeta(block);
		itemblock_wood_stairs_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_4);
	}

	private static void registerBlockStairsWood5(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_5 = new ItemBlockMeta(block);
		itemblock_wood_stairs_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_5);
	}

	private static void registerBlockStairsWood6(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_6 = new ItemBlockMeta(block);
		itemblock_wood_stairs_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_6);
	}

	private static void registerBlockStairsWood7(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_7 = new ItemBlockMeta(block);
		itemblock_wood_stairs_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_7);
	}

	private static void registerBlockStairsWood8(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_8 = new ItemBlockMeta(block);
		itemblock_wood_stairs_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_8);
	}

	private static void registerBlockStairsWood9(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_9 = new ItemBlockMeta(block);
		itemblock_wood_stairs_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_9);
	}

	private static void registerBlockStairsWood10(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_10 = new ItemBlockMeta(block);
		itemblock_wood_stairs_10.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_10);
	}

	private static void registerBlockStairsWood11(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_11 = new ItemBlockMeta(block);
		itemblock_wood_stairs_11.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_11);
	}

	private static void registerBlockStairsWood12(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_12 = new ItemBlockMeta(block);
		itemblock_wood_stairs_12.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_12);
	}

	private static void registerBlockStairsWood13(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_13 = new ItemBlockMeta(block);
		itemblock_wood_stairs_13.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_13);
	}

	private static void registerBlockStairsWood14(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_14 = new ItemBlockMeta(block);
		itemblock_wood_stairs_14.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_14);
	}

	private static void registerBlockStairsWood15(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_15 = new ItemBlockMeta(block);
		itemblock_wood_stairs_15.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_15);
	}

	private static void registerBlockStairsWood16(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_16 = new ItemBlockMeta(block);
		itemblock_wood_stairs_16.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_16);
	}

	private static void registerBlockStairsWood17(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_17 = new ItemBlockMeta(block);
		itemblock_wood_stairs_17.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_17);
	}

	private static void registerBlockStairsWood18(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_18 = new ItemBlockMeta(block);
		itemblock_wood_stairs_18.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_18);
	}

	private static void registerBlockStairsWood19(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_19 = new ItemBlockMeta(block);
		itemblock_wood_stairs_19.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_19);
	}

	private static void registerBlockStairsWood20(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_20 = new ItemBlockMeta(block);
		itemblock_wood_stairs_20.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_20);
	}

	private static void registerBlockStairsWood21(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_21 = new ItemBlockMeta(block);
		itemblock_wood_stairs_21.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_21);
	}

	private static void registerBlockStairsWood22(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_22 = new ItemBlockMeta(block);
		itemblock_wood_stairs_22.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_22);
	}

	private static void registerBlockStairsWood23(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_23 = new ItemBlockMeta(block);
		itemblock_wood_stairs_23.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_23);
	}

	private static void registerBlockStairsWood24(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_24 = new ItemBlockMeta(block);
		itemblock_wood_stairs_24.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_24);
	}

	private static void registerBlockStairsWood25(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_25 = new ItemBlockMeta(block);
		itemblock_wood_stairs_25.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_25);
	}

	private static void registerBlockStairsWood26(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_26 = new ItemBlockMeta(block);
		itemblock_wood_stairs_26.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_26);
	}

	private static void registerBlockStairsWood27(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_stairs_27 = new ItemBlockMeta(block);
		itemblock_wood_stairs_27.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_stairs_27);
	}

	//Half Wood Blocks
	private static void registerBlockHalfWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_half_1 = new ItemBlockMeta(block);
		itemblock_wood_half_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_half_1);
	}

	//Daylight Detector Wood Blocks
	private static void registerBlockDaylightDetectorWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_daylightdetector_1 = new ItemBlockMeta(block);
		itemblock_wood_daylightdetector_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_daylightdetector_1);
	}

	//Slab Wood Blocks
	private static void registerBlockSlabWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_slab_1 = new ItemBlockMeta(block);
		itemblock_wood_slab_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_slab_1);
	}

	private static void registerBlockSlabWood2(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_slab_2 = new ItemBlockMeta(block);
		itemblock_wood_slab_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_slab_2);
	}

	private static void registerBlockSlabWood3(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_slab_3 = new ItemBlockMeta(block);
		itemblock_wood_slab_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_slab_3);
	}

	private static void registerBlockSlabWood4(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_slab_4 = new ItemBlockMeta(block);
		itemblock_wood_slab_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_slab_4);
	}

	private static void registerBlockSlabWood5(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_slab_5 = new ItemBlockMeta(block);
		itemblock_wood_slab_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_slab_5);
	}

	private static void registerBlockSlabWood6(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_slab_6 = new ItemBlockMeta(block);
		itemblock_wood_slab_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_slab_6);
	}

	private static void registerBlockSlabWood7(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_slab_7 = new ItemBlockMeta(block);
		itemblock_wood_slab_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_slab_7);
	}

	//Wood Trapdoor Blocks
	private static void registerBlockTrapdoorWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_1 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_1);
	}

	private static void registerBlockTrapdoorWood2(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_2 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_2);
	}

	private static void registerBlockTrapdoorWood3(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_3 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_3);
	}

	private static void registerBlockTrapdoorWood4(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_4 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_4);
	}

	private static void registerBlockTrapdoorWood5(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_5 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_5);
	}

	private static void registerBlockTrapdoorWood6(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_6 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_6);
	}

	private static void registerBlockTrapdoorWood7(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_7 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_7);
	}

	private static void registerBlockTrapdoorWood8(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_8 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_8);
	}

	private static void registerBlockTrapdoorWood9(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_9 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_9);
	}

	private static void registerBlockTrapdoorWood10(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_10 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_10.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_10);
	}

	private static void registerBlockTrapdoorWood11(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_11 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_11.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_11);
	}

	private static void registerBlockTrapdoorWood12(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_12 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_12.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_12);
	}

	private static void registerBlockTrapdoorWood13(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_13 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_13.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_13);
	}

	private static void registerBlockTrapdoorWood14(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_14 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_14.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_14);
	}

	private static void registerBlockTrapdoorWood15(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_15 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_15.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_15);
	}

	private static void registerBlockTrapdoorWood16(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_16 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_16.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_16);
	}

	private static void registerBlockTrapdoorWood17(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_17 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_17.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_17);
	}

	private static void registerBlockTrapdoorWood18(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_18 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_18.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_18);
	}

	private static void registerBlockTrapdoorWood19(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_19 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_19.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_19);
	}

	private static void registerBlockTrapdoorWood20(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_20 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_20.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_20);
	}

	private static void registerBlockTrapdoorWood21(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_21 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_21.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_21);
	}

	private static void registerBlockTrapdoorWood22(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_22 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_22.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_22);
	}

	private static void registerBlockTrapdoorWood23(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_23 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_23.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_23);
	}

	private static void registerBlockTrapdoorWood24(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_24 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_24.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_24);
	}

	private static void registerBlockTrapdoorWood25(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_25 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_25.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_25);
	}

	private static void registerBlockTrapdoorWood26(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_26 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_26.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_26);
	}

	private static void registerBlockTrapdoorWood27(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_27 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_27.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_27);
	}

	private static void registerBlockTrapdoorWood28(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_28 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_28.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_28);
	}

	private static void registerBlockTrapdoorWood29(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_29 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_29.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_29);
	}

	private static void registerBlockTrapdoorWood30(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_30 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_30.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_30);
	}

	private static void registerBlockTrapdoorWood31(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_31 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_31.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_31);
	}

	private static void registerBlockTrapdoorWood32(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_32 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_32.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_32);
	}

	private static void registerBlockTrapdoorWood33(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_33 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_33.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_33);
	}

	private static void registerBlockTrapdoorWood34(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_34 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_34.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_34);
	}

	private static void registerBlockTrapdoorWood35(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_35 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_35.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_35);
	}

	private static void registerBlockTrapdoorWood36(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_36 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_36.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_36);
	}

	private static void registerBlockTrapdoorWood37(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_37 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_37.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_37);
	}

	private static void registerBlockTrapdoorWood38(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_38 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_38.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_38);
	}

	private static void registerBlockTrapdoorWood39(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_trapdoormodel_39 = new ItemBlockMeta(block);
		itemblock_wood_trapdoormodel_39.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_trapdoormodel_39);
	}

	//Wood Vertical Slab Blocks
	private static void registerBlockVerticalSlabWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_1 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_1);
	}

	private static void registerBlockVerticalSlabWood2(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_2 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_2);
	}

	private static void registerBlockVerticalSlabWood3(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_3 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_3);
	}

	private static void registerBlockVerticalSlabWood4(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_4 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_4);
	}

	private static void registerBlockVerticalSlabWood5(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_5 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_5);
	}

	private static void registerBlockVerticalSlabWood6(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_6 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_6);
	}

	private static void registerBlockVerticalSlabWood7(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_7 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_7);
	}

	private static void registerBlockVerticalSlabWood8(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_8 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_8);
	}

	private static void registerBlockVerticalSlabWood9(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_9 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_9);
	}

	private static void registerBlockVerticalSlabWood10(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_10 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_10.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_10);
	}

	private static void registerBlockVerticalSlabWood11(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_11 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_11.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_11);
	}

	private static void registerBlockVerticalSlabWood12(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_12 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_12.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_12);
	}

	private static void registerBlockVerticalSlabWood13(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_13 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_13.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_13);
	}

	private static void registerBlockVerticalSlabWood14(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_14 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_14.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_14);
	}

	private static void registerBlockVerticalSlabWood15(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_15 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_15.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_15);
	}

	private static void registerBlockVerticalSlabWood16(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_16 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_16.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_16);
	}

	private static void registerBlockVerticalSlabWood17(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_17 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_17.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_17);
	}

	private static void registerBlockVerticalSlabWood18(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_18 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_18.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_18);
	}

	private static void registerBlockVerticalSlabWood19(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_verticalslab_19 = new ItemBlockMeta(block);
		itemblock_wood_verticalslab_19.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_verticalslab_19);
	}

	//Wood Corner Blocks
	private static void registerBlockCornerWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_1 = new ItemBlockMeta(block);
		itemblock_wood_corner_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_1);
	}

	private static void registerBlockCornerWood2(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_2 = new ItemBlockMeta(block);
		itemblock_wood_corner_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_2);
	}

	private static void registerBlockCornerWood3(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_3 = new ItemBlockMeta(block);
		itemblock_wood_corner_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_3);
	}

	private static void registerBlockCornerWood4(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_4 = new ItemBlockMeta(block);
		itemblock_wood_corner_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_4);
	}

	private static void registerBlockCornerWood5(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_5 = new ItemBlockMeta(block);
		itemblock_wood_corner_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_5);
	}

	private static void registerBlockCornerWood6(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_6 = new ItemBlockMeta(block);
		itemblock_wood_corner_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_6);
	}

	private static void registerBlockCornerWood7(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_7 = new ItemBlockMeta(block);
		itemblock_wood_corner_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_7);
	}

	private static void registerBlockCornerWood8(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_8 = new ItemBlockMeta(block);
		itemblock_wood_corner_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_8);
	}

	private static void registerBlockCornerWood9(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_9 = new ItemBlockMeta(block);
		itemblock_wood_corner_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_9);
	}

	private static void registerBlockCornerWood10(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_10 = new ItemBlockMeta(block);
		itemblock_wood_corner_10.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_10);
	}

	private static void registerBlockCornerWood11(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_11 = new ItemBlockMeta(block);
		itemblock_wood_corner_11.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_11);
	}

	private static void registerBlockCornerWood12(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_12 = new ItemBlockMeta(block);
		itemblock_wood_corner_12.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_12);
	}

	private static void registerBlockCornerWood13(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_13 = new ItemBlockMeta(block);
		itemblock_wood_corner_13.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_13);
	}

	private static void registerBlockCornerWood14(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_14 = new ItemBlockMeta(block);
		itemblock_wood_corner_14.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_14);
	}

	private static void registerBlockCornerWood15(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_15 = new ItemBlockMeta(block);
		itemblock_wood_corner_15.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_15);
	}

	private static void registerBlockCornerWood16(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_16 = new ItemBlockMeta(block);
		itemblock_wood_corner_16.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_16);
	}

	private static void registerBlockCornerWood17(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_17 = new ItemBlockMeta(block);
		itemblock_wood_corner_17.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_17);
	}

	private static void registerBlockCornerWood18(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_18 = new ItemBlockMeta(block);
		itemblock_wood_corner_18.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_18);
	}

	private static void registerBlockCornerWood19(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_corner_19 = new ItemBlockMeta(block);
		itemblock_wood_corner_19.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_corner_19);
	}

	//Wood Ironbar Blocks
	private static void registerBlockIronbarWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_ironbar_1 = new ItemBlockMeta(block);
		itemblock_wood_ironbar_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_ironbar_1);
	}

	//Wood Carpet Blocks
	private static void registerBlockCarpetWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_carpet_1 = new ItemBlockMeta(block);
		itemblock_wood_carpet_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_carpet_1);
	}

	//No Collision Wood Blocks
	private static void registerBlockNoCollisionWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_nocollision_1 = new ItemBlockMeta(block);
		itemblock_wood_nocollision_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_nocollision_1);
	}

	//Directional No Collision Wood Blocks
	private static void registerBlockDirectionalNoCollisionWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_directionalnocollision_1 = new ItemBlockMeta(block);
		itemblock_wood_directionalnocollision_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_directionalnocollision_1);
	}

	//Directional Collision Trapdoor Wood Blocks
	private static void registerBlockDirectionalCollisionTrapdoorWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_directionalcollisiontrapdoor_1 = new ItemBlockMeta(block);
		itemblock_wood_directionalcollisiontrapdoor_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_directionalcollisiontrapdoor_1);
	}

	private static void registerBlockDirectionalCollisionTrapdoorWood2(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_directionalcollisiontrapdoor_2 = new ItemBlockMeta(block);
		itemblock_wood_directionalcollisiontrapdoor_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_directionalcollisiontrapdoor_2);
	}

	private static void registerBlockDirectionalCollisionTrapdoorWood3(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_directionalcollisiontrapdoor_3 = new ItemBlockMeta(block);
		itemblock_wood_directionalcollisiontrapdoor_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_directionalcollisiontrapdoor_3);
	}

	//Enchanted Wood Blocks
	private static void registerBlockEnchantedWood1(Block block)
	{
		GameRegistry.register(block);
		itemblock_wood_enchantedbook_1 = new ItemBlockMeta(block);
		itemblock_wood_enchantedbook_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_wood_enchantedbook_1);
	}

	//Full Iron Blocks
	private static void registerBlockIron1(Block block)
	{
		GameRegistry.register(block);
		itemblock_iron_full_1 = new ItemBlockMeta(block);
		itemblock_iron_full_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_iron_full_1);
	}

	//Full Partial Light 1.0 Iron Blocks
	private static void registerBlockFullPartialLight1Iron1(Block block)
	{
		GameRegistry.register(block);
		itemblock_iron_fullpartiallight10_1 = new ItemBlockMeta(block);
		itemblock_iron_fullpartiallight10_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_iron_fullpartiallight10_1);
	}

	//Iron Anvil Blocks
	private static void registerBlockAnvilIron1(Block block)
	{
		GameRegistry.register(block);
		itemblock_iron_anvil_1 = new ItemBlockMeta(block);
		itemblock_iron_anvil_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_iron_anvil_1);
	}

	//Iron Ironbar Blocks
	private static void registerBlockIronbarIron1(Block block)
	{
		GameRegistry.register(block);
		itemblock_iron_ironbar_1 = new ItemBlockMeta(block);
		itemblock_iron_ironbar_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_iron_ironbar_1);
	}

	//Stairs Iron Blocks
	private static void registerBlockStairsIron1(Block block)
	{
		GameRegistry.register(block);
		itemblock_iron_stairs_1 = new ItemBlockMeta(block);
		itemblock_iron_stairs_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_iron_stairs_1);
	}

	private static void registerBlockStairsIron2(Block block)
	{
		GameRegistry.register(block);
		itemblock_iron_stairs_2 = new ItemBlockMeta(block);
		itemblock_iron_stairs_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_iron_stairs_2);
	}
	//Iron Trapdoor Blocks
	private static void registerBlockTrapdoorIron1(Block block)
	{
		GameRegistry.register(block);
		itemblock_iron_trapdoormodel_1 = new ItemBlockMeta(block);
		itemblock_iron_trapdoormodel_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_iron_trapdoormodel_1);
	}

	private static void registerBlockTrapdoorIron2(Block block)
	{
		GameRegistry.register(block);
		itemblock_iron_trapdoormodel_2 = new ItemBlockMeta(block);
		itemblock_iron_trapdoormodel_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_iron_trapdoormodel_2);
	}

	//Directional No Collision Iron Blocks
	private static void registerBlockDirectionalNoCollisionIron1(Block block)
	{
		GameRegistry.register(block);
		itemblock_iron_directionalnocollision_1 = new ItemBlockMeta(block);
		itemblock_iron_directionalnocollision_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_iron_directionalnocollision_1);
	}
	private static void registerBlockDirectionalNoCollisionIron2(Block block)
	{
		GameRegistry.register(block);
		itemblock_iron_directionalnocollision_2 = new ItemBlockMeta(block);
		itemblock_iron_directionalnocollision_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_iron_directionalnocollision_2);
	}

	//Plants Log Blocks
	private static void registerBlockLogPlants1(Block block)
	{
		GameRegistry.register(block);
		itemblock_plants_log_1 = new ItemBlockMeta(block);
		itemblock_plants_log_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_plants_log_1);
	}

	//Stairs Plants Blocks
	private static void registerBlockStairsPlants1(Block block)
	{
		GameRegistry.register(block);
		itemblock_plants_stairs_1 = new ItemBlockMeta(block);
		itemblock_plants_stairs_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_plants_stairs_1);
	}

	//Slab Plants Blocks
	private static void registerBlockSlabPlants1(Block block)
	{
		GameRegistry.register(block);
		itemblock_plants_slab_1 = new ItemBlockMeta(block);
		itemblock_plants_slab_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_plants_slab_1);
	}

	//No Collision Plants Blocks
	private static void registerBlockNoCollisionPlants1(Block block)
	{
		GameRegistry.register(block);
		itemblock_plants_nocollision_1 = new ItemBlockMeta(block);
		itemblock_plants_nocollision_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_plants_nocollision_1);
	}

	private static void registerBlockNoCollisionPlants2(Block block)
	{
		GameRegistry.register(block);
		itemblock_plants_nocollision_2 = new ItemBlockMeta(block);
		itemblock_plants_nocollision_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_plants_nocollision_2);
	}

	private static void registerBlockNoCollisionPlants3(Block block)
	{
		GameRegistry.register(block);
		itemblock_plants_nocollision_3 = new ItemBlockMeta(block);
		itemblock_plants_nocollision_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_plants_nocollision_3);
	}

	private static void registerBlockNoCollisionPlants4(Block block)
	{
		GameRegistry.register(block);
		itemblock_plants_nocollision_4 = new ItemBlockMeta(block);
		itemblock_plants_nocollision_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_plants_nocollision_4);
	}

	private static void registerBlockNoCollisionPlants5(Block block)
	{
		GameRegistry.register(block);
		itemblock_plants_nocollision_5 = new ItemBlockMeta(block);
		itemblock_plants_nocollision_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_plants_nocollision_5);
	}

	//No Collision Biome Plants Blocks
	private static void registerBlockNoCollisionBiomePlants1(Block block)
	{
		GameRegistry.register(block);
		itemblock_plants_nocollisionbiome_1 = new ItemBlockMeta(block);
		itemblock_plants_nocollisionbiome_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_plants_nocollisionbiome_1);
	}

	//Full Leaves Blocks
	private static void registerBlockLeaves1(Block block)
	{
		GameRegistry.register(block);
		itemblock_leaves_full_1 = new ItemBlockMeta(block);
		itemblock_leaves_full_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_leaves_full_1);
	}

	//Full Sand Blocks
	private static void registerBlockSand1(Block block)
	{
		GameRegistry.register(block);
		itemblock_sand_full_1 = new ItemBlockMeta(block);
		itemblock_sand_full_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_sand_full_1);
	}

	//Stone Layer Blocks
	private static void registerBlockLayerSand1(Block block)
	{
		GameRegistry.register(block);
		itemblock_sand_layer_1 = new ItemLayerMeta(block);
		itemblock_sand_layer_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_sand_layer_1);
	}

	private static void registerBlockLayerSand2(Block block)
	{
		GameRegistry.register(block);
		itemblock_sand_layer_2 = new ItemLayerMeta(block);
		itemblock_sand_layer_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_sand_layer_2);
	}

	private static void registerBlockLayerSand3(Block block)
	{
		GameRegistry.register(block);
		itemblock_sand_layer_3 = new ItemLayerMeta(block);
		itemblock_sand_layer_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_sand_layer_3);
	}

	private static void registerBlockLayerSand4(Block block)
	{
		GameRegistry.register(block);
		itemblock_sand_layer_4 = new ItemLayerMeta(block);
		itemblock_sand_layer_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_sand_layer_4);
	}

	private static void registerBlockLayerSand5(Block block)
	{
		GameRegistry.register(block);
		itemblock_sand_layer_5 = new ItemLayerMeta(block);
		itemblock_sand_layer_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_sand_layer_5);
	}

	private static void registerBlockLayerSand6(Block block)
	{
		GameRegistry.register(block);
		itemblock_sand_layer_6 = new ItemLayerMeta(block);
		itemblock_sand_layer_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_sand_layer_6);
	}

	//Full Ground Blocks
	private static void registerBlockGround1(Block block)
	{
		GameRegistry.register(block);
		itemblock_ground_full_1 = new ItemBlockMeta(block);
		itemblock_ground_full_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ground_full_1);
	}

	private static void registerBlockGround2(Block block)
	{
		GameRegistry.register(block);
		itemblock_ground_full_2 = new ItemBlockMeta(block);
		itemblock_ground_full_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ground_full_2);
	}

	//Soulsand Ground Blocks
	private static void registerBlockSoulsandGround1(Block block)
	{
		GameRegistry.register(block);
		itemblock_ground_soulsand_1 = new ItemBlockMeta(block);
		itemblock_ground_soulsand_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ground_soulsand_1);
	}

	//Directional Full Partial Ground Blocks
	private static void registerBlockDirectionalFullPartialGround1(Block block)
	{
		GameRegistry.register(block);
		itemblock_ground_directionalfullpartial_1 = new ItemBlockMeta(block);
		itemblock_ground_directionalfullpartial_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ground_directionalfullpartial_1);
	}

	//Ground Layer Blocks
	private static void registerBlockLayerGround1(Block block)
	{
		GameRegistry.register(block);
		itemblock_ground_layer_1 = new ItemLayerMeta(block);
		itemblock_ground_layer_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ground_layer_1);
	}

	private static void registerBlockLayerGround2(Block block)
	{
		GameRegistry.register(block);
		itemblock_ground_layer_2 = new ItemLayerMeta(block);
		itemblock_ground_layer_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ground_layer_2);
	}

	private static void registerBlockLayerGround3(Block block)
	{
		GameRegistry.register(block);
		itemblock_ground_layer_3 = new ItemLayerMeta(block);
		itemblock_ground_layer_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ground_layer_3);
	}

	private static void registerBlockLayerGround4(Block block)
	{
		GameRegistry.register(block);
		itemblock_ground_layer_4 = new ItemLayerMeta(block);
		itemblock_ground_layer_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ground_layer_4);
	}

	private static void registerBlockLayerGround5(Block block)
	{
		GameRegistry.register(block);
		itemblock_ground_layer_5 = new ItemLayerMeta(block);
		itemblock_ground_layer_5.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ground_layer_5);
	}

	private static void registerBlockLayerGround6(Block block)
	{
		GameRegistry.register(block);
		itemblock_ground_layer_6 = new ItemLayerMeta(block);
		itemblock_ground_layer_6.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ground_layer_6);
	}

	private static void registerBlockLayerGround7(Block block)
	{
		GameRegistry.register(block);
		itemblock_ground_layer_7 = new ItemLayerMeta(block);
		itemblock_ground_layer_7.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ground_layer_7);
	}

	private static void registerBlockLayerGround8(Block block)
	{
		GameRegistry.register(block);
		itemblock_ground_layer_8 = new ItemLayerMeta(block);
		itemblock_ground_layer_8.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ground_layer_8);
	}

	private static void registerBlockLayerGround9(Block block)
	{
		GameRegistry.register(block);
		itemblock_ground_layer_9 = new ItemLayerMeta(block);
		itemblock_ground_layer_9.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ground_layer_9);
	}

	private static void registerBlockLayerGround10(Block block)
	{
		GameRegistry.register(block);
		itemblock_ground_layer_10 = new ItemLayerMeta(block);
		itemblock_ground_layer_10.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ground_layer_10);
	}

	private static void registerBlockLayerGround11(Block block)
	{
		GameRegistry.register(block);
		itemblock_ground_layer_11 = new ItemLayerMeta(block);
		itemblock_ground_layer_11.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ground_layer_11);
	}

	private static void registerBlockLayerGround12(Block block)
	{
		GameRegistry.register(block);
		itemblock_ground_layer_12 = new ItemLayerMeta(block);
		itemblock_ground_layer_12.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ground_layer_12);
	}

	private static void registerBlockLayerGround13(Block block)
	{
		GameRegistry.register(block);
		itemblock_ground_layer_13 = new ItemLayerMeta(block);
		itemblock_ground_layer_13.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ground_layer_13);
	}

	//Slab Cloth Blocks
	private static void registerBlockSlabCloth1(Block block)
	{
		GameRegistry.register(block);
		itemblock_cloth_slab_1 = new ItemBlockMeta(block);
		itemblock_cloth_slab_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_cloth_slab_1);
	}
	private static void registerBlockSlabCloth2(Block block)
	{
		GameRegistry.register(block);
		itemblock_cloth_slab_2 = new ItemBlockMeta(block);
		itemblock_cloth_slab_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_cloth_slab_2);
	}
	//Pane Cloth Blocks
	private static void registerBlockPaneCloth1(Block block)
	{
		GameRegistry.register(block);
		itemblock_cloth_pane_1 = new ItemBlockMeta(block);
		itemblock_cloth_pane_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_cloth_pane_1);
	}
	//Climbable Iron Bar Cloth Blocks
	private static void registerBlockClimbIronbarCloth1(Block block)
	{
		GameRegistry.register(block);
		itemblock_cloth_climbironbar_1 = new ItemBlockMeta(block);
		itemblock_cloth_climbironbar_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_cloth_climbironbar_1);
	}
	//Climbable Iron Bar Iron/Cloth Blocks
	private static void registerBlockIronClimbIronbarCloth1(Block block)
	{
		GameRegistry.register(block);
		itemblock_cloth_ironclimbironbar_1 = new ItemBlockMeta(block);
		itemblock_cloth_ironclimbironbar_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_cloth_ironclimbironbar_1);
	}

	//No Collision Cloth Blocks
	private static void registerBlockLight1Cloth1(Block block)
	{
		GameRegistry.register(block);
		itemblock_cloth_fulllight10_1 = new ItemBlockMeta(block);
		itemblock_cloth_fulllight10_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_cloth_fulllight10_1);
	}

	//No Collision Cloth Blocks
	private static void registerBlockNoCollisionCloth1(Block block)
	{
		GameRegistry.register(block);
		itemblock_cloth_nocollision_1 = new ItemBlockMeta(block);
		itemblock_cloth_nocollision_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_cloth_nocollision_1);
	}

	//Directional No Collision Cloth Blocks
	private static void registerBlockDirectionalNoCollisionCloth1(Block block)
	{
		GameRegistry.register(block);
		itemblock_cloth_directionalnocollision_1 = new ItemBlockMeta(block);
		itemblock_cloth_directionalnocollision_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_cloth_directionalnocollision_1);
	}


	//Translucent Ice Blocks
	private static void registerBlockTranslucentIce1(Block block)
	{
		GameRegistry.register(block);
		itemblock_ice_translucent_1 = new ItemBlockMeta(block);
		itemblock_ice_translucent_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ice_translucent_1);
	}

	//Slab Ice Blocks
	private static void registerBlockSlabIce1(Block block)
	{
		GameRegistry.register(block);
		itemblock_ice_slab_1 = new ItemBlockMeta(block);
		itemblock_ice_slab_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ice_slab_1);
	}

	//No Collision Ice Blocks
	private static void registerBlockNoCollisionIce1(Block block)
	{
		GameRegistry.register(block);
		itemblock_ice_nocollision_1 = new ItemBlockMeta(block);
		itemblock_ice_nocollision_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_ice_nocollision_1);
	}

	//Stairs Snow Blocks
	private static void registerBlockStairsSnow1(Block block)
	{
		GameRegistry.register(block);
		itemblock_snow_stairs_1 = new ItemBlockMeta(block);
		itemblock_snow_stairs_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_snow_stairs_1);
	}

	//No Collision No Material Blocks
	private static void registerBlockNoCollisionNoMaterial1(Block block)
	{
		GameRegistry.register(block);
		itemblock_nomaterial_nocollision_1 = new ItemBlockMeta(block);
		itemblock_nomaterial_nocollision_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_nomaterial_nocollision_1);
	}

	//Glass Glass Blocks
	private static void registerBlockFullGlass1(Block block)
	{
		GameRegistry.register(block);
		itemblock_glass_full_1 = new ItemBlockMeta(block);
		itemblock_glass_full_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_glass_full_1);
	}

	//Glass Glass Blocks
	private static void registerBlockGlass1(Block block)
	{
		GameRegistry.register(block);
		itemblock_glass_glass_1 = new ItemBlockMeta(block);
		itemblock_glass_glass_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_glass_glass_1);
	}

	private static void registerBlockGlass2(Block block)
	{
		GameRegistry.register(block);
		itemblock_glass_glass_2 = new ItemBlockMeta(block);
		itemblock_glass_glass_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_glass_glass_2);
	}

	//Glass Vertical Slab Blocks
	private static void registerBlockVerticalSlabGlass1(Block block)
	{
		GameRegistry.register(block);
		itemblock_glass_verticalslab_1 = new ItemBlockMeta(block);
		itemblock_glass_verticalslab_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_glass_verticalslab_1);
	}

	//Glass Trapdoor Blocks
	private static void registerBlockTrapdoorGlass1(Block block)
	{
		GameRegistry.register(block);
		itemblock_glass_trapdoormodel_1 = new ItemBlockMeta(block);
		itemblock_glass_trapdoormodel_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_glass_trapdoormodel_1);
	}

	private static void registerBlockTrapdoorGlass2(Block block)
	{
		GameRegistry.register(block);
		itemblock_glass_trapdoormodel_2 = new ItemBlockMeta(block);
		itemblock_glass_trapdoormodel_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_glass_trapdoormodel_2);
	}

	private static void registerBlockTrapdoorGlass3(Block block)
	{
		GameRegistry.register(block);
		itemblock_glass_trapdoormodel_3 = new ItemBlockMeta(block);
		itemblock_glass_trapdoormodel_3.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_glass_trapdoormodel_3);
	}

	private static void registerBlockTrapdoorGlass4(Block block)
	{
		GameRegistry.register(block);
		itemblock_glass_trapdoormodel_4 = new ItemBlockMeta(block);
		itemblock_glass_trapdoormodel_4.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_glass_trapdoormodel_4);
	}

	//Glass Pane Blocks
	private static void registerBlockPaneGlass1(Block block)
	{
		GameRegistry.register(block);
		itemblock_glass_pane_1 = new ItemBlockMeta(block);
		itemblock_glass_pane_1.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_glass_pane_1);
	}

	private static void registerBlockPaneGlass2(Block block)
	{
		GameRegistry.register(block);
		itemblock_glass_pane_2 = new ItemBlockMeta(block);
		itemblock_glass_pane_2.setRegistryName(block.getRegistryName());
		GameRegistry.register(itemblock_glass_pane_2);
	}

	/**
	 * For templates check Reference class.
	 */

}
