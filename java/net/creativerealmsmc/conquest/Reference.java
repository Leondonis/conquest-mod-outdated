package net.creativerealmsmc.conquest;

public class Reference 
{
/**
 * Created by Artemisia on 11/17/2016.
 */

	//Register templates
/**	
 	private static void registerBlockName(Block block)
	{
		GameRegistry.register(block);
		ItemBlockName = new ItemBlockMeta(block);
		ItemBlockName.setRegistryName(block.getRegistryName());
		GameRegistry.register(ItemBlockName);
	}
*/

/**
--> For only layer blocks !!

	private static void registerBlockName(Block block)
	{
		GameRegistry.register(block);
		ItemBlockName = new ItemLayerMeta(block);
		ItemBlockName.setRegistryName(block.getRegistryName());
		GameRegistry.register(ItemBlockName);
	}
*/
	
/**
	public static void registerRenders()
	{
		registerRender(ItemBlockName, 0, "conquest:_alpha");
		registerRender(ItemBlockName, 1, "conquest:_bravo");
		registerRender(ItemBlockName, 2, "conquest:_charlie");
		registerRender(ItemBlockName, 3, "conquest:_delta");
		registerRender(ItemBlockName, 4, "conquest:_echo");
		registerRender(ItemBlockName, 5, "conquest:_fox");
		registerRender(ItemBlockName, 6, "conquest:_golf");
		registerRender(ItemBlockName, 7, "conquest:_hotel");
		registerRender(ItemBlockName, 8, "conquest:_india");
		registerRender(ItemBlockName, 9, "conquest:_juliet");
		registerRender(ItemBlockName, 10, "conquest:_kilo");
		registerRender(ItemBlockName, 11, "conquest:_lima");
		registerRender(ItemBlockName, 12, "conquest:_mike");
		registerRender(ItemBlockName, 13, "conquest:_november");
		registerRender(ItemBlockName, 14, "conquest:_oscar");
		registerRender(ItemBlockName, 15, "conquest:_papa");
	}
*/

//Blockstates templates
/**
-->Cube Blocks(BlockSimple, BlockLeaves)
{
    "variants": {
        "variant=alpha": { "model": "conquest:_alpha" },
        "variant=bravo": { "model": "conquest:_bravo" },
        "variant=charlie": { "model": "conquest:_charlie" },
        "variant=delta": { "model": "conquest:_delta" },
        "variant=echo": { "model": "conquest:_echo" },
        "variant=fox": { "model": "conquest:_fox" },
        "variant=golf": { "model": "conquest:_golf" },
        "variant=hotel": { "model": "conquest:_hotel" },
        "variant=india": { "model": "conquest:_india" },
        "variant=juliet": { "model": "conquest:_juliet" },
        "variant=kilo": { "model": "conquest:_kilo" },
        "variant=lima": { "model": "conquest:_lima" },
        "variant=mike": { "model": "conquest:_mike" },
        "variant=november": { "model": "conquest:_november" },
        "variant=oscar": { "model": "conquest:_oscar" },
        "variant=papa": { "model": "conquest:_papa" }       
    }
}

-->Slabs
{
    "variants": {
        "half=bottom,variant=alpha": { "model": "conquest:_alpha" },
        "half=bottom,variant=bravo": { "model": "conquest:_bravo" },
        "half=bottom,variant=charlie": { "model": "conquest:_charlie" },
        "half=bottom,variant=delta": { "model": "conquest:_delta" },
        "half=bottom,variant=echo": { "model": "conquest:_echo" },
        "half=bottom,variant=fox": { "model": "conquest:_fox" },
        "half=bottom,variant=golf": { "model": "conquest:_golf" },
        "half=bottom,variant=hotel": { "model": "conquest:_hotel" },
        "half=top,variant=alpha": { "model": "conquest:_alpha" },
        "half=top,variant=bravo": { "model": "conquest:_bravo" },
        "half=top,variant=charlie": { "model": "conquest:_charlie" },
        "half=top,variant=delta": { "model": "conquest:_delta" },
        "half=top,variant=echo": { "model": "conquest:_echo" },
        "half=top,variant=fox": { "model": "conquest:_fox" },
        "half=top,variant=golf": { "model": "conquest:_golf" },
        "half=top,variant=hotel": { "model": "conquest:_hotel" }     
    }
}

-->Stairs
{
    "variants": {
        "facing=east,half=bottom,shape=straight,variant=alpha":  { "model": "conquest:_stairs_alpha" },
        "facing=west,half=bottom,shape=straight,variant=alpha":  { "model": "conquest:_stairs_alpha", "y": 180, "uvlock": true },
        "facing=south,half=bottom,shape=straight,variant=alpha": { "model": "conquest:_stairs_alpha", "y": 90, "uvlock": true },
        "facing=north,half=bottom,shape=straight,variant=alpha": { "model": "conquest:_stairs_alpha", "y": 270, "uvlock": true },
        "facing=east,half=bottom,shape=outer_right,variant=alpha":  { "model": "conquest:_outer_stairs_alpha" },
        "facing=west,half=bottom,shape=outer_right,variant=alpha":  { "model": "conquest:_outer_stairs_alpha", "y": 180, "uvlock": true },
        "facing=south,half=bottom,shape=outer_right,variant=alpha": { "model": "conquest:_outer_stairs_alpha", "y": 90, "uvlock": true },
        "facing=north,half=bottom,shape=outer_right,variant=alpha": { "model": "conquest:_outer_stairs_alpha", "y": 270, "uvlock": true },
        "facing=east,half=bottom,shape=outer_left,variant=alpha":  { "model": "conquest:_outer_stairs_alpha", "y": 270, "uvlock": true },
        "facing=west,half=bottom,shape=outer_left,variant=alpha":  { "model": "conquest:_outer_stairs_alpha", "y": 90, "uvlock": true },
        "facing=south,half=bottom,shape=outer_left,variant=alpha": { "model": "conquest:_outer_stairs_alpha" },
        "facing=north,half=bottom,shape=outer_left,variant=alpha": { "model": "conquest:_outer_stairs_alpha", "y": 180, "uvlock": true },
        "facing=east,half=bottom,shape=inner_right,variant=alpha":  { "model": "conquest:_inner_stairs_alpha" },
        "facing=west,half=bottom,shape=inner_right,variant=alpha":  { "model": "conquest:_inner_stairs_alpha", "y": 180, "uvlock": true },
        "facing=south,half=bottom,shape=inner_right,variant=alpha": { "model": "conquest:_inner_stairs_alpha", "y": 90, "uvlock": true },
        "facing=north,half=bottom,shape=inner_right,variant=alpha": { "model": "conquest:_inner_stairs_alpha", "y": 270, "uvlock": true },
        "facing=east,half=bottom,shape=inner_left,variant=alpha":  { "model": "conquest:_inner_stairs_alpha", "y": 270, "uvlock": true },
        "facing=west,half=bottom,shape=inner_left,variant=alpha":  { "model": "conquest:_inner_stairs_alpha", "y": 90, "uvlock": true },
        "facing=south,half=bottom,shape=inner_left,variant=alpha": { "model": "conquest:_inner_stairs_alpha" },
        "facing=north,half=bottom,shape=inner_left,variant=alpha": { "model": "conquest:_inner_stairs_alpha", "y": 180, "uvlock": true },
        "facing=east,half=top,shape=straight,variant=alpha":  { "model": "conquest:_stairs_alpha", "x": 180, "uvlock": true },
        "facing=west,half=top,shape=straight,variant=alpha":  { "model": "conquest:_stairs_alpha", "x": 180, "y": 180, "uvlock": true },
        "facing=south,half=top,shape=straight,variant=alpha": { "model": "conquest:_stairs_alpha", "x": 180, "y": 90, "uvlock": true },
        "facing=north,half=top,shape=straight,variant=alpha": { "model": "conquest:_stairs_alpha", "x": 180, "y": 270, "uvlock": true },
        "facing=east,half=top,shape=outer_right,variant=alpha":  { "model": "conquest:_outer_stairs_alpha", "x": 180, "y": 90, "uvlock": true },
        "facing=west,half=top,shape=outer_right,variant=alpha":  { "model": "conquest:_outer_stairs_alpha", "x": 180, "y": 270, "uvlock": true },
        "facing=south,half=top,shape=outer_right,variant=alpha": { "model": "conquest:_outer_stairs_alpha", "x": 180, "y": 180, "uvlock": true },
        "facing=north,half=top,shape=outer_right,variant=alpha": { "model": "conquest:_outer_stairs_alpha", "x": 180, "uvlock": true },
        "facing=east,half=top,shape=outer_left,variant=alpha":  { "model": "conquest:_outer_stairs_alpha", "x": 180, "uvlock": true },
        "facing=west,half=top,shape=outer_left,variant=alpha":  { "model": "conquest:_outer_stairs_alpha", "x": 180, "y": 180, "uvlock": true },
        "facing=south,half=top,shape=outer_left,variant=alpha": { "model": "conquest:_outer_stairs_alpha", "x": 180, "y": 90, "uvlock": true },
        "facing=north,half=top,shape=outer_left,variant=alpha": { "model": "conquest:_outer_stairs_alpha", "x": 180, "y": 270, "uvlock": true },
        "facing=east,half=top,shape=inner_right,variant=alpha":  { "model": "conquest:_inner_stairs_alpha", "x": 180, "y": 90, "uvlock": true },
        "facing=west,half=top,shape=inner_right,variant=alpha":  { "model": "conquest:_inner_stairs_alpha", "x": 180, "y": 270, "uvlock": true },
        "facing=south,half=top,shape=inner_right,variant=alpha": { "model": "conquest:_inner_stairs_alpha", "x": 180, "y": 180, "uvlock": true },
        "facing=north,half=top,shape=inner_right,variant=alpha": { "model": "conquest:_inner_stairs_alpha", "x": 180, "uvlock": true },
        "facing=east,half=top,shape=inner_left,variant=alpha":  { "model": "conquest:_inner_stairs_alpha", "x": 180, "uvlock": true },
        "facing=west,half=top,shape=inner_left,variant=alpha":  { "model": "conquest:_inner_stairs_alpha", "x": 180, "y": 180, "uvlock": true },
        "facing=south,half=top,shape=inner_left,variant=alpha": { "model": "conquest:_inner_stairs_alpha", "x": 180, "y": 90, "uvlock": true },
        "facing=north,half=top,shape=inner_left,variant=alpha": { "model": "conquest:_inner_stairs_alpha", "x": 180, "y": 270, "uvlock": true },
        
        "facing=east,half=bottom,shape=straight,variant=bravo":  { "model": "conquest:_stairs_bravo" },
        "facing=west,half=bottom,shape=straight,variant=bravo":  { "model": "conquest:_stairs_bravo", "y": 180, "uvlock": true },
        "facing=south,half=bottom,shape=straight,variant=bravo": { "model": "conquest:_stairs_bravo", "y": 90, "uvlock": true },
        "facing=north,half=bottom,shape=straight,variant=bravo": { "model": "conquest:_stairs_bravo", "y": 270, "uvlock": true },
        "facing=east,half=bottom,shape=outer_right,variant=bravo":  { "model": "conquest:_outer_stairs_bravo" },
        "facing=west,half=bottom,shape=outer_right,variant=bravo":  { "model": "conquest:_outer_stairs_bravo", "y": 180, "uvlock": true },
        "facing=south,half=bottom,shape=outer_right,variant=bravo": { "model": "conquest:_outer_stairs_bravo", "y": 90, "uvlock": true },
        "facing=north,half=bottom,shape=outer_right,variant=bravo": { "model": "conquest:_outer_stairs_bravo", "y": 270, "uvlock": true },
        "facing=east,half=bottom,shape=outer_left,variant=bravo":  { "model": "conquest:_outer_stairs_bravo", "y": 270, "uvlock": true },
        "facing=west,half=bottom,shape=outer_left,variant=bravo":  { "model": "conquest:_outer_stairs_bravo", "y": 90, "uvlock": true },
        "facing=south,half=bottom,shape=outer_left,variant=bravo": { "model": "conquest:_outer_stairs_bravo" },
        "facing=north,half=bottom,shape=outer_left,variant=bravo": { "model": "conquest:_outer_stairs_bravo", "y": 180, "uvlock": true },
        "facing=east,half=bottom,shape=inner_right,variant=bravo":  { "model": "conquest:_inner_stairs_bravo" },
        "facing=west,half=bottom,shape=inner_right,variant=bravo":  { "model": "conquest:_inner_stairs_bravo", "y": 180, "uvlock": true },
        "facing=south,half=bottom,shape=inner_right,variant=bravo": { "model": "conquest:_inner_stairs_bravo", "y": 90, "uvlock": true },
        "facing=north,half=bottom,shape=inner_right,variant=bravo": { "model": "conquest:_inner_stairs_bravo", "y": 270, "uvlock": true },
        "facing=east,half=bottom,shape=inner_left,variant=bravo":  { "model": "conquest:_inner_stairs_bravo", "y": 270, "uvlock": true },
        "facing=west,half=bottom,shape=inner_left,variant=bravo":  { "model": "conquest:_inner_stairs_bravo:_inner_stairs_bravo", "y": 90, "uvlock": true },
        "facing=south,half=bottom,shape=inner_left,variant=bravo": { "model": "conquest:_inner_stairs_bravo" },
        "facing=north,half=bottom,shape=inner_left,variant=bravo": { "model": "conquest:_inner_stairs_bravo", "y": 180, "uvlock": true },
        "facing=east,half=top,shape=straight,variant=bravo":  { "model": "conquest:_stairs_bravo", "x": 180, "uvlock": true },
        "facing=west,half=top,shape=straight,variant=bravo":  { "model": "conquest:_stairs_bravo", "x": 180, "y": 180, "uvlock": true },
        "facing=south,half=top,shape=straight,variant=bravo": { "model": "conquest:_stairs_bravo", "x": 180, "y": 90, "uvlock": true },
        "facing=north,half=top,shape=straight,variant=bravo": { "model": "conquest:_stairs_bravo", "x": 180, "y": 270, "uvlock": true },
        "facing=east,half=top,shape=outer_right,variant=bravo":  { "model": "conquest:_outer_stairs_bravo", "x": 180, "y": 90, "uvlock": true },
        "facing=west,half=top,shape=outer_right,variant=bravo":  { "model": "conquest:_outer_stairs_bravo", "x": 180, "y": 270, "uvlock": true },
        "facing=south,half=top,shape=outer_right,variant=bravo": { "model": "conquest:_outer_stairs_bravo", "x": 180, "y": 180, "uvlock": true },
        "facing=north,half=top,shape=outer_right,variant=bravo": { "model": "conquest:_outer_stairs_bravo", "x": 180, "uvlock": true },
        "facing=east,half=top,shape=outer_left,variant=bravo":  { "model": "conquest:_outer_stairs_bravo", "x": 180, "uvlock": true },
        "facing=west,half=top,shape=outer_left,variant=bravo":  { "model": "conquest:_outer_stairs_bravo", "x": 180, "y": 180, "uvlock": true },
        "facing=south,half=top,shape=outer_left,variant=bravo": { "model": "conquest:_outer_stairs_bravo", "x": 180, "y": 90, "uvlock": true },
        "facing=north,half=top,shape=outer_left,variant=bravo": { "model": "conquest:_outer_stairs_bravo", "x": 180, "y": 270, "uvlock": true },
        "facing=east,half=top,shape=inner_right,variant=bravo":  { "model": "conquest:_inner_stairs_bravo", "x": 180, "y": 90, "uvlock": true },
        "facing=west,half=top,shape=inner_right,variant=bravo":  { "model": "conquest:_inner_stairs_bravo", "x": 180, "y": 270, "uvlock": true },
        "facing=south,half=top,shape=inner_right,variant=bravo": { "model": "conquest:_inner_stairs_bravo", "x": 180, "y": 180, "uvlock": true },
        "facing=north,half=top,shape=inner_right,variant=bravo": { "model": "conquest:_inner_stairs_bravo", "x": 180, "uvlock": true },
        "facing=east,half=top,shape=inner_left,variant=bravo":  { "model": "conquest:_inner_stairs_bravo", "x": 180, "uvlock": true },
        "facing=west,half=top,shape=inner_left,variant=bravo":  { "model": "conquest:_inner_stairs_bravo", "x": 180, "y": 180, "uvlock": true },
        "facing=south,half=top,shape=inner_left,variant=bravo": { "model": "conquest:_inner_stairs_bravo", "x": 180, "y": 90, "uvlock": true },
        "facing=north,half=top,shape=inner_left,variant=bravo": { "model": "conquest:_inner_stairs_bravo", "x": 180, "y": 270, "uvlock": true }
    }
}

-->Anvil
{
    "variants": {
        "axis=y,variant=alpha": { "model": "conquest:anvil_base_" },
        "axis=x,variant=alpha": { "model": "conquest:anvil_", "y": 90 },
        "axis=z,variant=alpha": { "model": "conquest:anvil_", "y": 180 },
        "axis=none,variant=alpha": { "model": "conquest:anvil_body_" },
        
        "axis=y,variant=bravo": { "model": "conquest:anvil_base_" },
        "axis=x,variant=bravo": { "model": "conquest:anvil_", "y": 90 },
        "axis=z,variant=bravo": { "model": "conquest:anvil_", "y": 180 },
        "axis=none,variant=bravo": { "model": "conquest:anvil_body_" },
        
        "axis=y,variant=charlie": { "model": "conquest:anvil_base_" },
        "axis=x,variant=charlie": { "model": "conquest:anvil_", "y": 90 },
        "axis=z,variant=charlie": { "model": "conquest:anvil_", "y": 180 },
        "axis=none,variant=charlie": { "model": "conquest:anvil_body_" },
        
        "axis=y,variant=delta": { "model": "conquest:anvil_base_" },
        "axis=x,variant=delta": { "model": "conquest:anvil_", "y": 90 },
        "axis=z,variant=delta": { "model": "conquest:anvil_", "y": 180 },
        "axis=none,variant=delta": { "model": "conquest:anvil_body_" }
    }
}

--> Corner Block
{
    "variants": {
        "facing=east,variant=alpha": { "model": "conquest:corner_" },
        "facing=south,variant=alpha":  { "model": "conquest:corner_", "y": 90 },
        "facing=west,variant=alpha": { "model": "conquest:corner_", "y": 180 },
        "facing=north,variant=alpha":  { "model": "conquest:corner_", "y": 270 },
        
        "facing=east,variant=bravo": { "model": "conquest:corner_" },
        "facing=south,variant=bravo":  { "model": "conquest:corner_", "y": 90 },
        "facing=west,variant=bravo": { "model": "conquest:corner_", "y": 180 },
        "facing=north,variant=bravo":  { "model": "conquest:corner_", "y": 270 },
        
        "facing=east,variant=charlie": { "model": "conquest:corner_" },
        "facing=south,variant=charlie":  { "model": "conquest:corner_", "y": 90 },
        "facing=west,variant=charlie": { "model": "conquest:corner_", "y": 180 },
        "facing=north,variant=charlie":  { "model": "conquest:corner_", "y": 270 },
        
        "facing=east,variant=delta": { "model": "conquest:corner_" },
        "facing=south,variant=delta":  { "model": "conquest:corner_", "y": 90 },
        "facing=west,variant=delta": { "model": "conquest:corner_", "y": 180 },
        "facing=north,variant=delta":  { "model": "conquest:corner_", "y": 270 }
    }
}

-->Fence Blocks
{
    "multipart": [
        {   "when":  { variant=alpha },
            "apply": { "model": "conquest:fence_post_" }
        },
        {   "when":  { "north": "true", variant=alpha },
            "apply": { "model": "conquest:fence_side_", "uvlock": true }
        },
        {   "when":  { "east":  "true", variant=alpha },
            "apply": { "model": "conquest:fence_side_", "y": 90, "uvlock": true }
        },
        {   "when":  { "south": "true", variant=alpha },
            "apply": { "model": "conquest:fence_side_", "y": 180, "uvlock": true }
        },
        {   "when":  { "west":  "true", variant=alpha },
            "apply": { "model": "conquest:fence_side_", "y": 270, "uvlock": true }
        },
        
        {   "when":  { variant=bravo },
            "apply": { "model": "conquest:fence_post_" }
        },
        {   "when":  { "north": "true", variant=bravo },
            "apply": { "model": "conquest:fence_side_", "uvlock": true }
        },
        {   "when":  { "east":  "true", variant=bravo },
            "apply": { "model": "conquest:fence_side_", "y": 90, "uvlock": true }
        },
        {   "when":  { "south": "true", variant=bravo },
            "apply": { "model": "conquest:fence_side_", "y": 180, "uvlock": true }
        },
        {   "when":  { "west":  "true", variant=bravo },
            "apply": { "model": "conquest:fence_side_", "y": 270, "uvlock": true }
        },
        
        ...Same Unitil Last Variant
}

-->Layer
{
    "variants": {
        "height=height2,variant=alpha":  { "model": "conquest:layer_height2_" },
        "height=height4,variant=alpha":  { "model": "conquest:layer_height4_" },
        "height=height6,variant=alpha":  { "model": "conquest:layer_height6_" },
        "height=height8,variant=alpha":  { "model": "conquest:layer_height8_" },
        "height=height10,variant=alpha": { "model": "conquest:layer_height10_" },
        "height=height12,variant=alpha": { "model": "conquest:layer_height12_" },
        "height=height14,variant=alpha": { "model": "conquest:layer_height14_" },
        "height=full,variant=alpha":  	 { "model": "conquest:layer_full_" },
        
        "height=height2,variant=bravo":  { "model": "conquest:layer_height2_" },
        "height=height4,variant=bravo":  { "model": "conquest:layer_height4_" },
        "height=height6,variant=bravo":  { "model": "conquest:layer_height6_" },
        "height=height8,variant=bravo":  { "model": "conquest:layer_height8_" },
        "height=height10,variant=bravo": { "model": "conquest:layer_height12_" },
        "height=height12,variant=bravo": { "model": "conquest:layer_height14_" },
        "height=full,variant=bravo":  	 { "model": "conquest:layer_full_" }
    }
}

--> Pane
{
    "multipart": [
        {   "when": { "north": true, variant=alpha },
            "apply": { "model": "conquest:pane_post_" }
        },
        {   "when": { "north": true, variant=alpha },
            "apply": { "model": "conquest:pane_side_" }
        },
        {   "when": { "east": true, variant=alpha  },
            "apply": { "model": "conquest:pane_side_", "y": 90 }
        },
        {   "when": { "south": true, variant=alpha  },
            "apply": { "model": "conquest:pane_side_alt_" }
        },
        {   "when": { "west": true, variant=alpha  },
            "apply": { "model": "conquest:pane_side_alt_", "y": 90 }
        },
        {   "when": { "north": false, variant=alpha  },
            "apply": { "model": "conquest:pane_noside_" }
        },
        {   "when": { "east": false, variant=alpha  },
            "apply": { "model": "conquest:pane_noside_alt_" }
        },
        {   "when": { "south": false, variant=alpha  },
            "apply": { "model": "conquest:pane_noside_alt_", "y": 90 }
        },
        {   "when": { "west": false, variant=alpha  },
            "apply": { "model": "conquest:pane_noside_", "y": 270 }
        },
        
        {   "when": { "north": true, variant=bravo },
            "apply": { "model": "conquest:pane_post_" }
        },
        {   "when": { "north": true, variant=bravo },
            "apply": { "model": "conquest:pane_side_" }
        },
        {   "when": { "east": true, variant=bravo  },
            "apply": { "model": "conquest:pane_side_", "y": 90 }
        },
        {   "when": { "south": true, variant=bravo  },
            "apply": { "model": "conquest:pane_side_alt_" }
        },
        {   "when": { "west": true, variant=bravo  },
            "apply": { "model": "conquest:pane_side_alt_", "y": 90 }
        },
        {   "when": { "north": false, variant=bravo  },
            "apply": { "model": "conquest:pane_noside_" }
        },
        {   "when": { "east": false, variant=bravo  },
            "apply": { "model": "conquest:pane_noside_alt_" }
        },
        {   "when": { "south": false, variant=bravo  },
            "apply": { "model": "conquest:pane_noside_alt_", "y": 90 }
        },
        {   "when": { "west": false, variant=bravo  },
            "apply": { "model": "conquest:pane_noside_", "y": 270 }
        },
        
        ...Same Unitil Last Variant
}

-->Rotated Pillar and Log
{
    "variants": {
        "axis=y,variant=alpha": { "model": "conquest:pillar_" },
        "axis=z,variant=alpha": { "model": "conquest:pillar_", "x": 90 },
        "axis=x,variant=alpha": { "model": "conquest:pillar_", "x": 90, "y": 90 },
        "axis=none,variant=alpha": { "model": "conquest:pillar_" },
        
        "axis=y,variant=bravo": { "model": "conquest:pillar_" },
        "axis=z,variant=bravo": { "model": "conquest:pillar_", "x": 90 },
        "axis=x,variant=bravo": { "model": "conquest:pillar_", "x": 90, "y": 90 },
        "axis=none,variant=bravo": { "model": "conquest:pillar_" },
        
        "axis=y,variant=charlie": { "model": "conquest:pillar_" },
        "axis=z,variant=charlie": { "model": "conquest:pillar_", "x": 90 },
        "axis=x,variant=charlie": { "model": "conquest:pillar_", "x": 90, "y": 90 },
        "axis=none,variant=charlie": { "model": "conquest:pillar_" },
        
        "axis=y,variant=delta": { "model": "conquest:pillar_" },
        "axis=z,variant=delta": { "model": "conquest:pillar_", "x": 90 },
        "axis=x,variant=delta": { "model": "conquest:pillar_", "x": 90, "y": 90 },
        "axis=none,variant=delta": { "model": "conquest:pillar_" }
    }
}

-->Rotated Arrow Slit
{
    "variants": {
        "axis=y,variant=alpha": { "model": "conquest:slit_" },
        "axis=z,variant=alpha": { "model": "conquest:slit_", "x": 90 },
        "axis=x,variant=alpha": { "model": "conquest:slit_", "x": 90, "y": 90 },
        "axis=none,variant=alpha": { "model": "conquest:slit_frame_" },
        
        "axis=y,variant=bravo": { "model": "conquest:slit_" },
        "axis=z,variant=bravo": { "model": "conquest:slit_", "x": 90 },
        "axis=x,variant=bravo": { "model": "conquest:slit_", "x": 90, "y": 90 },
        "axis=none,variant=bravo": { "model": "conquest:slit_frame_" },
        
        "axis=y,variant=charlie": { "model": "conquest:slit_" },
        "axis=z,variant=charlie": { "model": "conquest:slit_", "x": 90 },
        "axis=x,variant=charlie": { "model": "conquest:slit_", "x": 90, "y": 90 },
        "axis=none,variant=charlie": { "model": "conquest:slit_frame_" },
        
        "axis=y,variant=delta": { "model": "conquest:slit_" },
        "axis=z,variant=delta": { "model": "conquest:slit_", "x": 90 },
        "axis=x,variant=delta": { "model": "conquest:slit_", "x": 90, "y": 90 },
        "axis=none,variant=delta": { "model": "conquest:slit_frame_" }
    }
}

-->Trap door
{
    "variants": {
        "facing=east,variant=alpha": { "model": "conquest:trap_door_", "y": 90 },
        "facing=south,variant=alpha": { "model": "conquest:trap_door_", "y": 180 },
        "facing=west,variant=alpha": { "model": "conquest:trap_door_", "y": 270 },
        "facing=north,variant=alpha": { "model": "conquest:trap_door_" },
        "facing=up,variant=alpha": { "model": "conquest:trap_door_bottom_" },
        "facing=down,variant=alpha": { "model": "conquest:trap_door_top_" },
        
        "facing=east,variant=bravo": { "model": "conquest:trap_door_", "y": 90 },
        "facing=south,variant=bravo": { "model": "conquest:trap_door_", "y": 180 },
        "facing=west,variant=bravo": { "model": "conquest:trap_door_", "y": 270 },
        "facing=north,variant=bravo": { "model": "conquest:trap_door_" },
        "facing=up,variant=bravo": { "model": "conquest:trap_door_bottom_" },
        "facing=down,variant=bravo": { "model": "conquest:trap_door_top_" }
    }
}

-->Vertical slab
{
    "variants": {
        "facing=east,variant=alpha": { "model": "conquest:vertical_slab_" },
        "facing=south,variant=alpha": { "model": "conquest:vertical_slab_", "y": 90 },
        "facing=west,variant=alpha": { "model": "conquest:vertical_slab_", "y": 180 },
        "facing=north,variant=alpha": { "model": "conquest:vertical_slab_", "y": 270 },
        
        "facing=east,variant=bravo": { "model": "conquest:vertical_slab_" },
        "facing=south,variant=bravo": { "model": "conquest:vertical_slab_", "y": 90 },
        "facing=west,variant=bravo": { "model": "conquest:vertical_slab_", "y": 180 },
        "facing=north,variant=bravo": { "model": "conquest:vertical_slab_", "y": 270 },
        
        "facing=east,variant=charlie": { "model": "conquest:vertical_slab_" },
        "facing=south,variant=charlie": { "model": "conquest:vertical_slab_", "y": 90 },
        "facing=west,variant=charlie": { "model": "conquest:vertical_slab_", "y": 180 },
        "facing=north,variant=charlie": { "model": "conquest:vertical_slab_", "y": 270 },
        
        "facing=east,variant=delta": { "model": "conquest:vertical_slab_" },
        "facing=south,variant=delta": { "model": "conquest:vertical_slab_", "y": 90 },
        "facing=west,variant=delta": { "model": "conquest:vertical_slab_", "y": 180 },
        "facing=north,variant=delta": { "model": "conquest:vertical_slab_", "y": 270 }
    }
}

-->
{
    "multipart": [
        {   "when":  { "up": "true", variant=alpha },
            "apply": { "model": "conquest:wall_post_" }
        },
        {   "when":  { "north": "true", variant=alpha },
            "apply": { "model": "conquest:wall_side_", "uvlock": true }
        },
        {   "when":  { "east": "true", variant=alpha },
            "apply": { "model": "conquest:wall_side_", "y": 90, "uvlock": true }
        },
        {   "when":  { "south": "true", variant=alpha },
            "apply": { "model": "conquest:wall_side_", "y": 180, "uvlock": true }
        },
        {   "when":  { "west": "true", variant=alpha },
            "apply": { "model": "conquest:wall_side_", "y": 270, "uvlock": true }
        },
        
        {   "when":  { "up": "true", variant=bravo },
            "apply": { "model": "conquest:wall_post_" }
        },
        {   "when":  { "north": "true", variant=bravo },
            "apply": { "model": "conquest:wall_side_", "uvlock": true }
        },
        {   "when":  { "east": "true", variant=bravo },
            "apply": { "model": "conquest:wall_side_", "y": 90, "uvlock": true }
        },
        {   "when":  { "south": "true", variant=bravo },
            "apply": { "model": "conquest:wall_side_", "y": 180, "uvlock": true }
        },
        {   "when":  { "west": "true", variant=bravo },
            "apply": { "model": "conquest:wall_side_", "y": 270, "uvlock": true }
        },
}

--> Vertical Beam
{
    "variants": {
        "activated=false,facing=east,variant=alpha": { "model": "conquest:beam_vertical_1" },
        "activated=false,facing=south,variant=alpha":{ "model": "conquest:beam_vertical_1", "y": 90 },
        "activated=false,facing=west,variant=alpha": { "model": "conquest:beam_vertical_1", "y": 180 },
        "activated=false,facing=north,variant=alpha":{ "model": "conquest:beam_vertical_1", "y": 270 },
        "activated=true,facing=east,variant=alpha": { "model": "conquest:beam_arch_1" },
        "activated=true,facing=south,variant=alpha":{ "model": "conquest:beam_arch_1", "y": 90 },
        "activated=true,facing=west,variant=alpha": { "model": "conquest:beam_arch_1", "y": 180 },
        "activated=true,facing=north,variant=alpha":{ "model": "conquest:beam_arch_1", "y": 270 },
        
        "activated=false,facing=east,variant=bravo": { "model": "conquest:beam_vertical_2" },
        "activated=false,facing=south,variant=bravo":{ "model": "conquest:beam_vertical_2", "y": 90 },
        "activated=false,facing=west,variant=bravo": { "model": "conquest:beam_vertical_2", "y": 180 },
        "activated=false,facing=north,variant=bravo":{ "model": "conquest:beam_vertical_2", "y": 270 },
        "activated=true,facing=east,variant=bravo": { "model": "conquest:beam_arch_2" },
        "activated=true,facing=south,variant=bravo":{ "model": "conquest:beam_arch_2", "y": 90 },
        "activated=true,facing=west,variant=bravo": { "model": "conquest:beam_arch_2", "y": 180 },
        "activated=true,facing=north,variant=bravo":{ "model": "conquest:beam_arch_2", "y": 270 }
    }
}

--> Horizontal Beam
{
    "variants": {
        "activated=1,facing=east": { "model": "conquest:beam_h_1tick_1" },
        "activated=1,facing=south":{ "model": "conquest:beam_h_1tick_1", "y": 90 },
        "activated=1,facing=west": { "model": "conquest:beam_h_1tick_1", "y": 180 },
        "activated=1,facing=north":{ "model": "conquest:beam_h_1tick_1", "y": 270 },
        "activated=2,facing=east": { "model": "conquest:beam_h_2tick_1" },
        "activated=2,facing=south":{ "model": "conquest:beam_h_2tick_1", "y": 90 },
        "activated=2,facing=west": { "model": "conquest:beam_h_2tick_1", "y": 180 },
        "activated=2,facing=north":{ "model": "conquest:beam_h_2tick_1", "y": 270 },
        "activated=3,facing=east": { "model": "conquest:beam_h_3tick_1" },
        "activated=3,facing=south":{ "model": "conquest:beam_h_3tick_1", "y": 90 },
        "activated=3,facing=west": { "model": "conquest:beam_h_3tick_1", "y": 180 },
        "activated=3,facing=north":{ "model": "conquest:beam_h_3tick_1", "y": 270 },
        "activated=4,facing=east": { "model": "conquest:beam_h_4tick_1" },
        "activated=4,facing=south":{ "model": "conquest:beam_h_4tick_1", "y": 90 },
        "activated=4,facing=west": { "model": "conquest:beam_h_4tick_1", "y": 180 },
        "activated=4,facing=north":{ "model": "conquest:beam_h_4tick_1", "y": 270 }
    }
}

--> OBJ and JSON combined
{
    "forge_marker": 1,
    "defaults": {
        "model": "conquest:barrel.obj"
    },
    "variants": {
        "facing=east,variant=a":  { "model": "conquest:barrel_1", "x": 90, "y": 90 },
        "facing=south,variant=a": { "model": "conquest:barrel_1", "x": 90, "y": 180 },
        "facing=west,variant=a":  { "model": "conquest:barrel_1", "x": 90, "y": 270 },
        "facing=north,variant=a": { "model": "conquest:barrel_1", "x": 90 },
        "facing=up,variant=a":    { "model": "conquest:barrel_1" },
        "facing=down,variant=a":  { "model": "conquest:barrel_1" },
        
        "facing=east,variant=b":  { "model": "conquest:barrel.obj", "x": 90, "y": 90 },
        "facing=south,variant=b": { "model": "conquest:barrel.obj", "x": 90, "y": 180 },
        "facing=west,variant=b":  { "model": "conquest:barrel.obj", "x": 90, "y": 270 },
        "facing=north,variant=b": { "model": "conquest:barrel.obj", "x": 90 },
        "facing=up,variant=b":    { "model": "conquest:barrel.obj" },
        "facing=down,variant=b":  { "model": "conquest:barrel.obj" }
    }
}
*/

//Lang file templates	
/**
tile..alpha.name=
tile..bravo.name=
tile..charlie.name=
tile..delta.name=
tile..echo.name=
tile..fox.name=
tile..golf.name=
tile..hotel.name=
tile..india.name=
tile..juliet.name=
tile..kilo.name=
tile..lima.name=
tile..mike.name=
tile..november.name=
tile..oscar.name=
tile..papa.name=
 */
}
