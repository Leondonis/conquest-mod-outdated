package net.creativerealmsmc.conquest.proxy;

import net.creativerealmsmc.conquest.init.Entities;
import net.creativerealmsmc.conquest.init.Items;
import net.creativerealmsmc.conquest.init.MetaBlocks;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public abstract class CommonProxy
{
	public void preInit(FMLPreInitializationEvent e)
	{
		Entities.init();
		MetaBlocks.init();
		MetaBlocks.register();
	}

	public void init(FMLInitializationEvent e)
	{
		//Items.init();
		//Items.register();
	}

	public void postInit(FMLPostInitializationEvent e)
	{

	}

	abstract public boolean playerIsInCreativeMode(EntityPlayer player);
	abstract public boolean isDedicatedServer();
}
