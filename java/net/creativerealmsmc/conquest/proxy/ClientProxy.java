package net.creativerealmsmc.conquest.proxy;

import net.creativerealmsmc.conquest.init.Entities;
import net.creativerealmsmc.conquest.init.Items;
import net.creativerealmsmc.conquest.init.MetaBlocks;
import net.creativerealmsmc.conquest.init.ModColourManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class ClientProxy extends CommonProxy
{
	@Override
	public void preInit(FMLPreInitializationEvent e)
	{
		super.preInit(e);
		Entities.registerRenders();
		MetaBlocks.registerRenders();
	}
	
	@Override
	public void init(FMLInitializationEvent e)
	{
		super.init(e);
		ModColourManager.registerColourHandlers();
		MetaBlocks.registerRendersNoMeta();
		//Items.registerRenders();
	}

	@Override
	public void postInit(FMLPostInitializationEvent e) {
		super.postInit(e);
	}

	@Override
	public boolean playerIsInCreativeMode(EntityPlayer player)
	{
		if (player instanceof EntityPlayerMP)
	    {
			EntityPlayerMP entityPlayerMP = (EntityPlayerMP)player;
			return entityPlayerMP.interactionManager.isCreative();
	    }
		else if (player instanceof EntityPlayerSP)
	    {
			return Minecraft.getMinecraft().playerController.isInCreativeMode();
	    }
	    return false;
	}

	@Override
	public boolean isDedicatedServer()
	{
		return false;
	}
}