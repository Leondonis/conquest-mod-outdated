package net.creativerealmsmc.conquest.world;

import java.util.Random;

import net.minecraft.world.World;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.common.gameevent.TickEvent.ServerTickEvent;

/** http://jabelarminecraft.blogspot.com/p/minecraft-forge-172-event-handling.html
 * 
 * @author Jugbot */
public class Wind {
	static World world;
	private static double vx = 0;
	private static double vz = 0;
	private static double nextvx = 0;
	private static double nextvz = 0;
	private static double prevvx = 0;
	private static double prevvz = 0;
	private static final double INTENSITY = 0.01;
	private static int tickCount = 0;
	private static final int TICKFREQ = 100;
	private static Random rand;
	private static final int MEANCORRECTION = 2; //Higher numbers = less correction (higher variation of wind speeds)

	@SubscribeEvent
	public void LoadWind(WorldEvent.Load e) {
		world = e.getWorld();
		rand = new Random(world.getSeed());
		vx = rand.nextGaussian() * INTENSITY;
		vz = rand.nextGaussian() * INTENSITY;
		nextvx += INTENSITY * rand.nextGaussian() - vx * vx / (vx + MEANCORRECTION);
		nextvz += INTENSITY * rand.nextGaussian() - vz * vz / (vz + MEANCORRECTION);
		prevvx = vx;
		prevvz = vz;
	}

	@SubscribeEvent
	public void UnloadWind(WorldEvent.Unload e) {
		world = null;
	}

	@SubscribeEvent
	public void nextWind(ServerTickEvent e) {
		if (e.phase == Phase.START) {
			tickCount++;
			if (tickCount == TICKFREQ) {
				tickCount = 0;
				prevvx = vx;
				prevvz = vz;
				nextvx += INTENSITY * rand.nextGaussian() - vx * vx / (vx + MEANCORRECTION);
				nextvz += INTENSITY * rand.nextGaussian() - vz * vz / (vz + MEANCORRECTION);
			}
			vx = prevvx + smoothstep(((double) tickCount) / TICKFREQ) * (nextvx - prevvx);
			vz = prevvz + smoothstep(((double) tickCount) / TICKFREQ) * (nextvz - prevvz);
		}
		//System.out.print("(" + vx + "," + vz+ ")");
	}

	public static double getWindSpeedX() { //always zero?
		return vx;
	}

	public static double getWindSpeedZ() {
		return vz;
	}

	/** @param x
	 *            from 0-1
	 * @return Number from 0-1 */
	public static double smoothstep(double x) {
		return 3 * x * x - 2 * x * x * x;
	}
}
