package net.creativerealmsmc.conquest.CreativeTabs;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;

/**
 * Created by Jeff on 6/5/2016.
 */
public class DecorationsTab extends CreativeTabs {
    public DecorationsTab(int index, String label) {
        super(index, label);
    }

    @Override
    public Item getTabIconItem() {
        return Item.getItemFromBlock(Blocks.STONE);
    }
}