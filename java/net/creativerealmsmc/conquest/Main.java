package net.creativerealmsmc.conquest;

import java.util.List;

import net.creativerealmsmc.conquest.CreativeTabs.AnimalsTab;
import net.creativerealmsmc.conquest.CreativeTabs.DecorationsTab;
import net.creativerealmsmc.conquest.CreativeTabs.DevTab;
import net.creativerealmsmc.conquest.CreativeTabs.FoodTab;
import net.creativerealmsmc.conquest.CreativeTabs.GeologyTab;
import net.creativerealmsmc.conquest.CreativeTabs.GroundTab;
import net.creativerealmsmc.conquest.CreativeTabs.NaturalTab;
import net.creativerealmsmc.conquest.CreativeTabs.RefinedTab;
import net.creativerealmsmc.conquest.CreativeTabs.RoofingTab;
import net.creativerealmsmc.conquest.CreativeTabs.WindowsTab;
import net.creativerealmsmc.conquest.CreativeTabs.WoodTab;
import net.creativerealmsmc.conquest.init.MetaBlocks;
import net.creativerealmsmc.conquest.proxy.CommonProxy;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@Mod(modid = Main.MODID, name = Main.MODNAME, version = Main.VERSION)
public class Main {

	public static final String MODID = "conquest";
	public static final String MODNAME = "Conquest Mod";
	public static final String VERSION = "0.0.1";

	@Instance
	public static Main instance = new Main();

	@SidedProxy(clientSide="net.creativerealmsmc.conquest.proxy.ClientProxy", serverSide="net.creativerealmsmc.conquest.proxy.ServerProxy")
	public static CommonProxy proxy;

	/**
	 * To add a block to a certain tab, find the tab you want, and to public void displayAllRelevantItems add:
	 * ItemStack <public static Block material_blockmodel_number> = new ItemStack (<public static Block material_blockmodel_number>,<amount - always use 1>,<metadata value>);
	 * list.add(<public static Block material_blockmodel_number>);//<Name of the Block in the Lang file>
	 */

	public static CreativeTabs animals = new AnimalsTab(CreativeTabs.getNextID(), "animals")
	{

		@Override
		@SideOnly(Side.CLIENT)
		public void displayAllRelevantItems(List<ItemStack> list)
		{
			//  Birds  //
			//Raven
			//Hawk
			//Owl
			//Seagull
			//Mallard Duck
			//Pidgeon
			//Bluejay
			//Bat

			//  Land Animals //
			//Rat
			//Toad

			//  Bugs  //
			//Flies
			//Fireflies


			super.displayAllRelevantItems(list);
		}

	};

	public static CreativeTabs decorations = new DecorationsTab(CreativeTabs.getNextID(), "decorations")
	{

		@Override
		@SideOnly(Side.CLIENT)
		public void displayAllRelevantItems(List<ItemStack> list)
		{
			//  Storage  //
			//3D Barrel
			//Barrel
			//Empty Barrel with Grille
			//Empty Barrel
			//Wicker Basket
			//Empty Basket
			//Barrel of Iron Ore
			//Barrel of Diamond Ore
			//Barrel of Emeralds
			//Barrel of Lapis Lazuli
			//Barrel of Clay
			//Gold Chest
			//Sack of Rubies
			//Sack of Pipeweed
			//Barrel of Coal
			//Charcoal
			//Iron Cauldron with Grille
			//Cauldron
			//Crate
			//Covered Crate

			//  Furniture  //
			//Iron Furnace
			//Flaming Iron Furnace
			//Flaming 3D Furnace
			//Sandstone Furnace
			//Flaming Sandstone Furnace
			//Shelves
			//Spice Rack
			//Scroll Rack
			//Cupboards
			//Cupboards with Tools
			//Kitchen Table with Tools
			//Workbench
			//Table
			//Table 2
			//Enchanting Table with Legs
			//Floating Book
			//Spruce Chair
			//Wicker Chair
			//Wood Chair
			//Red Cushion Chair
			//Blue Chair
			//Black Cushion Chair
			//Green Cushion Chair
			//Leather Chair
			//Wolf Fur Bed
			//Rustic Bed
			//Bear Fur Bed
			//Fancy Green Bed

			//  Droppers and Dispensers  //
			//Jungle Dispenser
			//Iron Dispenser
			//Iron Dropper
			//Sandstone Dispenser
			//Sandstone Dropper

			//  Lights  //
			//Large Lamp
			//Big Lantern
			//Small Lantern
			//Beacon
			//Chandelier
			//Lit Chandelier
			//White Paper Lantern
			//Yellow Paper Lantern
			//Asian Lantern
			//Low Invisible Light Block
			//Medium Invisible Light Block
			//High Invisible Light Block

			//  Ropes and Chains  //
			//Pile of Rope
			//Wall of Ropes
			//Rope
			//Rope 2
			//Rope (Rail)
			//Climbing Rope
			//Noose
			//Noose 2
			//Pile of Chains
			//Iron Chains
			//Iron Chains (Rail)
			//Rusty Chains
			//Small Chains
			//Gold Chains
			//Rope Hook
			//Metal Hook
			//Hanging Bags

			//  Bars and Panes  //
			//Cage Bars
			//Iron Bars
			//Dark Iron Fence
			//Rusted Iron Fence
			//Cell Bars
			//Cell Bars 2
			//Iron Grille
			//Weather Vane
			//Invisible Glass Pane

			//  Tools and Racks  //
			//Weapon Rack (Swords)
			//Weapon Rack (Axes)
			//Weapon Rack (Maces)
			//Weapon Rack (Hammers)
			//Weapon Rack (Daggers)
			//Weapon Rack (Shortswords)
			//Weapon Rack (Pistols)
			//Weapon Rack (Halberds)
			//Weapon Rack (Muskets)
			//Spears
			//Arrow Bundle
			//Arrows
			//Less Arrows
			//Bow
			//Arrows
			//Axe
			//Sword
			//Small Farming Tools
			//Brooms
			//Farming Tools
			//Shovel
			//Fishing Rod
			//Bow Saw
			//Hammer
			//Pickaxe
			//Sickle
			//Circular Saw
			//Ladder
			//Pile of Paper
			//Quill and Parchment
			//Book
			//Wheel
			//Net
			//Fishing Net
			//Net on the Ground
			//Scales (Web Model)
			//Abacus (Web Model)
			//Hourglass (Web Model)
			//Sextant (Web Model)
			//Telescope (Web Model)
			//Globe (Web Model)
			//Compass (Web Model)

			//  Cloth  //
			//Brown Wool Slab
			//Black Wool Slab
			//Gray Wool Slab
			//Light Gray Wool Slab
			//White Wool Slab
			//Red Wool Slab
			//Orange Wool Slab
			//Yellow Wool Slab
			//Lime Wool Slab
			//Green Wool Slab
			//Light Blue Wool Slab
			//Cyan Wool Slab
			//Blue Wool Slab
			//Purple Wool Slab
			//Magenta Wool Slab
			//Pink Wool Slab
			//Black Curtain
			//Black Curtain (Vine Model)
			//White Curtain
			//White Curtain (Vine Model)
			//Red Curtain
			//Red Curtain (Vine Model)
			//Green Curtain
			//Green Curtain (Vine Model)
			//Blue Curtain
			//Blue Curtain (Vine Model)
			//Purple Curtain
			//Purple Curtain (Vine Model)
			//Clothesline
			//Decorative Flags
			//Bear Hide
			//Bear Skin
			//Bear Skin 2
			//Wolf Skin
			//Sheep Skin
			//Hay on the Ground

			//  Misc  //
			//Map Table
			//Silver Coins
			//Gold Coins
			//Pot
			//Flower Box
			//Flower Pot
			//Black Pot
			//Cannonball
			//Hell's Eye
			//Checkerboard
			//Playing Cards
			//Steam
			//Smoke
			//Broken Bottle
			//Piñata
			//Gilded Bell
			//Mirror
			//Mirror (CTM)
			//Green Screen

			//  Raw Materials  //
			//Gold Coin Pile Stairs
			//Gold Coin Pile Slab

			//  Redstone  //
			//Wood Tile (Pressure Plate)
			//Stone Tile (Pressure Plate)
			//Lever
			//Tripwire Hook

			super.displayAllRelevantItems(list);
		}

	};

	public static CreativeTabs food = new FoodTab(CreativeTabs.getNextID(), "food")
	{

		@Override
		@SideOnly(Side.CLIENT)
		public void displayAllRelevantItems(List<ItemStack> list)
		{
			//  Containers  //
			//Apple
			//Barrel of Apples
			//Sack of Apples
			//Wicker Basket of Apples
			//Barrel of Bread
			//Sack of Bread
			//Sack of Flour
			//Barrel of Cabbages
			//Barrel of Fish
			//Sack of Fish
			//Barrel of Cocoa
			//Sack of Cocoa
			//Wicker Basket of Cocoa
			//Barrel of Potatoes
			//Sack of Potatoes
			//Sack of Hops
			//Sack of Grapes
			//Wine Keg (Open)
			//Wine Rack

			//  Block Foods (Cakes) //
			//Cheese Wheel
			//Apple Pie
			//Iced Cake with Fruit
			//Chocolate Pound Cake
			//Bread

			//  Hanging Meat //
			//Pig on a Spit
			//Brown Hanging Rabbit
			//White Hanging Rabbit
			//Hanging Rabbit
			//Beef Cut
			//Hanging Sausages
			//Big Sausages
			//Little Sausages

			// Hanging Fish //
			//Hanging Fish
			//Hanging Swordfish
			//Hanging Exotic Fish
			//Hanging Clownfish
			//Hanging Sardines

			// Hanging Vegetables //
			//Carrot Bundle
			//Onion Bundle
			//Banana Bundle
			//Herbs

			// Eating Utensils //
			//Metal Plate of Food
			//Clay Plate of Food
			//Wooden Plate of Food
			//Bottle
			//Potion Glass
			//Goblet

			super.displayAllRelevantItems(list);
		}

	};

	public static CreativeTabs geology = new GeologyTab(CreativeTabs.getNextID(), "geology")
	{

		@Override
		@SideOnly(Side.CLIENT)
		public void displayAllRelevantItems(List<ItemStack> list)
		{

			//  Stone  //
			list.add(new ItemStack(MetaBlocks.stone_full_14,1,4)); //Icy Stone
			list.add(new ItemStack(MetaBlocks.stone_full_14,1,5)); //Bone (Possibly Icy Stone (Top))
			//Icy Stone (Top)
			list.add(new ItemStack(MetaBlocks.stone_full_14,1,6)); //Mossy Stone
			list.add(new ItemStack(MetaBlocks.stone_full_14,1,7)); //Mossy Stone (Top)


			//  Slate  //
			list.add(new ItemStack(MetaBlocks.stone_full_1,1,4)); //Slate
			list.add(new ItemStack(MetaBlocks.stone_full_14,1,8)); //Wet Slate
			list.add(new ItemStack(MetaBlocks.stone_full_14,1,9)); //Full Slate
			list.add(new ItemStack(MetaBlocks.stone_full_1,1,5)); //Colorful Slate

			//  Marble  //
			list.add(new ItemStack(MetaBlocks.stone_full_14,1,10)); //Uncut Marble

			//  Granite  //
			list.add(new ItemStack(MetaBlocks.stone_full_14,1,11)); //Gray-Pink Granite

			//  Sandstone  //
			list.add(new ItemStack(MetaBlocks.stone_full_14,1,12)); //Natural Sandstone
			list.add(new ItemStack(MetaBlocks.stone_full_14,1,13)); //Light Brown Mesa Stone

			//  Lava  //
			list.add(new ItemStack(MetaBlocks.stone_full_14,1,14)); //Pahoehoe
			//Molten Obsidian

			//  Crystal  //
			//Red Crystal
			//Light Green Crystal
			//Green Crystal
			//Blue Crystal
			//Purple Crystal


			//  Misc  //
			//Endstone Slab
			//Stone Trapdoor

			//  Rocks  //
			//Stalactite
			//Stalagmite
			//Climbing Rocks
			//Sandstone Rock
			//Granite Rock
			//Limestone Rock
			/*
				ItemStack stone7 = new ItemStack(Item.getItemFromBlock(BlockRegistry.stone7));
				list.add(stone7);//Icy Stone
				ItemStack bone = new ItemStack(Item.getItemFromBlock(BlockRegistry.bone));
				list.add(bone);//Icy Stone (Top)
				ItemStack stone_mossy_full = new ItemStack(Item.getItemFromBlock(BlockRegistry.stone_mossy_full));
				list.add(stone_mossy_full);//Mossy Stone
				ItemStack doubleslab8 = new ItemStack(Item.getItemFromBlock(BlockRegistry.stone_mossytop_full));
				list.add(doubleslab8);//Mossy Stone (Top)
				ItemStack lapisblock7 = new ItemStack(Item.getItemFromBlock(BlockRegistry.lapisblock7));
				list.add(lapisblock7);//Wet Slate
				ItemStack stone10 = new ItemStack(Item.getItemFromBlock(BlockRegistry.stone10));
				list.add(stone10);//Slate
				ItemStack stone11 = new ItemStack(Item.getItemFromBlock(BlockRegistry.stone11));
				list.add(stone11);//Uncut Marble
				ItemStack stone12 = new ItemStack(Item.getItemFromBlock(BlockRegistry.stone12));
				list.add(stone12);//Gray and Pink Granite
				ItemStack sandstone5 = new ItemStack(Item.getItemFromBlock(BlockRegistry.sandstone5));
				list.add(sandstone5);//Natural Sandstone
				ItemStack clayorangemesa = new ItemStack(Item.getItemFromBlock(BlockRegistry.clayorangemesa));
				list.add(clayorangemesa);//Light Brown Mesa Stone
				ItemStack obsidian2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.obsidian2));
				list.add(obsidian2);//Pahoehoe
				ItemStack obsidian3 = new ItemStack(Item.getItemFromBlock(BlockRegistry.obsidian3));
				list.add(obsidian3);//Molten Obsidian
				ItemStack ice4 = new ItemStack(Item.getItemFromBlock(BlockRegistry.ice4));
				list.add(ice4);//Red Crystal
				ItemStack ice4ocean = new ItemStack(Item.getItemFromBlock(BlockRegistry.ice4ocean));
				list.add(ice4ocean);//Purple Crystal
				ItemStack ice4forest = new ItemStack(Item.getItemFromBlock(BlockRegistry.ice4forest));
				list.add(ice4forest);//Green Crystal
				ItemStack ice4mushroomisland = new ItemStack(Item.getItemFromBlock(BlockRegistry.ice4mushroomisland));
				list.add(ice4mushroomisland);//Blue Crystal
				ItemStack ice4taiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.ice4taiga));
				list.add(ice4taiga);//Light Green Crystal
				ItemStack stoneslab2sky = new ItemStack(Item.getItemFromBlock(BlockRegistry.stoneslab2sky));
				list.add(stoneslab2sky);//Endstone Slab
				ItemStack trapdoormegataiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.trapdoormegataiga));
				list.add(trapdoormegataiga);//Stone Trapdoor
				ItemStack web9extremehills = new ItemStack(Item.getItemFromBlock(BlockRegistry.web9extremehills));
				list.add(web9extremehills);//Stalactite
				ItemStack stalagmite1 = new ItemStack(Item.getItemFromBlock(BlockRegistry.stalagmite1));
				list.add(stalagmite1);//Stalagmite
				ItemStack rockladder = new ItemStack(Item.getItemFromBlock(BlockRegistry.rockladder));
				list.add(rockladder);//Climbing Rocks
				ItemStack sandstonerock = new ItemStack(Item.getItemFromBlock(BlockRegistry.sandstonerock));
				list.add(sandstonerock);//Sandstone Rock
				ItemStack graniterock = new ItemStack(Item.getItemFromBlock(BlockRegistry.graniterock));
				list.add(graniterock);//Granite Rock
				ItemStack limestonerock = new ItemStack(Item.getItemFromBlock(BlockRegistry.limestonerock));
				list.add(limestonerock);//Limestone Rock
            */

			super.displayAllRelevantItems(list);
		}

	};

	public static CreativeTabs ground = new GroundTab(CreativeTabs.getNextID(), "ground")
	{

		@Override
		@SideOnly(Side.CLIENT)
		public void displayAllRelevantItems(List<ItemStack> list)
		{
			//  Sand  //
			list.add(new ItemStack(MetaBlocks.sand_full_1,1,0)); //Sand and Grass

			//  Red Sand  //


			//  Clay  //
			list.add(new ItemStack(MetaBlocks.stone_full_14,1,15)); //Dry Light Brown Clay
			list.add(new ItemStack(MetaBlocks.stone_full_15,1,0)); //Dry Brown Clay

			//  Dirt  //


			//  Gravel  //


			//  Ice  //
			list.add(new ItemStack(MetaBlocks.stone_full_13,1,10)); //Normal Ice


			//  Slabs  //

		/*	ItemStack sand2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.sand2));
			list.add(sand2);//Grass and Sand
			ItemStack sand4 = new ItemStack(Item.getItemFromBlock(BlockRegistry.sand4));
			list.add(sand4);//Gravel and Sand
			ItemStack sand5 = new ItemStack(Item.getItemFromBlock(BlockRegistry.sand5));
			list.add(sand5);//Vegetation and Sand
			ItemStack stoneslab2desert = new ItemStack(Item.getItemFromBlock(BlockRegistry.stoneslab2desert));
			list.add(stoneslab2desert);//Sand Slab
			ItemStack snowlayer8desert = new ItemStack(Item.getItemFromBlock(BlockRegistry.snowlayer8desert));
			list.add(snowlayer8desert);//Sand
			ItemStack sandocean = new ItemStack(Item.getItemFromBlock(BlockRegistry.sandocean));
			list.add(sandocean);//Wet Sand
			ItemStack snowlayer8ocean = new ItemStack(Item.getItemFromBlock(BlockRegistry.snowlayer8ocean));
			list.add(snowlayer8ocean);//Wet Sand
			ItemStack wet_sand_gravel = new ItemStack(Item.getItemFromBlock(BlockRegistry.wet_sand_gravel));
			list.add(wet_sand_gravel);//Wet Sand and Gravel
			ItemStack wet_sand_mossy = new ItemStack(Item.getItemFromBlock(BlockRegistry.wet_sand_mossy));
			list.add(wet_sand_mossy);//Vegetation and Wet Sand
			ItemStack red_sand_gravel = new ItemStack(Item.getItemFromBlock(BlockRegistry.red_sand_gravel));
			list.add(red_sand_gravel);//Red Sand and Gravel
			ItemStack red_sand_mossy = new ItemStack(Item.getItemFromBlock(BlockRegistry.red_sand_mossy));
			list.add(red_sand_mossy);//Vegetation and Red Sand
			ItemStack stoneslab2mesa = new ItemStack(Item.getItemFromBlock(BlockRegistry.stoneslab2mesa));
			list.add(stoneslab2mesa);//Red Sand Slab
			ItemStack snowlayer8mesa = new ItemStack(Item.getItemFromBlock(BlockRegistry.snowlayer8mesa));
			list.add(snowlayer8mesa);//Red Sand
			ItemStack hardenedclay1 = new ItemStack(Item.getItemFromBlock(BlockRegistry.hardenedclay1));
			list.add(hardenedclay1);//Light Brown Dry Clay
			ItemStack hardenedclay2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.hardenedclay2));
			list.add(hardenedclay2);//Brown Dry Clay
			ItemStack dirt3 = new ItemStack(Item.getItemFromBlock(BlockRegistry.dirt3));
			list.add(dirt3);//Dirt Block
			ItemStack stoneslab2plains = new ItemStack(Item.getItemFromBlock(BlockRegistry.stoneslab2plains));
			list.add(stoneslab2plains);//Dirt Slab
			ItemStack snowlayer8plains = new ItemStack(Item.getItemFromBlock(BlockRegistry.snowlayer8plains));
			list.add(snowlayer8plains);//Dirt
			ItemStack dirt4 = new ItemStack(Item.getItemFromBlock(BlockRegistry.dirt4));
			list.add(dirt4);//Dirt Bones
			ItemStack dirt5 = new ItemStack(Item.getItemFromBlock(BlockRegistry.dirt5));
			list.add(dirt5);//Dirt Frozen
			ItemStack dirt6 = new ItemStack(Item.getItemFromBlock(BlockRegistry.dirt6));
			list.add(dirt6);//Dirt Gravel
			ItemStack dirtandgravelslab = new ItemStack(Item.getItemFromBlock(BlockRegistry.dirtandgravelslab));
			list.add(dirtandgravelslab);//Dirt Gravel Slab
			ItemStack snowlayerdirtgravel = new ItemStack(Item.getItemFromBlock(BlockRegistry.snowlayerdirtgravel));
			list.add(snowlayerdirtgravel);//Dirty Gravel
			ItemStack dirt7 = new ItemStack(Item.getItemFromBlock(BlockRegistry.dirt7));
			list.add(dirt7);//Dirt Mossy
			ItemStack dirt8 = new ItemStack(Item.getItemFromBlock(BlockRegistry.dirt8));
			list.add(dirt8);//Light Dirt
			ItemStack dirtpathlight = new ItemStack(Item.getItemFromBlock(BlockRegistry.dirtpathlight));
			list.add(dirtpathlight);//Light Dirt Path
			ItemStack dirtpathdark = new ItemStack(Item.getItemFromBlock(BlockRegistry.dirtpathdark));
			list.add(dirtpathdark);//Dark Dirt Path
			ItemStack burnt_dirt = new ItemStack(Item.getItemFromBlock(BlockRegistry.burnt_dirt));
			list.add(burnt_dirt);//Burnt Dirt
			ItemStack muddy_dirt = new ItemStack(Item.getItemFromBlock(BlockRegistry.muddy_dirt));
			list.add(muddy_dirt);//Muddy Dirt
			ItemStack soulsand1 = new ItemStack(Item.getItemFromBlock(BlockRegistry.soulsand1));
			list.add(soulsand1);//Mud
			ItemStack soulsand2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.soulsand2));
			list.add(soulsand2);//Ants
			ItemStack farmland = new ItemStack(Item.getItemFromBlock(BlockRegistry.farmland));
			list.add(farmland);//Farmland
			ItemStack farmland_diagonal = new ItemStack(Item.getItemFromBlock(BlockRegistry.farmland_diagonal));
			list.add(farmland_diagonal);//Farmland Plowed Diagonally
			ItemStack snowlayer8 = new ItemStack(Item.getItemFromBlock(BlockRegistry.snowlayer8));
			list.add(snowlayer8);//Grass
			ItemStack podzoltaiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.podzoltaiga));
			list.add(podzoltaiga);//Taiga Forest Floor
			ItemStack zautum = new ItemStack(Item.getItemFromBlock(BlockRegistry.zautum));
			list.add(zautum);//Zautum
			ItemStack podzol_overgrown = new ItemStack(Item.getItemFromBlock(BlockRegistry.podzol_overgrown));
			list.add(podzol_overgrown);//Overgrown Podzol
			ItemStack podzol_old = new ItemStack(Item.getItemFromBlock(BlockRegistry.podzol_old));
			list.add(podzol_old);//Old Podzol
			ItemStack stoneslab2megataiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.stoneslab2megataiga));
			list.add(stoneslab2megataiga);//Podzol Slab
			ItemStack stoneslab2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.stoneslab2));
			list.add(stoneslab2);//Gravel Slab
			ItemStack snowlayergravel = new ItemStack(Item.getItemFromBlock(BlockRegistry.snowlayergravel));
			list.add(snowlayergravel);//Gravel
			ItemStack gravel1 = new ItemStack(Item.getItemFromBlock(BlockRegistry.gravel1));
			list.add(gravel1);//Small Stones
			ItemStack stoneslab2extremehills = new ItemStack(Item.getItemFromBlock(BlockRegistry.stoneslab2extremehills));
			list.add(stoneslab2extremehills);//Small Stones Slab
			ItemStack snowlayerstones = new ItemStack(Item.getItemFromBlock(BlockRegistry.snowlayerstones));
			list.add(snowlayerstones);//Small Stones
			ItemStack gravel2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.gravel2));
			list.add(gravel2);//Grassy Gravel
			ItemStack gravel3 = new ItemStack(Item.getItemFromBlock(BlockRegistry.gravel3));
			list.add(gravel3);//Grassy Small Stones
			ItemStack gravelbirchforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.gravelbirchforest));
			list.add(gravelbirchforest);//White Gravel
			ItemStack ice2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.ice2));
			list.add(ice2);//Crystallized Ice
			ItemStack stoneslab2coldbeach = new ItemStack(Item.getItemFromBlock(BlockRegistry.stoneslab2coldbeach));
			list.add(stoneslab2coldbeach);//Packed Ice Slab
			ItemStack stoneslab2hell = new ItemStack(Item.getItemFromBlock(BlockRegistry.stoneslab2hell));
			list.add(stoneslab2hell);//Netherrack Slab
			ItemStack stoneslab2mushroomisland = new ItemStack(Item.getItemFromBlock(BlockRegistry.stoneslab2mushroomisland));
			list.add(stoneslab2mushroomisland);//Clay Slab
			ItemStack snowlayer8hell = new ItemStack(Item.getItemFromBlock(BlockRegistry.snowlayer8hell));
			list.add(snowlayer8hell);//Ash
			ItemStack quartzstairsiceplains = new ItemStack(Item.getItemFromBlock(BlockRegistry.quartzstairsiceplains));
			list.add(quartzstairsiceplains);//Snow Stairs
			*/

			super.displayAllRelevantItems(list);
		}

	};

	public static CreativeTabs natural = new NaturalTab(CreativeTabs.getNextID(), "natural")
	{

		@Override
		@SideOnly(Side.CLIENT)
		public void displayAllRelevantItems(List<ItemStack> list)
		{

			//  Leaves  //

			//  Saplings and Tree Stuff  //

			//  Flowers and Bushes  //

			//  Vines  //

			//  Crops  //

			//  Bamboo  //

			//  Herbs and Plants  //

			//  Mushrooms  //

			//  Moss and Lillypads  //

			//  Nests  //

			//  Snow and Ice  //

			//  Bones  //
			list.add(new ItemStack(MetaBlocks.stone_full_13,1,11)); //Bones
			list.add(new ItemStack(MetaBlocks.stone_full_13,1,12)); //Skeletons

			//  Coral  //
			list.add(new ItemStack(MetaBlocks.stone_full_13,1,13)); //Red Coral
			list.add(new ItemStack(MetaBlocks.stone_full_13,1,14)); //Yellow Coral
			list.add(new ItemStack(MetaBlocks.stone_full_13,1,15)); //Blue Coral


        /*    ItemStack acacialeaves2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialeaves2));
            list.add(acacialeaves2);//Cherry Blossoms
            ItemStack acacialeaves3 = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialeaves3));
            list.add(acacialeaves3);//Apple Tree Leaves
            ItemStack acacialeaves4 = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialeaves4));
            list.add(acacialeaves4);//Holly Berry Bush
            ItemStack acacialeaves5 = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialeaves5));
            list.add(acacialeaves5);//Citrus tree Leaves
            ItemStack acacialeaves6 = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialeaves6));
            list.add(acacialeaves6);//Pear Tree Leaves
            ItemStack acacialeaves7 = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialeaves7));
            list.add(acacialeaves7);//Blue Berry Bush Leaves
            ItemStack acacialeaves8 = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialeaves8));
            list.add(acacialeaves8);//Grape Vines
            ItemStack oakextremehills = new ItemStack(Item.getItemFromBlock(BlockRegistry.oakextremehills));
            list.add(oakextremehills);//Oak Leaves (Extreme Hills)
            ItemStack oakfrozenocean = new ItemStack(Item.getItemFromBlock(BlockRegistry.oakfrozenocean));
            list.add(oakfrozenocean);//Oak Leaves (Frozen Ocean)
            ItemStack darkoakfrozenocean = new ItemStack(Item.getItemFromBlock(BlockRegistry.darkoakfrozenocean));
            list.add(darkoakfrozenocean);//Dark Oak Leaves (Frozen Ocean)
            ItemStack jungletaiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.jungletaiga));
            list.add(jungletaiga);//Jungle Leaves (Taiga)
            ItemStack junglefrozenocean = new ItemStack(Item.getItemFromBlock(BlockRegistry.junglefrozenocean));
            list.add(junglefrozenocean);//Jungle Leaves (Frozen Ocean)
            ItemStack birchfrozenocean = new ItemStack(Item.getItemFromBlock(BlockRegistry.birchfrozenocean));
            list.add(birchfrozenocean);//Birch Leaves (Frozen Ocean)
            ItemStack birchextremehills = new ItemStack(Item.getItemFromBlock(BlockRegistry.birchextremehills));
            list.add(birchextremehills);//Birch Leaves (Extreme Hills)
            //End Leaves
            ItemStack sapling_oak = new ItemStack(Item.getItemFromBlock(BlockRegistry.sapling_oak));
            list.add(sapling_oak);//Oak Sapling
            ItemStack sapling_birch = new ItemStack(Item.getItemFromBlock(BlockRegistry.sapling_birch));
            list.add(sapling_birch);//Birch Sapling
            ItemStack sapling_jungle = new ItemStack(Item.getItemFromBlock(BlockRegistry.sapling_jungle));
            list.add(sapling_jungle);//Jungle Sapling
            ItemStack sapling_acacia = new ItemStack(Item.getItemFromBlock(BlockRegistry.sapling_acacia));
            list.add(sapling_acacia);//Acacia Sapling
            ItemStack sapling_spruce = new ItemStack(Item.getItemFromBlock(BlockRegistry.sapling_spruce));
            list.add(sapling_spruce);//Spruce Sapling
            ItemStack sapling_darkoak = new ItemStack(Item.getItemFromBlock(BlockRegistry.sapling_darkoak));
            list.add(sapling_darkoak);//Dark Oak Sapling
            ItemStack twigtree = new ItemStack(Item.getItemFromBlock(BlockRegistry.twigtree));
            list.add(twigtree);//Small Tree
            ItemStack treestump = new ItemStack(Item.getItemFromBlock(BlockRegistry.treestump));
            list.add(treestump);//Tree Stump
            //End Saplings
            ItemStack poppy9 = new ItemStack(Item.getItemFromBlock(BlockRegistry.poppy9));
            list.add(poppy9);//Beautyberry Bushes
            ItemStack poppy10 = new ItemStack(Item.getItemFromBlock(BlockRegistry.poppy10));
            list.add(poppy10);//Raspberry Bush
            ItemStack poppy11 = new ItemStack(Item.getItemFromBlock(BlockRegistry.poppy11));
            list.add(poppy11);//Blackberry Bush
            //End Bushes
            ItemStack vinesswampland = new ItemStack(Item.getItemFromBlock(BlockRegistry.vinesswampland));
            list.add(vinesswampland);//Jungle Vines
            ItemStack vinestaiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.vinestaiga));
            list.add(vinestaiga);//Ivy Vines
            ItemStack web9plains = new ItemStack(Item.getItemFromBlock(BlockRegistry.web9plains));
            list.add(web9plains);//Hanging Ivy
            ItemStack vinesforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.vinesforest));
            list.add(vinesforest);//Hanging Moss (Vines)
            ItemStack web9forest = new ItemStack(Item.getItemFromBlock(BlockRegistry.web9forest));
            list.add(web9forest);//Hanging Moss
            //End Vines
            ItemStack wheat = new ItemStack(Item.getItemFromBlock(BlockRegistry.wheat));
            list.add(wheat);//Mature Wheat
            ItemStack wheat_small = new ItemStack(Item.getItemFromBlock(BlockRegistry.wheat_small));
            list.add(wheat_small);//Small Wheat
            ItemStack wheat1 = new ItemStack(Item.getItemFromBlock(BlockRegistry.wheat1));
            list.add(wheat1);//Wheat
            ItemStack corn = new ItemStack(Item.getItemFromBlock(BlockRegistry.corn));
            list.add(corn);//Corn
            ItemStack poppy14 = new ItemStack(Item.getItemFromBlock(BlockRegistry.poppy14));
            list.add(poppy14);//Beans
            //End Crops
            ItemStack reedbirchforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.reedbirchforest));
            list.add(reedbirchforest);//Bamboo
            ItemStack bamboo = new ItemStack(Item.getItemFromBlock(BlockRegistry.bamboo));
            list.add(bamboo);//Bamboo
            ItemStack reeddesert = new ItemStack(Item.getItemFromBlock(BlockRegistry.reeddesert));
            list.add(reeddesert);//Dark Bamboo
            //End Bamboo
            ItemStack fernjungle = new ItemStack(Item.getItemFromBlock(BlockRegistry.fernjungle));
            list.add(fernjungle);//Jungle Fern
            ItemStack ferndesert = new ItemStack(Item.getItemFromBlock(BlockRegistry.ferndesert));
            list.add(ferndesert);//Desert Shrub
            ItemStack dandelion1 = new ItemStack(Item.getItemFromBlock(BlockRegistry.dandelion1));
            list.add(dandelion1);//Dead bushes & Grass
            ItemStack mushroom4 = new ItemStack(Item.getItemFromBlock(BlockRegistry.mushroom4));
            list.add(mushroom4);//Tall Cow Parsley
            ItemStack dandelion3 = new ItemStack(Item.getItemFromBlock(BlockRegistry.dandelion3));
            list.add(dandelion3);//Lily of the Valley
            ItemStack dandelion4 = new ItemStack(Item.getItemFromBlock(BlockRegistry.dandelion4));
            list.add(dandelion4);//Euphorbia Esula
            ItemStack poppy12 = new ItemStack(Item.getItemFromBlock(BlockRegistry.poppy12));
            list.add(poppy12);//Pulsatilla Vulgaris
            ItemStack poppy13 = new ItemStack(Item.getItemFromBlock(BlockRegistry.poppy13));
            list.add(poppy13);//Knabenkraut
            ItemStack thick_fern = new ItemStack(Item.getItemFromBlock(BlockRegistry.thick_fern));
            list.add(thick_fern);//Thick Fern
            ItemStack cicerbita_alpina = new ItemStack(Item.getItemFromBlock(BlockRegistry.cicerbita_alpina));
            list.add(cicerbita_alpina);//Cicerbita Alpina
            ItemStack sweetgrass = new ItemStack(Item.getItemFromBlock(BlockRegistry.sweetgrass));
            list.add(sweetgrass);//Sweet Grass
            ItemStack nettles = new ItemStack(Item.getItemFromBlock(BlockRegistry.nettles));
            list.add(nettles);//Nettles
            ItemStack heather = new ItemStack(Item.getItemFromBlock(BlockRegistry.heather));
            list.add(heather);//Heather
            ItemStack cottongrass = new ItemStack(Item.getItemFromBlock(BlockRegistry.cottongrass));
            list.add(cottongrass);//Cotton Grass
            ItemStack hemp = new ItemStack(Item.getItemFromBlock(BlockRegistry.hemp));
            list.add(hemp);//Hemp
            ItemStack reedsswampland = new ItemStack(Item.getItemFromBlock(BlockRegistry.reedswampland));
            list.add(reedsswampland);//Cat Tails
            ItemStack cannonball = new ItemStack(Item.getItemFromBlock(BlockRegistry.cannonball));
            list.add(cannonball);//Hanging Flowers
            ItemStack hangingflowers = new ItemStack(Item.getItemFromBlock(BlockRegistry.hangingflowers));
            list.add(hangingflowers);//Hanging Flowers
            //End Plants
            ItemStack mushroombrownswampland = new ItemStack(Item.getItemFromBlock(BlockRegistry.mushroombrownswampland));
            list.add(mushroombrownswampland);//Brown Mushrooms
            ItemStack mushroomredswampland = new ItemStack(Item.getItemFromBlock(BlockRegistry.mushroomredswampland));
            list.add(mushroomredswampland);//Red Mushrooms
            //End Mushrooms
            ItemStack lilypadsswampland = new ItemStack(Item.getItemFromBlock(BlockRegistry.lilypadswampland));
            list.add(lilypadsswampland);//Swamp Lilypads
            ItemStack greencarpetforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.greencarpetforest));
            list.add(greencarpetforest);//Moss on the Ground
            //End Moss/Lillypads
            ItemStack birdnest = new ItemStack(Item.getItemFromBlock(BlockRegistry.birdnest));
            list.add(birdnest);//Bird Nest
            ItemStack birdnest_small = new ItemStack(Item.getItemFromBlock(BlockRegistry.birdnest_small));
            list.add(birdnest_small);//Small Bird Nest
            ItemStack dragonegg6 = new ItemStack(Item.getItemFromBlock(BlockRegistry.dragonegg6));
            list.add(dragonegg6);//Beehive
            ItemStack dragonegg11 = new ItemStack(Item.getItemFromBlock(BlockRegistry.dragonegg11));
            list.add(dragonegg11);//Hornets Nest
            //End Nests
            ItemStack web9 = new ItemStack(Item.getItemFromBlock(BlockRegistry.web9));
            list.add(web9);//Icicles
            ItemStack stalagmite = new ItemStack(Item.getItemFromBlock(BlockRegistry.stalagmite));
            list.add(stalagmite);//Ice Spikes
            ItemStack lilypadtaiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.lilypadtaiga));
            list.add(lilypadtaiga);//Floating Ice
            //End Snow
            ItemStack endstone8 = new ItemStack(Item.getItemFromBlock(BlockRegistry.endstone8));
            list.add(endstone8);//Pile of Bones
            ItemStack endstone9 = new ItemStack(Item.getItemFromBlock(BlockRegistry.endstone9));
            list.add(endstone9);//Pile of Skeletons
            ItemStack ironpressureplatehell = new ItemStack(Item.getItemFromBlock(BlockRegistry.ironpressureplatehell));
            list.add(ironpressureplatehell);//Bones
            ItemStack corpse = new ItemStack(Item.getItemFromBlock(BlockRegistry.corpse));
            list.add(corpse);//Hanging Body
            //End Bones
            ItemStack sponge1 = new ItemStack(Item.getItemFromBlock(BlockRegistry.sponge1));
            list.add(sponge1);//Red Coral
            ItemStack sponge2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.sponge2));
            list.add(sponge2);//Yellow Coral
            ItemStack sponge3 = new ItemStack(Item.getItemFromBlock(BlockRegistry.sponge3));
            list.add(sponge3);//Blue Coral
            //End Coral
            ItemStack coalblock2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.coalblock2));
            list.add(coalblock2);//Hot Coals
            ItemStack ice1 = new ItemStack(Item.getItemFromBlock(BlockRegistry.ice1));
            list.add(ice1);//Clouds
            ItemStack cowplate = new ItemStack(Item.getItemFromBlock(BlockRegistry.cowplate));
            list.add(cowplate);//Cow Patty (WIP)
            ItemStack particle_block = new ItemStack(Item.getItemFromBlock(BlockRegistry.particle_block));
            list.add(particle_block);//TileEntity Particle Emitter
            */

			super.displayAllRelevantItems(list);
		}

	};

	public static CreativeTabs refined = new RefinedTab(CreativeTabs.getNextID(), "refined")
	{

		@Override
		@SideOnly(Side.CLIENT)
		public void displayAllRelevantItems(List<ItemStack> list)
		{
			// There is an old block named 'arrowslit' that I can't find a new equivalent for.
			//Arrowslit

			//  Plastered Stone  //
			list.add(new ItemStack(MetaBlocks.stone_full_1,1,0)); //Plastered Stone
			//Plastered Stone Stairs
			//Plastered Stone Slab
			//Plastered Stone Vertical Slab
			//Plastered Stone Corner
			//Plastered Stone Arch
			//Plastered Stone Arrow Slit
			//Plastered Stone Window Slit
			//Plastered Stone Wall
			//Plastered Stone Fence
			//Plastered Stone Gate
			//Plastered Stone Trapdoor
			//Plastered Stone Door
			//Plastered Stone Layer
			//Plastered Stone Anvil
			//Plastered Stone Hopper
			//Plastered Stone Dragon Egg
			//Plastered Stone Tiles
			list.add(new ItemStack(MetaBlocks.stone_full_1,1,1)); //Plastered Slate

			//  Slate  //
			//Slate Arrow Slit
			//Slate Stairs

			//  Hewn Stone  //
			list.add(new ItemStack(MetaBlocks.stone_full_1,1,2)); //Hewn Stone
			//Hewn Stone Stairs
			//Hewn Stone Arrow Slit
			//Hewn Stone Wall
			//Hewn Stone Fence
			//Hewn Stone Trapdoor
			//Hewn Stone Tiles
			list.add(new ItemStack(MetaBlocks.stone_full_1,1,3)); //Hewn Stone with Stone Brick


			//  Cobblestone //
			//Cobblestone Vertical Slab
			//Cobblestone Corner
			//Cobblestone Arch
			//Cobblestone Arrow Slit
			//Cobblestone Window Slit
			//Cobblestone Trapdoor
			//Cobblestone Door

			// Cobblestone Variants //
			list.add(new ItemStack(MetaBlocks.stone_full_1,1,8)); //Old Cobblestone
			list.add(new ItemStack(MetaBlocks.stone_full_1,1,9)); //Damaged Cobblestone
			list.add(new ItemStack(MetaBlocks.stone_full_1,1,10)); //Fishscale Cobblestone
			list.add(new ItemStack(MetaBlocks.stone_full_1,1,11)); //Dirty Fishscale Cobblestone
			list.add(new ItemStack(MetaBlocks.stone_full_1,1,12)); //Brick Corner Cobblestone
			list.add(new ItemStack(MetaBlocks.stone_full_1,1,13)); //Blue Marble (Stone Brick Corner)
			list.add(new ItemStack(MetaBlocks.stone_full_1,1,14)); //Frozen Cobblestone
			list.add(new ItemStack(MetaBlocks.stone_full_1,1,15)); //Mossy Cobblestone (All Sides)
			list.add(new ItemStack(MetaBlocks.stone_full_2,1,0)); //Overgrown Cobblestone
			list.add(new ItemStack(MetaBlocks.stone_full_2,1,1)); //Cobblestone with Vines
			list.add(new ItemStack(MetaBlocks.stone_full_2,1,5)); //Reinforced Cobblestone (CTM)
			list.add(new ItemStack(MetaBlocks.stone_full_2,1,6)); //Reinforced Cobblestone

			//  Stone Brick  //
			//Stone Brick Wall
			//Stone Brick Arrow Slit
			//Stone Brick Trapdoor
			list.add(new ItemStack(MetaBlocks.stone_full_2,1,2)); //Overgrown Stone Brick
			list.add(new ItemStack(MetaBlocks.stone_full_2,1,3)); //Chiseled Stone Brick
			list.add(new ItemStack(MetaBlocks.stone_full_2,1,4)); //Stone Cobblestone
			//Stone Balustrade
			//Stone Newel Cap
			//Stone Gargoyle
			//Stone Trapdoor


			//  Stone Details  //
			list.add(new ItemStack(MetaBlocks.stone_full_2,1,7)); //Polished Diorite
			list.add(new ItemStack(MetaBlocks.stone_full_2,1,8)); //Portcullis Groove
			list.add(new ItemStack(MetaBlocks.stone_full_2,1,9)); //Chiseled Stone
			list.add(new ItemStack(MetaBlocks.stone_full_2,1,10)); //Stone Pillar


			//  Runes  //
			list.add(new ItemStack(MetaBlocks.stone_full_1,1,6)); //Germanic Glyphs
			list.add(new ItemStack(MetaBlocks.stone_full_1,1,7)); //Germanic Glyphs 2


			//  Iron  //
			//Dark Iron Block
			//Dark Iron Stairs
			//Iron Cannon Stairs
			//Dark Iron Slab
			//Dark Iron Pillar
			//Dark Iron Fence
			//Iron Newel Cap
			//Anvil (Do we need this?)
			//Metal Grate Stairs
			//Metal Railing
			//Straight Metal Railing
			//Metal Grate
			//Iron Block 2
			//Iron Block 3
			//Rusty Iron Block
			//Iron Trapdoor (Do we need this?)


			//  Tudor and Dark Tudor Frames  //
			list.add(new ItemStack(MetaBlocks.stone_full_3,1,1)); //Tudor Slash
			list.add(new ItemStack(MetaBlocks.stone_full_3,1,2)); //Tudor Backslash
			list.add(new ItemStack(MetaBlocks.stone_full_3,1,3)); //Tudor X
			list.add(new ItemStack(MetaBlocks.stone_full_3,1,4)); //Tudor Up
			list.add(new ItemStack(MetaBlocks.stone_full_3,1,5)); //Tudor Down
			list.add(new ItemStack(MetaBlocks.stone_full_3,1,6)); //Tudor Box

			list.add(new ItemStack(MetaBlocks.stone_full_3,1,7)); //Tudor Slash (CTM)
			list.add(new ItemStack(MetaBlocks.stone_full_3,1,8)); //Tudor Backslash (CTM)
			list.add(new ItemStack(MetaBlocks.stone_full_3,1,9)); //Tudor Box (CTM)

			list.add(new ItemStack(MetaBlocks.stone_full_3,1,10)); //Dark Tudor Slash
			list.add(new ItemStack(MetaBlocks.stone_full_3,1,11)); //Dark Tudor Backslash
			list.add(new ItemStack(MetaBlocks.stone_full_3,1,12)); //Dark Tudor X
			list.add(new ItemStack(MetaBlocks.stone_full_3,1,13)); //Dark Tudor Up
			list.add(new ItemStack(MetaBlocks.stone_full_3,1,14)); //Dark Tudor Down
			list.add(new ItemStack(MetaBlocks.stone_full_3,1,15)); //Dark Tudor Box

			list.add(new ItemStack(MetaBlocks.stone_full_4,1,0)); //Dark Tudor Slash (CTM)
			list.add(new ItemStack(MetaBlocks.stone_full_4,1,1)); //Dark Tudor Backslash (CTM)
			list.add(new ItemStack(MetaBlocks.stone_full_4,1,2)); //Dark Tudor Box (CTM)


			//  Brick, Red Brick and Dark Brick Frames  //
			list.add(new ItemStack(MetaBlocks.stone_full_4,1,3)); //Brick Slash
			list.add(new ItemStack(MetaBlocks.stone_full_4,1,4)); //Brick Backslash
			list.add(new ItemStack(MetaBlocks.stone_full_4,1,5)); //Brick X
			list.add(new ItemStack(MetaBlocks.stone_full_4,1,6)); //Brick Up
			list.add(new ItemStack(MetaBlocks.stone_full_4,1,7)); //Brick Down
			list.add(new ItemStack(MetaBlocks.stone_full_4,1,8)); //Brick Box

			list.add(new ItemStack(MetaBlocks.stone_full_4,1,9)); //Brick Slash (CTM)
			list.add(new ItemStack(MetaBlocks.stone_full_4,1,10)); //Brick Backslash (CTM)
			list.add(new ItemStack(MetaBlocks.stone_full_4,1,11)); //Brick Box (CTM)

			list.add(new ItemStack(MetaBlocks.stone_full_4,1,12)); //Dark Brick Slash
			list.add(new ItemStack(MetaBlocks.stone_full_4,1,13)); //Dark Brick Backslash
			list.add(new ItemStack(MetaBlocks.stone_full_4,1,14)); //Dark Brick X
			list.add(new ItemStack(MetaBlocks.stone_full_4,1,15)); //Dark Brick Up
			list.add(new ItemStack(MetaBlocks.stone_full_5,1,0)); //Dark Brick Down
			list.add(new ItemStack(MetaBlocks.stone_full_5,1,1)); //Dark Brick Box

			list.add(new ItemStack(MetaBlocks.stone_full_5,1,2)); //Dark Brick Slash (CTM)
			list.add(new ItemStack(MetaBlocks.stone_full_5,1,3)); //Dark Brick Backslash (CTM)
			list.add(new ItemStack(MetaBlocks.stone_full_5,1,4)); //Dark Brick Box (CTM)

			list.add(new ItemStack(MetaBlocks.stone_full_5,1,5)); //Red Brick Slash
			list.add(new ItemStack(MetaBlocks.stone_full_5,1,6)); //Red Brick Backslash
			list.add(new ItemStack(MetaBlocks.stone_full_5,1,7)); //Red Brick X
			list.add(new ItemStack(MetaBlocks.stone_full_5,1,8)); //Red Brick Up
			list.add(new ItemStack(MetaBlocks.stone_full_5,1,9)); //Red Brick Down
			list.add(new ItemStack(MetaBlocks.stone_full_5,1,10)); //Red Brick Box

			list.add(new ItemStack(MetaBlocks.stone_full_5,1,11)); //Red Brick Slash (CTM)
			list.add(new ItemStack(MetaBlocks.stone_full_5,1,12)); //Red Brick Backslash (CTM)
			list.add(new ItemStack(MetaBlocks.stone_full_5,1,13)); //Red Brick Box (CTM)


			//  Bricks  //
			list.add(new ItemStack(MetaBlocks.stone_full_5,1,14)); //Brick
			list.add(new ItemStack(MetaBlocks.stone_full_5,1,15)); //Mossy Brick

			list.add(new ItemStack(MetaBlocks.stone_full_6,1,0)); //Dark Red Brick
			//Dark Red Brick Stairs
			//Dark Red Brick Slab
			//Dark Red Brick Wall
			list.add(new ItemStack(MetaBlocks.stone_full_6,1,1)); //Mossy Dark Red Brick

			list.add(new ItemStack(MetaBlocks.stone_full_6,1,2)); //Red Brick
			//Red Brick Stairs
			//Red Brick Slab
			//Red Brick Pillar
			list.add(new ItemStack(MetaBlocks.stone_full_6,1,3)); //Red Mossy Brick

			//Netherbrick and Red Tiles
			list.add(new ItemStack(MetaBlocks.stone_full_2,1,11)); //Normal Netherbrick
			list.add(new ItemStack(MetaBlocks.stone_full_2,1,12)); //Big Netherbrick
			list.add(new ItemStack(MetaBlocks.stone_full_2,1,13)); //Netherbrick Pillar
			list.add(new ItemStack(MetaBlocks.stone_full_2,1,14)); //Carved Netherbrick
			list.add(new ItemStack(MetaBlocks.stone_full_2,1,15)); //Red Netherbrick
			list.add(new ItemStack(MetaBlocks.stone_full_3,1,0)); //Red Tiles


			//  Roman Brick //
			list.add(new ItemStack(MetaBlocks.stone_full_6,1,4)); //Roman Brick
			//Roman Brick Stairs
			//Roman Brick Slab
			//Roman Brick Wall
			//Roman Brick Fence
			//Roman Brick Trapdoor


			//  Travertine  //
			list.add(new ItemStack(MetaBlocks.stone_full_6,1,5)); //Diorite Brick
			list.add(new ItemStack(MetaBlocks.stone_full_6,1,6)); //Travertine Brick
			//Travertine Brick Stairs
			//Travertine Brick Stairs
			//Travertine Brick Wall
			//Travertine Brick Fence
			//Travertine Brick Trapdoor


			//  Big Sandstone  //
			list.add(new ItemStack(MetaBlocks.stone_full_6,1,7)); //Big Sandstone Brick
			//Big Sandstone Stairs
			//Big Sandstone Slab
			//Dark Sandstone Arrow Slit
			//Big Sandstone Wall
			//Big Sandstone Fence
			//Big Sandstone Trapdoor
			//Sandstone Gargoyle


			//  Sandstone //
			list.add(new ItemStack(MetaBlocks.stone_full_6,1,8)); //Blue Marble (Sandstone Corners)
			list.add(new ItemStack(MetaBlocks.stone_full_6,1,9)); //Blue Marble (Sandstone Corners) 2
			list.add(new ItemStack(MetaBlocks.stone_full_6,1,10)); //Chiseled Sandstone
			list.add(new ItemStack(MetaBlocks.stone_full_6,1,11)); //Conglomerate Sandstone
			//Sandstone Wall
			//Sandstone Arrow Slit
			//Sandstone Newel Cap
			list.add(new ItemStack(MetaBlocks.stone_full_6,1,12)); //Mossy Sandstone
			list.add(new ItemStack(MetaBlocks.stone_full_6,1,13)); //Frieze Sandstone
			list.add(new ItemStack(MetaBlocks.stone_full_6,1,14)); //Inscribed Sandstone
			list.add(new ItemStack(MetaBlocks.stone_full_6,1,15)); //Sandstone Brick
			//Sandstone Brick Stairs
			//Sandstone Brick Slab
			//Sandstone Brick Wall
			//Sandstone Brick Fence
			//Sandstone Brick Trapdoor


			//  Clay Tile  //
			//list.add(new ItemStack(MetaBlocks.stone_fullpartial_2,1,10)); //Clay Tile (Carpet)
			//list.add(new ItemStack(MetaBlocks.stone_fullpartial_2,1,11)); //Mixed Tile (Carpet)
			//list.add(new ItemStack(MetaBlocks.stone_fullpartial_2,1,12)); //Light Tile (Carpet)


			//  Sandstone and Marble Columns  //
			//Sandstone Column
			list.add(new ItemStack(MetaBlocks.stone_full_7,1,0)); //Capital Sandstone
			list.add(new ItemStack(MetaBlocks.stone_full_7,1,1)); //Base Sandstone

			list.add(new ItemStack(MetaBlocks.stone_full_7,1,2)); //Capital Red Marble
			list.add(new ItemStack(MetaBlocks.stone_full_7,1,3)); //Base Red Marble
			list.add(new ItemStack(MetaBlocks.stone_full_7,1,4)); //Capital Red Sandstone
			list.add(new ItemStack(MetaBlocks.stone_full_7,1,5)); //Base Red Sandstone

			list.add(new ItemStack(MetaBlocks.stone_full_7,1,6)); //Capital Blue Marble
			list.add(new ItemStack(MetaBlocks.stone_full_7,1,7)); //Base Blue Marble
			list.add(new ItemStack(MetaBlocks.stone_full_7,1,8)); //Capital Blue Sandstone
			list.add(new ItemStack(MetaBlocks.stone_full_7,1,9)); //Base Blue Sandstone

			list.add(new ItemStack(MetaBlocks.stone_full_7,1,10)); //Capital Gold Marble
			list.add(new ItemStack(MetaBlocks.stone_full_7,1,11)); //Base Gold Marble
			list.add(new ItemStack(MetaBlocks.stone_full_7,1,12)); //Capital Gold Sandstone
			list.add(new ItemStack(MetaBlocks.stone_full_7,1,13)); //Base Gold Sandstone

			list.add(new ItemStack(MetaBlocks.stone_full_7,1,14)); //Capital Marble
			list.add(new ItemStack(MetaBlocks.stone_full_7,1,15)); //Base Marble

			list.add(new ItemStack(MetaBlocks.stone_full_8,1,0)); //Capital Corinthian
			list.add(new ItemStack(MetaBlocks.stone_full_8,1,1)); //Capital Corinthian Sandstone

			list.add(new ItemStack(MetaBlocks.stone_full_8,1,2)); //Cornice Sandstone
			//Cornice Sandstone Stairs
			//Cornice Sandstone Slab


			list.add(new ItemStack(MetaBlocks.stone_full_8,1,3)); //Plinth Sandstone
			//Plinth Sandstone Stairs
			//Plinth Sandstone Slab
			list.add(new ItemStack(MetaBlocks.stone_full_8,1,4)); //Marble and Sandstone Base
			//Marble and Sandstone Base Stairs
			//Marble and Sandstone Base Slab

			list.add(new ItemStack(MetaBlocks.stone_full_8,1,5)); //Cornice Marble
			//Cornice Marble Stairs
			//Cornice Marble Slab
			list.add(new ItemStack(MetaBlocks.stone_full_8,1,6)); //Marble Base
			//Marble Base Stairs
			//Marble Base Slab

			list.add(new ItemStack(MetaBlocks.stone_full_8,1,7)); //Marble
			list.add(new ItemStack(MetaBlocks.stone_full_8,1,8)); //Smooth White Marble
			//Marble Balustrade
			//Marble Newel Cap
			//Marble Railing

			list.add(new ItemStack(MetaBlocks.stone_full_8,1,9)); //Base Parian Marble
			list.add(new ItemStack(MetaBlocks.stone_full_8,1,10)); //Capital Parian Marble


			//Letters
			list.add(new ItemStack(MetaBlocks.stone_full_8,1,11)); //Engraved A
			list.add(new ItemStack(MetaBlocks.stone_full_8,1,12)); //Engraved B
			list.add(new ItemStack(MetaBlocks.stone_full_8,1,13)); //Engraved C
			list.add(new ItemStack(MetaBlocks.stone_full_8,1,14)); //Engraved D
			list.add(new ItemStack(MetaBlocks.stone_full_8,1,15)); //Engraved E
			list.add(new ItemStack(MetaBlocks.stone_full_9,1,0)); //Engraved F
			list.add(new ItemStack(MetaBlocks.stone_full_9,1,1)); //Engraved G
			list.add(new ItemStack(MetaBlocks.stone_full_9,1,2)); //Engraved H
			list.add(new ItemStack(MetaBlocks.stone_full_9,1,3)); //Engraved I
			list.add(new ItemStack(MetaBlocks.stone_full_9,1,4)); //Engraved Interpunct
			list.add(new ItemStack(MetaBlocks.stone_full_9,1,5)); //Engraved K
			list.add(new ItemStack(MetaBlocks.stone_full_9,1,6)); //Engraved L
			list.add(new ItemStack(MetaBlocks.stone_full_9,1,7)); //Engraved M
			list.add(new ItemStack(MetaBlocks.stone_full_9,1,8)); //Engraved M
			list.add(new ItemStack(MetaBlocks.stone_full_9,1,9)); //Engraved O
			list.add(new ItemStack(MetaBlocks.stone_full_9,1,10)); //Engraved P
			list.add(new ItemStack(MetaBlocks.stone_full_9,1,11)); //Engraved Q
			list.add(new ItemStack(MetaBlocks.stone_full_9,1,12)); //Engraved R
			list.add(new ItemStack(MetaBlocks.stone_full_9,1,13)); //Engraved S
			list.add(new ItemStack(MetaBlocks.stone_full_9,1,14)); //Engraved T
			list.add(new ItemStack(MetaBlocks.stone_full_9,1,15)); //Engraved V


			//  Roman Blocks  //
			list.add(new ItemStack(MetaBlocks.stone_full_10,1,0)); //Marble Big Slabs
			list.add(new ItemStack(MetaBlocks.stone_full_10,1,1)); //Marble Big Slabs (No Side CTM)
			list.add(new ItemStack(MetaBlocks.stone_full_10,1,2)); //Large Sandstone Slabs
			list.add(new ItemStack(MetaBlocks.stone_full_10,1,3)); //Large Sandstone Slabs (No Side CTM)
			list.add(new ItemStack(MetaBlocks.stone_full_10,1,4)); //Large Inscribed Sandstone Slabs
			list.add(new ItemStack(MetaBlocks.stone_full_10,1,5)); //Large Inscribed Sandstone Slabs (No Side CTM)

			list.add(new ItemStack(MetaBlocks.stone_full_10,1,6)); //Large Black Slabs
			list.add(new ItemStack(MetaBlocks.stone_full_10,1,7)); //Large Black Slabs (No Side CTM)
			list.add(new ItemStack(MetaBlocks.stone_full_10,1,8)); //Large Black Inscribed Slabs
			list.add(new ItemStack(MetaBlocks.stone_full_10,1,9)); //Large Black Inscribed Slabs (No Side CTM)

			//Mosaics
			list.add(new ItemStack(MetaBlocks.stone_full_10,1,10)); //Roman Mosiac
			list.add(new ItemStack(MetaBlocks.stone_full_10,1,11)); //Indian Mosiac
			list.add(new ItemStack(MetaBlocks.stone_full_10,1,12)); //Andalusian Mosaic

			//Stucco and Wall Designs
			list.add(new ItemStack(MetaBlocks.stone_full_10,1,13)); //(Yellow) Stucco
			//Yellow Stucco Slab
			list.add(new ItemStack(MetaBlocks.stone_full_10,1,14)); //White Stucco
			//White Stucco Slab

			list.add(new ItemStack(MetaBlocks.stone_full_10,1,15)); //Magenta Clean Stucco
			list.add(new ItemStack(MetaBlocks.stone_full_11,1,0)); //Light Gray Clean Stucco
			list.add(new ItemStack(MetaBlocks.stone_full_11,1,1)); //Yellow Clean Stucco
			list.add(new ItemStack(MetaBlocks.stone_full_11,1,2)); //White Clean Stucco
			list.add(new ItemStack(MetaBlocks.stone_full_11,1,3)); //Purple Clean Stucco
			list.add(new ItemStack(MetaBlocks.stone_full_11,1,4)); //Orange Stone
			//Orange Stained Clay Slab
			//Purple Stained Clay Slab
			//Black Stained Clay Slab


			list.add(new ItemStack(MetaBlocks.stone_full_11,1,5)); //Decorative Mosaic
			list.add(new ItemStack(MetaBlocks.stone_full_11,1,6)); //White Wall Design
			list.add(new ItemStack(MetaBlocks.stone_full_11,1,7)); //White Wall Design 2
			list.add(new ItemStack(MetaBlocks.stone_full_11,1,8)); //White Wall Design 3
			list.add(new ItemStack(MetaBlocks.stone_full_11,1,9)); //White Wall Design 4
			list.add(new ItemStack(MetaBlocks.stone_full_11,1,10)); //White Wall Design 5
			list.add(new ItemStack(MetaBlocks.stone_full_11,1,11)); //White Wall Design 6

			list.add(new ItemStack(MetaBlocks.stone_full_11,1,12)); //White Stucco Wall
			list.add(new ItemStack(MetaBlocks.stone_full_11,1,13)); //Tan Stucco Wall
			list.add(new ItemStack(MetaBlocks.stone_full_11,1,14)); //Brown Wall Design
			list.add(new ItemStack(MetaBlocks.stone_full_11,1,15)); //Brown Stucco Wall
			list.add(new ItemStack(MetaBlocks.stone_full_12,1,0)); //Purple Stucco Wall
			list.add(new ItemStack(MetaBlocks.stone_full_12,1,1)); //Magenta Wall Design
			list.add(new ItemStack(MetaBlocks.stone_full_12,1,2)); //Magenta Stucco
			list.add(new ItemStack(MetaBlocks.stone_full_12,1,3)); //Red Wall Design
			list.add(new ItemStack(MetaBlocks.stone_full_12,1,4)); //Red Wall Design 2
			list.add(new ItemStack(MetaBlocks.stone_full_12,1,5)); //Red Wall Design 3
			list.add(new ItemStack(MetaBlocks.stone_full_12,1,6)); //Red Wall Design 4
			list.add(new ItemStack(MetaBlocks.stone_full_12,1,7)); //Red Wall Design 5
			list.add(new ItemStack(MetaBlocks.stone_full_12,1,8)); //Red Wall Design 6
			list.add(new ItemStack(MetaBlocks.stone_full_12,1,9)); //Red Wall Design 7
			list.add(new ItemStack(MetaBlocks.stone_full_12,1,10)); //Red Wall Design 8
			list.add(new ItemStack(MetaBlocks.stone_full_12,1,11)); //Light Red Stucco
			list.add(new ItemStack(MetaBlocks.stone_full_12,1,12)); //Orange Wall Design
			list.add(new ItemStack(MetaBlocks.stone_full_12,1,13)); //Orange Wall Design 2
			list.add(new ItemStack(MetaBlocks.stone_full_12,1,14)); //Lime Stucco
			list.add(new ItemStack(MetaBlocks.stone_full_12,1,15)); //Green Wall Design
			list.add(new ItemStack(MetaBlocks.stone_full_13,1,0)); //Green Stucco
			list.add(new ItemStack(MetaBlocks.stone_full_13,1,1)); //Cyan Stucco
			list.add(new ItemStack(MetaBlocks.stone_full_13,1,2)); //Light Blue Stucco
			list.add(new ItemStack(MetaBlocks.stone_full_13,1,3)); //Blue Stucco
			list.add(new ItemStack(MetaBlocks.stone_full_13,1,4)); //Black Stucco

			//More Roman Stuff
			list.add(new ItemStack(MetaBlocks.stone_full_13,1,5)); //Roman Ornate Door
			list.add(new ItemStack(MetaBlocks.stone_full_13,1,6)); //Roman Ornate Door 2
			list.add(new ItemStack(MetaBlocks.stone_full_13,1,7)); //Gilded Bronze Block
			list.add(new ItemStack(MetaBlocks.stone_full_13,1,8)); //Fancy Bronze Block
			list.add(new ItemStack(MetaBlocks.stone_full_13,1,9)); //Gold Bars

			//Roof Tiles
			list.add(new ItemStack(MetaBlocks.stone_full_14,1,0)); //Red Roof Tiles
			list.add(new ItemStack(MetaBlocks.stone_full_14,1,1)); //Gray Roof Tiles
			list.add(new ItemStack(MetaBlocks.stone_full_14,1,2)); //Brown Roof Tiles
			list.add(new ItemStack(MetaBlocks.stone_full_14,1,3)); //Oxidized Copper

			list.add(new ItemStack(MetaBlocks.stone_full_15,1,1)); //Roman Vertical Brick
			list.add(new ItemStack(MetaBlocks.stone_full_15,1,2)); //Floor Tiles
			list.add(new ItemStack(MetaBlocks.stone_full_15,1,3)); //Architrave Polychrome
			list.add(new ItemStack(MetaBlocks.stone_full_15,1,4)); //Architrave Polychrome Legionares
			list.add(new ItemStack(MetaBlocks.stone_full_15,1,5)); //Architrave Polychrome 2
			list.add(new ItemStack(MetaBlocks.stone_full_15,1,6)); //Capital Corinthian Polychrome
			list.add(new ItemStack(MetaBlocks.stone_full_15,1,7)); //Black and White Marble
			list.add(new ItemStack(MetaBlocks.stone_full_15,1,8)); //Small Diagonal Checkered Marble
			list.add(new ItemStack(MetaBlocks.stone_full_15,1,9)); //Small Checkered Marble
			list.add(new ItemStack(MetaBlocks.stone_full_15,1,10)); //Red and White Marble
			list.add(new ItemStack(MetaBlocks.stone_full_15,1,11)); //Red and White Marble 2
			list.add(new ItemStack(MetaBlocks.stone_full_15,1,12)); //Blue and Yellow Marble
			list.add(new ItemStack(MetaBlocks.stone_full_15,1,13)); //Diagonal Checkered Marble
			list.add(new ItemStack(MetaBlocks.stone_full_15,1,14)); //Checkered Marble
			list.add(new ItemStack(MetaBlocks.stone_full_15,1,15)); //Black Marble
			list.add(new ItemStack(MetaBlocks.stone_full_16,1,1)); //Gray Marble
			list.add(new ItemStack(MetaBlocks.stone_full_16,1,3)); //Pink Marble
			list.add(new ItemStack(MetaBlocks.stone_full_16,1,4)); //Red Marble
			list.add(new ItemStack(MetaBlocks.stone_full_16,1,2)); //Green Marble
			list.add(new ItemStack(MetaBlocks.stone_full_16,1,12)); //Dark Green Marble
			list.add(new ItemStack(MetaBlocks.stone_full_16,1,0)); //Blue Marble
			list.add(new ItemStack(MetaBlocks.stone_full_16,1,13)); //Indigo Marble
			list.add(new ItemStack(MetaBlocks.stone_full_16,1,5)); //Red Jewels
			list.add(new ItemStack(MetaBlocks.stone_full_16,1,8)); //Gold Jewels
			list.add(new ItemStack(MetaBlocks.stone_full_16,1,6)); //Green Jewels
			list.add(new ItemStack(MetaBlocks.stone_full_16,1,7)); //Purple Jewels
			list.add(new ItemStack(MetaBlocks.stone_full_16,1,9)); //Dark Roman Brick
			list.add(new ItemStack(MetaBlocks.stone_full_16,1,10)); //Dark Roman Sandstone Brick
			list.add(new ItemStack(MetaBlocks.stone_full_16,1,11)); //Roman Sandstone Brick
			list.add(new ItemStack(MetaBlocks.stone_full_16,1,14)); //Roman Marble Pedestal
			list.add(new ItemStack(MetaBlocks.stone_full_16,1,15)); //Red and White Marble
			list.add(new ItemStack(MetaBlocks.stone_full_17,1,0)); //Stone Brick Inscription


			super.displayAllRelevantItems(list);
		}

	};

	public static CreativeTabs roofing = new RoofingTab(CreativeTabs.getNextID(), "roofing")
	{

		@Override
		@SideOnly(Side.CLIENT)
		public void displayAllRelevantItems(List<ItemStack> list)
		{
		/*	ItemStack clayredmesa = new ItemStack(Item.getItemFromBlock(BlockRegistry.clayredmesa));
			list.add(clayredmesa);//Red Roof Tiles
            ItemStack brickstairs8 = new ItemStack(Item.getItemFromBlock(BlockRegistry.brickstairs8));
            list.add(brickstairs8);//Red Tile Stairs
            ItemStack woodslab7taiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.woodslab7taiga));
            list.add(woodslab7taiga);//Red Tile Slab
            ItemStack rooftile_red_trapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_red_trapdoor));
            list.add(rooftile_red_trapdoor);//Red Tile Trapdoor
            ItemStack rooftile_red_wall = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_red_wall));
            list.add(rooftile_red_wall);//Red Tile Wall
            ItemStack rooftile_red_fence = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_red_fence));
            list.add(rooftile_red_fence);//Red Tile Fence
            ItemStack brickstairs8mushroomisland = new ItemStack(Item.getItemFromBlock(BlockRegistry.brickstairs8mushroomisland));
            list.add(brickstairs8mushroomisland);//Pink Tile Stairs
            ItemStack woodslab7pink = new ItemStack(Item.getItemFromBlock(BlockRegistry.woodslab7pink));
            list.add(woodslab7pink);//Pink Tile Slab
            ItemStack rooftile_pink_trapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_pink_trapdoor));
            list.add(rooftile_pink_trapdoor);//Pink Tile Trapdoor
            ItemStack rooftile_pink_wall = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_pink_wall));
            list.add(rooftile_pink_wall);//Pink Tile Wall
            ItemStack rooftile_pink_fence = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_pink_fence));
            list.add(rooftile_pink_fence);//Pink Tile Fence
            ItemStack claygreymesa = new ItemStack(Item.getItemFromBlock(BlockRegistry.claygreymesa));
			list.add(claygreymesa);//Gray Roof Tile
            ItemStack netherbrickstairs8 = new ItemStack(Item.getItemFromBlock(BlockRegistry.netherbrickstairs8));
            list.add(netherbrickstairs8);//Gray Tile Stairs
            ItemStack woodslab7swamp = new ItemStack(Item.getItemFromBlock(BlockRegistry.woodslab7swamp));
            list.add(woodslab7swamp);//Gray Tile Slab
            ItemStack rooftile_gray_trapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_gray_trapdoor));
            list.add(rooftile_gray_trapdoor);//Gray Tile Trapdoor
            ItemStack rooftile_gray_wall = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_gray_wall));
            list.add(rooftile_gray_wall);//Gray Tile Wall
            ItemStack rooftile_gray_fence = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_gray_fence));
            list.add(rooftile_gray_fence);//Gray Tile Fence
			ItemStack claybrownmesa = new ItemStack(Item.getItemFromBlock(BlockRegistry.claybrownmesa));
			list.add(claybrownmesa);//Brown Roof Tile
            ItemStack stonebrickstairs8 = new ItemStack(Item.getItemFromBlock(BlockRegistry.stonebrickstairs8));
            list.add(stonebrickstairs8);//Brown Tile Stairs
            ItemStack woodslab7mesa = new ItemStack(Item.getItemFromBlock(BlockRegistry.woodslab7mesa));
            list.add(woodslab7mesa);//Brown Tile Slab
            ItemStack rooftile_brown_trapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_brown_trapdoor));
            list.add(rooftile_brown_trapdoor);//Brown Tile Trapdoor
            ItemStack rooftile_brown_wall = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_brown_wall));
            list.add(rooftile_brown_wall);//Brown Tile Wall
            ItemStack rooftile_brown_fence = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_brown_fence));
            list.add(rooftile_brown_fence);//Brown Tile Fence
            ItemStack quartzstairs8mushroomisland = new ItemStack(Item.getItemFromBlock(BlockRegistry.quartzstairs8mushroomisland));
            list.add(quartzstairs8mushroomisland);//Blue Tile Stairs
            ItemStack woodslab7desert = new ItemStack(Item.getItemFromBlock(BlockRegistry.woodslab7desert));
            list.add(woodslab7desert);//Blue Tile Slab
            ItemStack bluetiletrapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.bluetiletrapdoor));
            list.add(bluetiletrapdoor);//Blue Tile Trapdoor
            ItemStack bluetilewall = new ItemStack(Item.getItemFromBlock(BlockRegistry.bluetilewall));
            list.add(bluetilewall);//Blue Tile Wall
            ItemStack bluetilefence = new ItemStack(Item.getItemFromBlock(BlockRegistry.bluetilefence));
            list.add(bluetilefence);//Blue Tile Fence
            ItemStack netherbrickstairs8taiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.netherbrickstairs8taiga));
            list.add(netherbrickstairs8taiga);//Cyan Tile Stairs
            ItemStack woodslab7cyan = new ItemStack(Item.getItemFromBlock(BlockRegistry.woodslab7cyan));
            list.add(woodslab7cyan);//Cyan Tile Slab
            ItemStack rooftile_cyan_trapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_cyan_trapdoor));
            list.add(rooftile_cyan_trapdoor);//Cyan Tile Trapdoor
            ItemStack rooftile_cyan_wall = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_cyan_wall));
            list.add(rooftile_cyan_wall);//Cyan Tile Wall
            ItemStack rooftile_cyan_fence = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_cyan_fence));
            list.add(rooftile_cyan_fence);//Cyan Tile Fence
            ItemStack quartzstairs8 = new ItemStack(Item.getItemFromBlock(BlockRegistry.quartzstairs8));
            list.add(quartzstairs8);//Light Blue Tile Stairs
            ItemStack woodslab7savanna = new ItemStack(Item.getItemFromBlock(BlockRegistry.woodslab7savanna));
            list.add(woodslab7savanna);//Light Blue Tile Slab
            ItemStack rooftile_lightblue_trapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_lightblue_trapdoor));
            list.add(rooftile_lightblue_trapdoor);//Light Blue Tile Trapdoor
            ItemStack rooftile_lightblue_wall = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_lightblue_wall));
            list.add(rooftile_lightblue_wall);//Light Blue Tile Wall
            ItemStack rooftile_lightblue_fence = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_lightblue_fence));
            list.add(rooftile_lightblue_fence);//Light Blue Tile Fence
            ItemStack cobblestairs8mushroomisland = new ItemStack(Item.getItemFromBlock(BlockRegistry.cobblestairs8mushroomisland));
            list.add(cobblestairs8mushroomisland);//Green Tile Stairs
            ItemStack woodslab7jungle = new ItemStack(Item.getItemFromBlock(BlockRegistry.woodslab7jungle));
            list.add(woodslab7jungle);//Green Tile Slab
            ItemStack rooftile_green_trapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_green_trapdoor));
            list.add(rooftile_green_trapdoor);//Green Tile Trapdoor
            ItemStack greentile_wall = new ItemStack(Item.getItemFromBlock(BlockRegistry.greentile_wall));
            list.add(greentile_wall);//Green Tile Wall
            ItemStack greentile_fence = new ItemStack(Item.getItemFromBlock(BlockRegistry.greentile_fence));
            list.add(greentile_fence);//Green Tile Fence
            ItemStack cobblestairs8 = new ItemStack(Item.getItemFromBlock(BlockRegistry.cobblestairs8));
            list.add(cobblestairs8);//Light Green Tile Stairs
            ItemStack woodslab7megataiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.woodslab7megataiga));
            list.add(woodslab7megataiga);//Light Green Tile Slab
            ItemStack rooftile_lightgreen_trapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.rooftile_lightgreen_trapdoor));
            list.add(rooftile_lightgreen_trapdoor);//Light Green Tile Trapdoor
            ItemStack lightgreentile_wall = new ItemStack(Item.getItemFromBlock(BlockRegistry.lightgreentile_wall));
            list.add(lightgreentile_wall);//Light Green Tile Wall
            ItemStack lightgreentile_fence  = new ItemStack(Item.getItemFromBlock(BlockRegistry.lightgreentile_fence));
            list.add(lightgreentile_fence);//Light green Tile Fence
            ItemStack oxidized_copper = new ItemStack(Item.getItemFromBlock(BlockRegistry.oxidized_copper));
            list.add(oxidized_copper);//Oxidized Copper Roof
            ItemStack oxidized_copper_stairs = new ItemStack(Item.getItemFromBlock(BlockRegistry.oxidized_copper_stairs));
            list.add(oxidized_copper_stairs);//Oxidized Copper Stairs
            ItemStack oxidized_copper_slab = new ItemStack(Item.getItemFromBlock(BlockRegistry.oxidized_copper_slab));
            list.add(oxidized_copper_slab);//Oxidized Copper Roof Slab
            ItemStack oxidized_copper_trapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.oxidized_copper_trapdoor));
            list.add(oxidized_copper_trapdoor);//Oxidized Copper Trapdoor
            ItemStack oxidized_copper_wall = new ItemStack(Item.getItemFromBlock(BlockRegistry.oxidized_copper_wall));
            list.add(oxidized_copper_wall);//Oxidized Copper wall
            ItemStack oxidized_copper_fence = new ItemStack(Item.getItemFromBlock(BlockRegistry.oxidized_copper_fence));
            list.add(oxidized_copper_fence);//Oxidized Copper Fence
			ItemStack haybale1 = new ItemStack(Item.getItemFromBlock(BlockRegistry.haybale1));
			list.add(haybale1);//Thatch
            ItemStack haystairs = new ItemStack(Item.getItemFromBlock(BlockRegistry.haystairs));
            list.add(haystairs);//Thatch Stairs
            ItemStack woodslab7plains = new ItemStack(Item.getItemFromBlock(BlockRegistry.woodslab7plains));
            list.add(woodslab7plains);//Thatch Slab
            ItemStack thatchtrapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.thatchtrapdoor));
            list.add(thatchtrapdoor);//Thatch Trapdoor
            ItemStack thatch_wall = new ItemStack(Item.getItemFromBlock(BlockRegistry.thatch_wall));
            list.add(thatch_wall);//Gray Thatch Wall
            ItemStack thatch_fence = new ItemStack(Item.getItemFromBlock(BlockRegistry.thatch_fence));
            list.add(thatch_fence);//Gray Thatch Fence
            ItemStack haybale2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.haybale2));
			list.add(haybale2);//Gray Thatch
			ItemStack haybale3 = new ItemStack(Item.getItemFromBlock(BlockRegistry.haybale3));
			list.add(haybale3);//Gray Woven Thatch
            ItemStack sandstonestairs8 = new ItemStack(Item.getItemFromBlock(BlockRegistry.sandstonestairs8));
            list.add(sandstonestairs8);//Gray Thatch Stairs
            ItemStack woodslab7ocean = new ItemStack(Item.getItemFromBlock(BlockRegistry.woodslab7ocean));
            list.add(woodslab7ocean);//Gray Thatch Slab
            ItemStack graythatchtrapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.graythatchtrapdoor));
            list.add(graythatchtrapdoor);//Gray Thatch Trapdoor
            ItemStack thatch_gray_wall = new ItemStack(Item.getItemFromBlock(BlockRegistry.thatch_gray_wall));
            list.add(thatch_gray_wall);//Gray Thatch Wall
            ItemStack thatch_gray_fence = new ItemStack(Item.getItemFromBlock(BlockRegistry.thatch_gray_fence));
            list.add(thatch_gray_fence);//Gray Thatch Fence
            ItemStack clay_roof_well = new ItemStack(Item.getItemFromBlock(BlockRegistry.clay_roof_well));
			list.add(clay_roof_well);//Gray Well Roof
			*/

			super.displayAllRelevantItems(list);
		}

	};

	public static CreativeTabs windows = new WindowsTab(CreativeTabs.getNextID(), "windows")
	{

		@Override
		@SideOnly(Side.CLIENT)
		public void displayAllRelevantItems(List<ItemStack> list)
		{
        /*    ItemStack glass1 = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass1));
            list.add(glass1);//Glass Block (No Connected Textures)
            ItemStack glass2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass2));
            list.add(glass2);//Straight Glass Block
            ItemStack glass3 = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass3));
            list.add(glass3);//Straight Glass Block 2
            ItemStack glasspane1 = new ItemStack(Item.getItemFromBlock(BlockRegistry.glasspane1));
            list.add(glasspane1);//Straight Glass Pane
            ItemStack glass4 = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass4));
            list.add(glass4);//Fancy Glass Block
            ItemStack glass5 = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass5));
            list.add(glass5);//Fancy Glass Block 2
            ItemStack glasspane2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.glasspane2));
            list.add(glasspane2);//Fancy Glass Pane
            ItemStack glass7 = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass7));
            list.add(glass7);//Dragon Glass Block
            ItemStack glass8 = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass8));
            list.add(glass8);//Dragon Glass Block 2
            ItemStack glasspane4 = new ItemStack(Item.getItemFromBlock(BlockRegistry.glasspane4));
            list.add(glasspane4);//Dragon Glass Pane
            ItemStack glass12 = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass12));
            list.add(glass12);//Window
            ItemStack glass_elongated_brown = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass_elongated_brown));
            list.add(glass_elongated_brown);//Brown Window(CTM Elongated)
            ItemStack glass_round_brown = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass_round_brown));
            list.add(glass_round_brown);//Brown Window (CTM Round Top)
            ItemStack glasspane_round_brown = new ItemStack(Item.getItemFromBlock(BlockRegistry.glasspane_round_brown));
            list.add(glasspane_round_brown);//Brown Window (CTM Round Top)
            ItemStack glasspane10taiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.glasspane10taiga));
            list.add(glasspane10taiga);//Brown Window
            ItemStack glasspane_elongated_brown = new ItemStack(Item.getItemFromBlock(BlockRegistry.glasspane_elongated_brown));
            list.add(glasspane_elongated_brown);//Brown Window (CTM Elongated)
            ItemStack glass12desert = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass12desert));
            list.add(glass12desert);//Yellow window
            ItemStack glass_round_yellow = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass_round_yellow));
            list.add(glass_round_yellow);//Yellow Window (CTM Round Top)
            ItemStack glass_elongated_yellow = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass_elongated_yellow));
            list.add(glass_elongated_yellow);//Yellow Window (CTM Elongated)
            ItemStack glasspane10desert = new ItemStack(Item.getItemFromBlock(BlockRegistry.glasspane10desert));
            list.add(glasspane10desert);//Yellow Window
            ItemStack glasspane_round_yellow = new ItemStack(Item.getItemFromBlock(BlockRegistry.glasspane_round_yellow));
            list.add(glasspane_round_yellow);//Yellow Window (CTM Round Top)
            ItemStack glasspane_elongated_yellow  = new ItemStack(Item.getItemFromBlock(BlockRegistry.glasspane_elongated_yellow));
            list.add(glasspane_elongated_yellow);//Yellow Window (CTM Elongated)
            ItemStack glass12forest = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass12forest));
            list.add(glass12forest);//Green Window
            ItemStack glass_round_green = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass_round_green));
            list.add(glass_round_green);//Green Window (CTM Round Top)
            ItemStack glass_elongated_green = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass_elongated_green));
            list.add(glass_elongated_green);//Green Window (CTM Elongated)
            ItemStack glasspane10forest = new ItemStack(Item.getItemFromBlock(BlockRegistry.glasspane10forest));
            list.add(glasspane10forest);//Green Window
            ItemStack glasspane_round_green = new ItemStack(Item.getItemFromBlock(BlockRegistry.glasspane_round_green));
            list.add(glasspane_round_green);//Green Window (CTM Round Top)
            ItemStack glasspane_elongated_green = new ItemStack(Item.getItemFromBlock(BlockRegistry.glasspane_elongated_green));
            list.add(glasspane_elongated_green);//Green Window (CTM Elongated)
            ItemStack glass12white = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass12white));
            list.add(glass12white);//White Window
            ItemStack glass_round_white = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass_round_white));
            list.add(glass_round_white);//White Window (CTM Round Top)
            ItemStack glass_elongated_white = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass_elongated_white));
            list.add(glass_elongated_white);//White Window (CTM Elongated)
            ItemStack glasspane10 = new ItemStack(Item.getItemFromBlock(BlockRegistry.glasspane10));
            list.add(glasspane10);//White Window
            ItemStack glasspane_round_white = new ItemStack(Item.getItemFromBlock(BlockRegistry.glasspane_round_white));
            list.add(glasspane_round_white);//White Window (CTM Round Top)
            ItemStack glasspane_elongated_white = new ItemStack(Item.getItemFromBlock(BlockRegistry.glasspane_elongated_white));
            list.add(glasspane_elongated_white);//White Window (CTM Elongated)
            ItemStack whiteglassbirchforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.whiteglassbirchforest));
            list.add(whiteglassbirchforest);//Shoji
            ItemStack whitepanebirchforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.whitepanebirchforest));
            list.add(whitepanebirchforest);//Shoji
            ItemStack bay_window = new ItemStack(Item.getItemFromBlock(BlockRegistry.bay_window));
            list.add(bay_window);//Bay Window
            */

			super.displayAllRelevantItems(list);
		}

	};

	public static CreativeTabs wood = new WoodTab(CreativeTabs.getNextID(), "wood")
	{

		@Override
		@SideOnly(Side.CLIENT)
		public void displayAllRelevantItems(List<ItemStack> list)
		{
        /*    ItemStack oakbark = new ItemStack(Item.getItemFromBlock(BlockRegistry.oakbark));
            list.add(oakbark);//Full Oak Log
            ItemStack ironblock9 = new ItemStack(Item.getItemFromBlock(BlockRegistry.ironblock9));
            list.add(ironblock9);//Connecting Oak Log
            ItemStack oaklogstairs = new ItemStack(Item.getItemFromBlock(BlockRegistry.oaklogstairs));
            list.add(oaklogstairs);//Oak Log Stairs
            ItemStack oaklogtrapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.oaklogtrapdoor));
            list.add(oaklogtrapdoor);//Oak Log Trapdoor
            ItemStack cobblewall5 = new ItemStack(Item.getItemFromBlock(BlockRegistry.cobblewall5));
            list.add(cobblewall5);//Small Oak Log
            ItemStack fence5 = new ItemStack(Item.getItemFromBlock(BlockRegistry.fence5));
            list.add(fence5);//Oak Log Fence
            ItemStack acacialog2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialog2));
            list.add(acacialog2);//Mossy Oak Log
            ItemStack acacialog2_bark = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialog2_bark));
            list.add(acacialog2_bark);//Mossy Oak Log (Full)
            ItemStack sprucebark = new ItemStack(Item.getItemFromBlock(BlockRegistry.sprucebark));
            list.add(sprucebark);//Full Spruce Log
            ItemStack ironblock9taiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.ironblock9taiga));
            list.add(ironblock9taiga);//Connecting Spruce Wood Log
            ItemStack sprucestairs12 = new ItemStack(Item.getItemFromBlock(BlockRegistry.sprucestairs12));
            list.add(sprucestairs12);//Spruce Log Stairs
            ItemStack sprucelogtrapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.sprucelogtrapdoor));
            list.add(sprucelogtrapdoor);//Spruce Log Trapdoor
            ItemStack cobblewall4 = new ItemStack(Item.getItemFromBlock(BlockRegistry.cobblewall4));
            list.add(cobblewall4);//Small Spruce Log
            ItemStack fence3 = new ItemStack(Item.getItemFromBlock(BlockRegistry.fence3));
            list.add(fence3);//Spruce Log Fence
            ItemStack acacialog2taiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialog2taiga));
            list.add(acacialog2taiga);//Mossy Spruce Log
            ItemStack acacialog2taiga_bark = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialog2taiga_bark));
            list.add(acacialog2taiga_bark);//Mossy Spruce Log (Full)
            ItemStack birchbark = new ItemStack(Item.getItemFromBlock(BlockRegistry.birchbark));
            list.add(birchbark);//Full Birch Log
            ItemStack ironblock9birchforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.ironblock9birchforest));
            list.add(ironblock9birchforest);//Connecting Birch Log
            ItemStack birchstairs12 = new ItemStack(Item.getItemFromBlock(BlockRegistry.birchstairs12));
            list.add(birchstairs12);//Birch Log Stairs
            ItemStack birchlogtrapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.birchlogtrapdoor));
            list.add(birchlogtrapdoor);//Birch Log Trapdoor
            ItemStack cobblewall11 = new ItemStack(Item.getItemFromBlock(BlockRegistry.cobblewall11));
            list.add(cobblewall11);//Small Birch Log
            ItemStack fence2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.fence2));
            list.add(fence2);//Birch Log Fence
            ItemStack acacialog2birchforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialog2birchforest));
            list.add(acacialog2birchforest);//Mossy Birch Log
            ItemStack acacialog2birchforest_bark = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialog2birchforest_bark));
            list.add(acacialog2birchforest_bark);//Mossy Birch Log (Full)
            ItemStack palm = new ItemStack(Item.getItemFromBlock(BlockRegistry.palm));
            list.add(palm);//Palm Tree
            ItemStack junglebark = new ItemStack(Item.getItemFromBlock(BlockRegistry.junglebark));
            list.add(junglebark);//Full Jungle Log
            ItemStack ironblock9jungle = new ItemStack(Item.getItemFromBlock(BlockRegistry.ironblock9jungle));
            list.add(ironblock9jungle);//Connecting Jungle Logs
            ItemStack junglelogstairs = new ItemStack(Item.getItemFromBlock(BlockRegistry.junglelogstairs));
            list.add(junglelogstairs);//Jungle Log Stairs
            ItemStack junglelogtrapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.junglelogtrapdoor));
            list.add(junglelogtrapdoor);//Jungle Log Trapdoor
            ItemStack cobblewall12 = new ItemStack(Item.getItemFromBlock(BlockRegistry.cobblewall12));
            list.add(cobblewall12);//Small Jungle Logs
            ItemStack fence4 = new ItemStack(Item.getItemFromBlock(BlockRegistry.fence4));
            list.add(fence4);//Jungle Log Fence
            ItemStack acacialog2jungle = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialog2jungle));
            list.add(acacialog2jungle);//Mossy Jungle Log
            ItemStack acacialog2jungle_bark = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialog2jungle_bark));
            list.add(acacialog2jungle_bark);//Mossy Jungle Log (Full)
            ItemStack acaciabark = new ItemStack(Item.getItemFromBlock(BlockRegistry.acaciabark));
            list.add(acaciabark);//Full Acacia Log
            ItemStack ironblock9savanna = new ItemStack(Item.getItemFromBlock(BlockRegistry.ironblock9savanna));
            list.add(ironblock9savanna);//Connecting Acacia Log
            ItemStack acacialogstairs = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialogstairs));
            list.add(acacialogstairs);//Acacia Log Stairs
            ItemStack acacialogtrapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialogtrapdoor));
            list.add(acacialogtrapdoor);//Acacia Log Trapdoor
            ItemStack cobblewall15 = new ItemStack(Item.getItemFromBlock(BlockRegistry.cobblewall15));
            list.add(cobblewall15);//Small Acacia Log
            ItemStack fence7 = new ItemStack(Item.getItemFromBlock(BlockRegistry.fence7));
            list.add(fence7);//Acacia Wood Fence
            ItemStack acacialog2desert = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialog2desert));
            list.add(acacialog2desert);//Mossy Acacia Log
            ItemStack acacialog2desert_bark = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialog2desert_bark));
            list.add(acacialog2desert_bark);//Mossy Acacia Log (Full)
            ItemStack darkoakbark = new ItemStack(Item.getItemFromBlock(BlockRegistry.darkoakbark));
            list.add(darkoakbark);//Full Dark Oak Log
            ItemStack ironblock9roofedforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.ironblock9roofedforest));
            list.add(ironblock9roofedforest);//Connecting Dark Oak Log
            ItemStack darkoaklogstairs = new ItemStack(Item.getItemFromBlock(BlockRegistry.darkoaklogstairs));
            list.add(darkoaklogstairs);//Dark Oak Log Stairs
            ItemStack darkoaklogtrapdoor = new ItemStack(Item.getItemFromBlock(BlockRegistry.darkoaklogtrapdoor));
            list.add(darkoaklogtrapdoor);//Dark Oak Log Trapdoor
            ItemStack cobblewall14 = new ItemStack(Item.getItemFromBlock(BlockRegistry.cobblewall14));
            list.add(cobblewall14);//Small Dark Oak Logs
            ItemStack fence6 = new ItemStack(Item.getItemFromBlock(BlockRegistry.fence6));
            list.add(fence6);//Dark Log Fence
            ItemStack acacialog2roofedforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialog2roofedforest));
            list.add(acacialog2roofedforest);//Mossy Dark Oak Log
            ItemStack acacialog2roofedforest_bark = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialog2roofedforest_bark));
            list.add(acacialog2roofedforest_bark);//Mossy Dark Oak Log (Full)
            ItemStack burntoak = new ItemStack(Item.getItemFromBlock(BlockRegistry.burntoak));
            list.add(burntoak);//Burnt Oak Log
            ItemStack burntash = new ItemStack(Item.getItemFromBlock(BlockRegistry.burntash));
            list.add(burntash);//Burnt Ash Log
            ItemStack acacialog2normal = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialog2normal));
            list.add(acacialog2normal);//Ornamental Log
            ItemStack plank14 = new ItemStack(Item.getItemFromBlock(BlockRegistry.plank14));
            list.add(plank14);//Birch Logs
            ItemStack cobblewall9 = new ItemStack(Item.getItemFromBlock(BlockRegistry.cobblewall9));
            list.add(cobblewall9);//Small Ornamental Log
            ItemStack acacialog2coldbeach = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialog2coldbeach));
            list.add(acacialog2coldbeach);//Ornamental Dark Log
            ItemStack dark_birch_log = new ItemStack(Item.getItemFromBlock(BlockRegistry.dark_birch_log));
            list.add(dark_birch_log);//Dark Birch Logs
            ItemStack cobblewall9iceplains = new ItemStack(Item.getItemFromBlock(BlockRegistry.cobblewall9iceplains));
            list.add(cobblewall9iceplains);//Small Ornamental Dark Log
            ItemStack acacialog3 = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialog3));
            list.add(acacialog3);//Rope around a Log
            ItemStack acacialog3taiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacialog3taiga));
            list.add(acacialog3taiga);//Chains around a Log
            ItemStack plank7 = new ItemStack(Item.getItemFromBlock(BlockRegistry.plank7));
            list.add(plank7);//Wood Logs
            ItemStack log_pile_full = new ItemStack(Item.getItemFromBlock(BlockRegistry.log_pile_full));
            list.add(log_pile_full);//Large Log Pile
            ItemStack log_pile_left = new ItemStack(Item.getItemFromBlock(BlockRegistry.log_pile_left));
            list.add(log_pile_left);//Log Pile (Left)
            ItemStack log_pile_right = new ItemStack(Item.getItemFromBlock(BlockRegistry.log_pile_right));
            list.add(log_pile_right);//Log pile (Right)
            ItemStack log_pile_single = new ItemStack(Item.getItemFromBlock(BlockRegistry.log_pile_single));
            list.add(log_pile_single);//Small Log Pile
            //End Logs
            ItemStack plank8 = new ItemStack(Item.getItemFromBlock(BlockRegistry.plank8));
            list.add(plank8);//Oak Platform
            ItemStack netherbrickfence3 = new ItemStack(Item.getItemFromBlock(BlockRegistry.netherbrickfence3));
            list.add(netherbrickfence3);//Plain Wood Fence
            ItemStack netherbrickfence4 = new ItemStack(Item.getItemFromBlock(BlockRegistry.netherbrickfence4));
            list.add(netherbrickfence4);//Plain Wood Fence
            ItemStack hopperforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.hopperforest));
            list.add(hopperforest);//Oak Wood Capital
            ItemStack browncarpetextremehills = new ItemStack(Item.getItemFromBlock(BlockRegistry.browncarpetextremehills));
            list.add(browncarpetextremehills);//Wood Floor
            ItemStack plank6 = new ItemStack(Item.getItemFromBlock(BlockRegistry.plank6));
            list.add(plank6);//Carved Oak Wood Block
            ItemStack cobblewall9forest = new ItemStack(Item.getItemFromBlock(BlockRegistry.cobblewall9forest));
            list.add(cobblewall9forest);//Carved Oak Wood Pillar
            ItemStack oakbeam = new ItemStack(Item.getItemFromBlock(BlockRegistry.oakbeam));
            list.add(oakbeam);//Oak Beam
            ItemStack ironbar9 = new ItemStack(Item.getItemFromBlock(BlockRegistry.ironbar9));
            list.add(ironbar9);//Wattle Fence
            ItemStack trapdoorextremehills = new ItemStack(Item.getItemFromBlock(BlockRegistry.trapdoorextremehills));
            list.add(trapdoorextremehills);//Wooden Board Trapdoor
            ItemStack trapdoormesa = new ItemStack(Item.getItemFromBlock(BlockRegistry.trapdoormesa));
            list.add(trapdoormesa);//Light Shutters
            ItemStack ironbar9taiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.ironbar9taiga));
            list.add(ironbar9taiga);//Wooden Fence
            ItemStack junglerail = new ItemStack(Item.getItemFromBlock(BlockRegistry.junglerail));
            list.add(junglerail);//Jungle Rail
            ItemStack woodenstairs = new ItemStack(Item.getItemFromBlock(BlockRegistry.woodenstairs));
            list.add(woodenstairs);//3D Wooden Stairs
            ItemStack wood_stairs = new ItemStack(Item.getItemFromBlock(BlockRegistry.wood_stairs));
            list.add(wood_stairs);//Wood Plank Stairs
            ItemStack wood_railing = new ItemStack(Item.getItemFromBlock(BlockRegistry.wood_railing));
            list.add(wood_railing);//Wood Railing
            ItemStack wood_railing_straight = new ItemStack(Item.getItemFromBlock(BlockRegistry.wood_railing_straight));
            list.add(wood_railing_straight);//Straight Wood Railing
            //End Oak
            ItemStack plank9 = new ItemStack(Item.getItemFromBlock(BlockRegistry.plank9));
            list.add(plank9);//Spruce Platform
            ItemStack plank12 = new ItemStack(Item.getItemFromBlock(BlockRegistry.plank12));
            list.add(plank12);//Spruce Planks
            ItemStack fence1  = new ItemStack(Item.getItemFromBlock(BlockRegistry.fence1));
            list.add(fence1);//Spruce Fence
            ItemStack fencegateextremehills = new ItemStack(Item.getItemFromBlock(BlockRegistry.fencegateextremehills));
            list.add(fencegateextremehills);//Spruce Fence Gate
            ItemStack dragonegg14 = new ItemStack(Item.getItemFromBlock(BlockRegistry.dragonegg14));
            list.add(dragonegg14);//Spruce Wood Newel Cap
            ItemStack hoppertaiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.hoppertaiga));
            list.add(hoppertaiga);//Spruce Wood Capital
            ItemStack anviltaiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.anviltaiga));
            list.add(anviltaiga);//Spruce Wood Balustrade
            ItemStack railjungle = new ItemStack(Item.getItemFromBlock(BlockRegistry.railjungle));
            list.add(railjungle);//Spruce Wood Floor (Rail)
            ItemStack plank6taiga = new ItemStack(Item.getItemFromBlock(BlockRegistry.plank6taiga));
            list.add(plank6taiga);//Carved Taiga Wood Block
            ItemStack cobblewall9coldbeach = new ItemStack(Item.getItemFromBlock(BlockRegistry.cobblewall9coldbeach));
            list.add(cobblewall9coldbeach);//Carved Spruce Wood Pillar
            ItemStack sprucebeam = new ItemStack(Item.getItemFromBlock(BlockRegistry.sprucebeam));
            list.add(sprucebeam);//Spruce Beam
            ItemStack well_support = new ItemStack(Item.getItemFromBlock(BlockRegistry.well_support));
            list.add(well_support);//Wooden Support
            ItemStack spruce_stairs = new ItemStack(Item.getItemFromBlock(BlockRegistry.spruce_stairs));
            list.add(spruce_stairs);//Spruce Plank Stairs
            ItemStack spruce_railing = new ItemStack(Item.getItemFromBlock(BlockRegistry.spruce_railing));
            list.add(spruce_railing);//Spruce Railing
            ItemStack spruce_railing_straight = new ItemStack(Item.getItemFromBlock(BlockRegistry.spruce_railing_straight));
            list.add(spruce_railing_straight);//Straight Spruce Railing
            //End Spruce
            ItemStack fencegatefrozenriver = new ItemStack(Item.getItemFromBlock(BlockRegistry.fencegatefrozenriver));
            list.add(fencegatefrozenriver);//White Fence Gate
            ItemStack birchfence0 = new ItemStack(Item.getItemFromBlock(BlockRegistry.birchfence0));
            list.add(birchfence0);//Birch fence
            ItemStack fencegatebirch = new ItemStack(Item.getItemFromBlock(BlockRegistry.fencegatebirch));
            list.add(fencegatebirch);//Birch Fence Gate
            ItemStack ironbar9birchforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.ironbar9birchforest));
            list.add(ironbar9birchforest);//Painted Wooden Fence
            ItemStack plank6birchforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.plank6birchforest));
            list.add(plank6birchforest);//Carved Birch Wood Block
            ItemStack cobblewall9icemountains = new ItemStack(Item.getItemFromBlock(BlockRegistry.cobblewall9icemountains));
            list.add(cobblewall9icemountains);//Carved Birch Wood Pillar
            ItemStack birchbeam = new ItemStack(Item.getItemFromBlock(BlockRegistry.birchbeam));
            list.add(birchbeam);//Birch Beam
            ItemStack birch_stairs = new ItemStack(Item.getItemFromBlock(BlockRegistry.birch_stairs));
            list.add(birch_stairs);//Birch Plank Stairs
            ItemStack birch_railing = new ItemStack(Item.getItemFromBlock(BlockRegistry.birch_railing));
            list.add(birch_railing);//Birch Railing
            ItemStack birch_railing_straight = new ItemStack(Item.getItemFromBlock(BlockRegistry.birch_railing_straight));
            list.add(birch_railing_straight);//Straight Birch Railing
            //End Birch
            ItemStack plank6jungle = new ItemStack(Item.getItemFromBlock(BlockRegistry.plank6jungle));
            list.add(plank6jungle);//Carved jungle Wood Block
            ItemStack glass9 = new ItemStack(Item.getItemFromBlock(BlockRegistry.glass9));
            list.add(glass9);//Carved jungle Wood Wall
            ItemStack junglebeam = new ItemStack(Item.getItemFromBlock(BlockRegistry.junglebeam));
            list.add(junglebeam);//Jungle Beam
            ItemStack junglefence0 = new ItemStack(Item.getItemFromBlock(BlockRegistry.junglefence0));
            list.add(junglefence0);//Roped Fence
            ItemStack fencegatejungle = new ItemStack(Item.getItemFromBlock(BlockRegistry.fencegatejungle));
            list.add(fencegatejungle);//Roped Fence Gate
            ItemStack jungle_stairs = new ItemStack(Item.getItemFromBlock(BlockRegistry.jungle_stairs));
            list.add(jungle_stairs);//Jungle Plank Stairs
            ItemStack jungle_railing = new ItemStack(Item.getItemFromBlock(BlockRegistry.jungle_railing));
            list.add( jungle_railing);//Jungle Railing
            ItemStack jungle_railing_straight = new ItemStack(Item.getItemFromBlock(BlockRegistry.jungle_railing_straight));
            list.add( jungle_railing_straight);//Straight Jungle Railing
            //End Jungle
            ItemStack acaciaplankbirchforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.acaciaplankbirchforest));
            list.add(acaciaplankbirchforest);//Red Acacia Block
            ItemStack plank6desert = new ItemStack(Item.getItemFromBlock(BlockRegistry.plank6desert));
            ItemStack acaciastairsbirchforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.acaciastairsbirchforest));
            list.add(acaciastairsbirchforest);//Red Acacia Stairs
            ItemStack woodslab4birchforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.woodslab4birchforest));
            list.add(woodslab4birchforest);//Red Acacia Slab
            ItemStack cobblewall2birchforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.cobblewall2birchforest));
            list.add(cobblewall2birchforest);//Asian Wood Pillar
            ItemStack netherbrickfencebirchforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.netherbrickfencebirchforest));
            list.add(netherbrickfencebirchforest);//Asian Fence
            ItemStack fencegateicemountains = new ItemStack(Item.getItemFromBlock(BlockRegistry.fencegateicemountains));
            list.add(fencegateicemountains);//Asian Wooden Fence Gate
            list.add(plank6desert);//Carved Acacia Wood Block
            ItemStack cobblewall9mesa = new ItemStack(Item.getItemFromBlock(BlockRegistry.cobblewall9mesa));
            list.add(cobblewall9mesa);//Carved Acacia Wood Pillar
            ItemStack acaciabeam = new ItemStack(Item.getItemFromBlock(BlockRegistry.acaciabeam));
            list.add(acaciabeam);//Acacia Beam
            ItemStack acacia_stairs = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacia_stairs));
            list.add( acacia_stairs);//Acacia Plank Stairs
            ItemStack acacia_railing = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacia_railing));
            list.add( acacia_railing);//Acacia Railing
            ItemStack acacia_railing_straight = new ItemStack(Item.getItemFromBlock(BlockRegistry.acacia_railing_straight));
            list.add( acacia_railing_straight);//Straight Acacia Railing
            //End Acacia
            ItemStack darkoakfence0 = new ItemStack(Item.getItemFromBlock(BlockRegistry.darkoakfence0));
            list.add(darkoakfence0);//3D Dark Oak Fence
            ItemStack fencegatedarkoak = new ItemStack(Item.getItemFromBlock(BlockRegistry.fencegatedarkoak));
            list.add(fencegatedarkoak);//Black Fence Gate
            ItemStack ironbar9savanna = new ItemStack(Item.getItemFromBlock(BlockRegistry.ironbar9savanna));
            list.add(ironbar9savanna);//Wooden Framing
            ItemStack netherbrickfence2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.netherbrickfence2));
            list.add(netherbrickfence2);//Newel Caped Wood Fence
            ItemStack plank6roofedforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.plank6roofedforest));
            list.add(plank6roofedforest);//Carved Dark Oak Wood Block
            ItemStack cobblewall9roofedforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.cobblewall9roofedforest));
            list.add(cobblewall9roofedforest);//Carved Dark Oak Wood Pillar
            ItemStack darkoakbeam = new ItemStack(Item.getItemFromBlock(BlockRegistry.darkoakbeam));
            list.add(darkoakbeam);//Dark Oak Beam
            ItemStack trapdoorroofedforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.trapdoorroofedforest));
            list.add(trapdoorroofedforest);//Dark Shutters
            //End Dark Oak
            ItemStack junglewoodocean = new ItemStack(Item.getItemFromBlock(BlockRegistry.junglewoodocean));
            list.add(junglewoodocean);//Driftwood
            ItemStack junglestairsocean = new ItemStack(Item.getItemFromBlock(BlockRegistry.junglestairsocean));
            list.add(junglestairsocean);//Driftwood Stairs
            ItemStack woodslab4ocean = new ItemStack(Item.getItemFromBlock(BlockRegistry.woodslab4ocean));
            list.add(woodslab4ocean);//Driftwood Slab
            ItemStack junglewoodjungle = new ItemStack(Item.getItemFromBlock(BlockRegistry.junglewoodjungle));
            list.add(junglewoodjungle);//Mossy Wood
            ItemStack junglestairsjungle = new ItemStack(Item.getItemFromBlock(BlockRegistry.junglestairsjungle));
            list.add(junglestairsjungle);//Mossy Wood Stairs
            ItemStack woodslab4roofedforest = new ItemStack(Item.getItemFromBlock(BlockRegistry.woodslab4roofedforest));
            list.add(woodslab4roofedforest);//Mossy Wood Slab
            //End Weathered Wood
            ItemStack oldwhiteplanks = new ItemStack(Item.getItemFromBlock(BlockRegistry.oldwhiteplanks));
            list.add(oldwhiteplanks);//White Painted Planks
            ItemStack plank15 = new ItemStack(Item.getItemFromBlock(BlockRegistry.plank15));
            list.add(plank15);//White Painted Wood
            ItemStack planksred = new ItemStack(Item.getItemFromBlock(BlockRegistry.planksred));
            list.add(planksred);//Red Painted Planks
            ItemStack plank15hell = new ItemStack(Item.getItemFromBlock(BlockRegistry.plank15hell));
            list.add(plank15hell);//Red Painted Wood
            ItemStack plankslightred = new ItemStack(Item.getItemFromBlock(BlockRegistry.plankslightred));
            list.add(plankslightred);//Light Red Painted Planks
            ItemStack oldlightredplanks = new ItemStack(Item.getItemFromBlock(BlockRegistry.oldlightredplanks));
            list.add(oldlightredplanks);//Weathered Light Red Planks
            ItemStack planksorange = new ItemStack(Item.getItemFromBlock(BlockRegistry.planksorange));
            list.add(planksorange);//Orange Painted Planks
            ItemStack planksyellow = new ItemStack(Item.getItemFromBlock(BlockRegistry.planksyellow));
            list.add(planksyellow);//Yellow Painted Planks
            ItemStack plank15desert = new ItemStack(Item.getItemFromBlock(BlockRegistry.plank15desert));
            list.add(plank15desert);//Yellow Painted Wood
            ItemStack planksgreen = new ItemStack(Item.getItemFromBlock(BlockRegistry.planksgreen));
            list.add(planksgreen);//Green Painted Planks
            ItemStack plank15forest = new ItemStack(Item.getItemFromBlock(BlockRegistry.plank15forest));
            list.add(plank15forest);//Green Painted Wood
            ItemStack plankslightgreen = new ItemStack(Item.getItemFromBlock(BlockRegistry.plankslightgreen));
            list.add(plankslightgreen);//Lime Painted Planks
            ItemStack oldlimeplanks = new ItemStack(Item.getItemFromBlock(BlockRegistry.oldlimeplanks));
            list.add(oldlimeplanks);//Weathered Lime Planks
            ItemStack plankscyan = new ItemStack(Item.getItemFromBlock(BlockRegistry.plankscyan));
            list.add(plankscyan);//Cyan Painted Planks
            ItemStack oldcyanplanks = new ItemStack(Item.getItemFromBlock(BlockRegistry.oldcyanplanks));
            list.add(oldcyanplanks);//Weathered Cyan Planks
            ItemStack planksblue = new ItemStack(Item.getItemFromBlock(BlockRegistry.planksblue));
            list.add(planksblue);//Blue Painted Planks
            ItemStack olddarkblueplanks = new ItemStack(Item.getItemFromBlock(BlockRegistry.olddarkblueplanks));
            list.add(olddarkblueplanks);//Weathered Dark Blue Planks
            ItemStack plank15ocean = new ItemStack(Item.getItemFromBlock(BlockRegistry.plank15ocean));
            list.add(plank15ocean);//Blue Painted Wood
            ItemStack plankslightblue = new ItemStack(Item.getItemFromBlock(BlockRegistry.plankslightblue));
            list.add(plankslightblue);//Light Blue Painted Planks
            ItemStack oldlightblueplanks = new ItemStack(Item.getItemFromBlock(BlockRegistry.oldlightblueplanks));
            list.add(oldlightblueplanks);//Weathered Light Blue Planks
            ItemStack plankspurple = new ItemStack(Item.getItemFromBlock(BlockRegistry.plankspurple));
            list.add(plankspurple);//Purple Painted Planks
            ItemStack planksbrown = new ItemStack(Item.getItemFromBlock(BlockRegistry.planksbrown));
            list.add(planksbrown);//Brown Painted Planks
            ItemStack oldbrownplanks = new ItemStack(Item.getItemFromBlock(BlockRegistry.oldbrownplanks));
            list.add(oldbrownplanks);//Weathered Brown Planks
            //End Painted Wood
            */

			super.displayAllRelevantItems(list);
		}

	};

	public static CreativeTabs dev = new DevTab(CreativeTabs.getNextID(), "dev")
	{

		@Override
		@SideOnly(Side.CLIENT)
		public void displayAllRelevantItems(List<ItemStack> list)
		{
        /*  ItemStack roman_brick_vertical = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_brick_vertical));
            list.add(roman_brick_vertical);//Roman Vertical Brick
            ItemStack roman_tile1 = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_tile1));
            list.add(roman_tile1);//Floor Tile
            ItemStack roman_architrave_polychrome = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_architrave_polychrome));
            list.add(roman_architrave_polychrome);//Polychrome Architrave
            ItemStack roman_architrave_polychrome2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_architrave_polychrome2));
            list.add(roman_architrave_polychrome2);//Polychrome Architrave with Legionaries
            ItemStack roman_architrave_polychrome3 = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_architrave_polychrome3));
            list.add(roman_architrave_polychrome3);//Polychrome Architrave 2
            ItemStack roman_corinthian_capital_polychrome = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_corinthian_capital_polychrome));
            list.add(roman_corinthian_capital_polychrome);//Polychrome Corinthian Capital
            ItemStack roman_marble_design1 = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_design1));
            list.add(roman_marble_design1);//Black and White Marble Pattern
            ItemStack roman_marble_design2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_design2));
            list.add(roman_marble_design2);//Marble Small Diagonal Checker Pattern
            ItemStack roman_marble_design3 = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_design3));
            list.add(roman_marble_design3);//Marble Small Checker Pattern
            ItemStack roman_marble_design4 = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_design4));
            list.add(roman_marble_design4);//Red and White Marble Pattern 2
            ItemStack roman_marble_design5 = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_design5));
            list.add(roman_marble_design5);//Red and White Marble Pattern
            ItemStack roman_marble_design6 = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_design6));
            list.add(roman_marble_design6);//Blue and Yellow Marble Pattern
            ItemStack roman_marble_design7 = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_design7));
            list.add(roman_marble_design7);//Marble Diagonal Checker Pattern
            ItemStack roman_marble_design8 = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_design8));
            list.add(roman_marble_design8);//Marble Checker Pattern
            ItemStack roman_marble_black = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_black));
            list.add(roman_marble_black);//Black Marble
            ItemStack roman_marble_blue = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_blue));
            list.add(roman_marble_blue);//Blue Marble
            ItemStack roman_marble_gray = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_gray));
            list.add(roman_marble_gray);//Gray Marble
            ItemStack roman_marble_green = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_green));
            list.add(roman_marble_green);//Green Marble
            ItemStack roman_marble_pink = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_pink));
            list.add(roman_marble_pink);//Pink Marble
            ItemStack roman_marble_red = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_red));
            list.add(roman_marble_red);//Red Marble
            ItemStack amphora = new ItemStack(Item.getItemFromBlock(BlockRegistry.amphora));
            list.add(amphora);//Amphora
            ItemStack cypress = new ItemStack(Item.getItemFromBlock(BlockRegistry.cypress));
            list.add(cypress);//Cypress Tree
            ItemStack garnet = new ItemStack(Item.getItemFromBlock(BlockRegistry.garnet));
            list.add(garnet);//Red Jewels
            ItemStack garnet2 = new ItemStack(Item.getItemFromBlock(BlockRegistry.garnet2));
            list.add(garnet2);//Green Jewels
            ItemStack garnet3 = new ItemStack(Item.getItemFromBlock(BlockRegistry.garnet3));
            list.add(garnet3);//Purple Jewels
            ItemStack garnet4 = new ItemStack(Item.getItemFromBlock(BlockRegistry.garnet4));
            list.add(garnet4);//Jewels and Gold
            ItemStack panel1 = new ItemStack(Item.getItemFromBlock(BlockRegistry.panel1));
            list.add(panel1);//Wooden Panel
            ItemStack roman_brick_dark = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_brick_dark));
            list.add(roman_brick_dark);//Dark Roman Brick
            ItemStack roman_brick_dark_stacked = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_brick_dark_stacked));
            list.add(roman_brick_dark_stacked);//Dark Roman Brick and Sandstone
            ItemStack roman_brick_stacked = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_brick_stacked));
            list.add(roman_brick_stacked);//Roman Brick and Sandstone
            ItemStack roman_column_gray = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_column_gray));
            list.add(roman_column_gray);//Black Column
            ItemStack roman_marble_darkgreen = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_darkgreen));
            list.add(roman_marble_darkgreen);//Dark Green Marble
            ItemStack roman_marble_indigo = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_indigo));
            list.add(roman_marble_indigo);//Indigo Marble
            ItemStack roman_marble_pedestal = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_pedestal));
            list.add(roman_marble_pedestal);//Marble Pedestal
            ItemStack roman_marble_redwhite = new ItemStack(Item.getItemFromBlock(BlockRegistry.roman_marble_redwhite));
            list.add(roman_marble_redwhite);//Red and White Marble
            ItemStack stonebrick_inscription = new ItemStack(Item.getItemFromBlock(BlockRegistry.stonebrick_inscription));
            list.add(stonebrick_inscription);//Stonebrick Inscription
            ItemStack wood_carved_gilded = new ItemStack(Item.getItemFromBlock(BlockRegistry.wood_carved_gilded));
            list.add(wood_carved_gilded);//Gilded Carved Wood
            */

			super.displayAllRelevantItems(list);
		}

	};

	@EventHandler
	public void preInit(FMLPreInitializationEvent e) {
		proxy.preInit(e);
	}

	@EventHandler
	public void init(FMLInitializationEvent e) {
		proxy.init(e);
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent e) {
		proxy.postInit(e);
	}

}